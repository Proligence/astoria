﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingsSection.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration
{
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the Assemblies configuration section.
    /// </summary>
    [CollectionDataContract(Name = "Settings", ItemName = "Setting")]
    public class SettingsSection : Collection<SettingSection>
    {
        /// <summary>
        /// Gets the value of the setting with the specified name.
        /// </summary>
        /// <param name="name">The name of the setting to get.</param>
        /// <returns>The value of the setting or <c>null</c> if there is no setting with the specified name.</returns>
        public string this[string name]
        {
            get
            {
                foreach (SettingSection setting in this)
                {
                    if (setting.Name == name)
                    {
                        return setting.Value;
                    }
                }

                return null;
            }
        }
    }
}
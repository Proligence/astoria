﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceConfigurationException.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// The exception that is thrown when an invalid service configuration is detected.
    /// </summary>
    [Serializable]
    [SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors")]
    public class ServiceConfigurationException : Exception
    {
        public ServiceConfigurationException(ValidationException validationException, string sectionName)
            : base(BuildMessage(validationException), validationException)
        {
            this.SectionName = sectionName;

            if (validationException != null)
            {
                if (validationException.ValidationResult.MemberNames != null)
                {
                    this.SettingName = validationException.ValidationResult.MemberNames.FirstOrDefault();
                }

                this.SettingValue = validationException.Value;
            }
        }

        public ServiceConfigurationException(string message)
            : base(message)
        {
        }

        public ServiceConfigurationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public ServiceConfigurationException(
            string message,
            Exception innerException,
            string sectionName,
            string settingName,
            object settingValue)
            : base(message, innerException)
        {
            this.SectionName = sectionName;
            this.SettingName = settingName;
            this.SettingValue = settingValue;
        }

        protected ServiceConfigurationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.SectionName = info.GetString("SectionName");
            this.SettingName = info.GetString("SettingName");
            this.SettingValue = info.GetValue("SettingValue", typeof(object));
        }

        /// <summary>
        /// Gets the name of the configuration section which failed to validate.
        /// </summary>
        public string SectionName { get; private set; }

        /// <summary>
        /// Gets the name of the setting which failed to validate.
        /// </summary>
        public string SettingName { get; private set; }

        /// <summary>
        /// Gets the value of the invalid setting.
        /// </summary>
        public object SettingValue { get; private set; }

        /// <summary>
        /// Gets the underlying <see cref="ValidationException"/>.
        /// </summary>
        public ValidationException ValidationException
        {
            get { return this.InnerException as ValidationException; }
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("SectionName", this.SectionName, typeof(string));
            info.AddValue("SettingName", this.SettingName, typeof(string));
            info.AddValue("SettingValue", this.SettingValue, typeof(object));
        }

        private static string BuildMessage(ValidationException validationException)
        {
            string message = "The service's configuration is invalid. ";

            if (validationException != null)
            {
                return message + validationException.Message;
            }

            return message.Trim();
        }
    }
}
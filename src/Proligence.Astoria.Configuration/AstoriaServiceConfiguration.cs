﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceConfiguration.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration
{
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;
    using System.Xml;
    using System.Xml.XPath;

    /// <summary>
    /// Represents the configuration of an Astoria service.
    /// </summary>
    [DataContract]
    public class AstoriaServiceConfiguration
    {
        [DataMember(Name = "Service")]
        private ServiceSection serviceSection;

        [Required]
        public ServiceSection Service
        {
            get { return this.serviceSection; }
        }

        /// <summary>
        /// Gets the XML document which represents the service's configuration.
        /// </summary>
        public IXPathNavigable Xml
        {
            get
            {
                if (this.XmlDocument != null)
                {
                    return this.XmlDocument;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the XML document which represents the service's configuration.
        /// </summary>
        internal XmlDocument XmlDocument { get; set; }
    }
}
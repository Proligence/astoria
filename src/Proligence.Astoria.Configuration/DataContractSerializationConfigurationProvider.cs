﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataContractSerializationConfigurationProvider.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Xml;
    using System.Xml.XPath;

    /// <summary>
    /// Reads Astoria service configuration files using .NET Data Contract serialization.
    /// </summary>
    /// <typeparam name="TConfig">The type that represents the service configuration.</typeparam>
    public class DataContractSerializationConfigurationProvider<TConfig> : IConfigurationProvider<TConfig>
        where TConfig : AstoriaServiceConfiguration
    {
        public TConfig GetConfiguration(string fileName)
        {
            string configFilePath = fileName;

            Assembly entryAssembly = Assembly.GetEntryAssembly();
            if (entryAssembly != null) 
            {
                string serviceDirectory = Path.GetDirectoryName(entryAssembly.Location);
                if (serviceDirectory != null)
                {
                    configFilePath = Path.Combine(serviceDirectory, fileName);   
                }
            }

            var serializer = new DataContractSerializer(typeof(TConfig));
            using (Stream stream = this.GetFileStream(configFilePath))
            {
                var document = new XmlDocument();
                document.Load(stream);
                
                IXPathNavigable processedDocument = this.PrepareXmlForDeserialization(document);
                XPathNavigator navigator = processedDocument.CreateNavigator();

                using (Stream processedStream = new MemoryStream())
                {
                    XmlWriter writer = XmlWriter.Create(processedStream);
                    // ReSharper disable once PossibleNullReferenceException
                    navigator.WriteSubtree(writer);
                    writer.Flush();
                    processedStream.Flush();
                    processedStream.Seek(0, SeekOrigin.Begin);

                    var configuration = (TConfig)serializer.ReadObject(processedStream);
                    configuration.XmlDocument = document;

                    string rootNodeName = document.ChildNodes[0].Name;
                    this.ValidateDataAnnotations(configuration, rootNodeName);

                    return configuration;
                }
            }
        }

        [ExcludeFromCodeCoverage]
        protected virtual Stream GetFileStream(string fileName)
        {
            return File.OpenRead(fileName);
        }

        /// <summary>
        /// Processes the XML document with the configuration before deserialization.
        /// </summary>
        /// <param name="document">The XML document which contains the configuration.</param>
        /// <returns>A new <see cref="XmlDocument"/> which contains the configuration in parsable format.</returns>
        protected virtual IXPathNavigable PrepareXmlForDeserialization(IXPathNavigable document)
        {
            if (document == null)
            {
                throw new ArgumentNullException("document");
            }

            XPathNavigator navigator = document.CreateNavigator();
            XmlDocument doc = new XmlDocument();
            // ReSharper disable once PossibleNullReferenceException
            doc.LoadXml(navigator.OuterXml);

            XmlDocument result = new XmlDocument();
            foreach (XmlNode childNode in doc.ChildNodes)
            {
                XmlNode sortedChild = FixNodeOrder(childNode);
                result.AppendChild(result.ImportNode(sortedChild, true));
            }

            return result.CreateNavigator();
        }

        /// <summary>
        /// Validates data annotation attributes for the specified object.
        /// </summary>
        /// <param name="obj">The object to validate.</param>
        /// <param name="sectionName">
        /// The name of the configuration section which contains the validated setting.
        /// </param>
        protected virtual void ValidateDataAnnotations(object obj, string sectionName)
        {
            if (obj != null)
            {
                if (Attribute.IsDefined(obj.GetType(), typeof(DataContractAttribute)))
                {
                    var validationContext = new ValidationContext(obj, null, null);

                    try
                    {
                        Validator.ValidateObject(obj, validationContext, true);
                    }
                    catch (ValidationException ex)
                    {
                        throw new ServiceConfigurationException(ex, sectionName);
                    }

                    BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
                    foreach (PropertyInfo propertyInfo in obj.GetType().GetProperties(bindingFlags))
                    {
                        if (propertyInfo.GetIndexParameters().Length == 0)
                        {
                            this.ValidateDataAnnotations(
                                propertyInfo.GetValue(obj, new object[0]),
                                propertyInfo.Name);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Recursively sorts the child nodes from the specified <see cref="XPathNavigator"/>.
        /// </summary>
        /// <param name="node">The XML node which child nodes will be sorted.</param>
        /// <returns>The new XML node which contains the sorted nodes.</returns>
        private static XmlNode FixNodeOrder(XmlNode node)
        {
            var nodes = new List<XmlNode>(node.ChildNodes.Cast<XmlNode>());
            var orderedNodes = new List<XmlNode>();

            // All nodes from external namespaces must be appended first.
            string baseNamespace = node.NamespaceURI;
            IEnumerable<XmlNode> externalNodes = nodes.Where(n => n.NamespaceURI != baseNamespace).ToArray();
            orderedNodes.AddRange(externalNodes);
            nodes.RemoveAll(externalNodes.Contains);
            
            // All other nodes must be sorted alphabetically.
            foreach (XmlNode childNode in nodes.OrderBy(n => n.Name))
            {
                orderedNodes.Add(FixNodeOrder(childNode));
            }

            XmlNode newNode = node.CloneNode(false);
            foreach (XmlNode orderedNode in orderedNodes)
            {
                newNode.AppendChild(orderedNode);
            }

            return newNode;
        }
    }
}
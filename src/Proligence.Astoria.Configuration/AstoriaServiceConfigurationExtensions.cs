﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceConfigurationExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration
{
    /// <summary>
    /// Implements extension methods for the <see cref="AstoriaServiceConfiguration"/> class.
    /// </summary>
    public static class AstoriaServiceConfigurationExtensions
    {
        /// <summary>
        /// Gets the value of the setting with the specified name.
        /// </summary>
        /// <param name="configuration">The service configuration.</param>
        /// <param name="name">The name of the setting to get.</param>
        /// <returns>The value of the setting or <c>null</c> if the setting was not found.</returns>
        public static string GetSetting(this AstoriaServiceConfiguration configuration, string name)
        {
            if (configuration != null)
            {
                if (configuration.Service != null)
                {
                    if (configuration.Service.Settings != null)
                    {
                        return configuration.Service.Settings[name];
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the value of the setting with the specified name or throws a
        /// <see cref="ServiceConfigurationException"/> if there is no setting with the specified name.
        /// </summary>
        /// <param name="configuration">The service configuration.</param>
        /// <param name="name">The name of the setting to get.</param>
        /// <returns>The value of the setting.</returns>
        public static string GetRequiredSetting(this AstoriaServiceConfiguration configuration, string name)
        {
            string value = configuration.GetSetting(name);
            if (string.IsNullOrEmpty(value))
            {
                string message = "Failed to find setting '" + name + "' in the service's configuration.";
                throw new ServiceConfigurationException(message, null, "Settings", name, null);
            }

            return value;
        }
    }
}
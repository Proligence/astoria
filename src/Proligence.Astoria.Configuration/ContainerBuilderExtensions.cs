﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContainerBuilderExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Reflection;
    using Autofac;

    public static class ContainerBuilderExtensions
    {
        /// <summary>
        /// Registers the specified Astoria service configuration in the container.
        /// </summary>
        /// <typeparam name="TConfig">The type which represents the service configuration.</typeparam>
        /// <param name="builder">The dependency injection container builder.</param>
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")]
        public static void RegisterConfiguration<TConfig>(this ContainerBuilder builder) 
            where TConfig : AstoriaServiceConfiguration
        {
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }

            RegisterConfigurationFileLoader<TConfig>(builder);
        }

        /// <summary>
        /// Expands the specified relative path using the service's location as the base directory.
        /// </summary>
        /// <param name="fileName">Relative path.</param>
        /// <returns>Full path.</returns>
        [ExcludeFromCodeCoverage]
        private static string ExpandPath(string fileName)
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();
            if (entryAssembly != null)
            {
                string serviceDirectory = Path.GetDirectoryName(entryAssembly.Location);
                if (serviceDirectory != null)
                {
                    return Path.Combine(serviceDirectory, fileName);
                }
            }

            return fileName;
        }

        /// <summary>
        /// Registers a handler which loads the service's configuration file and saves the parsed configuration object
        /// in the dependency injection container.
        /// </summary>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <param name="builder">The dependency injection container builder.</param>
        [ExcludeFromCodeCoverage]
        private static void RegisterConfigurationFileLoader<TConfig>(ContainerBuilder builder)
            where TConfig : AstoriaServiceConfiguration
        {
            builder.Register(
                x =>
                {
                    string configFilePath = Environment.MachineName + ".config";
                    if (!File.Exists(ExpandPath(configFilePath)))
                    {
                        configFilePath = "default.config";
                    }

                    var configProvider = new DataContractSerializationConfigurationProvider<TConfig>();
                    return configProvider.GetConfiguration(configFilePath);
                })
                .SingleInstance()
                .As(typeof(AstoriaServiceConfiguration), typeof(TConfig));
        }
    }
}
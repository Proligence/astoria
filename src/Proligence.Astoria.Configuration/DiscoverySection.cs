﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DiscoverySection.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration
{
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the configuration section which contains settings for service discovery.
    /// </summary>
    [DataContract]
    public class DiscoverySection
    {
        [DataMember(Name = "DiscoveryServerDisabled")]
        private bool discoveryServerDisabled;

        [DataMember(Name = "LoggingEnabled")]
        private bool loggingEnabled;

        /// <summary>
        /// Gets a value indicating whether the service's discovery server is disabled.
        /// </summary>
        public bool DiscoveryServerDisabled
        {
            get { return this.discoveryServerDisabled; }
        }

        /// <summary>
        /// Gets a value indicating whether the service should log discovery events.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public bool LoggingEnabled
        {
            get { return this.loggingEnabled; }
        }
    }
}
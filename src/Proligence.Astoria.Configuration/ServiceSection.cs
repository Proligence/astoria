﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceSection.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration
{
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the 'Service' configuration section.
    /// </summary>
    [DataContract]
    public class ServiceSection
    {
        [DataMember(Name = "Address"), Required]
        private string address;

        [DataMember(Name = "Logging")]
        private LoggingSection logging;

        [DataMember(Name = "Assemblies")]
        private AssembliesSection assemblies;

        [DataMember(Name = "Discovery")]
        private DiscoverySection discovery;

        [DataMember(Name = "Monitoring")]
        private MonitoringSection monitoring;

        [DataMember(Name = "Settings")]
        private SettingsSection settings;

        [Required]
        public string Address
        {
            get { return this.address; }
        }

        public AssembliesSection Assemblies
        {
            get { return this.assemblies; }
        }

        public LoggingSection Logging
        {
            get { return this.logging; }
        }

        public DiscoverySection Discovery
        {
            get { return this.discovery; }
        }

        public MonitoringSection Monitoring
        {
            get { return this.monitoring; }
        }

        public SettingsSection Settings
        {
            get { return this.settings; }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IConfigurationProvider.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration
{
    /// <summary>
    /// Provides configuration for Astoria services.
    /// </summary>
    /// <typeparam name="TConfig">The type that represents the service configuration.</typeparam>
    public interface IConfigurationProvider<out TConfig>
        where TConfig : AstoriaServiceConfiguration
    {
        /// <summary>
        /// Gets the service configuration.
        /// </summary>
        /// <param name="fileName">The name of the configuration file to read.</param>
        /// <returns>The service configuration.</returns>
        TConfig GetConfiguration(string fileName);
    }
}
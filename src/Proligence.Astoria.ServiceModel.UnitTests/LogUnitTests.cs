﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Logging;
    using Environment = System.Environment;

    public abstract class LogUnitTests
    {
        [Test]
        public void WriteWhenMessageNull()
        {
            ILog log = this.CreateLog();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => log.Write(null));

            Assert.That(exception.ParamName, Is.EqualTo("message"));
        }

        [Test]
        public void WriteWhenMessageEmpty()
        {
            ILog log = this.CreateLog();

            log.Write(string.Empty);

            Assert.That(log.ToString(), Is.EqualTo(string.Empty));
        }

        [Test]
        public void WriteWhenMessageNotEmpty()
        {
            ILog log = this.CreateLog();

            log.Write("Message 1");

            Assert.That(log.ToString(), Is.EqualTo("Message 1"));
        }

        [Test]
        public void WriteWhenFormatSpecified()
        {
            ILog log = this.CreateLog();

            log.Write("{0} - {1}", "one", 2);

            Assert.That(log.ToString(), Is.EqualTo("one - 2"));
        }

        [Test]
        public void WriteWhenCalledMultipleTimes()
        {
            ILog log = this.CreateLog();

            log.Write("Message 1");
            log.Write("Message 2");

            Assert.That(log.ToString(), Is.EqualTo("Message 1Message 2"));
        }

        [Test]
        public void WriteLineWhenNoArgs()
        {
            ILog log = this.CreateLog();

            log.WriteLine();

            Assert.That(log.ToString(), Is.EqualTo(Environment.NewLine));
        }

        [Test]
        public void WriteLineWhenNoArgsAndCalledMultipleTimes()
        {
            ILog log = this.CreateLog();

            log.WriteLine();
            log.WriteLine();

            Assert.That(log.ToString(), Is.EqualTo(Environment.NewLine + Environment.NewLine));
        }

        [Test]
        public void WriteLineWhenMessageNull()
        {
            ILog log = this.CreateLog();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => log.WriteLine(null));

            Assert.That(exception.ParamName, Is.EqualTo("message"));
        }

        [Test]
        public void WriteWhenLineMessageEmpty()
        {
            ILog log = this.CreateLog();

            log.WriteLine(string.Empty);

            Assert.That(log.ToString(), Is.EqualTo(Environment.NewLine));
        }

        [Test]
        public void WriteLineWhenMessageNotEmpty()
        {
            ILog log = this.CreateLog();

            log.WriteLine("Message 1");

            Assert.That(log.ToString(), Is.EqualTo("Message 1" + Environment.NewLine));
        }

        [Test]
        public void WriteLineWhenFormatSpecified()
        {
            ILog log = this.CreateLog();

            log.WriteLine("{0} - {1}", "one", 2);

            Assert.That(log.ToString(), Is.EqualTo("one - 2" + Environment.NewLine));
        }

        [Test]
        public void WriteLineWhenCalledMultipleTimes()
        {
            ILog log = this.CreateLog();

            log.WriteLine("Message 1");
            log.WriteLine("Message 2");

            Assert.That(
                log.ToString(),
                Is.EqualTo("Message 1" + Environment.NewLine + "Message 2" + Environment.NewLine));
        }

        [Test]
        public void ToStringWhenLogEmpty()
        {
            ILog log = this.CreateLog();

            Assert.That(log.ToString(), Is.EqualTo(string.Empty));
        }

        [Test]
        public void GetHashCodeWhenSameContent()
        {
            ILog log1 = this.CreateLog();
            log1.WriteLine("Test");

            ILog log2 = this.CreateLog();
            log2.WriteLine("Test");

            Assert.That(log1.GetHashCode(), Is.EqualTo(log2.GetHashCode()));
        }

        [Test]
        public void EqualsWhenSameContent()
        {
            ILog log1 = this.CreateLog();
            log1.WriteLine("Test");

            ILog log2 = this.CreateLog();
            log2.WriteLine("Test");

            Assert.That(log1.Equals(log2), Is.True);
        }

        [Test]
        public void EqualsWhenDifferentContent()
        {
            ILog log1 = this.CreateLog();
            log1.WriteLine("Test1");

            ILog log2 = this.CreateLog();
            log2.WriteLine("Test2");

            Assert.That(log1.Equals(log2), Is.False);
        }

        [Test]
        public void EqualsWhenOtherTypeSpecified()
        {
            ILog log = this.CreateLog();
            log.WriteLine("Test");

            // ReSharper disable SuspiciousTypeConversion.Global
            Assert.That(log.Equals("Test"), Is.False);
            // ReSharper restore SuspiciousTypeConversion.Global
        }

        protected abstract ILog CreateLog();
    }
}
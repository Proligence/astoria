﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceExtensionManagerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Extensions;

    [TestFixture]
    public class ServiceExtensionManagerUnitTests
    {
        [Test]
        public void InitializeServiceExtensionManager()
        {
            var ext1 = new ServiceExtensionMock { Name = "ext1" };
            var ext2 = new ServiceExtensionMock { Name = "ext2" };
            var ext3 = new ServiceExtensionMock { Name = "ext3" };
            var manager = new ServiceExtensionManager(new[] { ext1, ext2, ext3 });
            
            manager.Initialize();

            Assert.That(manager.IsInitialized, Is.True);
        }

        [Test]
        public void InitializeServiceExtensionManagerWithUnnamedExtension()
        {
            var ext1 = new ServiceExtensionMock { Name = "ext" };
            var ext2 = new ServiceExtensionMock { Name = null };
            var manager = new ServiceExtensionManager(new[] { ext1, ext2 });

            var exception = Assert.Throws<InvalidOperationException>(manager.Initialize);

            var expected = "The service extension '" + typeof(ServiceExtensionMock).FullName 
                + "' must specify a name.";
            Assert.That(exception.Message, Is.EqualTo(expected));
        }

        [Test]
        public void InitializeServiceExtensionManagerWithDuplicateExtensionName()
        {
            var ext1 = new ServiceExtensionMock { Name = "ext1" };
            var ext2 = new ServiceExtensionMock { Name = "ext" };
            var ext3 = new ServiceExtensionMock { Name = "ext" };
            var manager = new ServiceExtensionManager(new[] { ext1, ext2, ext3 });

            var exception = Assert.Throws<InvalidOperationException>(manager.Initialize);

            Assert.That(exception.Message, Is.EqualTo("Duplicate service extension name: ext"));
        }

        [Test]
        public void GetExtensionWhenNotInitialized()
        {
            var ext = new ServiceExtensionMock { Name = "ext" };
            var manager = new ServiceExtensionManager(new[] { ext });

            var exception = Assert.Throws<InvalidOperationException>(() => manager.GetExtension("ext"));

            Assert.That(exception.Message, Is.EqualTo("The service extension manager is not initialized."));
        }

        [Test]
        public void GetExtensionWhenNotRegistered()
        {
            var ext = new ServiceExtensionMock { Name = "ext" };
            var manager = new ServiceExtensionManager(new[] { ext });
            manager.Initialize();

            IServiceExtension result = manager.GetExtension("ext2");

            Assert.That(result, Is.Null);
        }

        [Test]
        public void GetExtensionWhenRegistered()
        {
            var ext = new ServiceExtensionMock { Name = "ext" };
            var manager = new ServiceExtensionManager(new[] { ext });
            manager.Initialize();

            IServiceExtension result = manager.GetExtension("ext");

            Assert.That(result, Is.SameAs(ext));
        }

        [Test]
        public void RegisterExtensionWhenNotInitialized()
        {
            var ext = new ServiceExtensionMock { Name = "ext" };
            var manager = new ServiceExtensionManager(new[] { ext });

            var exception = Assert.Throws<InvalidOperationException>(
                () => manager.RegisterExtension(new ServiceExtensionMock { Name = "ext2" }));

            Assert.That(exception.Message, Is.EqualTo("The service extension manager is not initialized."));
        }

        [Test]
        public void RegisterExtensionWithDuplicateName()
        {
            var ext = new ServiceExtensionMock { Name = "ext" };
            var ext2 = new ServiceExtensionMock { Name = "ext" };
            var manager = new ServiceExtensionManager(new[] { ext });
            manager.Initialize();

            var exception = Assert.Throws<InvalidOperationException>(() => manager.RegisterExtension(ext2));

            Assert.That(exception.Message, Is.EqualTo("Duplicate service extension name: ext"));
        }

        [Test]
        public void RegisterExtensionWithUniqueName()
        {
            var ext = new ServiceExtensionMock { Name = "ext" };
            var ext2 = new ServiceExtensionMock { Name = "ext2" };
            var manager = new ServiceExtensionManager(new[] { ext });
            manager.Initialize();

            manager.RegisterExtension(ext2);

            Assert.That(manager.GetExtension("ext"), Is.SameAs(ext));
            Assert.That(manager.GetExtension("ext2"), Is.SameAs(ext2));
        }

        [Test]
        public void UnregisterExtensionWhenNotInitialized()
        {
            var ext = new ServiceExtensionMock { Name = "ext" };
            var manager = new ServiceExtensionManager(new[] { ext });

            var exception = Assert.Throws<InvalidOperationException>(() => manager.UnregisterExtension("ext"));

            Assert.That(exception.Message, Is.EqualTo("The service extension manager is not initialized."));
        }

        [Test]
        public void UnregisterExtensionWhenNotRegistered()
        {
            var ext = new ServiceExtensionMock { Name = "ext" };
            var manager = new ServiceExtensionManager(new[] { ext });
            manager.Initialize();

            var exception = Assert.Throws<ArgumentException>(() => manager.UnregisterExtension("ext2"));

            Assert.That(
                exception.Message,
                Is.StringStarting("Failed to find a service extension with the specified name."));
            
            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        [Test]
        public void UnregisterExtensionWhenRegistered()
        {
            var ext = new ServiceExtensionMock { Name = "ext" };
            var manager = new ServiceExtensionManager(new[] { ext });
            manager.Initialize();

            manager.UnregisterExtension("ext");

            Assert.That(manager.GetExtension("ext"), Is.Null);
        }

        private class ServiceExtensionMock : IServiceExtension
        {
            public string Name { get; set; }

            public object Invoke(object[] parameters)
            {
                return null;
            }
        }
    }
}
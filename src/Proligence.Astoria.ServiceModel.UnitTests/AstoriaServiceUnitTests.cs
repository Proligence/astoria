﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Autofac;
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Discovery;
    using Proligence.Astoria.ServiceModel.Extensions;
    using Proligence.Astoria.ServiceModel.Hosting;
    using Proligence.Astoria.ServiceModel.Logging;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    public class AstoriaServiceUnitTests
    {
        private const string ServiceName = "My Service";
        private AstoriaServiceMock service;
        private Mock<IPerformanceCounterDiscoverer> performanceCounterDiscoverer;
        private Mock<IPerformanceCounterInstaller> performanceCounterInstaller;
        private Mock<IServiceExtensionManager> extensionManager;

        [AstoriaServiceBehavior(AllowMultiple = false)]
        internal interface ITestAstoriaServiceBehavior : IAstoriaServiceBehavior
        {
        }

        [AstoriaServiceBehavior]
        internal interface ITestAstoriaServiceBehavior2 : IAstoriaServiceBehavior
        {
        }

        [AstoriaServiceBehavior]
        internal interface ITestAstoriaServiceBehavior3 : IAstoriaServiceBehavior
        {
        }

        [AstoriaServiceBehavior]
        internal interface ITestAstoriaServiceBehavior4 : IAstoriaServiceBehavior
        {
        }

        [SetUp]
        public void TestSetup()
        {
            this.service = new AstoriaServiceMock(ServiceName, null);
            this.performanceCounterDiscoverer = new Mock<IPerformanceCounterDiscoverer>();
            this.performanceCounterInstaller = new Mock<IPerformanceCounterInstaller>();
            this.extensionManager = new Mock<IServiceExtensionManager>();
        }

        [Test]
        public void InitializeServiceName()
        {
            Assert.That(this.service.ServiceName, Is.EqualTo(ServiceName));
        }

        [Test]
        public void InitializeWhenOptionsNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.service.Initialize(null));

            Assert.That(exception.ParamName, Is.EqualTo("options"));
        }

        [Test]
        public void InitializeLoggerWhenNotInitialized()
        {
            var loggingBehavior = new Mock<ILoggingBehavior>();
            var logger = new Mock<ServiceLogger> { CallBase = true };
            
            loggingBehavior.Setup(x => x.CreateLogger(
                It.IsAny<AstoriaService<IDummy, AstoriaServiceConfiguration>>(),
                It.IsAny<IComponentContext>()))
                .Returns(logger.Object);

            this.service.DoInitializeServiceBehaviors = true;
            this.service.ReflectionBehaviorsFunc = () => new IAstoriaServiceBehavior[] { loggingBehavior.Object };

            this.service.Initialize(new ServiceStartOptions());

            Assert.That(this.service.Logger.IsInitialized, Is.True);
            loggingBehavior.VerifyAll();
        }

        [Test]
        public void InitializeLoggerWhenInitialized()
        {
            this.service.Initialize(new ServiceStartOptions());
            ServiceLogger logger = this.service.Logger;

            this.service.Initialize(new ServiceStartOptions());
            ServiceLogger logger2 = this.service.Logger;

            Assert.That(logger, Is.SameAs(logger2));
        }

        [Test]
        public void InitializeShouldRegisterLoggerInContainer()
        {
            this.service.Initialize(new ServiceStartOptions());

            var logger = this.service.Container.Resolve<ServiceLogger>();
            
            Assert.That(logger, Is.SameAs(this.service.Logger));
        }

        [Test]
        public void InitializeBehaviorManagerWhenNotInitialized()
        {
            this.service.Initialize(new ServiceStartOptions());

            Assert.That(this.service.BehaviorManager, Is.Not.Null);
        }

        [Test]
        public void InitializeBehaviorManagerWhenInitialized()
        {
            this.service.Initialize(new ServiceStartOptions());
            IBehaviorManager behaviorManager1 = this.service.BehaviorManager;

            this.service.Initialize(new ServiceStartOptions());
            IBehaviorManager behaviorManager2 = this.service.BehaviorManager;

            Assert.That(behaviorManager1, Is.SameAs(behaviorManager2));
        }

        [Test]
        public void InitializeWatchDogManagerWhenNotInitialized()
        {
            this.service.Initialize(new ServiceStartOptions());

            Assert.That(this.service.WatchDogManager, Is.Not.Null);
        }

        [Test]
        public void InitializeWatchDogManagerWhenInitialized()
        {
            this.service.Initialize(new ServiceStartOptions());
            IWatchDogManager watchDogManager1 = this.service.WatchDogManager;

            this.service.Initialize(new ServiceStartOptions());
            IWatchDogManager watchDogManager2 = this.service.WatchDogManager;

            Assert.That(watchDogManager1, Is.SameAs(watchDogManager2));
        }

        [Test]
        public void InitializeEnsureServiceExtensionManagerNotInitializedWhenDisabledInBehavior()
        {
            var builder = new ContainerBuilder();

            var behaviorManager = new BehaviorManager();
            behaviorManager.RegisterServiceBehavior(new ExtensionsBehavior { ExtensionsEnabled = false });
            builder.RegisterInstance(behaviorManager).As<IBehaviorManager>();

            var extManager = new Mock<IServiceExtensionManager>();
            builder.RegisterInstance(extManager.Object).As<IServiceExtensionManager>();
            extManager.Setup(x => x.Initialize()).Verifiable();

            builder.Update(this.service.Container);

            this.service.Initialize(new ServiceStartOptions());

            extManager.Verify(x => x.Initialize(), Times.Never());
        }

        [Test]
        public void InitializeEnsureServiceExtensionManagerInitializedWhenEnabledInBehavior()
        {
            var builder = new ContainerBuilder();

            var behaviorManager = new BehaviorManager();
            behaviorManager.RegisterServiceBehavior(new ExtensionsBehavior { ExtensionsEnabled = true });
            builder.RegisterInstance(behaviorManager).As<IBehaviorManager>();

            var extManager = new Mock<IServiceExtensionManager>();
            builder.RegisterInstance(extManager.Object).As<IServiceExtensionManager>();
            extManager.Setup(x => x.Initialize()).Verifiable();

            builder.Update(this.service.Container);

            this.service.Initialize(new ServiceStartOptions());

            extManager.Verify(x => x.Initialize(), Times.Once());
        }

        [Test]
        public void InitializeEnsureExtensionsBehaviorConfigurationMethodCalled()
        {
            var builder = new ContainerBuilder();

            var extManager = new Mock<IServiceExtensionManager>();
            builder.RegisterInstance(extManager.Object).As<IServiceExtensionManager>();

            var behavior = new Mock<ExtensionsBehavior> { CallBase = true };
            behavior.Object.ExtensionsEnabled = true;
            behavior.Setup(x => x.ConfigureExtensions(extManager.Object)).Verifiable();

            var behaviorManager = new BehaviorManager();
            behaviorManager.RegisterServiceBehavior(behavior.Object);
            builder.RegisterInstance(behaviorManager).As<IBehaviorManager>();

            builder.Update(this.service.Container);

            this.service.Initialize(new ServiceStartOptions());

            behavior.Verify(x => x.ConfigureExtensions(extManager.Object), Times.Once());
        }

        [Test]
        public void InitializeAsyncEnsureWatchDogManagerInitialized()
        {
            var watchDogManager = new Mock<IWatchDogManager>();

            var builder = new ContainerBuilder();
            builder.RegisterInstance(watchDogManager.Object).As<IWatchDogManager>();
            builder.Update(this.service.Container);

            var startOptions = new ServiceStartOptions();
            this.service.Initialize(startOptions);
            
            watchDogManager.Setup(x => x.Initialize()).Verifiable();

            this.service.InvokeInitializeAsync(startOptions);

            watchDogManager.Verify(x => x.Initialize(), Times.Once());
        }

        [Test]
        public void RunInWindowsService()
        {
            this.service.DoInitializeServiceBehaviors = true;

            this.RunService(this.service, new ServiceStartOptions { RunInWindowsService = true });
            
            Assert.That(this.service.OriginalServiceHostBehavior, Is.InstanceOf<WindowsServiceHostBehavior>());
        }

        [Test]
        public void RunEnsurePerformanceCountersDisabledInWindowsServiceHost()
        {
            this.service.DoInitializeServiceBehaviors = true;

            this.RunService(this.service, new ServiceStartOptions { RunInWindowsService = true });

            var monitoringBehavior = this.service.BehaviorManager.GetRequiredServiceBehavior<IMonitoringBehavior>();
            Assert.That(monitoringBehavior.PerformanceCountersEnabled, Is.True);
        }

        [Test]
        public void RunInProcess()
        {
            this.service.DoInitializeServiceBehaviors = true;

            this.RunService(this.service, new ServiceStartOptions { RunInWindowsService = false });

            Assert.That(this.service.OriginalServiceHostBehavior, Is.InstanceOf<ProcessHostBehavior>());
        }

        [Test]
        public void RunEnsurePerformanceCountersDisabledInProcessHost()
        {
            this.service.DoInitializeServiceBehaviors = true;

            this.RunService(this.service, new ServiceStartOptions { RunInWindowsService = false });

            var monitoringBehavior = this.service.BehaviorManager.GetRequiredServiceBehavior<IMonitoringBehavior>();
            Assert.That(monitoringBehavior.PerformanceCountersEnabled, Is.False);
        }

        [Test]
        public void RunEnsureDebugLoggingEnabledWhenDebugOutputEnabledTrue()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Logging>
        <SaharaServiceAddress>http://dummy</SaharaServiceAddress>
        <DebugOutputEnabled>true</DebugOutputEnabled>
    </Logging>
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");
            this.RunService(this.service, new ServiceStartOptions());

            Assert.That(this.service.Logger.DebugOutputEnabled, Is.True);
        }

        [Test]
        public void RunEnsureDebugLoggingDisabledWhenDebugOutputEnabledFalse()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Logging>
        <SaharaServiceAddress>http://dummy</SaharaServiceAddress>
        <DebugOutputEnabled>false</DebugOutputEnabled>
    </Logging>
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");
            this.RunService(this.service, new ServiceStartOptions());

            Assert.That(this.service.Logger.DebugOutputEnabled, Is.False);
        }

        [Test]
        public void RunEnsureDebugLoggingDisabledWhenDebugOutputEnabledMissing()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Logging>
        <SaharaServiceAddress>http://dummy</SaharaServiceAddress>
    </Logging>
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");
            this.RunService(this.service, new ServiceStartOptions());

            Assert.That(this.service.Logger.DebugOutputEnabled, Is.False);
        }

        [Test]
        public void RunEnsureDebugLoggingDisabledWhenLoggingSectionMissing()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");
            this.RunService(this.service, new ServiceStartOptions());

            Assert.That(this.service.Logger.DebugOutputEnabled, Is.False);
        }

        [Test]
        public void RunWithDebugger()
        {
            this.RunService(this.service, new ServiceStartOptions { AttachDebugger = true });

            Assert.That(this.service.DebuggerAttached, Is.True);
        }

        [Test]
        public void RunWithoutDebugger()
        {
            this.RunService(this.service, new ServiceStartOptions { AttachDebugger = false });

            Assert.That(this.service.DebuggerAttached, Is.False);
        }

        [Test]
        public void RunInInstallModeEnsureDeploymentProviderInvoked()
        {
            var startOptions = new ServiceStartOptions
            {
                InstallMode = true
            };

            this.RunService(this.service, startOptions);

            Assert.That(this.service.DeploymentProvider.InstallPerformed, Is.True);
        }

        [Test]
        public void RunInstallModeEnsurePerformanceCountersInstalled()
        {
            var startOptions = new ServiceStartOptions
            {
                InstallMode = true
            };

            var performanceCounters = new[] { new Mock<IPerformanceCounter>().Object };
            this.performanceCounterDiscoverer.Setup(x => x.DiscoverPerformanceCounters(this.service))
                .Returns(performanceCounters);
            
            this.performanceCounterInstaller.Setup(
                x => x.InstallPerformanceCounters(this.service, performanceCounters));

            this.RunService(this.service, startOptions);

            this.performanceCounterDiscoverer.VerifyAll();
            this.performanceCounterInstaller.VerifyAll();
        }

        [Test]
        public void RunInNonInstallModeEnsurePerformanceCountersInitialized()
        {
            var performanceCounters = new[] { new Mock<IPerformanceCounter>().Object };
            this.performanceCounterDiscoverer.Setup(x => x.DiscoverPerformanceCounters(this.service))
                .Returns(performanceCounters);
            this.performanceCounterInstaller.Setup(
                x => x.InstallPerformanceCounters(this.service, performanceCounters));

            this.RunService(this.service, new ServiceStartOptions());
            
            Assert.That(this.service.PerformanceCounters, Is.Not.Null);

            this.performanceCounterDiscoverer.VerifyAll();
            this.performanceCounterInstaller.Verify(
                x => x.InstallPerformanceCounters(this.service, performanceCounters),
                Times.Never());
        }

        [Test]
        public void TryToRunWithNullOptions()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => this.service.Run(null));
            
            Assert.That(exception.ParamName, Is.EqualTo("options"));
        }

        [Test]
        public void TestIfAssembliesLoaded()
        {
var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Assemblies>
        <Assembly>assembly1.dll</Assembly>
        <Assembly>assembly2.dll</Assembly>
        <Assembly>assembly3.dll</Assembly>
    </Assemblies>
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");
            this.RunService(this.service, new ServiceStartOptions());

            Assert.That(this.service.LoadedAssemblies[0], Is.EqualTo("assembly1.dll"));
            Assert.That(this.service.LoadedAssemblies[1], Is.EqualTo("assembly2.dll"));
            Assert.That(this.service.LoadedAssemblies[2], Is.EqualTo("assembly3.dll"));
        }

        [Test]
        public void RunDiscoveryServerStartedWhenDiscoveryEnabled()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Discovery>
      <DiscoveryServerDisabled>false</DiscoveryServerDisabled>
    </Discovery>
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");

            var discoveryServerMock = new Mock<IServiceDiscoveryServer>();
            discoveryServerMock.Setup(x => x.StartFor(this.service));
            this.service.DiscoveryServer = discoveryServerMock.Object;

            var startOptions = new ServiceStartOptions { EnableAutoDiscovery = true };
            this.service.Initialize(startOptions);
            this.service.BehaviorManager.GetServiceBehavior<IDiscoveryBehavior>().DiscoveryServerEnabled = true;

            this.RunService(this.service, startOptions);

            discoveryServerMock.Verify(x => x.StartFor(this.service), Times.Once());
        }

        [Test]
        public void RunDiscoveryServerStartedWhenDiscoveryConfigurationSectionMissing()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");

            var discoveryServerMock = new Mock<IServiceDiscoveryServer>();
            discoveryServerMock.Setup(x => x.StartFor(this.service));
            this.service.DiscoveryServer = discoveryServerMock.Object;

            var startOptions = new ServiceStartOptions { EnableAutoDiscovery = true };
            this.service.Initialize(startOptions);
            this.service.BehaviorManager.GetServiceBehavior<IDiscoveryBehavior>().DiscoveryServerEnabled = true;

            this.RunService(this.service, startOptions);

            discoveryServerMock.Verify(x => x.StartFor(this.service), Times.Once());
        }

        [Test]
        public void RunDiscoveryServerNotStartedWhenDiscoveryDisabledInConfiguration()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Discovery>
      <DiscoveryServerDisabled>true</DiscoveryServerDisabled>
    </Discovery>
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");

            var discoveryServerMock = new Mock<IServiceDiscoveryServer>();
            discoveryServerMock.Setup(x => x.StartFor(this.service));
            this.service.DiscoveryServer = discoveryServerMock.Object;

            var startOptions = new ServiceStartOptions { EnableAutoDiscovery = true };
            this.service.Initialize(startOptions);
            this.service.BehaviorManager.GetServiceBehavior<IDiscoveryBehavior>().DiscoveryServerEnabled = true;

            this.RunService(this.service, startOptions);

            discoveryServerMock.Verify(x => x.StartFor(this.service), Times.Never());
        }

        [Test]
        public void RunDiscoveryServerNotStartedWhenDiscoveryDisabledInStartOptions()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Discovery>
      <DiscoveryServerDisabled>false</DiscoveryServerDisabled>
    </Discovery>
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");

            var discoveryServerMock = new Mock<IServiceDiscoveryServer>();
            discoveryServerMock.Setup(x => x.StartFor(this.service));
            this.service.DiscoveryServer = discoveryServerMock.Object;

            var startOptions = new ServiceStartOptions { EnableAutoDiscovery = false };
            this.service.Initialize(startOptions);
            this.service.BehaviorManager.GetServiceBehavior<IDiscoveryBehavior>().DiscoveryServerEnabled = true;

            this.RunService(this.service, startOptions);

            discoveryServerMock.Verify(x => x.StartFor(this.service), Times.Never());
        }

        [Test]
        public void RunDiscoverServerWhenDiscoveryServerDisabledInDiscoveryBehavior()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Discovery>
      <DiscoveryServerDisabled>false</DiscoveryServerDisabled>
    </Discovery>
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");

            var discoveryServerMock = new Mock<IServiceDiscoveryServer>();
            this.service.DiscoveryServer = discoveryServerMock.Object;

            var startOptions = new ServiceStartOptions { EnableAutoDiscovery = true };
            this.service.Initialize(startOptions);
            this.service.BehaviorManager.GetServiceBehavior<IDiscoveryBehavior>().DiscoveryServerEnabled = false;

            this.RunService(this.service, startOptions);

            discoveryServerMock.Verify(x => x.StartFor(this.service), Times.Never());
        }

        [Test]
        public void RunWhenMonitoringEnabledInConfiguration()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Monitoring>
      <MonitoringDisabled>false</MonitoringDisabled>
    </Monitoring>
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");

            this.RunService(this.service, new ServiceStartOptions());

            Assert.That(this.service.ServiceHost.MonitoringEnabled, Is.True);
        }
        
        [Test]
        public void RunWhenMonitoringDisabledInConfiguration()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Monitoring>
      <MonitoringDisabled>true</MonitoringDisabled>
    </Monitoring>
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");

            this.RunService(this.service, new ServiceStartOptions());

            Assert.That(this.service.ServiceHost.MonitoringEnabled, Is.False);
        }

        [Test]
        public void RunWhenMonitoringConfigurationSectionMissing()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");

            this.RunService(this.service, new ServiceStartOptions());

            Assert.That(this.service.ServiceHost.MonitoringEnabled, Is.True);
        }

        [Test]
        public void RunWhenMonitoringDisabledSettingMissingInConfiguration()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Monitoring />
  </Service>
</AstoriaServiceConfiguration>");

            this.service.Configuration = configurationProvider.GetConfiguration("dummy");

            this.RunService(this.service, new ServiceStartOptions());

            Assert.That(this.service.ServiceHost.MonitoringEnabled, Is.True);
        }

        [Test]
        public void RunEnsureServiceBehaviorsInitialized()
        {
            this.service.DoInitializeServiceBehaviors = true;

            var behavior1A = new TestAstoriaServiceBehavior();
            var behavior1B = new TestAstoriaServiceBehavior();
            var behavior2 = new TestAstoriaServiceBehavior2();
            var behavior3 = new TestAstoriaServiceBehavior3();
            var behavior4 = new TestAstoriaServiceBehavior4();

            var reflectionBehaviors = new IAstoriaServiceBehavior[] { behavior1A, behavior2, behavior4 };
            this.service.ReflectionBehaviorsFunc = () => reflectionBehaviors;

            var addedBehaviors = new IAstoriaServiceBehavior[] { behavior1B, behavior3 };
            var removedBehaviors = new[] { typeof(TestAstoriaServiceBehavior2) };
            this.service.ConfigBehaviorsFunc =
                () => new Tuple<IEnumerable<IAstoriaServiceBehavior>, IEnumerable<Type>>(
                    addedBehaviors, 
                    removedBehaviors);

            this.RunService(this.service, new ServiceStartOptions());

            IAstoriaServiceBehavior[] actualBehaviors = 
                this.service.BehaviorManager.GetServiceBehaviors<IAstoriaServiceBehavior>().ToArray();

            Assert.That(actualBehaviors.Single(b => b is TestAstoriaServiceBehavior), Is.SameAs(behavior1B));
            Assert.That(actualBehaviors.Single(b => b is TestAstoriaServiceBehavior3), Is.SameAs(behavior3));
            Assert.That(actualBehaviors.Single(b => b is TestAstoriaServiceBehavior4), Is.SameAs(behavior4));
        }

        [Test]
        public void OnStartupCalledWhenStartingUp()
        {
            int callCount = 0;
            this.service.StartupAction = () => callCount++;
            this.RunService(this.service, new ServiceStartOptions());

            Assert.That(callCount, Is.EqualTo(1));
        }

        [Test]
        public void OnShutdownCalledWhenShuttingDown()
        {
            this.RunService(this.service, new ServiceStartOptions());
            int callCount = 0;
            this.service.ShutdownAction = () => callCount++;
            this.service.Shutdown();

            Assert.That(callCount, Is.EqualTo(1));
        }

        [Test]
        public void GetWcfServiceHost()
        {
            using (var astoriaService = new AstoriaServiceMock())
            {
                astoriaService.Run();
                var serviceHost = astoriaService.ServiceHost;
                
                Assert.That(astoriaService.WcfServiceHost, Is.SameAs(serviceHost.WcfServiceHost));
            }
        }

        [Test]
        public void GetServiceInfo()
        {
            Mock<IEnvironment> environment = new Mock<IEnvironment>();
            environment.Setup(x => x.MachineName).Returns("Machine");
            environment.Setup(x => x.UserDomainName).Returns("Domain");
            environment.Setup(x => x.UserName).Returns("User");
            environment.Setup(x => x.WorkingSet).Returns(7);

            this.service = new AstoriaServiceMock(ServiceName, environment.Object);
            ServiceInfo serviceInfo = this.service.GetServiceInfo();

            Assert.That(serviceInfo.MachineName, Is.EqualTo("Machine"));
            Assert.That(serviceInfo.AccountName, Is.EqualTo("Domain\\User"));
            Assert.That(serviceInfo.UsedMemory, Is.EqualTo(7));

            environment.VerifyAll();
        }

        [Test]
        public void GetWatchDogs()
        {
            var watchDogManager = new Mock<IWatchDogManager>();
            var builder = new ContainerBuilder();
            builder.RegisterInstance(watchDogManager.Object).As<IWatchDogManager>();
            builder.Update(this.service.Container);

            this.service.Initialize(new ServiceStartOptions());
            this.service.BehaviorManager.GetRequiredServiceBehavior<IWatchDogsBehavior>().WatchDogsEnabled = true;

            var watchDogs = new[]
                {
                    new WatchDogInfo("Watch dog 1", "Watch dog 1 description."),
                    new WatchDogInfo("Watch dog 2", "Watch dog 2 description."),
                    new WatchDogInfo("Watch dog 3", "Watch dog 3 description.")
                };

            watchDogManager.Setup(x => x.GetWatchDogs()).Returns(watchDogs);

            IEnumerable<WatchDogInfo> result = this.service.GetWatchDogs();

            Assert.That(result, Is.SameAs(watchDogs));
            watchDogManager.VerifyAll();
        }

        [Test]
        public void InvokeWatchDog()
        {
            var watchDogManager = new Mock<IWatchDogManager>();
            var builder = new ContainerBuilder();
            builder.RegisterInstance(watchDogManager.Object).As<IWatchDogManager>();
            builder.Update(this.service.Container);

            var watchDogInfo = new WatchDogInfo("Watch dog 2", "Watch dog 2 description.");
            var watchDog = new Mock<IServiceWatchDog>();
            watchDogManager.Setup(x => x.CreateWatchDog(watchDogInfo)).Returns(watchDog.Object);

            var watchDogResult = new WatchDogResult();
            watchDog.Setup(x => x.Inspect()).Returns(watchDogResult);

            this.service.Initialize(new ServiceStartOptions());

            WatchDogResult result = this.service.InvokeWatchDog(watchDogInfo);

            Assert.That(result, Is.SameAs(watchDogResult));
            watchDogManager.VerifyAll();
            watchDog.VerifyAll();
        }

        [Test]
        public void InvokeInternalWatchDog()
        {
            var behaviorManager = new Mock<IBehaviorManager>();
            behaviorManager.Setup(x => x.GetRequiredServiceBehavior<IWatchDogsBehavior>())
                .Returns(new WatchDogsBehavior { WatchDogsEnabled = true });

            var watchDogManager = new WatchDogManagerUnitTests.TestWatchDogManager(
                this.service.Container,
                behaviorManager.Object);

            var watchDog = new TestInternalWatchDog();

            var builder = new ContainerBuilder();
            builder.RegisterInstance(watchDog).As<TestInternalWatchDog>();
            builder.RegisterInstance(watchDogManager).As<IWatchDogManager>();
            builder.Update(this.service.Container);

            this.service.Initialize(new ServiceStartOptions());
            watchDogManager.Initialize();

            WatchDogInfo watchDogInfo = watchDogManager.Descriptors
                .Single(d => d.Value.Name == typeof(TestInternalWatchDog).FullName)
                .Value;

            bool watchDogCalled = false;
            watchDog.Action = wd =>
                {
                    Assert.That(wd.Service, Is.SameAs(this.service));
                    watchDogCalled = true;
                };

            this.service.InvokeWatchDog(watchDogInfo);

            Assert.That(watchDogCalled, Is.True);
            behaviorManager.VerifyAll();
        }

        [Test]
        public void InvokeExternalWatchDogWhenMultithreadingEnabled()
        {
            var behaviorManager = new Mock<IBehaviorManager>();
            behaviorManager.Setup(x => x.GetRequiredServiceBehavior<IWatchDogsBehavior>())
                .Returns(new WatchDogsBehavior { WatchDogsEnabled = true });

            var watchDogManager = new WatchDogManagerUnitTests.TestWatchDogManager(
                this.service.Container,
                behaviorManager.Object);

            var watchDog = new TestExternalWatchDog();

            var channel = new Mock<IDummy>();
            var channelFactory = new Mock<IAstoriaChannelFactory>();
            channelFactory.Setup(x => x.CreateChannel<IDummy>(this.service.Configuration.Service.Address))
                .Returns(channel.Object);

            var builder = new ContainerBuilder();
            builder.RegisterInstance(watchDog).As<TestExternalWatchDog>();
            builder.RegisterInstance(watchDogManager).As<IWatchDogManager>();
            builder.RegisterInstance(channelFactory.Object).As<IAstoriaChannelFactory>();
            builder.Update(this.service.Container);

            this.service.Initialize(new ServiceStartOptions());
            this.service.BehaviorManager.GetRequiredServiceBehavior<IServiceHostBehavior>().UseMultithreading = true;
            watchDogManager.Initialize();

            WatchDogInfo watchDogInfo = watchDogManager.Descriptors
                .Single(d => d.Value.Name == typeof(TestExternalWatchDog).FullName)
                .Value;

            bool watchDogCalled = false;
            watchDog.Action = wd =>
            {
                Assert.That(wd.Service, Is.SameAs(channel.Object));
                watchDogCalled = true;
            };

            this.service.InvokeWatchDog(watchDogInfo);

            Assert.That(watchDogCalled, Is.True);
            channelFactory.VerifyAll();
            behaviorManager.VerifyAll();
        }

        [Test]
        public void InvokeExternalWatchDogWhenMultithreadingDisabled()
        {
            var behaviorManager = new Mock<IBehaviorManager>();
            behaviorManager.Setup(x => x.GetRequiredServiceBehavior<IWatchDogsBehavior>())
                .Returns(new WatchDogsBehavior { WatchDogsEnabled = true });

            var watchDogManager = new WatchDogManagerUnitTests.TestWatchDogManager(
                this.service.Container,
                behaviorManager.Object);

            var watchDog = new TestExternalWatchDog();

            var builder = new ContainerBuilder();
            builder.RegisterInstance(watchDog).As<TestExternalWatchDog>();
            builder.RegisterInstance(watchDogManager).As<IWatchDogManager>();
            builder.Update(this.service.Container);

            this.service.Initialize(new ServiceStartOptions());
            this.service.BehaviorManager.GetRequiredServiceBehavior<IServiceHostBehavior>().UseMultithreading = false;
            watchDogManager.Initialize();

            WatchDogInfo watchDogInfo = watchDogManager.Descriptors
                .Single(d => d.Value.Name == typeof(TestExternalWatchDog).FullName)
                .Value;

            bool watchDogCalled = false;
            watchDog.Action = wd =>
            {
                Assert.That(wd.Service, Is.SameAs(this.service));
                watchDogCalled = true;
            };

            this.service.InvokeWatchDog(watchDogInfo);

            Assert.That(watchDogCalled, Is.True);
            behaviorManager.VerifyAll();
        }

        [Test]
        public void InvokeGenericExternalWatchDogWhenMultithreadingEnabled()
        {
            var behaviorManager = new Mock<IBehaviorManager>();
            behaviorManager.Setup(x => x.GetRequiredServiceBehavior<IWatchDogsBehavior>())
                .Returns(new WatchDogsBehavior { WatchDogsEnabled = true });

            var watchDogManager = new WatchDogManagerUnitTests.TestWatchDogManager(
                this.service.Container,
                behaviorManager.Object);

            var watchDog = new TestGenericExternalWatchDog();

            var channel = new Mock<IDummy>();
            var channelFactory = new Mock<IAstoriaChannelFactory>();
            channelFactory.Setup(x => x.CreateChannel<IDummy>(this.service.Configuration.Service.Address))
                .Returns(channel.Object);

            var builder = new ContainerBuilder();
            builder.RegisterInstance(watchDog).As<TestGenericExternalWatchDog>();
            builder.RegisterInstance(watchDogManager).As<IWatchDogManager>();
            builder.RegisterInstance(channelFactory.Object).As<IAstoriaChannelFactory>();
            builder.Update(this.service.Container);

            this.service.Initialize(new ServiceStartOptions());
            this.service.BehaviorManager.GetRequiredServiceBehavior<IServiceHostBehavior>().UseMultithreading = true;
            watchDogManager.Initialize();

            WatchDogInfo watchDogInfo = watchDogManager.Descriptors
                .Single(d => d.Value.Name == typeof(TestGenericExternalWatchDog).FullName)
                .Value;

            bool watchDogCalled = false;
            watchDog.Action = wd =>
            {
                Assert.That(wd.Service, Is.SameAs(channel.Object));
                watchDogCalled = true;
            };

            this.service.InvokeWatchDog(watchDogInfo);

            Assert.That(watchDogCalled, Is.True);
            channelFactory.VerifyAll();
            behaviorManager.VerifyAll();
        }

        [Test]
        public void InvokeGenericExternalWatchDogWhenMultithreadingDisabled()
        {
            var behaviorManager = new Mock<IBehaviorManager>();
            behaviorManager.Setup(x => x.GetRequiredServiceBehavior<IWatchDogsBehavior>())
                .Returns(new WatchDogsBehavior { WatchDogsEnabled = true });

            var watchDogManager = new WatchDogManagerUnitTests.TestWatchDogManager(
                this.service.Container,
                behaviorManager.Object);

            var watchDog = new TestGenericExternalWatchDog();

            var builder = new ContainerBuilder();
            builder.RegisterInstance(watchDog).As<TestGenericExternalWatchDog>();
            builder.RegisterInstance(watchDogManager).As<IWatchDogManager>();
            builder.Update(this.service.Container);

            this.service.Initialize(new ServiceStartOptions());
            this.service.BehaviorManager.GetRequiredServiceBehavior<IServiceHostBehavior>().UseMultithreading = false;
            watchDogManager.Initialize();

            WatchDogInfo watchDogInfo = watchDogManager.Descriptors
                .Single(d => d.Value.Name == typeof(TestGenericExternalWatchDog).FullName)
                .Value;

            bool watchDogCalled = false;
            watchDog.Action = wd =>
            {
                Assert.That(wd.Service, Is.SameAs(this.service));
                watchDogCalled = true;
            };

            this.service.InvokeWatchDog(watchDogInfo);

            Assert.That(watchDogCalled, Is.True);
            behaviorManager.VerifyAll();
        }

        [Test]
        public void InvokeWatchDogWhenWatchDogFailsToInvoke()
        {
            var exception = new InvalidOperationException("Test exception");

            var behaviorManager = new Mock<IBehaviorManager>();
            behaviorManager.Setup(x => x.GetRequiredServiceBehavior<IWatchDogsBehavior>())
                .Returns(new WatchDogsBehavior { WatchDogsEnabled = true });

            var watchDogManager = new WatchDogManagerUnitTests.TestWatchDogManager(
                this.service.Container,
                behaviorManager.Object);
            
            var watchDog = new TestWatchDog { Exception = exception };

            var builder = new ContainerBuilder();
            builder.RegisterInstance(watchDog).As<TestWatchDog>();
            builder.RegisterInstance(watchDogManager).As<IWatchDogManager>();
            builder.Update(this.service.Container);

            this.service.Initialize(new ServiceStartOptions());
            watchDogManager.Initialize();

            WatchDogInfo watchDogInfo = watchDogManager.Descriptors
                .Single(d => d.Value.Name == typeof(TestWatchDog).FullName)
                .Value;

            WatchDogResult result = this.service.InvokeWatchDog(watchDogInfo);

            Assert.That(result.Status, Is.EqualTo(WatchDogStatus.Error));
            Assert.That(result.Errors.Any(e => e == exception.ToString()));
            behaviorManager.VerifyAll();
        }

        [Test]
        public void InvokeExtensionWhenExtensionNotRegistered()
        {
            this.service.Initialize(new ServiceStartOptions());
            var extManager = Mock.Get(this.service.ExtensionManager);
            extManager.Setup(x => x.GetExtension("ext")).Returns((IServiceExtension)null);

            var exception = Assert.Throws<ArgumentException>(
                () => this.service.InvokeExtension("ext", "foo", "bar"));

            Assert.That(exception.Message, Is.StringStarting("Extension with name 'ext' is not registered."));
            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        [Test]
        public void InvokeExtensionWhenExtensionRegistered()
        {
            this.service.Initialize(new ServiceStartOptions());
            var ext = new Mock<IServiceExtension>();
            ext.Setup(x => x.Invoke(It.Is<object[]>(p => p[0].Equals("foo") && p[1].Equals("bar")))).Returns("baz");
            var extManager = Mock.Get(this.service.ExtensionManager);
            extManager.Setup(x => x.GetExtension("ext")).Returns(ext.Object);

            object result = this.service.InvokeExtension("ext", "foo", "bar");

            Assert.That(result, Is.EqualTo("baz"));
        }

        [Test]
        public void InvokeExtensionShouldLogErrors()
        {
            var loggingBehavior = new Mock<ILoggingBehavior>();
            var logger = new Mock<ServiceLogger> { CallBase = true };

            loggingBehavior.Setup(x => x.CreateLogger(
                It.IsAny<AstoriaService<IDummy, AstoriaServiceConfiguration>>(),
                It.IsAny<IComponentContext>()))
                .Returns(logger.Object);

            this.service.DoInitializeServiceBehaviors = true;
            this.service.ReflectionBehaviorsFunc = () => new IAstoriaServiceBehavior[] { loggingBehavior.Object };

            this.service.Initialize(new ServiceStartOptions());
            var ext = new Mock<IServiceExtension>();
            var exception = new InvalidOperationException("Test exception message");
            ext.Setup(x => x.Invoke(It.Is<object[]>(p => p[0].Equals("foo") && p[1].Equals("bar")))).Throws(exception);
            var extManager = Mock.Get(this.service.ExtensionManager);
            extManager.Setup(x => x.GetExtension("ext")).Returns(ext.Object);
            logger.Setup(x => x.LogError(It.IsAny<string>(), exception));

            Assert.Throws<InvalidOperationException>(() => this.service.InvokeExtension("ext", "foo", "bar"));
            logger.VerifyAll();
        }

        [Test]
        public void SetDependencyContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();
            using (IContainer container = builder.Build())
            {
                this.service.SetDependencyContainer(container);

                Assert.That(this.service.Container, Is.SameAs(container));
            }
        }

        [Test]
        public void TryToSetDependencyContainerNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => this.service.SetDependencyContainer(null));
            
            Assert.That(exception.ParamName, Is.EqualTo("container"));
        }

        [Test]
        public void DisposeServiceHost()
        {
            AstoriaServiceHostMock serviceHost;
            using (var serviceMock = new AstoriaServiceMock())
            {
                serviceHost = new AstoriaServiceHostMock(serviceMock);
                serviceMock.ServiceHost = serviceHost;
            }

            Assert.That(serviceHost.IsDisposed, Is.True);
        }

        [Test]
        public void DisposeServiceLogger()
        {
            ServiceLogger logger;
            using (var serviceMock = new AstoriaServiceMock())
            {
                serviceMock.Run();
                logger = serviceMock.Logger;
            }

            Assert.That(logger.IsDisposed, Is.True);
        }

        private void RunService(AstoriaServiceMock svc, ServiceStartOptions options)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<AstoriaServiceModelTestModule>();
            builder.RegisterInstance(this.performanceCounterDiscoverer.Object).As<IPerformanceCounterDiscoverer>();
            builder.RegisterInstance(this.performanceCounterInstaller.Object).As<IPerformanceCounterInstaller>();
            builder.RegisterInstance(this.extensionManager.Object).As<IServiceExtensionManager>();
            
            using (IContainer container = builder.Build())
            {
                svc.SetDependencyContainer(container);
                svc.Run(options);
            }
        }

        [ServiceWatchDog]
        public class TestInternalWatchDog : InternalWatchDog<AstoriaServiceMock, IDummy, AstoriaServiceConfiguration>
        {
            public Action<TestInternalWatchDog> Action { get; set; }

            protected override void Inspect(WatchDogResult result)
            {
                this.Action(this);
            }
        }

        [ServiceWatchDog]
        public class TestExternalWatchDog : ExternalWatchDog<IDummy>
        {
            public Action<TestExternalWatchDog> Action { get; set; }

            protected override void Inspect(WatchDogResult result)
            {
                this.Action(this);
            }
        }

        [ServiceWatchDog]
        public class TestGenericExternalWatchDog : ExternalWatchDog<IAstoriaService>
        {
            public Action<TestGenericExternalWatchDog> Action { get; set; }

            protected override void Inspect(WatchDogResult result)
            {
                this.Action(this);
            }
        }

        [ServiceWatchDog]
        public class TestWatchDog : IServiceWatchDog 
        {
            public Exception Exception { get; set; }

            public void Dispose()
            {
            }

            public void Initialize()
            {
            }

            public WatchDogResult Inspect()
            {
                if (this.Exception != null)
                {
                    throw this.Exception;
                }

                return new WatchDogResult();
            }
        }

        private class TestAstoriaServiceBehavior : ITestAstoriaServiceBehavior
        {
        }

        private class TestAstoriaServiceBehavior2 : ITestAstoriaServiceBehavior2
        {
        }

        private class TestAstoriaServiceBehavior3 : ITestAstoriaServiceBehavior3
        {
        }

        private class TestAstoriaServiceBehavior4 : ITestAstoriaServiceBehavior4
        {
        }
    }
}
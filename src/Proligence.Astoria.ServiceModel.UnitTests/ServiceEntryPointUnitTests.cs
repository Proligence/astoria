﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceEntryPointUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using NUnit.Framework;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.Mocks;
    using Environment = Proligence.Astoria.ServiceModel.Environment;

    [TestFixture]
    public class ServiceEntryPointUnitTests
    {
        private AstoriaServiceMock service;
        private ServiceEntryPointMock<AstoriaServiceMock, IDummy, AstoriaServiceConfiguration> entryPoint;

        [SetUp]
        public void Setup()
        {
            this.service = new AstoriaServiceMock("My Service", new Environment());
            this.entryPoint = new ServiceEntryPointMock<AstoriaServiceMock, IDummy, AstoriaServiceConfiguration>
            { 
                Service = this.service 
            };
        }

        [Test]
        public void StartService()
        {
            this.entryPoint.Run(new string[0]);

            Assert.That(this.entryPoint.StartedService, Is.SameAs(this.service));
        }

        [Test]
        public void StartServiceWithParameters()
        {
            this.entryPoint.Run(new[] { "-console" });

            Assert.That(this.entryPoint.StartedService, Is.SameAs(this.service));
            Assert.That(this.entryPoint.StartOptions.RunInWindowsService, Is.False);
        }

        [Test]
        public void StartServiceWithCustomInitialization()
        {
            bool delegateCalled = false;
            ServiceEntryPoint<AstoriaServiceMock, IDummy, AstoriaServiceConfiguration> entry = null;
            
            this.entryPoint.Initialization = e =>
            {
                entry = e;
                delegateCalled = true;
            };

            this.entryPoint.Run(new string[0]);

            Assert.That(delegateCalled, Is.True);
            Assert.That(entry, Is.EqualTo(this.entryPoint));
        }

        [Test]
        public void StartServiceWithTypesToResolve()
        {
            var types = new[] { typeof(bool), typeof(string) };
            this.entryPoint.TypesToResolve = types;

            this.entryPoint.Run(new string[0]);

            CollectionAssert.AreEqual(types, this.entryPoint.ResolvedTypes);
        }

        [Test]
        public void StartServiceWithCustomStartOptionsConfiguration()
        {
            bool delegateCalled = false;
            ServiceEntryPoint<AstoriaServiceMock, IDummy, AstoriaServiceConfiguration> entry = null;
            ServiceStartOptions options = null;
            IEnumerable<string> args = null;

            this.entryPoint.StartOptionsConfiguration = (e, o) => 
            {
                entry = e;
                options = o;
                args = new List<string>(e.Arguments);
                delegateCalled = true;
            };

            string[] callArgs = { "-console" };
            this.entryPoint.Run(callArgs);

            Assert.That(delegateCalled, Is.True);
            Assert.That(entry, Is.EqualTo(this.entryPoint));
            Assert.That(options.RunInWindowsService, Is.False);
            CollectionAssert.AreEqual(callArgs, args);
        }

        [Test]
        public void StartServiceWithCustomServiceConfiguration()
        {
            bool delegateCalled = false;
            ServiceEntryPoint<AstoriaServiceMock, IDummy, AstoriaServiceConfiguration> entry = null;
            AstoriaService<IDummy, AstoriaServiceConfiguration> svc = null;
            IEnumerable<string> args = null;

            this.entryPoint.ServiceConfiguration = (e, s) => 
            {
                entry = e;
                svc = s;
                args = new List<string>(e.Arguments);
                delegateCalled = true;
            };

            string[] callArgs = { "-console" };
            this.entryPoint.Run(callArgs);

            Assert.That(delegateCalled, Is.True);
            Assert.That(entry, Is.EqualTo(this.entryPoint));
            Assert.That(svc, Is.EqualTo(this.service));
            CollectionAssert.AreEqual(callArgs, args);
        }

        [Test]
        public void StartServiceWithError()
        {
            Exception exception = new InvalidOperationException("Sample exception");
            this.entryPoint.Initialization = e =>
            {
                throw exception;
            };

            bool delegateCalled = false;
            ServiceEntryPoint<AstoriaServiceMock, IDummy, AstoriaServiceConfiguration> entry = null;
            IEnumerable<string> args = null;
            Exception exc = null;

            this.entryPoint.ErrorHandler = (e, ex) => 
            {
                entry = e;
                exc = ex;
                args = new List<string>(e.Arguments);
                delegateCalled = true;
            };

            string[] callArgs = { "-console" };
            this.entryPoint.Run(callArgs);

            Assert.That(delegateCalled, Is.True);
            Assert.That(entry, Is.EqualTo(this.entryPoint));
            Assert.That(exc, Is.EqualTo(exception));
            CollectionAssert.AreEqual(callArgs, args);
        }

        [Test]
        public void DebuggerNotLaunchedIfNoError()
        {
            this.entryPoint.Run(new string[0]);

            Assert.That(this.entryPoint.DebuggerLaunched, Is.False);
        }

        [Test]
        public void DebuggerNotLaunchedIfError()
        {
            this.entryPoint.Initialization = e => 
            {
                throw new InvalidOperationException("Sample exception");
            };

            this.entryPoint.Run(new string[0]);

            Assert.That(this.entryPoint.DebuggerLaunched, Is.True);
        }

        [Test]
        public void WriteExceptionDetailsWhenExceptionNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.entryPoint.InvokeWriteExceptionDetails(null, Console.Out));

            Assert.That(exception.ParamName, Is.EqualTo("exception"));
        }

        [Test]
        public void WriteExceptionDetailsWhenWriterNull()
        {
            var excetpion = new InvalidOperationException("Test exception message");

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.entryPoint.InvokeWriteExceptionDetails(excetpion, null));

            Assert.That(exception.ParamName, Is.EqualTo("writer"));
        }

        [Test]
        public void WriteExceptionDetailsWhenRegularExceptionNull()
        {
            using (var writer = new StringWriter(CultureInfo.InvariantCulture))
            {
                var exception = new InvalidOperationException("Test exception message");
                
                this.entryPoint.InvokeWriteExceptionDetails(exception, writer);

                Assert.That(writer.ToString(), Is.StringContaining(exception.ToString()));
            }
        }

        [Test]
        public void WriteExceptionDetailsReflectionTypeLoadExceptionNull()
        {
            using (var writer = new StringWriter(CultureInfo.InvariantCulture))
            {
                var innerException = new InvalidOperationException("Test exception message");
                var exception = new ReflectionTypeLoadException(
                    new[] { typeof(string), typeof(int) },
                    new Exception[] { innerException });

                this.entryPoint.InvokeWriteExceptionDetails(exception, writer);

                string result = writer.ToString();
                Assert.That(result, Is.StringContaining(exception.ToString()));
                Assert.That(result, Is.StringContaining(innerException.ToString()));
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaMonitoringServiceBehaviorUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using Autofac;
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Logging;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    public class AstoriaMonitoringServiceBehaviorUnitTests
    {
        [TestCase(null)]
        [TestCase("")]
        public void CreateAstoriaMonitoringServiceBehaviorWhenServiceNameNull(string serviceName)
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new AstoriaMonitoringServiceBehavior(
                    serviceName, 
                    new Mock<IContainer>().Object,
                    new NullLogger()));

            Assert.That(exception.ParamName, Is.EqualTo("serviceName"));
        }

        [Test]
        public void CreateAstoriaMonitoringServiceBehaviorWhenContainerNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new AstoriaMonitoringServiceBehavior("ServiceName", null, new NullLogger()));

            Assert.That(exception.ParamName, Is.EqualTo("container"));
        }

        [Test]
        public void CreateAstoriaMonitoringServiceBehaviorWhenLoggerNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new AstoriaMonitoringServiceBehavior("ServiceName", new Mock<IContainer>().Object, null));

            Assert.That(exception.ParamName, Is.EqualTo("logger"));
        }

        [Test]
        public void ApplyDispatchBehaviorWhenServiceDescriptionNull()
        {
            var behavior = new AstoriaMonitoringServiceBehavior(
                "MyService",
                new Mock<IContainer>().Object,
                new NullLogger());

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => behavior.ApplyDispatchBehavior(null, new ServiceHost(typeof(string))));

            Assert.That(exception.ParamName, Is.EqualTo("serviceDescription"));
        }

        [Test]
        public void ApplyDispatchBehaviorWhenCommonCase()
        {
            var contract = new ContractDescription("MyContract");
            
            var operation1 = new OperationDescription("Op1", contract);
            contract.Operations.Add(operation1);

            var operation2 = new OperationDescription("Op2", contract);
            contract.Operations.Add(operation2);

            var operation3 = new OperationDescription("Op3", contract);
            contract.Operations.Add(operation3);

            var service = new ServiceDescription(new[] { new ServiceEndpoint(contract) });

            var behavior = new AstoriaMonitoringServiceBehavior(
                "MyService",
                new Mock<IContainer>().Object,
                new NullLogger());

            behavior.ApplyDispatchBehavior(service, new ServiceHost(typeof(string)));

            var behavior1 = operation1.Behaviors.Find<AstoriaMonitoringOperationBehavior>();
            Assert.That(behavior1.ServiceName, Is.EqualTo("MyService"));

            var behavior2 = operation2.Behaviors.Find<AstoriaMonitoringOperationBehavior>();
            Assert.That(behavior2.ServiceName, Is.EqualTo("MyService"));

            var behavior3 = operation3.Behaviors.Find<AstoriaMonitoringOperationBehavior>();
            Assert.That(behavior3.ServiceName, Is.EqualTo("MyService"));
        }

        [Test]
        public void ApplyDispatchBehaviorWhenAstoriaMonitoringOperationBehaviorInstalled()
        {
            var contract = new ContractDescription("MyContract");

            var operation1 = new OperationDescription("Op1", contract);
            var operationBehavior = new AstoriaMonitoringOperationBehavior(
                "MyService", 
                new Mock<IContainer>().Object, 
                new NullLogger());
            
            operation1.Behaviors.Add(operationBehavior);
            contract.Operations.Add(operation1);
            
            var service = new ServiceDescription(new[] { new ServiceEndpoint(contract) });

            var serviceBehavior = new AstoriaMonitoringServiceBehavior(
                "MyService",
                new Mock<IContainer>().Object,
                new NullLogger());

            serviceBehavior.ApplyDispatchBehavior(service, new ServiceHost(typeof(string)));

            var behavior = operation1.Behaviors.Find<AstoriaMonitoringOperationBehavior>();
            Assert.That(behavior.ServiceName, Is.EqualTo("MyService"));
        }
    }
}
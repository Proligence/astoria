﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WinNtPerformanceCounterInstallerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Moq;
    using Moq.Protected;
    using NUnit.Framework;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    [SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly")]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    public class WinNtPerformanceCounterInstallerUnitTests
    {
        [Test]
        public void InstallPerformanceCountersWhenServiceNull()
        {
            var installer = new WinNtPerformanceCounterInstaller();
            IPerformanceCounter[] counters = { new Mock<IPerformanceCounter>().Object };

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => installer.InstallPerformanceCounters((AstoriaServiceMock)null, counters));

            Assert.That(exception.ParamName, Is.EqualTo("service"));
        }

        [Test]
        public void InstallPerformanceCountersWhenCountersNull()
        {
            var service = new AstoriaServiceMock();
            var installer = new WinNtPerformanceCounterInstaller();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => installer.InstallPerformanceCounters(service, null));

            Assert.That(exception.ParamName, Is.EqualTo("counters"));
        }

        [Test]
        public void InstallPerformanceCountersWhenCountersEmpty()
        {
            var installer = new Mock<WinNtPerformanceCounterInstaller> { CallBase = true };
            var service = new AstoriaServiceMock();

            installer.Protected().Setup(
                "InstallPerformanceCounters",
                ItExpr.Is<string>(category => category == service.ServiceName),
                ItExpr.Is<IEnumerable<IPerformanceCounter>>(counters => !counters.Any()));

            installer.Object.InstallPerformanceCounters(service, new IPerformanceCounter[0]);

            installer.VerifyAll();
        }

        [Test]
        public void InstallPerformanceCountersWhenCountersNotEmpty()
        {
            Mock<IPerformanceCounter>[] counters =
            {
                new Mock<IPerformanceCounter>(),
                new Mock<IPerformanceCounter>(),
                new Mock<IPerformanceCounter>()
            };

            counters[0].SetupGet(x => x.CounterName).Returns("Counter1");
            counters[1].SetupGet(x => x.CounterName).Returns("Counter2");
            counters[2].SetupGet(x => x.CounterName).Returns("Counter3");

            var manager = new Mock<WinNtPerformanceCounterInstaller> { CallBase = true };
            var service = new AstoriaServiceMock();

            IPerformanceCounter[] actualCounters = null;
            manager.Protected().Setup(
                "InstallPerformanceCounters",
                ItExpr.Is<string>(category => category == service.ServiceName),
                ItExpr.IsAny<IEnumerable<IPerformanceCounter>>())
                .Callback<string, IEnumerable<IPerformanceCounter>>((cat, cnts) => actualCounters = cnts.ToArray());

            manager.Object.InstallPerformanceCounters(service, counters.Select(x => x.Object));

            Assert.That(actualCounters.Length, Is.EqualTo(3));
            Assert.That(actualCounters[0], Is.SameAs(counters[0].Object));
            Assert.That(actualCounters[1], Is.SameAs(counters[1].Object));
            Assert.That(actualCounters[2], Is.SameAs(counters[2].Object));

            manager.VerifyAll();
        }

        [Test]
        public void InstallPerformanceCountersWhenDuplicateCounterNames()
        {
            Mock<IPerformanceCounter>[] counters =
            {
                new Mock<IPerformanceCounter>(),
                new Mock<IPerformanceCounter>(),
                new Mock<IPerformanceCounter>(),
                new Mock<IPerformanceCounter>(),
                new Mock<IPerformanceCounter>(),
                new Mock<IPerformanceCounter>(),
                new Mock<IPerformanceCounter>(),
                new Mock<IPerformanceCounter>(),
                new Mock<IPerformanceCounter>()
            };

            counters[0].SetupGet(x => x.CounterName).Returns("Counter1");
            counters[1].SetupGet(x => x.CounterName).Returns("Counter2");
            counters[2].SetupGet(x => x.CounterName).Returns("Counter1");
            counters[3].SetupGet(x => x.CounterName).Returns("Counter1");
            counters[4].SetupGet(x => x.CounterName).Returns("Counter3");
            counters[5].SetupGet(x => x.CounterName).Returns("Counter4");
            counters[6].SetupGet(x => x.CounterName).Returns("Counter3");
            counters[7].SetupGet(x => x.CounterName).Returns("Counter4");
            counters[8].SetupGet(x => x.CounterName).Returns("Counter5");

            var manager = new Mock<WinNtPerformanceCounterInstaller> { CallBase = true };
            var service = new AstoriaServiceMock();

            IPerformanceCounter[] actualCounters = null;
            manager.Protected().Setup(
                "InstallPerformanceCounters",
                ItExpr.Is<string>(category => category == service.ServiceName),
                ItExpr.IsAny<IEnumerable<IPerformanceCounter>>())
                .Callback<string, IEnumerable<IPerformanceCounter>>((cat, cnts) => actualCounters = cnts.ToArray());

            manager.Object.InstallPerformanceCounters(service, counters.Select(x => x.Object));

            actualCounters = actualCounters.OrderBy(x => x.CounterName).ToArray();
            Assert.That(actualCounters.Length, Is.EqualTo(5));
            Assert.That(actualCounters[0], Is.SameAs(counters[0].Object));
            Assert.That(actualCounters[1], Is.SameAs(counters[1].Object));
            Assert.That(actualCounters[2], Is.SameAs(counters[4].Object));
            Assert.That(actualCounters[3], Is.SameAs(counters[5].Object));
            Assert.That(actualCounters[4], Is.SameAs(counters[8].Object));

            manager.VerifyAll();
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionPerformanceCounterDiscovererUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    public class ReflectionPerformanceCounterDiscovererUnitTests
    {
        private Mock<IPerformanceCounterFactory> factoryMock;

        [SetUp]
        public void Setup()
        {
            this.factoryMock = new Mock<IPerformanceCounterFactory>();
            this.factoryMock.Setup(x => x.CreatePerformanceCounter(It.IsAny<IPerformanceCounterParameters>()))
                .Returns<IPerformanceCounterParameters>(p =>
                {
                    var counterMock = new Mock<IPerformanceCounter>();
                    counterMock.SetupGet(x => x.CounterName).Returns(p.CounterName);
                    counterMock.SetupGet(x => x.CategoryName).Returns(p.CategoryName);
                    counterMock.SetupGet(x => x.InstanceName).Returns(p.InstanceName);
                    counterMock.SetupProperty(x => x.RawValue);
                    return counterMock.Object;
                });
        }

        [Test]
        public void CreateReflectionPerformanceCounterDiscovererWhenPerformanceCounterFactoryNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new ReflectionPerformanceCounterDiscoverer(null));

            Assert.That(exception.ParamName, Is.EqualTo("factory"));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenServiceNull()
        {
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => discoverer.DiscoverPerformanceCounters<AstoriaServiceConfiguration, IDummy>(null));

            Assert.That(exception.ParamName, Is.EqualTo("service"));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenNoProperties()
        {
            var service = this.SetupServiceMock<TestCountersNoProperties>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);
            
            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);
            Assert.That(counters.Count(), Is.EqualTo(3));   // number of standard Astoria performance counters
        }

        [Test]
        public void DiscoverPerformanceCountersWhenValidCounterProperty()
        {
            var service = this.SetupServiceMock<ValidTestCounters>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);

            IPerformanceCounter counter = counters.Single(c => c.CounterName == "MyCounter");
            Assert.That(counter.CounterName, Is.EqualTo("MyCounter"));
            Assert.That(counter.InstanceName, Is.Null);

            ValidTestCounters countersObj = (ValidTestCounters)service.PerformanceCounters;
            Assert.That(countersObj.MyCounter, Is.SameAs(counter));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenValidInstanceCounterProperty()
        {
            var service = this.SetupServiceMock<ValidTestCounters>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);

            IPerformanceCounter counter = counters.Single(c => c.CounterName == "MyInstanceCounter");
            Assert.That(counter.CounterName, Is.EqualTo("MyInstanceCounter"));
            Assert.That(counter.InstanceName, Is.EqualTo("MyInstance"));

            ValidTestCounters countersObj = (ValidTestCounters)service.PerformanceCounters;
            Assert.That(countersObj.MyInstanceCounter, Is.SameAs(counter));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenCategoryNameNull()
        {
            var service = this.SetupServiceMock<ValidTestCounters>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);

            IPerformanceCounter counter = counters.Single(c => c.CounterName == "MyCounter");
            Assert.That(counter.CategoryName, Is.EqualTo(service.ServiceName));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenCategoryNameNotNull()
        {
            var service = this.SetupServiceMock<ValidTestCounters>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);

            IPerformanceCounter counter = counters.Single(c => c.CounterName == "MyCounterWithCategory");
            Assert.That(counter.CategoryName, Is.EqualTo("MyCategory"));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenCounterPropertyHasInvalidType()
        {
            var service = this.SetupServiceMock<InvalidTestCounters>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            InvalidOperationException exception = Assert.Throws<InvalidOperationException>(
                () => discoverer.DiscoverPerformanceCounters(service));

            string expected = 
                "The type of the InvalidTestCounters.MyCounter property must be " +
                "Proligence.Astoria.ServiceModel.Monitoring.IPerformanceCounter.";
            
            Assert.That(exception.Message, Is.EqualTo(expected));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenCounterPropertiesWithoutAttribute()
        {
            var service = this.SetupServiceMock<TestCountersWithNoAttributes>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service).ToArray();

            TestCountersWithNoAttributes countersObj = (TestCountersWithNoAttributes)service.PerformanceCounters;
            Assert.That(countersObj.MyCounter, Is.Null);
            Assert.That(counters.SingleOrDefault(x => x.CounterName == "MyCounter"), Is.Null);
        }

        [Test]
        public void DiscoverPerformanceCountersWhenNonCounterProperty()
        {
            var service = this.SetupServiceMock<TestCountersWithNoAttributes>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);

            TestCountersWithNoAttributes countersObj = (TestCountersWithNoAttributes)service.PerformanceCounters;
            Assert.That(countersObj.MyString, Is.Null);
            Assert.That(counters.SingleOrDefault(x => x.CounterName == "MyString"), Is.Null);
        }

        [Test]
        public void DiscoverPerformanceCountersWhenCounterPropertyInherited()
        {
            var service = this.SetupServiceMock<DerivedTestCounters>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service).ToArray();

            IPerformanceCounter counter = counters.Single(c => c.CounterName == "MyCounter");
            Assert.That(counter.CounterName, Is.EqualTo("MyCounter"));

            IPerformanceCounter counter2 = counters.Single(c => c.CounterName == "MyCounter2");
            Assert.That(counter2.CounterName, Is.EqualTo("MyCounter2"));

            DerivedTestCounters countersObj = (DerivedTestCounters)service.PerformanceCounters;
            Assert.That(countersObj.MyCounter, Is.SameAs(counter));
            Assert.That(countersObj.MyCounter2, Is.SameAs(counter2));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenCounterPropertyPrivate()
        {
            var service = this.SetupServiceMock<NonPublicTestCounters>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);

            IPerformanceCounter counter = counters.Single(c => c.CounterName == "PrivateCounter");
            Assert.That(counter.CounterName, Is.EqualTo("PrivateCounter"));

            NonPublicTestCounters countersObj = (NonPublicTestCounters)service.PerformanceCounters;
            Assert.That(countersObj.GetPrivateCounter(), Is.SameAs(counter));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenCounterPropertyProtected()
        {
            var service = this.SetupServiceMock<NonPublicTestCounters>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);

            IPerformanceCounter counter = counters.Single(c => c.CounterName == "ProtectedCounter");
            Assert.That(counter.CounterName, Is.EqualTo("ProtectedCounter"));

            NonPublicTestCounters countersObj = (NonPublicTestCounters)service.PerformanceCounters;
            Assert.That(countersObj.GetProtectedCounter(), Is.SameAs(counter));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenCounterPropertyInternal()
        {
            var service = this.SetupServiceMock<NonPublicTestCounters>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);

            IPerformanceCounter counter = counters.Single(c => c.CounterName == "InternalCounter");
            Assert.That(counter.CounterName, Is.EqualTo("InternalCounter"));

            NonPublicTestCounters countersObj = (NonPublicTestCounters)service.PerformanceCounters;
            Assert.That(countersObj.InternalCounter, Is.SameAs(counter));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenCounterPropertyWithInitialValue()
        {
            var service = this.SetupServiceMock<InitialValueTestCounters>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);

            Assert.That(counters.Single(c => c.CounterName == "CounterWithInitialValue").RawValue, Is.EqualTo(7));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenCounterPropertyWithoutInitialValue()
        {
            var service = this.SetupServiceMock<ValidTestCounters>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);

            Assert.That(counters.Single(c => c.CounterName == "MyCounter").RawValue, Is.EqualTo(0));
        }

        [Test]
        public void DiscoverPerformanceCountersWhenPerformanceCountersObjectNull()
        {
            var service = new AstoriaServiceMock();
            service.PerformanceCounters = null;
            service.Initialize(new ServiceStartOptions());

            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service);
            Assert.That(counters, Is.Empty);
        }

        [Test]
        public void DiscoverPerformanceCountersResultContainsStandardAstoriaCounters()
        {
            var service = this.SetupServiceMock<TestCountersNoProperties>();
            var discoverer = new ReflectionPerformanceCounterDiscoverer(this.factoryMock.Object);

            IEnumerable<IPerformanceCounter> counters = discoverer.DiscoverPerformanceCounters(service).ToArray();

            Assert.That(
                counters.SingleOrDefault(x => x.CounterName == ServicePerformanceCounters.TotalCallCounterName),
                Is.Not.Null);
            
            Assert.That(
                counters.SingleOrDefault(x => x.CounterName == ServicePerformanceCounters.FailedCallCounterName),
                Is.Not.Null);
            
            Assert.That(
                counters.SingleOrDefault(x => x.CounterName == ServicePerformanceCounters.CallDurationCounterName),
                Is.Not.Null);
        }

        private AstoriaServiceMock SetupServiceMock<TCounters>()
            where TCounters : ServicePerformanceCounters, new()
        {
            var service = new AstoriaServiceMock { PerformanceCounters = new TCounters() };
            service.Initialize(new ServiceStartOptions());

            service.BehaviorManager.RegisterServiceBehavior(
                new MonitoringBehavior { PerformanceCountersEnabled = true });

            return service;
        }

        internal class TestCountersNoProperties : ServicePerformanceCounters
        {
        }

        /* ReSharper disable UnusedAutoPropertyAccessor.Local */

        internal class ValidTestCounters : ServicePerformanceCounters
        {
            [PerformanceCounter("MyCounter")]
            public IPerformanceCounter MyCounter { get; private set; }

            [PerformanceCounter("MyCounterWithCategory", CategoryName = "MyCategory")]
            public IPerformanceCounter MyCounterWithCategory { get; private set; }

            [PerformanceCounter("MyInstanceCounter", InstanceName = "MyInstance")]
            public IPerformanceCounter MyInstanceCounter { get; private set; }
        }

        internal class InvalidTestCounters : ServicePerformanceCounters
        {
            [PerformanceCounter("MyInvalidCounter")]
            public string MyCounter { get; private set; }
        }

        internal class TestCountersWithNoAttributes : ServicePerformanceCounters
        {
            public IPerformanceCounter MyCounter { get; private set; }
            public string MyString { get; private set; }
        }

        internal class DerivedTestCounters : ValidTestCounters
        {
            [PerformanceCounter("MyCounter2")]
            public IPerformanceCounter MyCounter2 { get; private set; }
        }

        internal class NonPublicTestCounters : ServicePerformanceCounters
        {
            [PerformanceCounter("InternalCounter")]
            internal IPerformanceCounter InternalCounter { get; private set; }

            [PerformanceCounter("ProtectedCounter")]
            protected IPerformanceCounter ProtectedCounter { get; private set; }

            [PerformanceCounter("PrivateCounter")]
            private IPerformanceCounter PrivateCounter { get; set; }

            public IPerformanceCounter GetProtectedCounter()
            {
                return this.ProtectedCounter;
            }

            public IPerformanceCounter GetPrivateCounter()
            {
                return this.PrivateCounter;
            }
        }

        internal class InitialValueTestCounters : ServicePerformanceCounters
        {
            [PerformanceCounter("CounterWithInitialValue", InitialValue = 7)]
            internal IPerformanceCounter CounterWithInitialValue { get; private set; }
        }

        /* ReSharper restore UnusedAutoPropertyAccessor.Local */
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoggingBehaviorUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using Autofac;
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Logging;

    [TestFixture]
    public class LoggingBehaviorUnitTests
    {
        private AstoriaServiceMock service;
        private Mock<IComponentContext> container;
        private LoggingBehavior behavior;

        [SetUp]
        public void Setup()
        {
            this.service = new AstoriaServiceMock();
            this.container = new Mock<IComponentContext>();
            this.behavior = new LoggingBehavior();
        }
        
        [Test]
        public void CreateLoggerWhenServiceNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.behavior.CreateLogger<IDummy, AstoriaServiceConfiguration>(null, this.container.Object));

            Assert.That(exception.ParamName, Is.EqualTo("service"));
        }

        [Test]
        public void CreateLoggerWhenContainerNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.behavior.CreateLogger(this.service, null));

            Assert.That(exception.ParamName, Is.EqualTo("container"));
        }

        [Test]
        public void CreateLoggerWhenConsoleLogTarget()
        {
            this.behavior.LogTarget = ServiceLogTarget.Console;

            ServiceLogger logger = this.behavior.CreateLogger(this.service, this.container.Object);

            Assert.That(logger, Is.InstanceOf<ConsoleLogger>());
        }

        [Test]
        public void CreateLoggerWhenEventLogLogTarget()
        {
            this.behavior.LogTarget = ServiceLogTarget.EventLog;

            ServiceLogger logger = this.behavior.CreateLogger(this.service, this.container.Object);

            Assert.That(logger, Is.InstanceOf<EventLogLogger>());
        }

        [Test]
        public void CreateLoggerWhenEventLogLogTargetAndEventLogNameSpecified()
        {
            string xml =
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service>
    <Address>net.tcp://localhost/Test</Address>
    <Logging>
        <EventLogName>MyLogName</EventLogName>
    </Logging>
  </Service>
</AstoriaServiceConfiguration>";

            this.service.Configuration = CreateConfiguration(xml);
            this.behavior.LogTarget = ServiceLogTarget.EventLog;

            ServiceLogger logger = this.behavior.CreateLogger(this.service, this.container.Object);

            Assert.That(logger, Is.InstanceOf<EventLogLogger>());
            Assert.That(((EventLogLogger)logger).LogName, Is.EqualTo("MyLogName"));
        }

        [Test]
        public void CreateLoggerWhenEventLogLogTargetAndEventLogNameNull()
        {
            string xml =
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service>
    <Address>net.tcp://localhost/Test</Address>
    <Logging>
    </Logging>
  </Service>
</AstoriaServiceConfiguration>";

            this.service.Configuration = CreateConfiguration(xml);
            this.behavior.LogTarget = ServiceLogTarget.EventLog;

            ServiceLogger logger = this.behavior.CreateLogger(this.service, this.container.Object);

            Assert.That(logger, Is.InstanceOf<EventLogLogger>());
        }

        [TestCase("")]
        [TestCase("\t\r\n")]
        public void CreateLoggerWhenEventLogLogTargetAndEventLogNameEmptyOrWhiteSpace(string name)
        {
            string xml =
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service>
    <Address>net.tcp://localhost/Test</Address>
    <Logging>
        <EventLogName>" + name + @"</EventLogName>
    </Logging>
  </Service>
</AstoriaServiceConfiguration>";

            this.service.Configuration = CreateConfiguration(xml);
            this.behavior.LogTarget = ServiceLogTarget.EventLog;

            ServiceLogger logger = this.behavior.CreateLogger(this.service, this.container.Object);

            Assert.That(logger, Is.InstanceOf<EventLogLogger>());
        }

        [Test]
        public void CreateLoggerWhenNoLogTarget()
        {
            this.behavior.LogTarget = ServiceLogTarget.None;

            ServiceLogger logger = this.behavior.CreateLogger(this.service, this.container.Object);

            Assert.That(logger, Is.InstanceOf<NullLogger>());
        }

        [Test]
        public void CreateLoggerWhenUnknownLogTarget()
        {
            this.behavior.LogTarget = (ServiceLogTarget)int.MaxValue;

            ServiceLogger logger = this.behavior.CreateLogger(this.service, this.container.Object);

            Assert.That(logger, Is.InstanceOf<NullLogger>());
        }

        private static AstoriaServiceConfiguration CreateConfiguration(string xml)
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(xml);
            return configurationProvider.GetConfiguration("dummy");
        }
    }
}
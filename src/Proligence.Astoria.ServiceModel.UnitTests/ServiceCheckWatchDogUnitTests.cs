﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceCheckWatchDogUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    public class ServiceCheckWatchDogUnitTests
    {
        [Test]
        public void InspectShouldCallService()
        {
            var channelFactory = new Mock<IAstoriaChannelFactory>(MockBehavior.Strict);
            var channel = new Mock<IAstoriaService>(MockBehavior.Strict);
            var watchDog = new TestServiceCheckWatchDog(channelFactory.Object);
            channelFactory.Setup(x => x.CreateChannel<IAstoriaService>("net.tcp://dummy")).Returns(channel.Object);
            channel.Setup(x => x.GetServiceInfo()).Returns(new ServiceInfo());

            WatchDogResult result = watchDog.Inspect();

            Assert.That(result.Status, Is.EqualTo(WatchDogStatus.Ok));
            channelFactory.VerifyAll();
            channel.VerifyAll();
        }

        private class TestServiceCheckWatchDog 
            : ServiceCheckWatchDog<AstoriaServiceMock, IDummy, AstoriaServiceConfiguration>
        {
            public TestServiceCheckWatchDog(IAstoriaChannelFactory channelFactory)
                : base(channelFactory)
            {
                this.ServiceAddress = "net.tcp://dummy";
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowsServiceHostUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Linq;
    using System.Reflection;
    using NUnit.Framework;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Hosting;
    using Environment = Proligence.Astoria.ServiceModel.Environment;

    [TestFixture]
    public class WindowsServiceHostUnitTests
    {
        private WindowsServiceHostMock host;

        [SetUp]
        public void Setup()
        {
            this.host = new WindowsServiceHostMock();
            this.host.Service.Run();
        }

        [TearDown]
        public void Teardown()
        {
            if (this.host != null)
            {
                this.host.Dispose();
            }
        }

        [Test]
        public void StartWcfHostOnServiceStartup()
        {
            this.host.InvokeOnServiceStarted();

            Assert.That(this.host.WcfHostStarted, Is.True);
        }

        [Test]
        public void StopWcfHostOnServiceShutdown()
        {
            this.host.InvokeOnServiceStopped();

            Assert.That(this.host.WcfHostStopped, Is.True);
        }

        [Test]
        public void LogServiceStartup()
        {
            this.host.InvokeOnServiceStarted();

            Assert.That(this.host.Logger.Messages.Count, Is.EqualTo(1));
            Assert.That(this.host.Logger.Messages.ToArray()[0], Is.EqualTo("My Service: starting service."));
        }

        [Test]
        public void LogServiceStartupErrors()
        {
            this.host.Failure = new InvalidOperationException("My exception message.");
            var exception = Assert.Throws<TargetInvocationException>(() => this.host.InvokeOnServiceStarted());
            
            Assert.That(exception.InnerException, Is.EqualTo(this.host.Failure));
            Assert.That(this.host.Logger.Messages.Single(), Is.EqualTo("My Service: starting service."));
            Assert.That(this.host.Logger.Errors.Single().Key, Is.EqualTo("My Service: failed to start WCF service."));
            Assert.That(this.host.Logger.Errors.Single().Value, Is.EqualTo(exception.InnerException));
        }

        [Test]
        public void LogServiceShutdown()
        {
            this.host.InvokeOnServiceStopped();

            Assert.That(this.host.Logger.Messages.Count, Is.EqualTo(1));
            Assert.That(this.host.Logger.Messages.ToArray()[0], Is.EqualTo("My Service: stopping service."));
        }

        [Test]
        public void LogServiceShutdownErrors()
        {
            this.host.Failure = new InvalidOperationException("My exception message.");
            var exception = Assert.Throws<TargetInvocationException>(() => this.host.InvokeOnServiceStopped());

            Assert.That(exception.InnerException, Is.EqualTo(this.host.Failure));
            Assert.That(this.host.Logger.Messages.Single(), Is.EqualTo("My Service: stopping service."));
            Assert.That(this.host.Logger.Errors.Single().Key, Is.EqualTo("My Service: failed to close WCF service."));
            Assert.That(this.host.Logger.Errors.Single().Value, Is.EqualTo(exception.InnerException));
        }

        [Test]
        public void WindowsServiceDisposedWhenDisposingHost()
        {
            bool disposed = false;
            using (var windowsServiceHost = new WindowsServiceHostMock())
            {
                windowsServiceHost.WindowsService.Disposed += (sender, e) => disposed = true;
            }

            Assert.That(disposed, Is.True);
        }

        private class WindowsServiceHostMock : WindowsServiceHost<IDummy, AstoriaServiceConfiguration>
        {
            public WindowsServiceHostMock()
                : base(new AstoriaServiceMock("My Service", new Environment(), new ServiceLoggerMock()))
            {
            }

            public bool WcfHostStarted { get; private set; }
            public bool WcfHostStopped { get; private set; }
            public Exception Failure { get; set; }

            public new AstoriaServiceMock Service
            {
                get
                {
                    return (AstoriaServiceMock)base.Service;
                }
            }

            public ServiceLoggerMock Logger
            {
                get
                {
                    return (ServiceLoggerMock)this.Service.Logger;
                }
            }

            public AstoriaWindowsService WindowsService
            {
                get
                {
                    /* ReSharper disable PossibleNullReferenceException */
                    FieldInfo fieldInfo = this.GetType().BaseType.GetField(
                        "windowsService", BindingFlags.Instance | BindingFlags.NonPublic);
                    return (AstoriaWindowsService)fieldInfo.GetValue(this);
                    /* ReSharper restore PossibleNullReferenceException */
                }
            }

            public void InvokeOnServiceStarted()
            {
                /* ReSharper disable PossibleNullReferenceException */
                MethodInfo method = this.GetType().BaseType.GetMethod(
                    "OnServiceStarted", BindingFlags.Instance | BindingFlags.NonPublic);
                /* ReSharper restore PossibleNullReferenceException */

                method.Invoke(this, new object[] { this, EventArgs.Empty });
            }

            public void InvokeOnServiceStopped()
            {
                /* ReSharper disable PossibleNullReferenceException */
                MethodInfo method = this.GetType().BaseType.GetMethod(
                    "OnServiceStopped", BindingFlags.Instance | BindingFlags.NonPublic);
                /* ReSharper restore PossibleNullReferenceException */

                method.Invoke(this, new object[] { this, EventArgs.Empty });
            }

            public override void StartHost()
            {
            }

            public override void StartWcfHost()
            {
                if (this.Failure != null)
                {
                    throw this.Failure;
                }

                this.WcfHostStarted = true;
            }

            public override void StopWcfHost()
            {
                if (this.Failure != null)
                {
                    throw this.Failure;
                }

                this.WcfHostStopped = true;
            }
        }
    }
}
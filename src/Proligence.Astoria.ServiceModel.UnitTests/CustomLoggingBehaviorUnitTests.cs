﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomLoggingBehaviorUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System.Linq;
    using Autofac;
    using Moq;
    using NLog;
    using NLog.Targets;
    using NUnit.Framework;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Logging;

    [TestFixture]
    public class CustomLoggingBehaviorUnitTests
    {
        private AstoriaServiceMock service;
        private Mock<IComponentContext> container;
        private CustomLoggingBehavior behavior;

        [SetUp]
        public void Setup()
        {
            this.service = new AstoriaServiceMock();
            this.container = new Mock<IComponentContext>();
            this.behavior = new CustomLoggingBehavior();
        }

        [Test]
        public void CreateLoggerWhenConfigurationNullOrEmpty()
        {
            this.behavior.Configuration = null;

            ServiceConfigurationException exception = Assert.Throws<ServiceConfigurationException>(
                () => this.behavior.CreateLogger(this.service, this.container.Object));

            Assert.That(
                exception.Message,
                Is.StringContaining("No NLog configuration specified for CustomLoggingBehavior."));

            Assert.That(exception.SectionName, Is.EqualTo(typeof(CustomLoggingBehavior).FullName));
            Assert.That(exception.SettingName, Is.EqualTo("Configuration"));
            Assert.That(exception.SettingValue, Is.Null);
        }

        [Test]
        public void CreateLoggerWhenInvalidConfiguration()
        {
            this.behavior.Configuration = "Invalid configuration";

            ServiceConfigurationException exception = Assert.Throws<ServiceConfigurationException>(
                () => this.behavior.CreateLogger(this.service, this.container.Object));

            Assert.That(
                exception.Message,
                Is.StringContaining("The NLog configuration specified for CustomLoggingBehavior is invalid."));

            Assert.That(exception.SectionName, Is.EqualTo(typeof(CustomLoggingBehavior).FullName));
            Assert.That(exception.SettingName, Is.EqualTo("Configuration"));
            Assert.That(exception.SettingValue, Is.EqualTo("Invalid configuration"));
            Assert.That(exception.InnerException, Is.InstanceOf<NLogConfigurationException>());
        }

        [Test]
        public void CreateLoggerWhenValidConfiguration()
        {
            this.behavior.Configuration =
@"<nlog xmlns=""http://www.nlog-project.org/schemas/NLog.xsd"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
  <targets>
     <target name=""ds"" xsi:type=""OutputDebugString""/>
  </targets>
</nlog>";

            this.behavior.CreateLogger(this.service, this.container.Object);

            Target target = NLog.LogManager.Configuration.AllTargets.Single(x => x.Name == "ds");
            Assert.That(target, Is.InstanceOf<OutputDebugStringTarget>());
        }
    }
}
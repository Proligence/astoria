﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WinNtPerformanceCounterUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    [SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly")]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    public class WinNtPerformanceCounterUnitTests
    {
        [Test]
        public void CreatePerformanceCounterWhenUnderlyingCounterIsNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new WinNtPerformanceCounter(null));

            Assert.That(exception.ParamName, Is.EqualTo("counter"));
        }

        [Test]
        public void CreatePerformanceCounterWhenUnderlyingCounterIsNotNull()
        {
            using (var pc = new PerformanceCounter())
            {
                using (var counter = new WinNtPerformanceCounter(pc))
                {
                    Assert.That(counter.Counter, Is.SameAs(pc));
                }
            }
        }

        [Test]
        public void GetCounterName()
        {
            using (var pc = new PerformanceCounter())
            {
                pc.CounterName = "MyCounter";

                using (var counter = new WinNtPerformanceCounter(pc))
                {
                    Assert.That(counter.CounterName, Is.EqualTo("MyCounter"));
                }
            }
        }

        [Test]
        public void GetCategoryName()
        {
            using (var pc = new PerformanceCounter())
            {
                pc.CategoryName = "MyCategory";

                using (var counter = new WinNtPerformanceCounter(pc))
                {
                    Assert.That(counter.CategoryName, Is.EqualTo("MyCategory"));
                }
            }
        }

        [Test]
        public void GetInstanceName()
        {
            using (var pc = new PerformanceCounter())
            {
                pc.InstanceName = "MyInstance";

                using (var counter = new WinNtPerformanceCounter(pc))
                {
                    Assert.That(counter.InstanceName, Is.EqualTo("MyInstance"));
                }
            }
        }

        [Test]
        public void Dispose()
        {
            using (var pc = new PerformanceCounter())
            {
                bool disposed = false;
                pc.Disposed += (sender, args) => { disposed = true; };

                var counter = new WinNtPerformanceCounter(pc);
                counter.Dispose();

                Assert.That(disposed, Is.True);
            }
        }
    }
}
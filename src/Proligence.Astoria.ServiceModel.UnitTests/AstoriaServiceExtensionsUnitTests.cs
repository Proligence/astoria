﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Logging;
    using Environment = Proligence.Astoria.ServiceModel.Environment;

    /* ReSharper disable InvokeAsExtensionMethod */

    public class AstoriaServiceExtensionsUnitTests
    {
        private AstoriaServiceMock service;
        private ServiceLoggerMock logger;

        [SetUp]
        public void Setup()
        {
            this.logger = new ServiceLoggerMock();
            this.service = new AstoriaServiceMock("My service", new Environment(), this.logger);
            this.service.Run();
        }

        [TearDown]
        public void Teardown()
        {
            if (this.service != null)
            {
                this.service.Dispose();
            }
        }

        [Test]
        public void LogMessage()
        {
            AstoriaServiceExtensions.LogMessage(this.service, "My message");
            
            Assert.That(this.logger.Messages.Single(), Is.EqualTo("My message"));
        }

        [Test]
        public void TryLogMessageWhenServiceNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => AstoriaServiceExtensions.LogMessage((AstoriaServiceMock)null, "My message"));
            
            Assert.That(exception.ParamName, Is.EqualTo("service"));
        }

        [Test, Sequential]
        public void LogEmptyMessage(
            [Values(null, "", "\t\r\n")]string message)
        {
            AstoriaServiceExtensions.LogMessage(this.service, message);

            Assert.That(this.logger.Messages.Count(), Is.EqualTo(0));
        }

        [Test]
        public void LogWarning()
        {
            AstoriaServiceExtensions.LogWarning(this.service, "My warning");
            
            Assert.That(this.logger.Warnings.Single(), Is.EqualTo("My warning"));
        }

        [Test]
        public void TryLogWarningWhenServiceNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => AstoriaServiceExtensions.LogWarning((AstoriaServiceMock)null, "My warning"));

            Assert.That(exception.ParamName, Is.EqualTo("service"));
        }

        [Test, Sequential]
        public void LogEmptyWarning(
            [Values(null, "", "\t\r\n")]string warning)
        {
            AstoriaServiceExtensions.LogWarning(this.service, warning);

            Assert.That(this.logger.Warnings.Count(), Is.EqualTo(0));
        }

        [Test]
        public void LogError()
        {
            var exception = new InvalidOperationException("My exception message");
            AstoriaServiceExtensions.LogError(this.service, "My error", exception);
            
            Assert.That(this.logger.Errors.Single().Key, Is.EqualTo("My error"));
            Assert.That(this.logger.Errors.Single().Value, Is.EqualTo(exception));
        }

        [Test]
        public void TryLogErrorWhenServiceNull()
        {
            var ex = new InvalidOperationException("My exception message");
            var exception = Assert.Throws<ArgumentNullException>(
                () => AstoriaServiceExtensions.LogError((AstoriaServiceMock)null, "My error", ex));

            Assert.That(exception.ParamName, Is.EqualTo("service"));
        }

        [Test]
        public void InvokeActionWithLogWithoutResult()
        {
            bool actionInvoked = false;
            AstoriaServiceExtensions.WithLog<IDummy, AstoriaServiceConfiguration>(
                this.service, () => actionInvoked = true, logSuccess: true);

            Assert.That(actionInvoked, Is.True);
            Assert.That(this.logger.Messages.First(), Is.EqualTo("InvokeActionWithLogWithoutResult: call succeeded"));
        }

        [Test]
        public void InvokeActionWithLogWithoutResultWithoutLogSuccess()
        {
            bool actionInvoked = false;
            AstoriaServiceExtensions.WithLog<IDummy, AstoriaServiceConfiguration>(
                this.service, () => actionInvoked = true, logSuccess: false);

            Assert.That(actionInvoked, Is.True);
            Assert.That(this.logger.Messages.Count, Is.EqualTo(0));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void InvokeActionWithLogWithoutResultWithError(bool logSuccess)
        {
            var exception = new InvalidOperationException("Test");
            var thrownException = Assert.Throws<InvalidOperationException>(
                () => AstoriaServiceExtensions.WithLog(this.service, () => { throw exception; }, logSuccess));

            Assert.That(thrownException, Is.EqualTo(exception));
            Assert.That(this.logger.Messages.Count, Is.EqualTo(0));
            Assert.That(this.logger.Errors.First().Value, Is.EqualTo(exception));
            StringAssert.Contains("InvokeActionWithLogWithoutResultWithError", this.logger.Errors.First().Key);
            StringAssert.Contains("call failed", this.logger.Errors.First().Key);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void WithLogWithoutResultWhenServiceNull(bool logSuccess)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => AstoriaServiceExtensions.WithLog<IDummy, AstoriaServiceConfiguration>(
                    null, 
                    () =>
                    {    
                    }, 
                    logSuccess));

            Assert.That(exception.ParamName, Is.EqualTo("service"));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void WithLogWithoutResultWhenActionNull(bool logSuccess)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => AstoriaServiceExtensions.WithLog(this.service, (Action)null, logSuccess));

            Assert.That(exception.ParamName, Is.EqualTo("action"));
        }

        [Test]
        public void InvokeActionWithLogWithResult()
        {
            bool actionInvoked = false;
            int result = AstoriaServiceExtensions.WithLog(
                this.service,
                () =>
                {
                    actionInvoked = true;
                    return 7;
                },
                logSuccess: true);

            Assert.That(actionInvoked, Is.True);
            Assert.That(result, Is.EqualTo(7));

            string message = this.logger.Messages.First();
            StringAssert.Contains("InvokeActionWithLogWithResult", message);
            StringAssert.Contains("call succeeded", message);
        }

        [Test]
        public void InvokeActionWithLogWithResultWithoutLogSuccess()
        {
            bool actionInvoked = false;
            int result = AstoriaServiceExtensions.WithLog(
                this.service, 
                () =>
                { 
                    actionInvoked = true;
                    return 7;
                }, 
                logSuccess: false);

            Assert.That(actionInvoked, Is.True);
            Assert.That(result, Is.EqualTo(7));
            Assert.That(this.logger.Messages.Count, Is.EqualTo(0));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void InvokeActionWithLogWithResultWithError(bool logSuccess)
        {
            var exception = new InvalidOperationException("Test");
            var thrownException = Assert.Throws<InvalidOperationException>(
                () => AstoriaServiceExtensions.WithLog<IDummy, AstoriaServiceConfiguration, int>(
                    this.service, 
                    () =>
                    {
                        throw exception;
                    }, 
                    logSuccess));

            Assert.That(thrownException, Is.EqualTo(exception));
            Assert.That(this.logger.Messages.Count, Is.EqualTo(0));
            Assert.That(this.logger.Errors.First().Value, Is.EqualTo(exception));

            string message = this.logger.Errors.First().Key;
            StringAssert.Contains("InvokeActionWithLogWithResultWithError", message);
            StringAssert.Contains("call failed", message);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void WithLogWithResultWhenServiceNull(bool logSuccess)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => AstoriaServiceExtensions.WithLog<IDummy, AstoriaServiceConfiguration, int>(
                    null, () => 0, logSuccess));

            Assert.That(exception.ParamName, Is.EqualTo("service"));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void WithLogWithResultWhenActionNull(bool logSuccess)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => AstoriaServiceExtensions.WithLog(this.service, (Func<int>)null, logSuccess));

            Assert.That(exception.ParamName, Is.EqualTo("func"));
        }

        [Test]
        public void InvokeActionWithCustomLogWithoutResult()
        {
            bool actionInvoked = false;
            AstoriaServiceExtensions.WithLog(
                this.service, 
                log =>
                {
                    log.Write("Test");
                    actionInvoked = true;
                }, 
                logSuccess: true);

            Assert.That(actionInvoked, Is.True);
            Assert.That(this.logger.Messages.First(), Is.EqualTo("Test"));
        }

        [Test]
        public void InvokeActionWithCustomLogWithoutResultWithoutLogSuccess()
        {
            bool actionInvoked = false;
            AstoriaServiceExtensions.WithLog(
                this.service, 
                log =>
                {
                    log.WriteLine("Test");
                    actionInvoked = true;
                }, 
                logSuccess: false);

            Assert.That(actionInvoked, Is.True);
            Assert.That(this.logger.Messages.Count, Is.EqualTo(0));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void InvokeActionWithCustomLogWithoutResultWithError(bool logSuccess)
        {
            var exception = new InvalidOperationException("Test");
            var thrownException = Assert.Throws<InvalidOperationException>(
                () => AstoriaServiceExtensions.WithLog(
                    this.service, 
                    log =>
                    {
                        log.WriteLine("Test");
                        throw exception;
                    }, 
                    logSuccess));

            Assert.That(thrownException, Is.EqualTo(exception));
            Assert.That(this.logger.Messages.Count, Is.EqualTo(0));
            Assert.That(this.logger.Errors.First().Value, Is.EqualTo(exception));
            StringAssert.Contains("InvokeActionWithCustomLogWithoutResultWithError", this.logger.Errors.First().Key);
            StringAssert.Contains("call failed", this.logger.Errors.First().Key);
            StringAssert.Contains("Test", this.logger.Errors.First().Key);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void WithCustomLogWithoutResultWhenServiceNull(bool logSuccess)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => AstoriaServiceExtensions.WithLog<IDummy, AstoriaServiceConfiguration>(
                    null,
                    log =>
                    {
                    },
                    logSuccess));

            Assert.That(exception.ParamName, Is.EqualTo("service"));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void WithCustomLogWithoutResultWhenActionNull(bool logSuccess)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => AstoriaServiceExtensions.WithLog(this.service, (Action<ILog>)null, logSuccess));

            Assert.That(exception.ParamName, Is.EqualTo("action"));
        }

        [Test]
        public void InvokeActionWithCustomLogWithResult()
        {
            bool actionInvoked = false;
            int result = AstoriaServiceExtensions.WithLog(
                this.service,
                log =>
                {
                    log.WriteLine("Test");
                    actionInvoked = true;
                    return 7;
                },
                logSuccess: true);

            Assert.That(actionInvoked, Is.True);
            Assert.That(result, Is.EqualTo(7));

            string message = this.logger.Messages.First();
            StringAssert.Contains("Test", message);
        }

        [Test]
        public void InvokeActionWithCustomLogWithResultWithoutLogSuccess()
        {
            bool actionInvoked = false;
            int result = AstoriaServiceExtensions.WithLog(
                this.service,
                log =>
                {
                    log.WriteLine("Test");
                    actionInvoked = true;
                    return 7;
                },
                logSuccess: false);

            Assert.That(actionInvoked, Is.True);
            Assert.That(result, Is.EqualTo(7));
            Assert.That(this.logger.Messages.Count, Is.EqualTo(0));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void InvokeActionWithCustomLogWithResultWithError(bool logSuccess)
        {
            var exception = new InvalidOperationException("Test");
            var thrownException = Assert.Throws<InvalidOperationException>(
                () => AstoriaServiceExtensions.WithLog<IDummy, AstoriaServiceConfiguration, int>(
                    this.service,
                    log =>
                    {
                        log.WriteLine("Test");
                        throw exception;
                    },
                    logSuccess));

            Assert.That(thrownException, Is.EqualTo(exception));
            Assert.That(this.logger.Messages.Count, Is.EqualTo(0));
            Assert.That(this.logger.Errors.First().Value, Is.EqualTo(exception));

            string message = this.logger.Errors.First().Key;
            StringAssert.Contains("InvokeActionWithCustomLogWithResultWithError", message);
            StringAssert.Contains("call failed", message);
            StringAssert.Contains("Test", message);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void WithCustomLogWithResultWhenServiceNull(bool logSuccess)
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => AstoriaServiceExtensions.WithLog<IDummy, AstoriaServiceConfiguration, int>(
                    null, log => 0, logSuccess));

            Assert.That(exception.ParamName, Is.EqualTo("service"));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void WithCustomLogWithResultWhenActionNull(bool logSuccess)
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => AstoriaServiceExtensions.WithLog(this.service, (Func<ILog, int>)null, logSuccess));

            Assert.That(exception.ParamName, Is.EqualTo("func"));
        }
    }

    /* ReSharper restore InvokeAsExtensionMethod */
}
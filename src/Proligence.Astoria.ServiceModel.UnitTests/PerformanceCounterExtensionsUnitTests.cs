﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PerformanceCounterExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using NUnit.Framework;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Deployment;

    /* ReSharper disable InvokeAsExtensionMethod */

    [TestFixture]
    public class PerformanceCounterExtensionsUnitTests
    {
        private AstoriaDeploymentProviderMock provider;
        private ServiceLoggerMock logger;
        private CounterCreationData[] counterCreationData;

        [SetUp]
        public void Setup()
        {
            this.provider = new AstoriaDeploymentProviderMock();
            this.logger = new ServiceLoggerMock();

            this.counterCreationData = new[]
            {
                new CounterCreationData("Counter 1", "Counter 1 help", PerformanceCounterType.NumberOfItems32),
                new CounterCreationData("Counter 2", "Counter 2 help", PerformanceCounterType.RawBase),
                new CounterCreationData("Counter 3", "Counter 3 help", PerformanceCounterType.RateOfCountsPerSecond64)
            };
        }

        [Test]
        public void CreatePerformanceCounterCategoryWhenProviderNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => PerformanceCounterExtensions.CreatePerformanceCounterCategory(
                    null, 
                    this.logger, 
                    "My Category", 
                    "My category description.", 
                    PerformanceCounterCategoryType.SingleInstance, 
                    this.counterCreationData));

            Assert.That(exception.ParamName, Is.EqualTo("provider"));
        }

        [Test]
        public void CreatePerformanceCounterCategoryWhenLoggerNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => PerformanceCounterExtensions.CreatePerformanceCounterCategory(
                    this.provider,
                    null,
                    "My Category",
                    "My category description.",
                    PerformanceCounterCategoryType.SingleInstance,
                    this.counterCreationData));

            Assert.That(exception.ParamName, Is.EqualTo("logger"));
        }

        [Test, Sequential]
        public void CreatePerformanceCounterCategoryWhenNameNullOrWhiteSpace(
            [Values(null, "", " ", "\r\n", "\t")] string categoryName)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => PerformanceCounterExtensions.CreatePerformanceCounterCategory(
                    this.provider,
                    this.logger,
                    categoryName,
                    "My category description.",
                    PerformanceCounterCategoryType.SingleInstance,
                    this.counterCreationData));

            Assert.That(exception.ParamName, Is.EqualTo("categoryName"));
        }

        [Test, Sequential]
        public void CreatePerformanceCounterCategoryWhenDescriptionNullOrWhiteSpace(
            [Values(null, "", " ", "\r\n", "\t")] string categoryDescription)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => PerformanceCounterExtensions.CreatePerformanceCounterCategory(
                    this.provider,
                    this.logger,
                    "My Category",
                    categoryDescription,
                    PerformanceCounterCategoryType.SingleInstance,
                    this.counterCreationData));

            Assert.That(exception.ParamName, Is.EqualTo("categoryDescription"));
        }

        [Test]
        public void CreatePerformanceCounterCategoryWhenPerformanceCountersNull()
        {
            var exception = Assert.Throws<ArgumentException>(
                () => PerformanceCounterExtensions.CreatePerformanceCounterCategory(
                    this.provider,
                    this.logger,
                    "My Category",
                    "My category description.",
                    PerformanceCounterCategoryType.SingleInstance,
                    null));

            Assert.That(
                exception.Message,
                Is.EqualTo("No counters specified for performance counter category.\r\nParameter name: counters"));
            Assert.That(exception.ParamName, Is.EqualTo("counters"));
        }

        [Test]
        public void CreatePerformanceCounterCategoryWhenPerformanceCountersEmpty()
        {
            var exception = Assert.Throws<ArgumentException>(
                () => PerformanceCounterExtensions.CreatePerformanceCounterCategory(
                    this.provider,
                    this.logger,
                    "My Category",
                    "My category description.",
                    PerformanceCounterCategoryType.SingleInstance,
                    new CounterCreationData[0]));

            Assert.That(
                exception.Message, 
                Is.EqualTo("No counters specified for performance counter category.\r\nParameter name: counters"));
            Assert.That(exception.ParamName, Is.EqualTo("counters"));
        }

        [Test]
        public void CreatePerformanceCounterCategoryWhenCategoryDoesNotExist()
        {
            CounterCreationData[] data = this.counterCreationData;

            bool result = PerformanceCounterExtensions.CreatePerformanceCounterCategory(
                this.provider,
                this.logger,
                "My Category",
                "My Category description",
                PerformanceCounterCategoryType.SingleInstance,
                data);

            Assert.That(result, Is.True);
            Assert.That(this.provider.PerformanceCounterManager.Categories.Single().Key, Is.EqualTo("My Category"));
            CollectionAssert.AreEqual(
                this.provider.PerformanceCounterManager.Categories.Single().Value,
                new[] { data[0].CounterName, data[1].CounterName, data[2].CounterName });

            Assert.That(this.logger.Messages.Count, Is.EqualTo(1));
            Assert.That(
                this.logger.Messages.ToArray()[0],
                Is.EqualTo("Created performance counter category: My Category"));
            Assert.That(this.logger.Warnings, Is.Empty);
            Assert.That(this.logger.Errors, Is.Empty);
        }

        [Test]
        public void CreatePerformanceCounterCategoryWhenExistsWithSameCounters()
        {
            CounterCreationData[] data = this.counterCreationData;
            
            this.provider.PerformanceCounterManager.Categories.Add(
                "My Category", 
                new List<string>(new[] { data[0].CounterName, data[1].CounterName, data[2].CounterName }));

            bool result = PerformanceCounterExtensions.CreatePerformanceCounterCategory(
                this.provider,
                this.logger,
                "My Category",
                "My Category description",
                PerformanceCounterCategoryType.SingleInstance,
                data);

            Assert.That(result, Is.False);
            Assert.That(this.provider.PerformanceCounterManager.Categories.Single().Key, Is.EqualTo("My Category"));
            CollectionAssert.AreEqual(
                this.provider.PerformanceCounterManager.Categories.Single().Value,
                new[] { data[0].CounterName, data[1].CounterName, data[2].CounterName });

            Assert.That(this.logger.Messages, Is.Empty);
            Assert.That(this.logger.Warnings, Is.Empty);
            Assert.That(this.logger.Errors, Is.Empty);
        }

        [Test]
        public void CreatePerformanceCounterCategoryWhenExistsWithDifferentCounters()
        {
            this.provider.PerformanceCounterManager.Categories.Add(
                "My Category", new List<string>(new[] { "a", "b", "c" }));

            bool result = PerformanceCounterExtensions.CreatePerformanceCounterCategory(
                this.provider,
                this.logger,
                "My Category",
                "My Category description",
                PerformanceCounterCategoryType.SingleInstance,
                new CounterCreationData("a", "a help", PerformanceCounterType.NumberOfItems32),
                new CounterCreationData("d", "d help", PerformanceCounterType.NumberOfItems32),
                new CounterCreationData("c", "c help", PerformanceCounterType.NumberOfItems32));

            Assert.That(result, Is.True);
            Assert.That(this.provider.PerformanceCounterManager.Categories.Single().Key, Is.EqualTo("My Category"));
            CollectionAssert.AreEqual(
                this.provider.PerformanceCounterManager.Categories.Single().Value, 
                new[] { "a", "d", "c" });

            Assert.That(this.logger.Messages.Count, Is.EqualTo(2));
            Assert.That(
                this.logger.Messages.ToArray()[0], 
                Is.EqualTo("Deleted existing performance counter category: My Category"));
            Assert.That(
                this.logger.Messages.ToArray()[1],
                Is.EqualTo("Created performance counter category: My Category"));
            Assert.That(this.logger.Warnings, Is.Empty);
            Assert.That(this.logger.Errors, Is.Empty);
        }

        [Test]
        public void CreatePerformanceCounterCategoryWhenExistsWithMissingCounters()
        {
            this.provider.PerformanceCounterManager.Categories.Add(
                "My Category", new List<string>(new[] { "a", "c" }));

            bool result = PerformanceCounterExtensions.CreatePerformanceCounterCategory(
                this.provider,
                this.logger,
                "My Category",
                "My Category description",
                PerformanceCounterCategoryType.SingleInstance,
                new CounterCreationData("a", "a help", PerformanceCounterType.NumberOfItems32),
                new CounterCreationData("d", "d help", PerformanceCounterType.NumberOfItems32),
                new CounterCreationData("c", "c help", PerformanceCounterType.NumberOfItems32));

            Assert.That(result, Is.True);
            Assert.That(this.provider.PerformanceCounterManager.Categories.Single().Key, Is.EqualTo("My Category"));
            CollectionAssert.AreEqual(
                this.provider.PerformanceCounterManager.Categories.Single().Value,
                new[] { "a", "d", "c" });

            Assert.That(this.logger.Messages.Count, Is.EqualTo(2));
            Assert.That(
                this.logger.Messages.ToArray()[0],
                Is.EqualTo("Deleted existing performance counter category: My Category"));
            Assert.That(
                this.logger.Messages.ToArray()[1],
                Is.EqualTo("Created performance counter category: My Category"));
            Assert.That(this.logger.Warnings, Is.Empty);
            Assert.That(this.logger.Errors, Is.Empty);
        }

        [Test]
        public void CreatePerformanceCounterCategoryWhenExistsWithAdditionalCounters()
        {
            this.provider.PerformanceCounterManager.Categories.Add(
                "My Category", new List<string>(new[] { "a", "b", "c", "d" }));

            bool result = PerformanceCounterExtensions.CreatePerformanceCounterCategory(
                this.provider,
                this.logger,
                "My Category",
                "My Category description",
                PerformanceCounterCategoryType.SingleInstance,
                new CounterCreationData("a", "a help", PerformanceCounterType.NumberOfItems32),
                new CounterCreationData("d", "d help", PerformanceCounterType.NumberOfItems32),
                new CounterCreationData("c", "c help", PerformanceCounterType.NumberOfItems32));

            Assert.That(result, Is.False);
            Assert.That(this.provider.PerformanceCounterManager.Categories.Single().Key, Is.EqualTo("My Category"));
            CollectionAssert.AreEqual(
                this.provider.PerformanceCounterManager.Categories.Single().Value,
                new[] { "a", "b", "c", "d" });

            Assert.That(this.logger.Messages, Is.Empty);
            Assert.That(this.logger.Warnings, Is.Empty);
            Assert.That(this.logger.Errors, Is.Empty);
        }
    }

    /* ReSharper restore InvokeAsExtensionMethod */
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConsoleLoggerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using NLog.Targets;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Logging;

    [TestFixture]
    public class ConsoleLoggerUnitTests
    {
        private ConsoleLogger logger;

        [SetUp]
        public void Setup()
        {
            this.logger = new ConsoleLogger();
            this.logger.Initialize();
        }

        [Test]
        public void LoggerInitialized()
        {
            Assert.That(this.logger.IsInitialized, Is.True);
        }

        [Test]
        public void CheckEventLogger()
        {
            Assert.That(this.logger.EventLogger, Is.Not.Null);
            Assert.That(this.logger.LogTarget, Is.TypeOf(typeof(ColoredConsoleTarget)));
        }

        [Test]
        public void CheckLogLayout()
        {
            ColoredConsoleTarget logTarget = (ColoredConsoleTarget)this.logger.LogTarget;
            string expected = "'" + NLogLogger.DefaultLayout + "'";
            
            Assert.That(logTarget.Layout.ToString(), Is.EqualTo(expected));
        }
    }
}
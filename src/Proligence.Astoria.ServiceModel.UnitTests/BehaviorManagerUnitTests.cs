﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BehaviorManagerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;

    [TestFixture]
    public class BehaviorManagerUnitTests
    {
        [AstoriaServiceBehavior]
        internal interface ITestAstoriaServiceBehavior : IAstoriaServiceBehavior
        {
        }

        [AstoriaServiceBehavior]
        internal interface ITestAstoriaServiceBehavior2 : IAstoriaServiceBehavior
        {
        }

        [AstoriaServiceBehavior(AllowMultiple = false)]
        internal interface ITestAstoriaServiceBehavior3 : IAstoriaServiceBehavior
        {
        }

        [Test]
        public void RegisterServiceBehaviorWhenBehaviorNull()
        {
            var manager = new BehaviorManager();
            
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => manager.RegisterServiceBehavior<TestAstoriaServiceBehavior>(null));

            Assert.That(exception.ParamName, Is.EqualTo("behavior"));
        }

        [Test]
        public void RegisterServiceBehaviorWhenBehaviorNotNull()
        {
            var manager = new BehaviorManagerMock();
            var behavior = new TestAstoriaServiceBehavior();

            manager.RegisterServiceBehavior(behavior);

            Assert.That(manager.Behaviors.Single(), Is.SameAs(behavior));
        }

        [Test]
        public void RegisterServiceBehaviorWhenAllowMultipleFalseAndBehaviorOfSameTypeRegistered()
        {
            var behavior1 = new TestAstoriaServiceBehavior();
            var behavior2 = new TestAstoriaServiceBehavior3A();
            var behavior3 = new TestAstoriaServiceBehavior3B();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(behavior2);

            manager.RegisterServiceBehavior(behavior3);

            Assert.That(manager.Behaviors.Count, Is.EqualTo(2));
            Assert.That(manager.Behaviors, Contains.Item(behavior1));
            Assert.That(manager.Behaviors, Contains.Item(behavior3));
        }

        [Test]
        public void GetServiceBehaviorByInterfaceWhenBehaviorRegistered()
        {
            var behavior = new TestAstoriaServiceBehavior();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(new TestAstoriaServiceBehavior2());
            manager.Behaviors.Add(behavior);

            var result = manager.GetServiceBehavior<ITestAstoriaServiceBehavior>();

            Assert.That(result, Is.SameAs(behavior));
        }

        [Test]
        public void GetServiceBehaviorByClassWhenBehaviorRegistered()
        {
            var behavior = new TestAstoriaServiceBehavior();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(new TestAstoriaServiceBehavior2());
            manager.Behaviors.Add(behavior);

            var result = manager.GetServiceBehavior<TestAstoriaServiceBehavior>();

            Assert.That(result, Is.SameAs(behavior));
        }

        [Test]
        public void GetServiceBehaviorWhenMultipleBehaviorsRegistered()
        {
            var behavior1 = new TestAstoriaServiceBehavior();
            var behavior2 = new TestAstoriaServiceBehavior();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(new TestAstoriaServiceBehavior2());
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(behavior2);

            var result = manager.GetServiceBehavior<TestAstoriaServiceBehavior>();

            Assert.That(result, Is.SameAs(behavior1).Or.SameAs(behavior2));
        }

        [Test]
        public void GetServiceBehaviorWhenBehaviorNotRegistered()
        {
            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(new TestAstoriaServiceBehavior2());

            var result = manager.GetServiceBehavior<TestAstoriaServiceBehavior>();

            Assert.That(result, Is.Null);
        }

        [Test]
        public void GetRequiredServiceBehaviorByInterfaceWhenBehaviorRegistered()
        {
            var behavior = new TestAstoriaServiceBehavior();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(new TestAstoriaServiceBehavior2());
            manager.Behaviors.Add(behavior);

            var result = manager.GetRequiredServiceBehavior<ITestAstoriaServiceBehavior>();

            Assert.That(result, Is.SameAs(behavior));
        }

        [Test]
        public void GetRequiredServiceBehaviorByClassWhenBehaviorRegistered()
        {
            var behavior = new TestAstoriaServiceBehavior();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(new TestAstoriaServiceBehavior2());
            manager.Behaviors.Add(behavior);

            var result = manager.GetRequiredServiceBehavior<TestAstoriaServiceBehavior>();

            Assert.That(result, Is.SameAs(behavior));
        }

        [Test]
        public void GetRequiredServiceBehaviorWhenBehaviorNotRegistered()
        {
            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(new TestAstoriaServiceBehavior2());

            InvalidOperationException exception = Assert.Throws<InvalidOperationException>(
                () => manager.GetRequiredServiceBehavior<TestAstoriaServiceBehavior>());

            string expected = 
                "Failed to find service behavior of type '" + typeof(TestAstoriaServiceBehavior).FullName + "'.";

            Assert.That(exception.Message, Is.StringContaining(expected));
        }

        [Test]
        public void GetServiceBehaviorsByInterfaceWhenNoBehaviorRegistered()
        {
            var behavior = new TestAstoriaServiceBehavior();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(new TestAstoriaServiceBehavior2());
            manager.Behaviors.Add(behavior);

            var result = manager.GetServiceBehaviors<ITestAstoriaServiceBehavior>();

            Assert.That(result.ToArray(), Is.EquivalentTo(new[] { behavior }));
        }

        [Test]
        public void GetServiceBehaviorsByClassWhenNoBehaviorRegistered()
        {
            var behavior = new TestAstoriaServiceBehavior();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(new TestAstoriaServiceBehavior2());
            manager.Behaviors.Add(behavior);

            var result = manager.GetServiceBehaviors<TestAstoriaServiceBehavior>();

            Assert.That(result.ToArray(), Is.EquivalentTo(new[] { behavior }));
        }

        [Test]
        public void GetServiceBehaviorsWhenNoBehaviorRegistered()
        {
            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(new TestAstoriaServiceBehavior2());
            
            var result = manager.GetServiceBehaviors<TestAstoriaServiceBehavior>();

            Assert.That(result, Is.Empty);
        }

        [Test]
        public void GetServiceBehaviorsWhenMultipleBehaviorsRegistered()
        {
            var behavior1 = new TestAstoriaServiceBehavior();
            var behavior2 = new TestAstoriaServiceBehavior();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(new TestAstoriaServiceBehavior2());
            manager.Behaviors.Add(behavior2);

            var result = manager.GetServiceBehaviors<TestAstoriaServiceBehavior>();

            Assert.That(result.ToArray(), Is.EquivalentTo(new[] { behavior1, behavior2 }));
        }

        [Test]
        public void GetOrRegisterServiceBehaviorWhenBehaviorRegistered()
        {
            var behavior = new TestAstoriaServiceBehavior();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior);

            var result = manager.GetOrRegisterServiceBehavior<TestAstoriaServiceBehavior>();

            Assert.That(result, Is.SameAs(behavior));
            Assert.That(manager.Behaviors.Single(), Is.SameAs(result));
        }

        [Test]
        public void GetOrRegisterServiceBehaviorWhenBehaviorNotRegistered()
        {
            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(new TestAstoriaServiceBehavior());

            var result = manager.GetOrRegisterServiceBehavior<TestAstoriaServiceBehavior2>();

            Assert.That(result, Is.InstanceOf<TestAstoriaServiceBehavior2>());
            Assert.That(manager.Behaviors.Count, Is.EqualTo(2));
            Assert.That(manager.Behaviors, Contains.Item(result));
        }

        [Test]
        public void GetOrRegisterServiceBehaviorWhenAllowMultipleFalseAndBehaviorOfSameTypeRegistered()
        {
            var behavior1 = new TestAstoriaServiceBehavior();
            var behavior2 = new TestAstoriaServiceBehavior3A();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(behavior2);

            var result = manager.GetOrRegisterServiceBehavior<TestAstoriaServiceBehavior3B>();

            Assert.That(result, Is.InstanceOf<TestAstoriaServiceBehavior3B>());
            Assert.That(manager.Behaviors.Count, Is.EqualTo(2)); 
            Assert.That(manager.Behaviors, Contains.Item(result));
            Assert.That(manager.Behaviors, Contains.Item(behavior1));
        }

        [Test]
        public void RemoveServiceBehaviorsOfSpecifiedTypeWhenBehaviorNotPresent()
        {
            var behavior1 = new TestAstoriaServiceBehavior2();
            var behavior2 = new TestAstoriaServiceBehavior3A();
            var behavior3 = new TestAstoriaServiceBehavior3B();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(behavior2);
            manager.Behaviors.Add(behavior3);

            var result = manager.RemoveServiceBehavior<ITestAstoriaServiceBehavior>();

            Assert.That(result, Is.Empty);
            Assert.That(
                manager.Behaviors.ToArray(),
                Is.EquivalentTo(new IAstoriaServiceBehavior[] { behavior1, behavior2, behavior3 }));
        }

        [Test]
        public void RemoveServiceBehaviorsOfSpecifiedTypeWhenSingleBehaviorPresent()
        {
            var behavior1 = new TestAstoriaServiceBehavior3A();
            var behavior2 = new TestAstoriaServiceBehavior2();
            var behavior3 = new TestAstoriaServiceBehavior3B();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(behavior2);
            manager.Behaviors.Add(behavior3);

            var result = manager.RemoveServiceBehavior<ITestAstoriaServiceBehavior2>();

            Assert.That(result.ToArray(), Is.EqualTo(new[] { behavior2 }));
            Assert.That(
                manager.Behaviors.ToArray(),
                Is.EquivalentTo(new IAstoriaServiceBehavior[] { behavior1, behavior3 }));
        }

        [Test]
        public void RemoveServiceBehaviorOfSpecifiedTypeWhenMultipleBehaviorsPresent()
        {
            var behavior1 = new TestAstoriaServiceBehavior3A();
            var behavior2 = new TestAstoriaServiceBehavior2();
            var behavior3 = new TestAstoriaServiceBehavior3B();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(behavior2);
            manager.Behaviors.Add(behavior3);

            var result = manager.RemoveServiceBehavior<ITestAstoriaServiceBehavior3>();

            Assert.That(result.ToArray(), Is.EqualTo(new ITestAstoriaServiceBehavior3[] { behavior1, behavior3 }));
            Assert.That(manager.Behaviors.ToArray(), Is.EquivalentTo(new IAstoriaServiceBehavior[] { behavior2 }));
        }

        [Test]
        public void RemoveServiceBehaviorsOfSpecifiedTypeNonGenericWhenBehaviorNotPresent()
        {
            var behavior1 = new TestAstoriaServiceBehavior2();
            var behavior2 = new TestAstoriaServiceBehavior3A();
            var behavior3 = new TestAstoriaServiceBehavior3B();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(behavior2);
            manager.Behaviors.Add(behavior3);

            var result = manager.RemoveServiceBehavior(typeof(ITestAstoriaServiceBehavior));

            Assert.That(result, Is.Empty);
            Assert.That(
                manager.Behaviors.ToArray(),
                Is.EquivalentTo(new IAstoriaServiceBehavior[] { behavior1, behavior2, behavior3 }));
        }

        [Test]
        public void RemoveServiceBehaviorsOfSpecifiedTypeNonGenericWhenSingleBehaviorPresent()
        {
            var behavior1 = new TestAstoriaServiceBehavior3A();
            var behavior2 = new TestAstoriaServiceBehavior2();
            var behavior3 = new TestAstoriaServiceBehavior3B();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(behavior2);
            manager.Behaviors.Add(behavior3);

            var result = manager.RemoveServiceBehavior(typeof(ITestAstoriaServiceBehavior2));

            Assert.That(result.ToArray(), Is.EqualTo(new[] { behavior2 }));
            Assert.That(
                manager.Behaviors.ToArray(),
                Is.EquivalentTo(new IAstoriaServiceBehavior[] { behavior1, behavior3 }));
        }

        [Test]
        public void RemoveServiceBehaviorOfSpecifiedTypeNonGenericWhenMultipleBehaviorsPresent()
        {
            var behavior1 = new TestAstoriaServiceBehavior3A();
            var behavior2 = new TestAstoriaServiceBehavior2();
            var behavior3 = new TestAstoriaServiceBehavior3B();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(behavior2);
            manager.Behaviors.Add(behavior3);

            var result = manager.RemoveServiceBehavior(typeof(ITestAstoriaServiceBehavior3));

            Assert.That(result.ToArray(), Is.EqualTo(new ITestAstoriaServiceBehavior3[] { behavior1, behavior3 }));
            Assert.That(manager.Behaviors.ToArray(), Is.EquivalentTo(new IAstoriaServiceBehavior[] { behavior2 }));
        }

        [Test]
        public void RemoveServiceBehaviorWhenBehaviorNull()
        {
            var manager = new BehaviorManagerMock();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => manager.RemoveServiceBehavior<ITestAstoriaServiceBehavior>(null));

            Assert.That(exception.ParamName, Is.EqualTo("behavior"));
        }

        [Test]
        public void RemoveServiceBehaviorWhenBehaviorNotPresent()
        {
            var behavior1 = new TestAstoriaServiceBehavior();
            var behavior2 = new TestAstoriaServiceBehavior2();
            var behavior3 = new TestAstoriaServiceBehavior3A();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(behavior2);
            manager.Behaviors.Add(behavior3);

            bool result = manager.RemoveServiceBehavior(new TestAstoriaServiceBehavior());

            Assert.That(result, Is.False);
            Assert.That(
                manager.Behaviors.ToArray(),
                Is.EquivalentTo(new IAstoriaServiceBehavior[] { behavior1, behavior2, behavior3 }));
        }

        [Test]
        public void RemoveServiceBehaviorWhenBehaviorPresent()
        {
            var behavior1 = new TestAstoriaServiceBehavior();
            var behavior2 = new TestAstoriaServiceBehavior2();
            var behavior3 = new TestAstoriaServiceBehavior3A();

            var manager = new BehaviorManagerMock();
            manager.Behaviors.Add(behavior1);
            manager.Behaviors.Add(behavior2);
            manager.Behaviors.Add(behavior3);

            bool result = manager.RemoveServiceBehavior(behavior2);

            Assert.That(result, Is.True);
            Assert.That(
                manager.Behaviors.ToArray(),
                Is.EquivalentTo(new IAstoriaServiceBehavior[] { behavior1, behavior3 }));
        }

        private class BehaviorManagerMock : BehaviorManager 
        {
            public new IList<IAstoriaServiceBehavior> Behaviors
            {
                get { return base.Behaviors; }
            }
        }

        private class TestAstoriaServiceBehavior : ITestAstoriaServiceBehavior
        {
        }

        private class TestAstoriaServiceBehavior2 : ITestAstoriaServiceBehavior2
        {
        }

        private class TestAstoriaServiceBehavior3A : ITestAstoriaServiceBehavior3
        {
        }

        private class TestAstoriaServiceBehavior3B : ITestAstoriaServiceBehavior3
        {
        }
    }
}
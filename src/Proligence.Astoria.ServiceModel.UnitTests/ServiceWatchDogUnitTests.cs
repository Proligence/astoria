﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceWatchDogUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    public class ServiceWatchDogUnitTests
    {
        [Test]
        public void InspectWhenExceptionThrown()
        {
            var exception = new InvalidOperationException("Test exception message");
            
            var watchDog = new ServiceWatchDogStub
            {
                InspectAction = () =>
                {
                    throw exception;
                }
            };

            WatchDogResult result = watchDog.Inspect();

            Assert.That(result.Errors.Single(), Is.StringContaining(exception.ToString()));
        }

        private class ServiceWatchDogStub : ServiceWatchDog
        {
            public Action InspectAction { get; set; }

            protected override void Inspect(WatchDogResult result)
            {
                if (this.InspectAction != null)
                {
                    this.InspectAction();
                }

                base.Inspect(result);
            }
        }
    }
}
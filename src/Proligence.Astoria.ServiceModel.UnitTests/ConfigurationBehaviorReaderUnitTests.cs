﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationBehaviorReaderUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Linq;
    using System.Xml;
    using NUnit.Framework;
    using Proligence.Astoria.Configuration;

    [TestFixture]
    public class ConfigurationBehaviorReaderUnitTests
    {
        internal enum TestEnum
        {
            Value1,
            Value2
        }

        [AstoriaServiceBehavior(AllowMultiple = true)]
        internal interface ITestBehavior1 : IAstoriaServiceBehavior
        {
            string StringValue { get; set; }
            int IntValue { get; set; }
            bool BoolValue { get; set; }
            TestEnum EnumValue { get; set; }
        }

        [AstoriaServiceBehavior(AllowMultiple = false)]
        internal interface ITestBehavior2 : IAstoriaServiceBehavior
        {
            string Value { get; }
        }

        [AstoriaServiceBehavior]
        internal interface ITestBehavior3 : IAstoriaServiceBehavior
        {
            string Value { get; }
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenTypeNull()
        {
            var reader = new ConfigurationBehaviorReader();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => reader.ReadServiceBehaviorsFromConfiguration(null));

            Assert.That(exception.ParamName, Is.EqualTo("configuration"));
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenXmlConfigurationMissing()
        {
            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration();

            ArgumentException exception = Assert.Throws<ArgumentException>(
                () => reader.ReadServiceBehaviorsFromConfiguration(configuration));

            Assert.That(exception.Message, Is.StringContaining("Configuration XML document not found."));
            Assert.That(exception.ParamName, Is.EqualTo("configuration"));
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenServiceElementMissing()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            reader.ReadServiceBehaviorsFromConfiguration(configuration);

            Assert.That(reader.AddedServiceBehaviors, Is.Empty);
            Assert.That(reader.RemovedServiceBehaviors, Is.Empty);
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenBehaviorsElementMissing()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            reader.ReadServiceBehaviorsFromConfiguration(configuration);

            Assert.That(reader.AddedServiceBehaviors, Is.Empty);
            Assert.That(reader.RemovedServiceBehaviors, Is.Empty);
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenEmptyBehaviorsElement()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
    <Behaviors>
    </Behaviors>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            reader.ReadServiceBehaviorsFromConfiguration(configuration);

            Assert.That(reader.AddedServiceBehaviors, Is.Empty);
            Assert.That(reader.RemovedServiceBehaviors, Is.Empty);
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenSingleBehaviorAdded()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
    <Behaviors>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior2 />
    </Behaviors>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            reader.ReadServiceBehaviorsFromConfiguration(configuration);

            Assert.That(reader.AddedServiceBehaviors.Count(), Is.EqualTo(1));
            Assert.That(reader.RemovedServiceBehaviors, Is.Empty);
            
            TestBehavior2 behavior = (TestBehavior2)reader.AddedServiceBehaviors.ElementAt(0);
            Assert.That(behavior.Value, Is.Null);
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenMultipleBehaviorsAdded()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
    <Behaviors>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1 />
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior2 />
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior3 />
    </Behaviors>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            reader.ReadServiceBehaviorsFromConfiguration(configuration);

            Assert.That(reader.AddedServiceBehaviors.Count(), Is.EqualTo(3));
            Assert.That(reader.RemovedServiceBehaviors, Is.Empty);

            TestBehavior1 behavior1 = (TestBehavior1)reader.AddedServiceBehaviors.ElementAt(0);
            Assert.That(behavior1.StringValue, Is.Null);
            Assert.That(behavior1.IntValue, Is.EqualTo(0));
            Assert.That(behavior1.BoolValue, Is.EqualTo(false));

            TestBehavior2 behavior2 = (TestBehavior2)reader.AddedServiceBehaviors.ElementAt(1);
            Assert.That(behavior2.Value, Is.Null);

            TestBehavior3 behavior3 = (TestBehavior3)reader.AddedServiceBehaviors.ElementAt(2);
            Assert.That(behavior3.Value, Is.Null);
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenBehaviorWithParametersAdded()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
    <Behaviors>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1>
            <StringValue>Test</StringValue>
            <IntValue>7</IntValue>
            <BoolValue>true</BoolValue>
            <EnumValue>Value2</EnumValue>
        </Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1>
    </Behaviors>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            reader.ReadServiceBehaviorsFromConfiguration(configuration);

            Assert.That(reader.AddedServiceBehaviors.Count(), Is.EqualTo(1));
            Assert.That(reader.RemovedServiceBehaviors, Is.Empty);

            TestBehavior1 behavior = (TestBehavior1)reader.AddedServiceBehaviors.ElementAt(0);
            Assert.That(behavior.StringValue, Is.EqualTo("Test"));
            Assert.That(behavior.IntValue, Is.EqualTo(7));
            Assert.That(behavior.BoolValue, Is.EqualTo(true));
            Assert.That(behavior.EnumValue, Is.EqualTo(TestEnum.Value2));
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenInvalidBehaviorName()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
    <Behaviors>
        <InvalidBehavior />
    </Behaviors>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            ServiceConfigurationException exception = Assert.Throws<ServiceConfigurationException>(
                () => reader.ReadServiceBehaviorsFromConfiguration(configuration));

            string expectedMessage =
                "Failed to find behavior type 'InvalidBehavior'. " +
                "Make sure that the class is implemented and loaded into the current AppDomain.";

            Assert.That(exception.Message, Is.EqualTo(expectedMessage));
            Assert.That(exception.SectionName, Is.EqualTo("Behaviors"));
            Assert.That(exception.SettingName, Is.EqualTo("InvalidBehavior"));
            Assert.That(exception.SettingValue, Is.Null);
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenInvalidBehaviorPropertyName()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
    <Behaviors>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1>
            <InvalidProperty>Test</InvalidProperty>
        </Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1>
    </Behaviors>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            ServiceConfigurationException exception = Assert.Throws<ServiceConfigurationException>(
                () => reader.ReadServiceBehaviorsFromConfiguration(configuration));

            string expectedMessage = 
                "The service behavior 'Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1' does not have " +
                "a property 'InvalidProperty'.";

            Assert.That(exception.Message, Is.EqualTo(expectedMessage));
            Assert.That(exception.SectionName, Is.EqualTo("Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1"));
            Assert.That(exception.SettingName, Is.EqualTo("InvalidProperty"));
            Assert.That(exception.SettingValue, Is.Null);
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenInvalidBehaviorPropertyValue()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
    <Behaviors>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1>
            <IntValue>Not a number</IntValue>
        </Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1>
    </Behaviors>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            ServiceConfigurationException exception = Assert.Throws<ServiceConfigurationException>(
                () => reader.ReadServiceBehaviorsFromConfiguration(configuration));

            Assert.That(exception.Message, Is.EqualTo("Failed to convert 'Not a number' to type 'System.Int32'."));
            Assert.That(exception.SectionName, Is.EqualTo("Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1"));
            Assert.That(exception.SettingName, Is.EqualTo("IntValue"));
            Assert.That(exception.SettingValue, Is.EqualTo("Not a number"));
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenBehaviorWithEmptyParametersAdded()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
    <Behaviors>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1>
            <StringValue />
            <IntValue />
            <BoolValue />
        </Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1>
    </Behaviors>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            reader.ReadServiceBehaviorsFromConfiguration(configuration);

            Assert.That(reader.AddedServiceBehaviors.Count(), Is.EqualTo(1));
            Assert.That(reader.RemovedServiceBehaviors, Is.Empty);

            TestBehavior1 behavior = (TestBehavior1)reader.AddedServiceBehaviors.ElementAt(0);
            Assert.That(behavior.StringValue, Is.Null);
            Assert.That(behavior.IntValue, Is.EqualTo(0));
            Assert.That(behavior.BoolValue, Is.EqualTo(false));
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenSingleBehaviorRemoved()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
    <Behaviors>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1 remove=""true""/>
    </Behaviors>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            reader.ReadServiceBehaviorsFromConfiguration(configuration);

            Assert.That(reader.AddedServiceBehaviors, Is.Empty);
            Assert.That(reader.RemovedServiceBehaviors.Count(), Is.EqualTo(1));
            Assert.That(reader.RemovedServiceBehaviors.ElementAt(0), Is.EqualTo(typeof(TestBehavior1)));
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenMultipleBehaviorsRemoved()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
    <Behaviors>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1 remove=""true""/>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior2 remove=""true""/>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior3 remove=""true""/>
    </Behaviors>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            reader.ReadServiceBehaviorsFromConfiguration(configuration);

            Assert.That(reader.AddedServiceBehaviors, Is.Empty);
            Assert.That(reader.RemovedServiceBehaviors.Count(), Is.EqualTo(3));
            Assert.That(reader.RemovedServiceBehaviors.ElementAt(0), Is.EqualTo(typeof(TestBehavior1)));
            Assert.That(reader.RemovedServiceBehaviors.ElementAt(1), Is.EqualTo(typeof(TestBehavior2)));
            Assert.That(reader.RemovedServiceBehaviors.ElementAt(2), Is.EqualTo(typeof(TestBehavior3)));
        }

        [Test]
        public void ReadServiceBehaviorsFromConfigurationWhenBehaviorsAddedAndRemoved()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.ServiceHost.UnitTests"">
    <Address>net.tcp://localhost/Test</Address>
    <Behaviors>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior1 />
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior2 remove=""true""/>
        <Proligence.Astoria.ServiceModel.UnitTests.TestBehavior3 />
    </Behaviors>
  </Service>
</TestConfiguration>";

            var reader = new ConfigurationBehaviorReader();
            var configuration = new TestConfiguration(xml);

            reader.ReadServiceBehaviorsFromConfiguration(configuration);

            Assert.That(reader.AddedServiceBehaviors.Count(), Is.EqualTo(2));
            Assert.That(reader.RemovedServiceBehaviors.Count(), Is.EqualTo(1));

            TestBehavior1 behavior1 = (TestBehavior1)reader.AddedServiceBehaviors.ElementAt(0);
            Assert.That(behavior1.StringValue, Is.Null);
            Assert.That(behavior1.IntValue, Is.EqualTo(0));
            Assert.That(behavior1.BoolValue, Is.EqualTo(false));

            TestBehavior3 behavior3 = (TestBehavior3)reader.AddedServiceBehaviors.ElementAt(1);
            Assert.That(behavior3.Value, Is.Null);

            Assert.That(reader.RemovedServiceBehaviors.ElementAt(0), Is.EqualTo(typeof(TestBehavior2)));
        }

        private class TestConfiguration : AstoriaServiceConfiguration
        {
            public TestConfiguration()
            {
            }

            public TestConfiguration(string xml)
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xml);
                
                this.XmlDocument = xmlDocument;
            }   
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    internal sealed class TestBehavior1 : Attribute, ConfigurationBehaviorReaderUnitTests.ITestBehavior1
    {
        public string StringValue { get; set; }
        public int IntValue { get; set; }
        public bool BoolValue { get; set; }
        public ConfigurationBehaviorReaderUnitTests.TestEnum EnumValue { get; set; }
    }

    /* ReSharper disable UnusedAutoPropertyAccessor.Local */

    [AttributeUsage(AttributeTargets.Class)]
    internal sealed class TestBehavior2 : Attribute, ConfigurationBehaviorReaderUnitTests.ITestBehavior2
    {
        public string Value { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    internal sealed class TestBehavior3 : Attribute, ConfigurationBehaviorReaderUnitTests.ITestBehavior3
    {
        public string Value { get; set; }
    }

    /* ReSharper restore UnusedAutoPropertyAccessor.Local */
}
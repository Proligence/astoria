﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WinNtPerformanceCounterFactoryUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    [SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly")]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    public class WinNtPerformanceCounterFactoryUnitTests
    {
        private Mock<IBehaviorManager> behaviorManager;
        private WinNtPerformanceCounterFactory factory;
        private MonitoringBehavior monitoringBehavior;

        [SetUp]
        public void Setup()
        {
            this.behaviorManager = new Mock<IBehaviorManager>();
            this.monitoringBehavior = new MonitoringBehavior { PerformanceCountersEnabled = true };
            this.behaviorManager.Setup(x => x.GetRequiredServiceBehavior<IMonitoringBehavior>())
                .Returns(this.monitoringBehavior);

            this.factory = new WinNtPerformanceCounterFactory(this.behaviorManager.Object);
        }

        [Test]
        public void CreatePerformanceCounterWhenParametersNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.factory.CreatePerformanceCounter(null));

            Assert.That(exception.ParamName, Is.EqualTo("parameters"));
        }

        [TestCase(null)]
        [TestCase("")]
        public void CreatePerformanceCounterWhenNameCounterNameNullOrEmpty(string name)
        {
            var parameters = new Mock<IPerformanceCounterParameters>();
            parameters.SetupGet(x => x.CounterName).Returns(name);

            ArgumentException exception = Assert.Throws<ArgumentException>(
                () => this.factory.CreatePerformanceCounter(parameters.Object));

            Assert.That(exception.Message, Is.StringContaining("The specified counter name is invalid."));
            Assert.That(exception.ParamName, Is.EqualTo("parameters"));
        }

        [Test]
        public void CreatePerformanceCounterWhenCounterNameSpecified()
        {
            var parameters = new Mock<IPerformanceCounterParameters>();
            parameters.SetupGet(x => x.CounterName).Returns("MyCounter");

            using (var counter = this.factory.CreatePerformanceCounter(parameters.Object))
            {
                Assert.That(counter, Is.InstanceOf<WinNtPerformanceCounter>());
                Assert.That(counter.CounterName, Is.EqualTo("MyCounter"));
                Assert.That(counter.CategoryName, Is.Empty);
                Assert.That(counter.InstanceName, Is.Empty);
            }
        }

        [Test]
        public void CreatePerformanceCounterWhenCategoryNameSpecified()
        {
            var parameters = new Mock<IPerformanceCounterParameters>();
            parameters.SetupGet(x => x.CounterName).Returns("MyCounter");
            parameters.SetupGet(x => x.CategoryName).Returns("MyCategory");

            using (var counter = this.factory.CreatePerformanceCounter(parameters.Object))
            {
                Assert.That(counter, Is.InstanceOf<WinNtPerformanceCounter>());
                Assert.That(counter.CounterName, Is.EqualTo("MyCounter"));
                Assert.That(counter.CategoryName, Is.EqualTo("MyCategory"));
                Assert.That(counter.InstanceName, Is.Empty);
            }
        }

        [Test]
        public void CreatePerformanceCounterWhenInstanceNameSpecified()
        {
            var parameters = new Mock<IPerformanceCounterParameters>();
            parameters.SetupGet(x => x.CounterName).Returns("MyCounter");
            parameters.SetupGet(x => x.InstanceName).Returns("MyInstance");

            using (var counter = this.factory.CreatePerformanceCounter(parameters.Object))
            {
                Assert.That(counter, Is.InstanceOf<WinNtPerformanceCounter>());
                Assert.That(counter.CounterName, Is.EqualTo("MyCounter"));
                Assert.That(counter.CategoryName, Is.Empty);
                Assert.That(counter.InstanceName, Is.EqualTo("MyInstance"));
            }
        }

        [Test]
        public void CreatePerformanceCounterWhenPerformanceCountersDisabled()
        {
            this.monitoringBehavior.PerformanceCountersEnabled = false;

            var parameters = new Mock<IPerformanceCounterParameters>();
            parameters.SetupGet(x => x.CounterName).Returns("MyCounter");
            parameters.SetupGet(x => x.CategoryName).Returns("MyCategory");
            parameters.SetupGet(x => x.InstanceName).Returns("MyInstance");

            using (var counter = this.factory.CreatePerformanceCounter(parameters.Object))
            {
                Assert.That(counter, Is.InstanceOf<NullPerformanceCounter>());
                Assert.That(counter.CounterName, Is.EqualTo("MyCounter"));
                Assert.That(counter.CategoryName, Is.EqualTo("MyCategory"));
                Assert.That(counter.InstanceName, Is.EqualTo("MyInstance"));
            }
        }
    }
}
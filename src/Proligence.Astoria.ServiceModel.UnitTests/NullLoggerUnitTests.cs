﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NullLoggerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Logging;

    [TestFixture]
    public class NullLoggerUnitTests
    {
        private NullLogger logger;

        [SetUp]
        public void Setup()
        {
            this.logger = new NullLogger();
            this.logger.Initialize();
        }

        [Test]
        public void LoggerInitialized()
        {
            Assert.That(this.logger.IsInitialized, Is.True);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void LogDebugMessage(bool debugOutputEnabled)
        {
            this.logger.DebugOutputEnabled = debugOutputEnabled;
            this.logger.LogDebugMessage("My message");
        }

        [Test]
        public void LogMessage()
        {
            this.logger.LogMessage("My message");
        }

        [Test]
        public void LogWarning()
        {
            this.logger.LogWarning("My message");
        }

        [Test]
        public void LogError()
        {
            this.logger.LogError("My message", null);
        }

        [Test]
        public void WriteErrorWithException()
        {
            this.logger.LogError("My message", new InvalidOperationException("My exception message"));
        }
    }
}
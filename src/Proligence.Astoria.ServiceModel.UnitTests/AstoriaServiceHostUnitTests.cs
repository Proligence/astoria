﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceHostUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Wcf;

    [TestFixture]
    public class AstoriaServiceHostUnitTests
    {
        private AstoriaServiceMock service;
        private AstoriaServiceHostMock serviceHost;

        [SetUp]
        public void Setup()
        {
            this.service = new AstoriaServiceMock("My service", null);
            this.serviceHost = new AstoriaServiceHostMock(this.service);
        }

        [TearDown]
        public void Teardown()
        {
            if (this.service != null)
            {
                this.service.Dispose();
            }

            if (this.serviceHost != null)
            {
                this.serviceHost.Dispose();
            }
        }

        [Test]
        public void CreateHost()
        {
            this.service = new AstoriaServiceMock("My service", null);
            this.serviceHost = new AstoriaServiceHostMock(this.service);

            Assert.That(this.serviceHost.Service, Is.SameAs(this.service));
        }

        [Test]
        public void CallInitializeWcfHostForWcfHostBehaviors()
        {
            var wcfHostBehavior1 = new Mock<IWcfHostBehavior>();
            WcfServiceHost wcfServiceHost1 = null;
            wcfHostBehavior1.Setup(x => x.InitializeWcfHost(It.IsAny<WcfServiceHost>()))
                .Callback<WcfServiceHost>(host =>
                    { 
                        Assert.That(((WcfServiceHostMock)host).IsOpen, Is.False);
                        wcfServiceHost1 = host;
                    });

            var wcfHostBehavior2 = new Mock<IWcfHostBehavior>();
            WcfServiceHost wcfServiceHost2 = null;
            wcfHostBehavior2.Setup(x => x.InitializeWcfHost(It.IsAny<WcfServiceHost>()))
                .Callback<WcfServiceHost>(host =>
                    {
                        Assert.That(((WcfServiceHostMock)host).IsOpen, Is.False);
                        wcfServiceHost2 = host;
                    });

            this.service.Initialize(new ServiceStartOptions());
            this.service.BehaviorManager.RegisterServiceBehavior(wcfHostBehavior1.Object);
            this.service.BehaviorManager.RegisterServiceBehavior(wcfHostBehavior2.Object);

            this.serviceHost.StartHost();

            Assert.That(wcfServiceHost1, Is.SameAs(this.serviceHost.WcfServiceHost));
            Assert.That(wcfServiceHost2, Is.SameAs(this.serviceHost.WcfServiceHost));
            wcfHostBehavior1.VerifyAll();
            wcfHostBehavior2.VerifyAll();
        }

        [Test]
        public void CallStartWcfHostForWcfHostBehaviors()
        {
            var wcfHostBehavior1 = new Mock<IWcfHostBehavior>();
            WcfServiceHost wcfServiceHost1 = null;
            wcfHostBehavior1.Setup(x => x.StartWcfHost(It.IsAny<WcfServiceHost>()))
                .Callback<WcfServiceHost>(host =>
                    {
                        Assert.That(((WcfServiceHostMock)host).IsOpen, Is.True);
                        wcfServiceHost1 = host;
                    });

            var wcfHostBehavior2 = new Mock<IWcfHostBehavior>();
            WcfServiceHost wcfServiceHost2 = null;
            wcfHostBehavior2.Setup(x => x.StartWcfHost(It.IsAny<WcfServiceHost>()))
                .Callback<WcfServiceHost>(host =>
                    {
                        Assert.That(((WcfServiceHostMock)host).IsOpen, Is.True);
                        wcfServiceHost2 = host;
                    });

            this.service.Initialize(new ServiceStartOptions());
            this.service.BehaviorManager.RegisterServiceBehavior(wcfHostBehavior1.Object);
            this.service.BehaviorManager.RegisterServiceBehavior(wcfHostBehavior2.Object);

            this.serviceHost.StartHost();

            Assert.That(wcfServiceHost1, Is.SameAs(this.serviceHost.WcfServiceHost));
            Assert.That(wcfServiceHost2, Is.SameAs(this.serviceHost.WcfServiceHost));
            wcfHostBehavior1.VerifyAll();
            wcfHostBehavior2.VerifyAll();
        }

        [Test]
        public void CallCloseWcfHostForWcfHostBehaviors()
        {
            var wcfHostBehavior1 = new Mock<IWcfHostBehavior>();
            var wcfHostBehavior2 = new Mock<IWcfHostBehavior>();

            this.service.Initialize(new ServiceStartOptions());
            this.service.BehaviorManager.RegisterServiceBehavior(wcfHostBehavior1.Object);
            this.service.BehaviorManager.RegisterServiceBehavior(wcfHostBehavior2.Object);

            this.serviceHost.StartHost();

            wcfHostBehavior1.Setup(x => x.CloseWcfHost(this.serviceHost.WcfServiceHost));
            wcfHostBehavior2.Setup(x => x.CloseWcfHost(this.serviceHost.WcfServiceHost));

            this.serviceHost.StopWcfHost();

            wcfHostBehavior1.VerifyAll();
            wcfHostBehavior2.VerifyAll();
        }
    }
}
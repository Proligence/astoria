﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowsServiceExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using NUnit.Framework;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Deployment;

    /* ReSharper disable InvokeAsExtensionMethod */

    [TestFixture]
    public class WindowsServiceExtensionsUnitTests
    {
        private const string ServiceName = "My Service";
        private static readonly string ExistingPath = Assembly.GetExecutingAssembly().Location;
        private static readonly string NonExistingPath = Path.Combine(@"C:\Temp", Guid.NewGuid().ToString());
        private AstoriaDeploymentProviderMock provider;
        private ServiceLoggerMock logger;

        [SetUp]
        public void Setup()
        {
            this.provider = new AstoriaDeploymentProviderMock();
            this.logger = new ServiceLoggerMock();
        }

        [TearDown]
        public void Teardown()
        {
            this.AssertInvariants();
        }

        [Test]
        public void CreateServiceWhenProviderNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => WindowsServiceExtensions.CreateWindowsService(
                    null, this.logger, ServiceName, ServiceName, ExistingPath));

            Assert.That(exception.ParamName, Is.EqualTo("provider"));
        }

        [Test]
        public void CreateServiceWhenLoggerNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => WindowsServiceExtensions.CreateWindowsService(
                    this.provider, null, ServiceName, ServiceName, ExistingPath));

            Assert.That(exception.ParamName, Is.EqualTo("logger"));
        }

        [TestCase(null)]
        [TestCase(" ")]
        [TestCase("\t")]
        [TestCase("\r\n")]
        public void CreateServiceWhenNameNullOrWhiteSpace(string serviceName)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => WindowsServiceExtensions.CreateWindowsService(
                    this.provider, this.logger, serviceName, ServiceName, ExistingPath));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        [Test]
        public void CreateServiceWithMaximumNameLength()
        {
            string name = new string('x', WindowsServiceExtensions.MaximumServiceNameLength);
            bool result = WindowsServiceExtensions.CreateWindowsService(
                this.provider, this.logger, name, ServiceName, ExistingPath);
            
            Assert.That(result, Is.True);
            this.AssertSingleService(name, ServiceName, ExistingPath);
            this.AssertSingleMessage("Created service: " + name);
        }

        [Test]
        public void CreateServiceWhenNameTooLong()
        {
            string name = new string('x', WindowsServiceExtensions.MaximumServiceNameLength + 1);
            var exception = Assert.Throws<ArgumentException>(
                () => WindowsServiceExtensions.CreateWindowsService(
                    this.provider, this.logger, name, ServiceName, ExistingPath));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
            StringAssert.Contains("The maximum name for a service is 255 characters.", exception.Message);
        }

        [TestCase('\\')]
        [TestCase('/')]
        public void CreateServiceWithInvalidCharactersInName(char invalidCharacter)
        {
            string name = "abc" + invalidCharacter + "def";
            var exception = Assert.Throws<ArgumentException>(
                () => WindowsServiceExtensions.CreateWindowsService(
                    this.provider, this.logger, name, ServiceName, ExistingPath));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
            StringAssert.Contains(new string(invalidCharacter, 1), exception.Message);
        }

        [Test]
        public void CreateServiceWithMaximumDisplayNameLength()
        {
            string name = new string('x', WindowsServiceExtensions.MaximumServiceDisplayNameLength);
            bool result = WindowsServiceExtensions.CreateWindowsService(
                this.provider, this.logger, ServiceName, name, ExistingPath);

            Assert.That(result, Is.True);
            this.AssertSingleService(ServiceName, name, ExistingPath);
            this.AssertSingleMessage("Created service: " + ServiceName);
        }

        [Test]
        public void CreateServiceWhenDisplayNameTooLong()
        {
            string name = new string('x', WindowsServiceExtensions.MaximumServiceDisplayNameLength + 1);
            var exception = Assert.Throws<ArgumentException>(
                () => WindowsServiceExtensions.CreateWindowsService(
                    this.provider, this.logger, ServiceName, name, ExistingPath));

            Assert.That(exception.ParamName, Is.EqualTo("displayName"));
            StringAssert.Contains("The maximum display name for a service is 255 characters.", exception.Message);
        }

        [Test]
        public void CreateServiceUseNameAsDisplayName()
        {
            bool result = WindowsServiceExtensions.CreateWindowsService(
                this.provider, this.logger, ServiceName, null, ExistingPath);

            Assert.That(result, Is.True);
            this.AssertSingleService(ServiceName, ServiceName, ExistingPath);
            this.AssertSingleMessage("Created service: " + ServiceName);
        }

        [TestCase(null)]
        [TestCase(" ")]
        [TestCase("\t")]
        [TestCase("\r\n")]
        public void CreateServiceWhenPathNullOrWhiteSpace(string path)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => WindowsServiceExtensions.CreateWindowsService(
                    this.provider, this.logger, ServiceName, ServiceName, path));

            Assert.That(exception.ParamName, Is.EqualTo("path"));
        }

        [Test]
        public void CreateServiceWhenExecutableFileDoesNotExist()
        {
            var exception = Assert.Throws<ArgumentException>(
                () => WindowsServiceExtensions.CreateWindowsService(
                    this.provider, this.logger, ServiceName, ServiceName, NonExistingPath));

            Assert.That(exception.ParamName, Is.EqualTo("path"));
            StringAssert.Contains("The file '" + NonExistingPath + "' does not exist.", exception.Message);
        }
        
        [Test]
        public void CreateServiceWhenServiceAlreadyExists()
        {
            this.provider.WindowsServiceManager.Services.Add(ServiceName, ServiceName + "," + ExistingPath);
            bool result = WindowsServiceExtensions.CreateWindowsService(
                this.provider, this.logger, ServiceName, ServiceName, ExistingPath);

            Assert.That(result, Is.False);
            this.AssertSingleService(ServiceName, ServiceName, ExistingPath);
            Assert.That(this.logger.Errors, Is.Empty);
            Assert.That(this.logger.Warnings, Is.Empty);
            Assert.That(this.logger.Messages, Is.Empty);
        }

        private void AssertInvariants()
        {
            WindowsServiceManagerMock manager = this.provider.WindowsServiceManager;
            Assert.That(manager.ScmHandles, Is.Empty, "Open SCM handles detected.");
            Assert.That(manager.ServiceHandles, Is.Empty, "Open service handles detected.");
        }

        private void AssertSingleService(string name, string displayName, string path)
        {
            Assert.That(this.provider.WindowsServiceManager.Services.Single().Key, Is.EqualTo(name));
            Assert.That(
                this.provider.WindowsServiceManager.Services.Single().Value,
                Is.EqualTo(displayName + "," + path));
        }

        private void AssertSingleMessage(string message)
        {
            Assert.That(this.logger.Errors, Is.Empty);
            Assert.That(this.logger.Warnings, Is.Empty);
            Assert.That(this.logger.Messages.Single(), Is.EqualTo(message));
        }
    }

    /* ReSharper restore InvokeAsExtensionMethod */
}
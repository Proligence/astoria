﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MonitoringOperationInvokerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.ServiceModel.Dispatcher;
    using System.Threading;
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Logging;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    public class MonitoringOperationInvokerUnitTests
    {
        private Mock<IPerformanceCounterFactory> counterFactoryMock;
        private Mock<IPerformanceCounter> totalCallsCounterMock;
        private Mock<IPerformanceCounter> failedCallsCounterMock;
        private Mock<IPerformanceCounter> callDurationCounterMock;

        [SetUp]
        public void Setup()
        {
            this.totalCallsCounterMock = new Mock<IPerformanceCounter>();
            this.failedCallsCounterMock = new Mock<IPerformanceCounter>();
            this.callDurationCounterMock = new Mock<IPerformanceCounter>();

            this.counterFactoryMock = new Mock<IPerformanceCounterFactory>();

            this.counterFactoryMock.Setup(x => x.CreatePerformanceCounter(It.IsAny<IPerformanceCounterParameters>()))
                .Returns<IPerformanceCounterParameters>(counterParameters =>
                {
                    Mock<IPerformanceCounter> counterMock;
                    if (counterParameters.CounterName == ServicePerformanceCounters.TotalCallCounterName)
                    {
                        counterMock = this.totalCallsCounterMock;
                    }
                    else if (counterParameters.CounterName == ServicePerformanceCounters.FailedCallCounterName)
                    {
                        counterMock = this.failedCallsCounterMock;
                    }
                    else if (counterParameters.CounterName == ServicePerformanceCounters.CallDurationCounterName)
                    {
                        counterMock = this.callDurationCounterMock;
                    }
                    else
                    {
                        return null;
                    }
                    
                    counterMock.Setup(c => c.CounterName).Returns(counterParameters.CounterName);
                    counterMock.Setup(c => c.CategoryName).Returns(counterParameters.CategoryName);
                    counterMock.Setup(c => c.InstanceName).Returns(counterParameters.InstanceName);

                    return counterMock.Object;
                });
        }

        [TestCase(null)]
        [TestCase("")]
        public void CreateMonitoringOperationInvokerWhenServiceNameNullOrEmpty(string serviceName)
        {
            IOperationInvoker baseInvoker = new Mock<IOperationInvoker>().Object;
            IPerformanceCounterFactory counterFactory = this.counterFactoryMock.Object;
            ServiceLogger logger = new NullLogger();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new MonitoringOperationInvoker(serviceName, "MyMethod", baseInvoker, counterFactory, logger));

            Assert.That(exception.ParamName, Is.EqualTo("serviceName"));
        }

        [TestCase(null)]
        [TestCase("")]
        public void CreateMonitoringOperationInvokerWhenMethodNameNullOrEmpty(string methodName)
        {
            IOperationInvoker baseInvoker = new Mock<IOperationInvoker>().Object;
            IPerformanceCounterFactory counterFactory = this.counterFactoryMock.Object;
            ServiceLogger logger = new NullLogger();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new MonitoringOperationInvoker("MyService", methodName, baseInvoker, counterFactory, logger));

            Assert.That(exception.ParamName, Is.EqualTo("methodName"));
        }

        [Test]
        public void CreateMonitoringOperationInvokerWhenBaseInvokerNull()
        {
            IPerformanceCounterFactory counterFactory = this.counterFactoryMock.Object;
            ServiceLogger logger = new NullLogger();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new MonitoringOperationInvoker("MyService", "MyMethod", null, counterFactory, logger));

            Assert.That(exception.ParamName, Is.EqualTo("baseInvoker"));
        }

        [Test]
        public void CreateMonitoringOperationInvokerWhenCounterFactoryNull()
        {
            IOperationInvoker invoker = new Mock<IOperationInvoker>().Object;
            ServiceLogger logger = new NullLogger();
            
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new MonitoringOperationInvoker("MyService", "MyMethod", invoker, null, logger));

            Assert.That(exception.ParamName, Is.EqualTo("counterFactory"));
        }

        [Test]
        public void CreateMonitoringOperationInvokerWhenLoggerNull()
        {
            IOperationInvoker invoker = new Mock<IOperationInvoker>().Object;
            IPerformanceCounterFactory counterFactory = this.counterFactoryMock.Object;

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new MonitoringOperationInvoker("MyService", "MyMethod", invoker, counterFactory, null));

            Assert.That(exception.ParamName, Is.EqualTo("logger"));
        }

        [Test]
        public void CreateMonitoringOperationInvokerWhenValidArgs()
        {
            IOperationInvoker baseInvoker = new Mock<IOperationInvoker>().Object;
            IPerformanceCounterFactory counterFactory = this.counterFactoryMock.Object;
            ServiceLogger logger = new NullLogger();

            var invoker = new MonitoringOperationInvoker("MyService", "MyMethod", baseInvoker, counterFactory, logger);

            Assert.That(invoker.ServiceName, Is.EqualTo("MyService"));
            Assert.That(invoker.MethodName, Is.EqualTo("MyMethod"));
            Assert.That(invoker.BaseInvoker, Is.SameAs(baseInvoker));
        }

        [Test]
        public void CreateMonitoringOperationInvokerCheckTotalCallCounter()
        {
            IOperationInvoker baseInvoker = new Mock<IOperationInvoker>().Object;
            IPerformanceCounterFactory counterFactory = this.counterFactoryMock.Object;
            ServiceLogger logger = new NullLogger();

            this.counterFactoryMock.Setup(x => x.CreatePerformanceCounter(It.IsAny<IPerformanceCounterParameters>()))
                .Returns<IPerformanceCounterParameters>(counterParameters =>
                {
                    var counter = new Mock<IPerformanceCounter>();
                    counter.Setup(c => c.CounterName).Returns(counterParameters.CounterName);
                    counter.Setup(c => c.CategoryName).Returns(counterParameters.CategoryName);
                    counter.Setup(c => c.InstanceName).Returns(counterParameters.InstanceName);

                    return counter.Object;
                });

            var invoker = new MonitoringOperationInvoker("MyService", "MyMethod", baseInvoker, counterFactory, logger);

            IPerformanceCounter totalCallCounter = invoker.TotalCallCounter;
            Assert.That(totalCallCounter.CounterName, Is.EqualTo(ServicePerformanceCounters.TotalCallCounterName));
            Assert.That(totalCallCounter.CategoryName, Is.EqualTo("MyService"));
            Assert.That(totalCallCounter.InstanceName, Is.EqualTo("MyMethod"));

            this.counterFactoryMock.VerifyAll();
        }

        [Test]
        public void CreateMonitoringOperationInvokerCheckFailedCallCounter()
        {
            IOperationInvoker baseInvoker = new Mock<IOperationInvoker>().Object;
            IPerformanceCounterFactory counterFactory = this.counterFactoryMock.Object;
            ServiceLogger logger = new NullLogger();

            this.counterFactoryMock.Setup(x => x.CreatePerformanceCounter(It.IsAny<IPerformanceCounterParameters>()))
                .Returns<IPerformanceCounterParameters>(counterParameters =>
                {
                    var counter = new Mock<IPerformanceCounter>();
                    counter.Setup(c => c.CounterName).Returns(counterParameters.CounterName);
                    counter.Setup(c => c.CategoryName).Returns(counterParameters.CategoryName);
                    counter.Setup(c => c.InstanceName).Returns(counterParameters.InstanceName);

                    return counter.Object;
                });

            var invoker = new MonitoringOperationInvoker("MyService", "MyMethod", baseInvoker, counterFactory, logger);

            IPerformanceCounter failedCallCounter = invoker.FailedCallCounter;
            Assert.That(failedCallCounter.CounterName, Is.EqualTo(ServicePerformanceCounters.FailedCallCounterName));
            Assert.That(failedCallCounter.CategoryName, Is.EqualTo("MyService"));
            Assert.That(failedCallCounter.InstanceName, Is.EqualTo("MyMethod"));

            this.counterFactoryMock.VerifyAll();
        }

        [Test]
        public void CreateMonitoringOperationInvokerCheckCallDurationCounter()
        {
            IOperationInvoker baseInvoker = new Mock<IOperationInvoker>().Object;
            IPerformanceCounterFactory counterFactory = this.counterFactoryMock.Object;
            ServiceLogger logger = new NullLogger();

            this.counterFactoryMock.Setup(x => x.CreatePerformanceCounter(It.IsAny<IPerformanceCounterParameters>()))
                .Returns<IPerformanceCounterParameters>(counterParameters =>
                {
                    var counter = new Mock<IPerformanceCounter>();
                    counter.Setup(c => c.CounterName).Returns(counterParameters.CounterName);
                    counter.Setup(c => c.CategoryName).Returns(counterParameters.CategoryName);
                    counter.Setup(c => c.InstanceName).Returns(counterParameters.InstanceName);

                    return counter.Object;
                });

            var invoker = new MonitoringOperationInvoker("MyService", "MyMethod", baseInvoker, counterFactory, logger);

            IPerformanceCounter durationCounter = invoker.CallDurationCounter;
            Assert.That(durationCounter.CounterName, Is.EqualTo(ServicePerformanceCounters.CallDurationCounterName));
            Assert.That(durationCounter.CategoryName, Is.EqualTo("MyService"));
            Assert.That(durationCounter.InstanceName, Is.EqualTo("MyMethod"));

            this.counterFactoryMock.VerifyAll();
        }

        [Test]
        public void TestIsSynchronous()
        {
            Mock<IOperationInvoker> baseInvokerMock = new Mock<IOperationInvoker>();
            baseInvokerMock.Setup(x => x.IsSynchronous).Returns(true);

            ServiceLogger logger = new NullLogger();

            var invoker = new MonitoringOperationInvoker(
                "MyService",
                "MyMethod",
                baseInvokerMock.Object,
                new Mock<IPerformanceCounterFactory>().Object,
                logger);

            bool result = invoker.IsSynchronous;

            Assert.That(result, Is.True);
            baseInvokerMock.VerifyAll();
        }

        [Test]
        public void TestAllocateInputs()
        {
            Mock<IOperationInvoker> baseInvokerMock = new Mock<IOperationInvoker>();
            baseInvokerMock.Setup(x => x.AllocateInputs());

            ServiceLogger logger = new NullLogger();

            var invoker = new MonitoringOperationInvoker(
                "MyService",
                "MyMethod",
                baseInvokerMock.Object,
                new Mock<IPerformanceCounterFactory>().Object,
                logger);

            invoker.AllocateInputs();

            baseInvokerMock.VerifyAll();
        }

        [Test]
        public void InvokeWhenCallSucceeded()
        {
            object instance = new object();
            object[] inputs = new[] { new object() };
            object[] outputs = new[] { new object() };
            object result = new object();

            Mock<IOperationInvoker> baseInvokerMock = new Mock<IOperationInvoker>();
            baseInvokerMock.Setup(x => x.Invoke(instance, inputs, out outputs)).Returns(result);

            ServiceLogger logger = new NullLogger();

            this.totalCallsCounterMock.Setup(x => x.Increment(1)).Verifiable();
            this.failedCallsCounterMock.Setup(x => x.Increment(It.IsAny<long>())).Verifiable();

            var invoker = new MonitoringOperationInvoker(
                "MyService",
                "MyMethod",
                baseInvokerMock.Object,
                this.counterFactoryMock.Object,
                logger);

            object[] actualOutputs;
            object actualResult = invoker.Invoke(instance, inputs, out actualOutputs);

            Assert.That(actualResult, Is.SameAs(result));
            Assert.That(actualOutputs, Is.SameAs(outputs));
            
            baseInvokerMock.VerifyAll();
            this.failedCallsCounterMock.Verify(x => x.Increment(It.IsAny<long>()), Times.Never());
            this.totalCallsCounterMock.Verify(x => x.Increment(1), Times.Once());
        }

        [Test]
        public void InvokeWhenCallFailed()
        {
            object instance = new object();
            object[] inputs = new[] { new object() };
            object[] outputs;
            var exception = new InvalidOperationException("Test exception message.");

            Mock<IOperationInvoker> baseInvokerMock = new Mock<IOperationInvoker>();
            baseInvokerMock.Setup(x => x.Invoke(instance, inputs, out outputs)).Throws(exception);

            ServiceLogger logger = new NullLogger();

            this.totalCallsCounterMock.Setup(x => x.Increment(1)).Verifiable();
            this.failedCallsCounterMock.Setup(x => x.Increment(1)).Verifiable();

            var invoker = new MonitoringOperationInvoker(
                "MyService",
                "MyMethod",
                baseInvokerMock.Object,
                this.counterFactoryMock.Object,
                logger);

            object[] actualOutputs;
            InvalidOperationException actualException = Assert.Throws<InvalidOperationException>(
                () => invoker.Invoke(instance, inputs, out actualOutputs));

            Assert.That(actualException, Is.EqualTo(exception));
            
            baseInvokerMock.VerifyAll();
            this.failedCallsCounterMock.Verify(x => x.Increment(1), Times.Once());
            this.totalCallsCounterMock.Verify(x => x.Increment(1), Times.Once());
        }

        [Test]
        public void InvokeWhenCallSucceededEnsureCallDurationCounterUpdated()
        {
            object instance = new object();
            object[] inputs = new[] { new object() };
            object[] outputs = new[] { new object() };
            object result = new object();

            Mock<IOperationInvoker> baseInvokerMock = new Mock<IOperationInvoker>();
            baseInvokerMock.Setup(x => x.Invoke(instance, inputs, out outputs)).Returns(result);

            ServiceLogger logger = new NullLogger();

            this.callDurationCounterMock
                .SetupSet(x => x.RawValue = It.IsInRange(10L, 100L, Range.Inclusive))
                .Verifiable();
            
            var invoker = new MonitoringOperationInvoker(
                "MyService",
                "MyMethod",
                baseInvokerMock.Object,
                this.counterFactoryMock.Object,
                logger);

            object[] actualOutputs;
            object actualResult = invoker.Invoke(instance, inputs, out actualOutputs);

            Assert.That(actualResult, Is.SameAs(result));
            Assert.That(actualOutputs, Is.SameAs(outputs));

            baseInvokerMock.VerifyAll();
            this.callDurationCounterMock.VerifySet(x => x.RawValue = It.IsAny<long>(), Times.Once());
        }

        [Test]
        public void InvokeWhenCallFailedEnsureCallDurationCounterUpdated()
        {
            object instance = new object();
            object[] inputs = new[] { new object() };
            object[] outputs;
            var exception = new InvalidOperationException("Test exception message.");
            ServiceLogger logger = new NullLogger();

            Mock<IOperationInvoker> baseInvokerMock = new Mock<IOperationInvoker>();
            baseInvokerMock.Setup(x => x.Invoke(instance, inputs, out outputs))
                .Callback(() => Thread.Sleep(10))
                .Throws(exception);

            this.callDurationCounterMock
                .SetupSet(x => x.RawValue = It.IsInRange(10L, 100L, Range.Inclusive))
                .Verifiable();

            var invoker = new MonitoringOperationInvoker(
                "MyService",
                "MyMethod",
                baseInvokerMock.Object,
                this.counterFactoryMock.Object,
                logger);

            object[] actualOutputs;
            InvalidOperationException actualException = Assert.Throws<InvalidOperationException>(
                () => invoker.Invoke(instance, inputs, out actualOutputs));

            Assert.That(actualException, Is.EqualTo(exception));

            baseInvokerMock.VerifyAll();
            this.callDurationCounterMock.VerifySet(x => x.RawValue = It.IsAny<long>(), Times.Once());
        }

        [Test]
        public void TestInvokeBegin()
        {
            object instance = new object();
            object[] inputs = new[] { new object() };
            AsyncCallback callback = ar => { };
            object state = new object();
            IAsyncResult asyncResult = new Mock<IAsyncResult>().Object;
            ServiceLogger logger = new NullLogger();

            Mock<IOperationInvoker> baseInvokerMock = new Mock<IOperationInvoker>();
            baseInvokerMock.Setup(x => x.InvokeBegin(instance, inputs, callback, state)).Returns(asyncResult);

            var invoker = new MonitoringOperationInvoker(
                "MyService",
                "MyMethod",
                baseInvokerMock.Object,
                new Mock<IPerformanceCounterFactory>().Object,
                logger);

            IAsyncResult result = invoker.InvokeBegin(instance, inputs, callback, state);

            Assert.That(result, Is.SameAs(asyncResult));
            baseInvokerMock.VerifyAll();
        }

        [Test]
        public void TestInvokeEnd()
        {
            object instance = new object();
            object[] outputs = new[] { new object() };
            IAsyncResult asyncResult = new Mock<IAsyncResult>().Object;
            object result = new object();
            ServiceLogger logger = new NullLogger();

            Mock<IOperationInvoker> baseInvokerMock = new Mock<IOperationInvoker>();
            baseInvokerMock.Setup(x => x.InvokeEnd(instance, out outputs, asyncResult)).Returns(result);

            var invoker = new MonitoringOperationInvoker(
                "MyService",
                "MyMethod",
                baseInvokerMock.Object,
                new Mock<IPerformanceCounterFactory>().Object,
                logger);

            object[] actualOutputs;
            object actualResult = invoker.InvokeEnd(instance, out actualOutputs, asyncResult);

            Assert.That(actualOutputs, Is.SameAs(outputs));
            Assert.That(actualResult, Is.SameAs(result));
            baseInvokerMock.VerifyAll();
        }

        [Test]
        public void TestIfCallMadeIfFailedToUpdatePerformanceCounter()
        {
            object instance = new object();
            object[] inputs = new[] { new object() };
            object[] outputs = new[] { new object() };
            object result = new object();
            Mock<ServiceLogger> logger = new Mock<ServiceLogger>();

            Mock<IOperationInvoker> baseInvokerMock = new Mock<IOperationInvoker>();
            baseInvokerMock.Setup(x => x.Invoke(instance, inputs, out outputs)).Returns(result);

            var exception = new InvalidOperationException("Failed to update performance counter - test.");
            this.callDurationCounterMock.SetupSet(x => x.RawValue = It.IsAny<long>())
                .Throws(exception)
                .Verifiable();

            logger.Setup(x => x.LogWarning("Failed to update performance counter. " + exception.Message));

            var invoker = new MonitoringOperationInvoker(
                "MyService",
                "MyMethod",
                baseInvokerMock.Object,
                this.counterFactoryMock.Object,
                logger.Object);

            object[] actualOutputs;
            object actualResult = invoker.Invoke(instance, inputs, out actualOutputs);

            Assert.That(actualResult, Is.SameAs(result));
            Assert.That(actualOutputs, Is.SameAs(outputs));

            baseInvokerMock.VerifyAll();
            this.callDurationCounterMock.VerifySet(x => x.RawValue = It.IsAny<long>());
            logger.VerifyAll();
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionBehaviorReaderUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Linq;
    using NUnit.Framework;

    [TestFixture]
    public class ReflectionBehaviorReaderUnitTests
    {
        [AstoriaServiceBehavior(AllowMultiple = true)]
        private interface ITestBehavior1 : IAstoriaServiceBehavior
        {
            string Value { get; }
        }

        [AstoriaServiceBehavior(AllowMultiple = false)]
        private interface ITestBehavior2 : IAstoriaServiceBehavior
        {
            string Value { get; }
        }

        [AstoriaServiceBehavior]
        private interface ITestBehavior3 : IAstoriaServiceBehavior
        {
            string Value { get; }
        }

        [Test]
        public void ReadServiceBehaviorsFromTypeWhenTypeNull()
        {
            var reader = new ReflectionBehaviorReader();
            
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => reader.ReadServiceBehaviorsFromType(null));

            Assert.That(exception.ParamName, Is.EqualTo("type"));
        }

        [Test]
        public void ReadServiceBehaviorsFromTypeWhenTypeDoesNotHaveAnyBehaviorAttributes()
        {
            var reader = new ReflectionBehaviorReader();

            reader.ReadServiceBehaviorsFromType(typeof(TestClass1));

            Assert.That(reader.ServiceBehaviors, Is.Empty);
        }

        [Test]
        public void ReadServiceBehaviorsFromTypeWhenTypeDoesNotHaveBaseClass()
        {
            var reader = new ReflectionBehaviorReader();

            reader.ReadServiceBehaviorsFromType(typeof(TestClass2));

            IAstoriaServiceBehavior[] behaviors = reader.ServiceBehaviors.ToArray();
            Assert.That(behaviors.Length, Is.EqualTo(2));

            var behavior1 = (TestBehavior1)behaviors.Single(b => b is TestBehavior1);
            Assert.That(behavior1.Value, Is.EqualTo("1"));

            var behavior2 = (TestBehavior2)behaviors.Single(b => b is TestBehavior2);
            Assert.That(behavior2.Value, Is.EqualTo("2"));
        }

        [Test]
        public void ReadServiceBehaviorsFromTypeWhenBaseClassDoesNotHaveBehaviorAttributes()
        {
            var reader = new ReflectionBehaviorReader();

            reader.ReadServiceBehaviorsFromType(typeof(TestClass3));

            IAstoriaServiceBehavior[] behaviors = reader.ServiceBehaviors.ToArray();
            Assert.That(behaviors.Length, Is.EqualTo(3));

            var behavior1 = (TestBehavior1)behaviors.Single(x => x is TestBehavior1);
            Assert.That(behavior1.Value, Is.EqualTo("3"));

            var behavior2 = (TestBehavior2)behaviors.Single(x => x is TestBehavior2);
            Assert.That(behavior2.Value, Is.EqualTo("4"));

            var behavior3 = (TestBehavior3)behaviors.Single(x => x is TestBehavior3);
            Assert.That(behavior3.Value, Is.EqualTo("5"));

            // Make sure that base class behaviors are registered before the behaviors of the current class.
            Assert.That(Array.IndexOf(behaviors, behavior3), Is.LessThan(Array.IndexOf(behaviors, behavior1)));
            Assert.That(Array.IndexOf(behaviors, behavior3), Is.LessThan(Array.IndexOf(behaviors, behavior2)));
        }

        [AttributeUsage(AttributeTargets.Class)]
        private sealed class TestBehavior1 : Attribute, ITestBehavior1
        {
            public string Value { get; set; }
        }

        [AttributeUsage(AttributeTargets.Class)]
        private sealed class TestBehavior2 : Attribute, ITestBehavior2
        {
            public string Value { get; set; }
        }

        [AttributeUsage(AttributeTargets.Class)]
        private sealed class TestBehavior3 : Attribute, ITestBehavior3
        {
            public string Value { get; set; }
        }

        private class TestClass1
        {
        }

        [TestBehavior1(Value = "1")]
        [TestBehavior2(Value = "2")]
        private class TestClass2
        {
        }

        [TestBehavior1(Value = "3")]
        [TestBehavior2(Value = "4")]
        private class TestClass3 : TestClassBase
        {
        }

        [TestBehavior3(Value = "5")]
        private class TestClassBase
        {
        }
    }
}
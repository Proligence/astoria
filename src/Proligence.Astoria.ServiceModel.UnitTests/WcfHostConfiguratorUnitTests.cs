﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WcfHostConfiguratorUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using Autofac;
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Logging;
    using Proligence.Astoria.ServiceModel.Monitoring;
    using Proligence.Astoria.ServiceModel.Wcf;

    [TestFixture]
    public class WcfHostConfiguratorUnitTests
    {
        private AstoriaServiceMock service;
        private AstoriaServiceHostMock host;
        private WcfHostConfigurator configurator;

        [SetUp]
        public void Setup()
        {
            this.service = new AstoriaServiceMock();
            
            this.host = new AstoriaServiceHostMock(this.service) { WcfServiceHost = new WcfServiceHostMock() };
            this.host.WcfServiceHost.Behaviors.Add(new ServiceDebugBehavior());
            this.host.WcfServiceHost.Behaviors.Add(new ServiceBehaviorAttribute());

            this.configurator = new WcfHostConfigurator();
        }

        [Test]
        public void SetupWcfBehaviorsWhenHostNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.configurator.SetupWcfBehaviors<IDummy, AstoriaServiceConfiguration>(null));

            Assert.That(exception.ParamName, Is.EqualTo("host"));
        }

        [Test]
        public void SetupWcfBehaviorsIncludeExceptionDetailInFaultsTrue()
        {
            this.configurator.SetupWcfBehaviors(this.host);

            ServiceDebugBehavior debugBehavior = this.host.WcfServiceHost.Behaviors.Find<ServiceDebugBehavior>();
            Assert.IsTrue(debugBehavior.IncludeExceptionDetailInFaults);
        }

        [Test]
        public void SetupWcfBehaviorsInstanceContextModeSingle()
        {
            this.configurator.SetupWcfBehaviors(this.host);

            ServiceBehaviorAttribute serviceBehavior = 
                this.host.WcfServiceHost.Behaviors.Find<ServiceBehaviorAttribute>();

            Assert.That(serviceBehavior.InstanceContextMode, Is.EqualTo(InstanceContextMode.Single));
        }

        [Test]
        public void SetupWcfBehaviorsInstanceConcurrencyModeMultipleWhenMultithreadingEnabled()
        {
            this.host.UseMultithreading = true;
            
            this.configurator.SetupWcfBehaviors(this.host);

            ServiceBehaviorAttribute serviceBehavior = 
                this.host.WcfServiceHost.Behaviors.Find<ServiceBehaviorAttribute>();

            Assert.That(serviceBehavior.ConcurrencyMode, Is.EqualTo(ConcurrencyMode.Multiple));
        }

        [Test]
        public void SetupWcfBehaviorsWhenMonitoringEnabledAndAstoriaMonitoringServiceBehaviorNotInstalled()
        {
            this.host.MonitoringEnabled = true;

            this.configurator.SetupWcfBehaviors(this.host);

            Assert.That(this.host.WcfServiceHost.Behaviors.Find<AstoriaMonitoringServiceBehavior>(), Is.Not.Null);
        }

        [Test]
        public void SetupWcfBehaviorsWhenMonitoringDisabledAndAstoriaMonitoringServiceBehaviorNotInstalled()
        {
            this.host.MonitoringEnabled = false;

            this.configurator.SetupWcfBehaviors(this.host);

            Assert.That(this.host.WcfServiceHost.Behaviors.Find<AstoriaMonitoringServiceBehavior>(), Is.Null);
        }

        [Test]
        public void SetupWcfBehaviorsWhenMonitoringEnabledAndAstoriaMonitoringServiceBehaviorInstalled()
        {
            this.service.ServiceHost.WcfServiceHost = new WcfServiceHostMock();
            this.service.ServiceHost.WcfServiceHost.Behaviors.Add(
                new AstoriaMonitoringServiceBehavior(
                    "MyService",
                    new Mock<IContainer>().Object,
                    new NullLogger()));

            this.host.MonitoringEnabled = true;

            this.configurator.SetupWcfBehaviors(this.host);

            var behavior = this.host.WcfServiceHost.Behaviors.Find<AstoriaMonitoringServiceBehavior>();
            Assert.That(behavior, Is.Not.Null);
            Assert.That(behavior.ServiceName, Is.EqualTo(this.service.ServiceName));
        }

        [Test]
        public void ConfigureAstoriaBindingWhenHostNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.configurator.ConfigureAstoriaBinding<IDummy, AstoriaServiceConfiguration>(null));

            Assert.That(exception.ParamName, Is.EqualTo("host"));
        }

        [Test]
        public void ConfigureAstoriaBindingWhenHostNotNull()
        {
            this.configurator.ConfigureAstoriaBinding(this.host);

            ServiceEndpoint endpoint = ((WcfServiceHostMock)this.host.WcfServiceHost).AddedEndpoints.Single();
            Assert.That(endpoint.Address.ToString(), Is.StringEnding("/ast"));
            Assert.That(endpoint.Binding, Is.InstanceOf<AstoriaBinding>());
        }
    }
}
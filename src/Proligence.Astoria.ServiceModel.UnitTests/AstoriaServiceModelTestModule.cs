﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceModelTestModule.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using Autofac;
    using Moq;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Monitoring;
    using Proligence.Astoria.ServiceModel.Wcf;

    public class AstoriaServiceModelTestModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var mockChannelFactory = new AstoriaChannelFactoryMock();
            builder.RegisterInstance(mockChannelFactory).As<IAstoriaChannelFactory>();

            builder.RegisterType(typeof(ServicePerformanceCounters));
            
            builder.RegisterInstance(new Mock<IPerformanceCounterDiscoverer>().Object)
                .As<IPerformanceCounterDiscoverer>();

            builder.RegisterInstance(new Mock<IPerformanceCounterInstaller>().Object)
                .As<IPerformanceCounterInstaller>();

            builder.RegisterInstance(new Mock<IWcfHostConfigurator>().Object)
                .As<IWcfHostConfigurator>();

            builder.RegisterType<BehaviorManager>().As<IBehaviorManager>();
            builder.RegisterInstance(new Mock<IWatchDogManager>().Object).As<IWatchDogManager>();
        }
    }
}
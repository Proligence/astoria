﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WatchDogManagerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Autofac;
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    public class WatchDogManagerUnitTests
    {
        private IContainer container;
        private Mock<IBehaviorManager> behaviorManager;
        private WatchDogsBehavior watchDogsBehavior;

        [SetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<TestWatchDog>();
            this.container = builder.Build();

            this.behaviorManager = new Mock<IBehaviorManager>();
            this.watchDogsBehavior = new WatchDogsBehavior { WatchDogsEnabled = true };
            
            this.behaviorManager.Setup(x => x.GetRequiredServiceBehavior<IWatchDogsBehavior>())
                .Returns(this.watchDogsBehavior);
        }

        [Test]
        public void InitializeWhenNotInitialized()
        {
            var manager = new TestWatchDogManager(this.container, this.behaviorManager.Object);

            manager.Initialize();

            Assert.That(manager.Descriptors, Is.Not.Null);
            Assert.That(manager.Factories, Is.Not.Null);
        }

        [Test]
        public void GetWatchDogsWhenNotInitialized()
        {
            var manager = new TestWatchDogManager(this.container, this.behaviorManager.Object);

            InvalidOperationException expcetion = Assert.Throws<InvalidOperationException>(
                () => manager.GetWatchDogs());

            Assert.That(expcetion.Message, Is.EqualTo("The watch dog manager is not initialized."));
        }

        [Test]
        public void GetWatchDogsWhenInitialized()
        {
            var manager = new TestWatchDogManager(this.container, this.behaviorManager.Object);
            manager.Initialize();

            IEnumerable<WatchDogInfo> result = manager.GetWatchDogs();

            WatchDogInfo watchDogInfo = result.Single(x => x.Name == typeof(TestWatchDog).FullName);
            Assert.That(watchDogInfo.Description, Is.EqualTo("Test watch dog."));
        }

        [Test]
        public void GetWatchDogsWhenWatchDogsEnabled()
        {
            this.watchDogsBehavior.WatchDogsEnabled = true;
            
            var manager = new TestWatchDogManager(this.container, this.behaviorManager.Object);
            manager.Initialize();

            WatchDogInfo[] result = manager.GetWatchDogs().ToArray();

            Assert.That(
                result.Count(wd => wd.Name == typeof(AstoriaServiceUnitTests.TestInternalWatchDog).FullName),
                Is.EqualTo(1));

            Assert.That(
                result.Count(wd => wd.Name == typeof(AstoriaServiceUnitTests.TestExternalWatchDog).FullName),
                Is.EqualTo(1));

            this.behaviorManager.VerifyAll();
        }

        [Test]
        public void GetWatchDogsWhenWatchDogsDisabled()
        {
            this.watchDogsBehavior.WatchDogsEnabled = false;

            var manager = new TestWatchDogManager(this.container, this.behaviorManager.Object);
            manager.Initialize();

            IEnumerable<WatchDogInfo> result = manager.GetWatchDogs();

            Assert.That(result, Is.Empty);
            this.behaviorManager.VerifyAll();
        }

        [Test]
        public void GetWatchDogsWhenWatchDogsDisabledByName()
        {
            this.watchDogsBehavior.WatchDogsEnabled = true;
            this.watchDogsBehavior.DisabledWatchDogs = 
                typeof(AstoriaServiceUnitTests.TestInternalWatchDog).FullName + ", " +
                typeof(AstoriaServiceUnitTests.TestExternalWatchDog).FullName;

            var manager = new TestWatchDogManager(this.container, this.behaviorManager.Object);
            manager.Initialize();

            WatchDogInfo[] result = manager.GetWatchDogs().ToArray();

            Assert.That(result.All(wd => wd.Name != typeof(AstoriaServiceUnitTests.TestInternalWatchDog).FullName));
            Assert.That(result.All(wd => wd.Name != typeof(AstoriaServiceUnitTests.TestExternalWatchDog).FullName));
        }

        [Test]
        public void CreateWatchDogWhenNotInitialized()
        {
            var manager = new TestWatchDogManager(this.container, this.behaviorManager.Object);

            InvalidOperationException expcetion = Assert.Throws<InvalidOperationException>(
                () => manager.CreateWatchDog(new WatchDogInfo("Test", "Test")));

            Assert.That(expcetion.Message, Is.EqualTo("The watch dog manager is not initialized."));
        }

        [Test]
        public void CreateWatchDogWhenWatchDogInfoNull()
        {
            var manager = new TestWatchDogManager(this.container, this.behaviorManager.Object);
            manager.Initialize();

            ArgumentNullException expcetion = Assert.Throws<ArgumentNullException>(
                () => manager.CreateWatchDog(null));

            Assert.That(expcetion.ParamName, Is.EqualTo("watchDogInfo"));
        }

        [Test]
        public void CreateWatchDogWhenInvalidWatchDogInfoSpecified()
        {
            var manager = new TestWatchDogManager(this.container, this.behaviorManager.Object);
            manager.Initialize();

            var watchDogInfo = new WatchDogInfo("Test", "Test descriptor");

            ArgumentException expcetion = Assert.Throws<ArgumentException>(
                () => manager.CreateWatchDog(watchDogInfo));

            Assert.That(expcetion.ParamName, Is.EqualTo("watchDogInfo"));
            Assert.That(
                expcetion.Message,
                Is.StringContaining("The specified watch dog descriptor does not represent a registered watch dog."));
        }

        [Test]
        public void CreateWatchDogWhenCommonCase()
        {
            var manager = new TestWatchDogManager(this.container, this.behaviorManager.Object);
            manager.Initialize();

            WatchDogInfo watchDogInfo = manager.Descriptors
                .Single(d => d.Value.Name == typeof(TestWatchDog).FullName)
                .Value;

            IServiceWatchDog watchDog = manager.CreateWatchDog(watchDogInfo);
            watchDog.Initialize();

            TestWatchDog.Inspected = false;
            watchDog.Inspect();

            Assert.That(TestWatchDog.Inspected, Is.True);
        }

        [ServiceWatchDog(Description = "Test watch dog.")]
        public class TestWatchDog : IServiceWatchDog
        {
            public static bool Inspected { get; set; }

            public void Dispose()
            {
            }

            public void Initialize()
            {
            }

            public WatchDogResult Inspect()
            {
                Inspected = true;
                return new WatchDogResult();
            }
        }

        internal class TestWatchDogManager : WatchDogManager
        {
            public TestWatchDogManager(IComponentContext container, IBehaviorManager behaviorManager)
                : base(container, behaviorManager)
            {
            }

            public new IDictionary<Guid, WatchDogInfo> Descriptors
            {
                get { return base.Descriptors; }
            }

            public new Dictionary<Guid, Func<IServiceWatchDog>> Factories
            {
                get { return base.Factories; }
            }
        }
    }
}
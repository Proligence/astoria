﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceModelModuleUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using Autofac;
    using NUnit.Framework;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.ServiceModel.Deployment;
    using Proligence.Astoria.ServiceModel.Monitoring;
    using Proligence.Astoria.ServiceModel.Wcf;
    using Proligence.Astoria.ServiceModel.Windows;

    [TestFixture]
    public class AstoriaServiceModelModuleUnitTests
    {
        private IContainer container;

        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new AstoriaServiceModelModule());
            
            this.container = builder.Build();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            if (this.container != null)
            {
                this.container.Dispose();
            }
        }

        [Test]
        public void ResolveEnvironment()
        {
            Assert.That(this.container.Resolve<IEnvironment>(), Is.InstanceOf<Environment>());
        }

        [Test]
        public void ResolveEventLogManager()
        {
            Assert.That(this.container.Resolve<IEventLogManager>(), Is.InstanceOf<EventLogManager>());
        }

        [Test]
        public void ResolveDeploymentPerformanceCounterManager()
        {
            Assert.That(
                this.container.Resolve<IPerformanceCounterManager>(), 
                Is.InstanceOf<PerformanceCounterManager>());
        }

        [Test]
        public void ResolveWindowsServiceManager()
        {
            Assert.That(this.container.Resolve<IWindowsServiceManager>(), Is.InstanceOf<WindowsServiceManager>());
        }

        [Test]
        public void ResolveMonitoringPerformanceCounterFactory()
        {
            Assert.That(
                this.container.Resolve<IPerformanceCounterFactory>(),
                Is.InstanceOf<WinNtPerformanceCounterFactory>());
        }

        [Test]
        public void ResolveMonitoringPerformanceCounterInstaller()
        {
            Assert.That(
                this.container.Resolve<IPerformanceCounterInstaller>(),
                Is.InstanceOf<WinNtPerformanceCounterInstaller>());
        }

        [Test]
        public void ResolveMonitoringPerformanceCounterDiscoverer()
        {
            Assert.That(
                this.container.Resolve<IPerformanceCounterDiscoverer>(),
                Is.InstanceOf<ReflectionPerformanceCounterDiscoverer>());
        }

        [Test]
        public void ResolveMonitoringServicePerformanceCounters()
        {
            Assert.That(
                this.container.Resolve<ServicePerformanceCounters>(),
                Is.InstanceOf<ServicePerformanceCounters>());
        }

        [Test]
        public void ResolveWcfHostConfigurator()
        {
            Assert.That(this.container.Resolve<IWcfHostConfigurator>(), Is.InstanceOf<WcfHostConfigurator>());
        }

        [Test]
        public void ResolveBehaviorManager()
        {
            Assert.That(
                this.container.Resolve<IBehaviorManager>(),
                Is.InstanceOf<BehaviorManager>());
        }

        [Test]
        public void ResolveWatchDogManager()
        {
            Assert.That(
                this.container.Resolve<IWatchDogManager>(),
                Is.InstanceOf<WatchDogManager>());
        }

        [Test]
        public void ResolveMemoryInfo()
        {
            Assert.That(
                this.container.Resolve<IMemoryInfo>(),
                Is.InstanceOf<MemoryInfo>());
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventLogLoggerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using NLog.Targets;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Logging;

    [TestFixture]
    public class EventLogLoggerUnitTests
    {
        private const string LogName = "My Log";
        private const string LogSource = "My Source";
        private const string MachineName = "My Machine";
        private EventLogLogger logger;

        [TestFixtureSetUp]
        public void Setup()
        {
            this.logger = new EventLogLogger(LogName, LogSource, MachineName);
            this.logger.Initialize();
        }

        [Test]
        public void LoggerInitialized()
        {
            Assert.That(this.logger.IsInitialized, Is.True);
        }

        [Test]
        public void CheckEventLogger()
        {
            Assert.That(this.logger.EventLogger, Is.Not.Null);
            Assert.That(this.logger.LogTarget, Is.TypeOf(typeof(EventLogTarget)));
        }

        [Test]
        public void CheckEventLogName()
        {
            EventLogTarget logTarget = (EventLogTarget)this.logger.LogTarget;
            Assert.That(logTarget.Log, Is.EqualTo(LogName));
        }

        [Test]
        public void CheckEventSource()
        {
            EventLogTarget logTarget = (EventLogTarget)this.logger.LogTarget;
            Assert.That(logTarget.Source, Is.EqualTo(LogSource));
        }

        [Test]
        public void CheckEventLogMachineName()
        {
            EventLogTarget logTarget = (EventLogTarget)this.logger.LogTarget;
            Assert.That(logTarget.MachineName, Is.EqualTo(MachineName));
        }

        [Test]
        public void CheckLogLayout()
        {
            EventLogTarget logTarget = (EventLogTarget)this.logger.LogTarget;
            string expected = "'" + NLogLogger.DefaultLayout + "'";

            Assert.That(logTarget.Layout.ToString(), Is.EqualTo(expected));
        }

        [Test]
        public void CreateLoggerWithLogNameNullOrEmpty()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => new EventLogLogger(null, LogSource, MachineName));
            
            Assert.That(exception.ParamName, Is.EqualTo("log"));
        }

        [Test]
        public void CreateLoggerWithSourceNameNullOrEmpty()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => new EventLogLogger(LogName, null, MachineName));
            
            Assert.That(exception.ParamName, Is.EqualTo("source"));
        }

        [Test]
        public void CreateLoggerWithMachineNameNullOrEmpty()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => new EventLogLogger(LogName, LogSource, null));
            
            Assert.That(exception.ParamName, Is.EqualTo("machine"));
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NLogLoggerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using NLog;
    using NLog.Config;
    using NLog.Targets;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Logging;

    [TestFixture]
    public class NLogLoggerUnitTests
    {
        private TargetMock target;
        private NLogLogger logger;

        [SetUp]
        public void Setup()
        {
            this.target = new TargetMock();
            
            this.logger = new NLogLogger(this.target);
            this.logger.Initialize();
        }

        [Test]
        public void CreateNLogLogger()
        {
            Assert.That(this.logger.EventLogger, Is.Not.Null);
            Assert.That(this.logger.IsInitialized, Is.True);
        }

        [Test]
        public void CreateNLogLoggerWithNullTarget()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => new NLogLogger((Target)null));
            Assert.That(exception.ParamName, Is.EqualTo("target"));
        }

        [Test]
        public void CheckLoggerTarget()
        {
            Assert.That(this.logger.LogTarget, Is.SameAs(this.target));
        }

        [Test]
        public void LogDebugMessage()
        {
            this.logger.DebugOutputEnabled = true;
            this.logger.LogDebugMessage("My message.");

            Assert.That(this.target.LastLogEvent.FormattedMessage, Is.EqualTo("My message."));
            Assert.That(this.target.LastLogEvent.Level, Is.EqualTo(LogLevel.Debug));
        }

        [Test]
        public void LogDebugMessageWhenWhenNotInitialized()
        {
            var testLogger = new NLogLogger(this.target) { DebugOutputEnabled = true };

            InvalidOperationException exception = Assert.Throws<InvalidOperationException>(
                () => testLogger.LogDebugMessage("My message."));

            Assert.That(exception.Message, Is.EqualTo("The logger is not initialized."));
        }

        [Test]
        public void LogMessage()
        {
            this.logger.LogMessage("My message.");

            Assert.That(this.target.LastLogEvent.FormattedMessage, Is.EqualTo("My message."));
            Assert.That(this.target.LastLogEvent.Level, Is.EqualTo(LogLevel.Info));
        }

        [Test]
        public void LogMessageWhenNotInitialized()
        {
            var testLogger = new NLogLogger(this.target);
            var exception = Assert.Throws<InvalidOperationException>(() => testLogger.LogMessage("My message."));
            
            Assert.That(exception.Message, Is.EqualTo("The logger is not initialized."));
        }

        [Test]
        public void LogWarning()
        {
            this.logger.LogWarning("My warning.");

            Assert.That(this.target.LastLogEvent.FormattedMessage, Is.EqualTo("My warning."));
            Assert.That(this.target.LastLogEvent.Level, Is.EqualTo(LogLevel.Warn));
        }

        [Test]
        public void LogWarningWhenNotInitialized()
        {
            var testLogger = new NLogLogger(this.target);
            var exception = Assert.Throws<InvalidOperationException>(() => testLogger.LogWarning("My warning."));

            Assert.That(exception.Message, Is.EqualTo("The logger is not initialized."));
        }

        [Test]
        public void LogError()
        {
            this.logger.LogError("My error.", null);

            Assert.That(this.target.LastLogEvent.FormattedMessage, Is.EqualTo("My error."));
            Assert.That(this.target.LastLogEvent.Level, Is.EqualTo(LogLevel.Error));
        }

        [Test]
        public void LogErrorWhenNotInitialized()
        {
            var testLogger = new NLogLogger(this.target);
            var exception = Assert.Throws<InvalidOperationException>(() => testLogger.LogError("My error.", null));

            Assert.That(exception.Message, Is.EqualTo("The logger is not initialized."));
        }

        [Test]
        public void LogErrorWithException()
        {
            Exception exception;
            try 
            {
                throw new InvalidOperationException("My exception message.");
            }
            catch (InvalidOperationException ex)
            {
                exception = ex;
            }
            
            this.logger.LogError("My error.", exception);

            StringAssert.StartsWith("My error. My exception message.", this.target.LastLogEvent.FormattedMessage);
            StringAssert.Contains(exception.StackTrace, this.target.LastLogEvent.FormattedMessage);
            Assert.That(this.target.LastLogEvent.Level, Is.EqualTo(LogLevel.Error));
            Assert.That(this.target.LastLogEvent.Exception, Is.EqualTo(exception));
        }

        [Test]
        public void CheckLoggerName()
        {
            Assert.That(
                this.logger.EventLogger.Name,
                Is.EqualTo("Proligence.Astoria.ServiceModel.UnitTests.NLogLoggerUnitTests"));
        }

        [Test]
        public void CheckDefaultRule()
        {
            LoggingConfiguration configuration = this.logger.EventLogger.Factory.Configuration;
            LoggingRule defaultRule = configuration.LoggingRules.First(rule => rule.LoggerNamePattern == "*");

            Assert.That(defaultRule.IsLoggingEnabledForLevel(LogLevel.Trace), Is.True);
            Assert.That(defaultRule.IsLoggingEnabledForLevel(LogLevel.Debug), Is.True);
            Assert.That(defaultRule.IsLoggingEnabledForLevel(LogLevel.Info), Is.True);
            Assert.That(defaultRule.IsLoggingEnabledForLevel(LogLevel.Warn), Is.True);
            Assert.That(defaultRule.IsLoggingEnabledForLevel(LogLevel.Error), Is.True);
            Assert.That(defaultRule.IsLoggingEnabledForLevel(LogLevel.Fatal), Is.True);
            Assert.That(defaultRule.Targets.Contains(this.target));
        }

        private class TargetMock : Target
        {
            public LogEventInfo LastLogEvent { get; private set; }

            protected override void Write(LogEventInfo logEvent)
            {
                this.LastLogEvent = logEvent;
                base.Write(logEvent);
            }
        }
    }
}
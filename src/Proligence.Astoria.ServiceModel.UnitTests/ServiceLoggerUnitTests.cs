﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceLoggerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using Moq;
    using Moq.Protected;
    using NUnit.Framework;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Logging;

    [TestFixture]
    public class ServiceLoggerUnitTests
    {
        private ServiceLoggerMock logger;

        [SetUp]
        public void Setup()
        {
            this.logger = new ServiceLoggerMock();
            this.logger.Initialize();
        }

        [TearDown]
        public void Teardown()
        {
            if (this.logger != null)
            {
                this.logger.Dispose();
            }
        }

        [Test]
        public void DisposeLogger()
        {
            this.logger.Dispose();

            Assert.That(this.logger.IsDisposed);
        }

        [Test]
        public void FormatErrorMessageWithoutException()
        {
            Assert.That(
                this.logger.FormatErrorMessage("My message.", null), 
                Is.EqualTo("My message."));
        }

        [Test]
        public void FormatErrorMessageWithSpaceAtEnd()
        {
            Assert.That(
                this.logger.FormatErrorMessage("My message. ", null),
                Is.EqualTo("My message."));
        }

        [Test]
        public void FormatErrorMessageWithException()
        {
            var exception = new InvalidOperationException("My exception message.");

            Assert.That(
                this.logger.FormatErrorMessage("My message.", exception),
                Is.EqualTo("My message. My exception message."));
        }

        [Test]
        public void FormatErrorMessageWithExceptionAndSpaceAtEndOfExceptionMessage()
        {
            var exception = new InvalidOperationException("My exception message. ");
            
            Assert.That(
                this.logger.FormatErrorMessage("My message.", exception),
                Is.EqualTo("My message. My exception message."));
        }

        [Test]
        public void FormatErrorMessageWithExceptionAndSpaceAtEndOfMessage()
        {
            var exception = new InvalidOperationException("My exception message.");
            
            Assert.That(
                this.logger.FormatErrorMessage("My message. ", exception),
                Is.EqualTo("My message. My exception message."));
        }

        [Test]
        public void FormatErrorMessageWithInnerExceptions()
        {
            var exception2 = new InvalidOperationException("My exception message 2.");
            var exception = new InvalidOperationException("My exception message 1.", exception2);
            
            Assert.That(
                this.logger.FormatErrorMessage("My message. ", exception),
                Is.EqualTo("My message. My exception message 1. My exception message 2."));
        }

        [Test, Sequential]
        public void FormatErrorMessageWithMessageNullOrWhiteSpace(
            [Values(null, "", " \t\r\n")] string message)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => this.logger.FormatErrorMessage(message, null));

            Assert.That(exception.ParamName, Is.EqualTo("message"));
        }

        [Test]
        public void LogDebugMessageWhenDebugOutputEnabled()
        {
            var loggerMock = new Mock<ServiceLogger> { CallBase = true };
            loggerMock.Object.DebugOutputEnabled = true;
            loggerMock.Protected().Setup("LogDebugMessageInternal", "My message.");

            loggerMock.Object.LogDebugMessage("My message.");

            loggerMock.Protected().Verify("LogDebugMessageInternal", Times.Once(), "My message.");
        }

        [Test]
        public void LogDebugMessageWhenDebugOutputDisabled()
        {
            var loggerMock = new Mock<ServiceLogger> { CallBase = true };
            loggerMock.Object.DebugOutputEnabled = false;
            loggerMock.Protected().Setup("LogDebugMessageInternal", "My message.");

            loggerMock.Object.LogDebugMessage("My message.");

            loggerMock.Protected().Verify("LogDebugMessageInternal", Times.Never(), "My message.");
        }
    }
}
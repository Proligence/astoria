﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaMonitoringOperationBehaviorUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Reflection;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;
    using Autofac;
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Logging;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    public class AstoriaMonitoringOperationBehaviorUnitTests
    {
        [TestCase(null)]
        [TestCase("")]
        public void CreateAstoriaMonitoringOperationBehaviorWhenServiceNameNull(string serviceName)
        {
            ServiceLogger logger = new NullLogger();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new AstoriaMonitoringOperationBehavior(serviceName, new Mock<IContainer>().Object, logger));

            Assert.That(exception.ParamName, Is.EqualTo("serviceName"));
        }

        [Test]
        public void CreateAstoriaMonitoringOperationBehaviorWhenContainerNull()
        {
            ServiceLogger logger = new NullLogger();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new AstoriaMonitoringOperationBehavior("ServiceName", null, logger));

            Assert.That(exception.ParamName, Is.EqualTo("container"));
        }

        [Test]
        public void CreateAstoriaMonitoringOperationBehaviorWhenLoggerNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new AstoriaMonitoringOperationBehavior("ServiceName", new Mock<IContainer>().Object, null));

            Assert.That(exception.ParamName, Is.EqualTo("logger"));
        }

        [Test]
        public void ApplyDispatchBehaviorWhenOperationDescriptionNull()
        {
            var behavior = new AstoriaMonitoringOperationBehavior(
                "ServiceName",
                new Mock<IContainer>().Object,
                new NullLogger());

            ConstructorInfo dispatchRuntimeCtor = typeof(DispatchRuntime).GetConstructor(
                bindingAttr: BindingFlags.Instance | BindingFlags.NonPublic,
                binder: null,
                types: new[] { typeof(EndpointDispatcher) },
                modifiers: null);

            DispatchRuntime dispatchRuntime = (DispatchRuntime)dispatchRuntimeCtor.Invoke(
                new object[] { new EndpointDispatcher(null, null, null) });

            IOperationInvoker operationInvoker = new Mock<IOperationInvoker>().Object;
            var dispatchOperation = new DispatchOperation(dispatchRuntime, "name", "action")
            {
                Invoker = operationInvoker
            };

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => behavior.ApplyDispatchBehavior(null, dispatchOperation));

            Assert.That(exception.ParamName, Is.EqualTo("operationDescription"));
        }

        [Test]
        public void ApplyDispatchBehaviorWhenDispatchOperationNull()
        {
            var behavior = new AstoriaMonitoringOperationBehavior(
                "ServiceName", 
                new Mock<IContainer>().Object,
                new NullLogger());

            var operationDescription = new OperationDescription("MyOperation", new ContractDescription("MyContract"));

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => behavior.ApplyDispatchBehavior(operationDescription, null));

            Assert.That(exception.ParamName, Is.EqualTo("dispatchOperation"));
        }

        [Test]
        public void ApplyDispatchBehaviorWhenDispatchOperationNotNull()
        {
            ConstructorInfo dispatchRuntimeCtor = typeof(DispatchRuntime).GetConstructor(
                bindingAttr: BindingFlags.Instance | BindingFlags.NonPublic, 
                binder: null, 
                types: new[] { typeof(EndpointDispatcher) }, 
                modifiers: null);
            
            DispatchRuntime dispatchRuntime = (DispatchRuntime)dispatchRuntimeCtor.Invoke(
                new object[] { new EndpointDispatcher(null, null, null) });

            var operationDescription = new OperationDescription("MyOperation", new ContractDescription("MyContract"));

            IPerformanceCounterFactory performanceCounterFactory = new Mock<IPerformanceCounterFactory>().Object;
            
            var builder = new ContainerBuilder();
            builder.RegisterInstance(performanceCounterFactory).As<IPerformanceCounterFactory>();
            using (IContainer container = builder.Build())
            {
                IOperationInvoker operationInvoker = new Mock<IOperationInvoker>().Object;
                var dispatchOperation = new DispatchOperation(dispatchRuntime, "name", "action")
                {
                    Invoker = operationInvoker
                };

                var behavior = new AstoriaMonitoringOperationBehavior("MyService", container, new NullLogger());
                behavior.ApplyDispatchBehavior(operationDescription, dispatchOperation);

                var monitoringOperationInvoker = (MonitoringOperationInvoker)dispatchOperation.Invoker;
                Assert.That(monitoringOperationInvoker.BaseInvoker, Is.SameAs(operationInvoker));
                Assert.That(monitoringOperationInvoker.ServiceName, Is.EqualTo("MyService"));
                Assert.That(monitoringOperationInvoker.MethodName, Is.EqualTo("MyOperation"));
            }
        }
    }
}
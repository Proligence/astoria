﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NullPerformanceCounterUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    public class NullPerformanceCounterUnitTests
    {
        [Test]
        public void Create()
        {
            var counter = new NullPerformanceCounter("MyCounter", "MyCategory", "MyInstance", 5);

            Assert.That(counter.CounterName, Is.EqualTo("MyCounter"));
            Assert.That(counter.CategoryName, Is.EqualTo("MyCategory"));
            Assert.That(counter.InstanceName, Is.EqualTo("MyInstance"));
            Assert.That(counter.RawValue, Is.EqualTo(5));
        }

        [Test]
        public void Increment()
        {
            var counter = new NullPerformanceCounter("MyCounter", "MyCategory", "MyInstance", 5);

            counter.Increment(2);

            Assert.That(counter.RawValue, Is.EqualTo(7));
        }

        [Test]
        public void Decrement()
        {
            var counter = new NullPerformanceCounter("MyCounter", "MyCategory", "MyInstance", 5);

            counter.Decrement(2);

            Assert.That(counter.RawValue, Is.EqualTo(3));
        }
    }
}
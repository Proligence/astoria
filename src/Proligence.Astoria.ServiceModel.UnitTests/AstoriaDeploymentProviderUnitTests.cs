﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaDeploymentProviderUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Linq;
    using System.Reflection;
    using NUnit.Framework;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Logging;

    [TestFixture]
    public class AstoriaDeploymentProviderUnitTests
    {
        private AstoriaDeploymentProviderMock provider;
        private AstoriaServiceConfiguration sampleConfiguration;
        private ServiceLogger sampleLogger;

        [SetUp]
        public void Setup()
        {
            this.provider = new AstoriaDeploymentProviderMock();
            this.sampleConfiguration = new AstoriaServiceConfiguration();
            this.sampleLogger = new NullLogger();
        }

        [TearDown]
        public void Teardown()
        {
            if (this.sampleLogger != null)
            {
                this.sampleLogger.Dispose();   
            }
        }

        [Test]
        public void PerformInstallCreatesEventLog()
        {
            this.provider.PerformInstall("My Service", this.sampleConfiguration, this.sampleLogger);

            Assert.That(this.provider.EventLogManager.Logs.Single(), Is.EqualTo("MyService"));
        }

        [Test]
        public void PerformInstallCreatesEventLogWithNameFromConfig()
        {
            var configurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Logging>
        <EventLogName>My Event Log</EventLogName>
    </Logging>
  </Service>
</AstoriaServiceConfiguration>");

            AstoriaServiceConfiguration configuration = configurationProvider.GetConfiguration("dummy");
            this.provider.PerformInstall("My Service", configuration, this.sampleLogger);

            Assert.That(this.provider.EventLogManager.Logs.Single(), Is.EqualTo("My Event Log"));
        }

        [Test]
        public void PerformInstallCreatesWindowsService()
        {
            string path = Assembly.GetExecutingAssembly().Location;
            this.provider.EntryAssemblyLocation = path;
            this.provider.PerformInstall("My Service", this.sampleConfiguration, this.sampleLogger);

            Assert.That(this.provider.WindowsServiceManager.Services.Single().Key, Is.EqualTo("My Service"));
            Assert.That(
                this.provider.WindowsServiceManager.Services.Single().Value, 
                Is.EqualTo(@"My Service," + path));
        }

        [TestCase(null)]
        [TestCase("")]
        public void TryPerformInstallWithoutServiceName(string serviceName)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => this.provider.PerformInstall(serviceName, this.sampleConfiguration, this.sampleLogger));
            
            Assert.That(exception.ParamName, Is.EqualTo("serviceName"));
        }

        [Test]
        public void TryPerformInstallWithoutConfiguration()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => this.provider.PerformInstall("My Service", null, this.sampleLogger));

            Assert.That(exception.ParamName, Is.EqualTo("configuration"));
        }

        [Test]
        public void TryPerformInstallWithoutLogger()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => this.provider.PerformInstall("My Service", this.sampleConfiguration, null));

            Assert.That(exception.ParamName, Is.EqualTo("logger"));
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PerformanceCounterAttributeUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using NUnit.Framework;
    using Proligence.Astoria.ServiceModel.Monitoring;

    [TestFixture]
    public class PerformanceCounterAttributeUnitTests
    {
        [TestCase(null)]
        [TestCase("")]
        public void CreateAttributeWhenCounterNameNullOrEmpty(string name)
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new PerformanceCounterAttribute(name));

            Assert.That(exception.ParamName, Is.EqualTo("counterName"));
        }
        
        [Test]
        public void CreateAttributeWhenCounterNameValid()
        {
            var attr = new PerformanceCounterAttribute("MyCounter");

            Assert.That(attr.CounterName, Is.EqualTo("MyCounter"));
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventLogExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.UnitTests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using Proligence.Astoria.Mocks;
    using Proligence.Astoria.ServiceModel.Deployment;

    /* ReSharper disable InvokeAsExtensionMethod */

    [TestFixture]
    public class EventLogExtensionsUnitTests
    {
        private AstoriaDeploymentProviderMock provider;
        private ServiceLoggerMock logger;

        [SetUp]
        public void Setup()
        {
            this.provider = new AstoriaDeploymentProviderMock();
            this.logger = new ServiceLoggerMock();
        }

        [Test]
        public void CreateEventLogWhenLogDoesNotExist()
        {
            bool result = EventLogExtensions.CreateEventLog(this.provider, this.logger, "My Log", "My Source");
            
            Assert.That(result, Is.True);
            Assert.That(this.provider.EventLogManager.Logs.Single(), Is.EqualTo("My Log"));
            Assert.That(this.logger.Messages.Single(), Is.EqualTo("Created event log: My Log"));
        }

        [Test]
        public void CreateEventLogWhenLogExists()
        {
            this.provider.EventLogManager.Logs.Add("My Log");

            bool result = EventLogExtensions.CreateEventLog(this.provider, this.logger, "My Log", "My Source");

            Assert.That(result, Is.False);
            Assert.That(this.provider.EventLogManager.Logs.Single(), Is.EqualTo("My Log"));
            Assert.That(this.logger.Messages, Is.Empty);
        }

        [Test]
        public void CreateEventLogWithError()
        {
            var exception = new InvalidOperationException("My exception message");
            this.provider.EventLogManager.Failure = exception;

            var thrownException = Assert.Throws<InvalidOperationException>(
                () => EventLogExtensions.CreateEventLog(this.provider, this.logger, "My Log", "My Source"));
            
            Assert.That(thrownException, Is.EqualTo(exception));
            Assert.That(this.logger.Errors.Single().Key, Is.EqualTo("Failed to create event log: My Log"));
            Assert.That(this.logger.Errors.Single().Value, Is.EqualTo(exception));
        }

        [Test]
        public void CreateEventLogWhenProviderNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => EventLogExtensions.CreateEventLog(null, this.logger, "My Log", "My Source"));

            Assert.That(exception.ParamName, Is.EqualTo("provider"));
        }

        [Test]
        public void CreateEventLogWhenLoggerNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => EventLogExtensions.CreateEventLog(this.provider, null, "My Log", "My Source"));

            Assert.That(exception.ParamName, Is.EqualTo("logger"));
        }

        [Test, Sequential]
        public void CreateEventLogWhenLogNameNullOrWhiteSpace(
            [Values(null, "", "\t", "\r\n", " ")] string logName)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => EventLogExtensions.CreateEventLog(this.provider, this.logger, logName, "My Source"));

            Assert.That(exception.ParamName, Is.EqualTo("logName"));
        }

        [Test, Sequential]
        public void CreateEventLogWhenSourceNameNullOrWhiteSpace(
            [Values(null, "", "\t", "\r\n", " ")] string source)
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => EventLogExtensions.CreateEventLog(this.provider, this.logger, "My Log", source));

            Assert.That(exception.ParamName, Is.EqualTo("source"));
        }
    }

    /* ReSharper restore InvokeAsExtensionMethod */
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WatchDogInfo.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a service watch dog.
    /// </summary>
    [DataContract]
    public class WatchDogInfo
    {
        [DataMember]
        private Guid id;

        [DataMember]
        private string name;

        [DataMember]
        private string description;

        public WatchDogInfo(string name, string description)
        {
            this.id = Guid.NewGuid();
            this.name = name;
            this.description = description;
        }

        /// <summary>
        /// Gets the unique identifier of the watch dog.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets the name of the watch dog.
        /// </summary>
        public string Name
        {
            get { return this.name; }
        }

        /// <summary>
        /// Gets the description of the watch dog.
        /// </summary>
        public string Description
        {
            get { return this.description; }
        }
    }
}
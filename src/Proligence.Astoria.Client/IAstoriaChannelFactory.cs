﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAstoriaChannelFactory.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    using System;
    using System.ServiceModel.Channels;

    /// <summary>
    /// Defines the interface for classes which create channels for Astoria services.
    /// </summary>
    public interface IAstoriaChannelFactory
    {
        /// <summary>
        /// Creates a channel to an Astoria service.
        /// </summary>
        /// <typeparam name="TApi">The interface that defines the API of the service.</typeparam>
        /// <param name="address">The url to the service.</param>
        /// <returns>The created service channel.</returns>
        TApi CreateChannel<TApi>(string address) 
            where TApi : class, IAstoriaService;

        /// <summary>
        /// Creates a channel to an Astoria service.
        /// </summary>
        /// <typeparam name="TApi">The interface that defines the API of the service.</typeparam>
        /// <param name="address">The url to the service.</param>
        /// <param name="configurationAction">
        /// An action which preforms configuration of the WCF channel factory.
        /// </param>
        /// <returns>The created service channel.</returns>
        TApi CreateChannel<TApi>(string address, Action<IChannelFactory<TApi>> configurationAction) 
            where TApi : class, IAstoriaService;
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DiscoveryResult.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client.Discovery
{
    using System;

    /// <summary>
    /// Single service discovery operation result.
    /// </summary>
    public class DiscoveryResult
    {
        /// <summary>
        /// Gets or sets the type of the service contract, if found.
        /// </summary>
        public Type ContractType { get; set; }

        /// <summary>
        /// Gets or sets the type name that was provided by a remote service.
        /// </summary>
        public string DiscoveredType { get; set; }

        /// <summary>
        /// Gets or sets the address of the service discovered.
        /// </summary>
        public string ServiceAddress { get; set; }

        /// <summary>
        /// Gets or sets the name of the discovered service.
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Gets or sets the description of the service discovered.
        /// </summary>
        public string ServiceDescription { get; set; }
    }
}
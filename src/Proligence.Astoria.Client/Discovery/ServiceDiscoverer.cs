﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceDiscoverer.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client.Discovery
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;

    /// <summary>
    /// Helper class for discovering running Astoria-based services.
    /// </summary>
    public class ServiceDiscoverer : IServiceDiscoverer
    {
        /// <summary>
        /// Default discovery port.
        /// </summary>
        public const int Port = 33000;

        /// <summary>
        /// Discovery token sent as a discovery request.
        /// </summary>
        public const string DiscoveryRequestToken = "ANYBODY_HERE";

        /// <summary>
        /// Discovery token sent as a beginning of the discovery response.
        /// </summary>
        public const string DiscoveryResponseToken = "HELLO";

        /// <summary>
        /// Runs the discovery operation to find all Astoria-based services
        /// running in local subnet.
        /// </summary>
        /// <param name="timeout">Response receive timeout.</param>
        /// <returns>List of the services found.</returns>
        public IEnumerable<DiscoveryResult> Discover(int timeout = 5000)
        {
            var localSendEndPoint = new IPEndPoint(IPAddress.Broadcast, Port);
            var localReceiveEndPoint = new IPEndPoint(IPAddress.Any, 0);
            var results = new ConcurrentBag<DiscoveryResult>();

            using (var udpServer = new UdpClient(localReceiveEndPoint))
            {
                udpServer.EnableBroadcast = true;
                var remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                var state = new UdpState { Endpoint = remoteEndPoint, Client = udpServer, Results = results };
                udpServer.BeginReceive(this.ReceiveCallback, state);

                var bytes = Encoding.UTF8.GetBytes(DiscoveryRequestToken);
                udpServer.Send(bytes, bytes.Length, localSendEndPoint);

                // Wait for background thread to end.
                // 20 second timeout in case something goes wrong and this thread would not be signalled
                Thread.Sleep(timeout);

                return results;
            }
        }

        /// <summary>
        /// Receives the discovery request.
        /// </summary>
        private void ReceiveCallback(IAsyncResult ar)
        {
            if (ar == null)
            {
                return;
            }

            var state = (UdpState)ar.AsyncState;
            var endpoint = state.Endpoint;

            try
            {
                byte[] response = state.Client.EndReceive(ar, ref endpoint);
                state.Client.BeginReceive(this.ReceiveCallback, state);

                var dataString = Encoding.UTF8.GetString(response).Trim();
                var data = dataString.Split(new[] { "::" }, StringSplitOptions.None);

                if (!dataString.StartsWith(DiscoveryResponseToken, StringComparison.Ordinal) || data.Length != 5)
                {
                    return;
                }

                var type = Type.GetType(data[1], false);
                state.Results.Add(
                    new DiscoveryResult
                        {
                            DiscoveredType = data[1],
                            ContractType = type,
                            ServiceAddress = data[2],
                            ServiceName = data[3],
                            ServiceDescription = data[4]
                        });
            }
            catch (SocketException ex)
            {
                Debug.WriteLine(ex.Message);

                // Timeout has been reached
                if (ex.SocketErrorCode != SocketError.TimedOut)
                {
                    throw;
                }
            }
            catch (ObjectDisposedException ex)
            {
                // Need to catch and suppress this as needed by the (dumb) design of socket operations in .NET
                // Reference: http://stackoverflow.com/questions/4662553/how-to-abort-sockets-beginreceive
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
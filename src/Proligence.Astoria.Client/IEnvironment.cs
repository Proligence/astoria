﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEnvironment.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    /// <summary>
    /// Defines the API for access to environment information.
    /// </summary>
    public interface IEnvironment
    {
        /// <summary>
        /// Gets the NetBIOS name of this local computer.
        /// </summary>
        string MachineName { get; }

        /// <summary>
        /// Gets the network domain name associated with the current user.
        /// </summary>
        string UserDomainName { get; }

        /// <summary>
        /// Gets the user name of the person who is currently logged on to the Windows operating system.
        /// </summary>
        string UserName { get; }

        /// <summary>
        /// Gets the amount of physical memory mapped to the process context.
        /// </summary>
        long WorkingSet { get; }
    }
}
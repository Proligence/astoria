// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IServiceChannelCache.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    using System;
    using System.ServiceModel.Channels;

    /// <summary>
    /// Interface describing cache and management object for a single Astoria service channel.
    /// </summary>
    /// <typeparam name="TChannel">The type of the service's channel.</typeparam>
    public interface IServiceChannelCache<TChannel>
        where TChannel : class, IAstoriaService
    {
        /// <summary>
        /// Gets or sets the address of the service.
        /// </summary>
        string ServiceAddress { get; set; }

        /// <summary>
        /// Optional declaration of underlying channel configuration.
        /// </summary>
        /// <param name="configuration">Action that configures the underlying channel.</param>
        /// <returns>Self - object on which the method is called.</returns>
        IServiceChannelCache<TChannel> WithConfig(Action<IChannelFactory<TChannel>> configuration);

        /// <summary>
        /// Clears the current channel configuration options.
        /// </summary>
        /// <returns>Self - object on which the method is called.</returns>
        IServiceChannelCache<TChannel> ClearConfig();

        /// <summary>
        /// Performs the specified operation using the service.
        /// </summary>
        /// <param name="action">The operation to perform.</param>
        void Use(Action<TChannel> action);

        /// <summary>
        /// Performs the specified operation using the service.
        /// </summary>
        /// <typeparam name="T">The type of the operation's result.</typeparam>
        /// <param name="action">The operation to perform.</param>
        /// <returns>The result of the operation.</returns>
        /// <exception cref="InvalidOperationException">Service address is not set.</exception>
        T Use<T>(Func<TChannel, T> action);
    }
}
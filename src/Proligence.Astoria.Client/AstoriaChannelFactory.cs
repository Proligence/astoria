﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaChannelFactory.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;

    /// <summary>
    /// Creates client channels to Astoria services.
    /// </summary>
    public class AstoriaChannelFactory : IAstoriaChannelFactory
    {
        /// <summary>
        /// The standard binding for Astoria service channels.
        /// </summary>
        private readonly AstoriaBinding astoriaBinding;

        /// <summary>
        /// The object used to synchronize access to the <see cref="Factories"/> property.
        /// </summary>
        private readonly object syncLock = new object();

        public AstoriaChannelFactory()
        {
            this.astoriaBinding = new AstoriaBinding();
            this.Factories = new Dictionary<Type, IChannelFactory>();
        }

        /// <summary>
        /// Gets the cached WCF channel factories.
        /// </summary>
        protected IDictionary<Type, IChannelFactory> Factories { get; private set; }

        /// <summary>
        /// Creates a channel to an Astoria service.
        /// </summary>
        /// <typeparam name="TApi">The interface that defines the API of the service.</typeparam>
        /// <param name="address">The url to the service.</param>
        /// <returns>The created service channel.</returns>
        public TApi CreateChannel<TApi>(string address)
            where TApi : class, IAstoriaService
        {
            return this.CreateChannel<TApi>(address, null);
        }

        /// <summary>
        /// Creates a channel to an Astoria service.
        /// </summary>
        /// <typeparam name="TApi">The interface that defines the API of the service.</typeparam>
        /// <param name="address">The url to the service.</param>
        /// <param name="configurationAction">
        /// An action which preforms configuration of the WCF channel factory.
        /// </param>
        /// <returns>The created service channel.</returns>
        public TApi CreateChannel<TApi>(string address, Action<IChannelFactory<TApi>> configurationAction) 
            where TApi : class, IAstoriaService
        {
            if (address == null)
            {
                throw new ArgumentNullException("address");
            }

            if (!address.EndsWith("/ast", StringComparison.Ordinal))
            {
                address += "/ast";
            }

            IChannelFactory<TApi> factory = this.GetChannelFactory(configurationAction);
            return factory.CreateChannel(new EndpointAddress(address));
        }

        /// <summary>
        /// Gets or creates a channel factory for the specified interface.
        /// </summary>
        /// <typeparam name="TApi">The interface of the channels which will be created by the factory.</typeparam>
        /// <param name="configurationAction">The configuration action to invoke when the factory is created.</param>
        /// <returns>The channel factory.</returns>
        protected virtual IChannelFactory<TApi> GetChannelFactory<TApi>(
            Action<IChannelFactory<TApi>> configurationAction)
            where TApi : class, IAstoriaService
        {
            IChannelFactory<TApi> factory;

            lock (this.syncLock)
            {
                IChannelFactory factoryObject;
                if (!this.Factories.TryGetValue(typeof(TApi), out factoryObject))
                {
                    factory = this.CreateChannelFactory<TApi>();

                    var concreteFactory = factory as ChannelFactory<TApi>;
                    if (concreteFactory != null)
                    {
                        SetMaxObjectsInGraph(concreteFactory.Endpoint, int.MaxValue);
                    }

                    if (configurationAction != null)
                    {
                        configurationAction(factory);
                    }

                    factory.Open();

                    this.Factories.Add(typeof(TApi), factory);
                }
                else
                {
                    factory = (IChannelFactory<TApi>)factoryObject;
                }
            }

            return factory;
        }

        /// <summary>
        /// Creates a new WCF channel factory for the specified interface.
        /// </summary>
        /// <typeparam name="TApi">The interface of the channels which will be created by the factory.</typeparam>
        /// <returns>The created factory.</returns>
        [ExcludeFromCodeCoverage]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected virtual IChannelFactory<TApi> CreateChannelFactory<TApi>()
            where TApi : class
        {
            return new ChannelFactory<TApi>(this.astoriaBinding);
        } 

        /// <summary>
        /// Sets the maximum number of items to serialize or deserialize for the specified endpoint.
        /// </summary>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="maxItemsInObjectGraph">The maximum number of items to serialize or deserialize.</param>
        private static void SetMaxObjectsInGraph(ServiceEndpoint endpoint, int maxItemsInObjectGraph)
        {
            foreach (var op in endpoint.Contract.Operations) 
            {
                var dataContractBehavior = op.Behaviors.Find<DataContractSerializerOperationBehavior>();
                if (dataContractBehavior != null) 
                {
                    dataContractBehavior.MaxItemsInObjectGraph = maxItemsInObjectGraph;
                }
            }
        }
    }
}
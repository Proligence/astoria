﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaBinding.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    using System;
    using System.Net.Security;
    using System.ServiceModel;
    using System.Xml;

    /// <summary>
    /// The standard WCF binding used by Astoria services.
    /// </summary>
    public class AstoriaBinding : NetTcpBinding
    {
        /// <summary>
        /// The byte quota for messages send over the binding.
        /// </summary>
        private const int ByteQuota = 2147483647;

        public AstoriaBinding()
        {
            // Timeouts
            this.CloseTimeout = TimeSpan.FromMinutes(30);
            this.OpenTimeout = TimeSpan.FromMinutes(30);
            this.ReceiveTimeout = TimeSpan.FromMinutes(30);
            this.SendTimeout = TimeSpan.FromMinutes(30);

            // Quotas
            this.ReaderQuotas = new XmlDictionaryReaderQuotas
            {
                MaxDepth = ByteQuota,
                MaxStringContentLength = ByteQuota,
                MaxArrayLength = ByteQuota,
                MaxBytesPerRead = ByteQuota,
                MaxNameTableCharCount = ByteQuota
            };

            // Security
            this.Security = new NetTcpSecurity
            {
                Mode = SecurityMode.None,
                Message = new MessageSecurityOverTcp
                {
                    ClientCredentialType = MessageCredentialType.None
                },
                Transport = new TcpTransportSecurity
                {
                    ClientCredentialType = TcpClientCredentialType.None,
                    ProtectionLevel = ProtectionLevel.None
                }
            };

            this.MaxConnections = 100;
            this.MaxBufferSize = ByteQuota;
            this.MaxReceivedMessageSize = ByteQuota;
            this.MaxBufferPoolSize = ByteQuota;
            this.ListenBacklog = ByteQuota;
            this.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            this.TransferMode = TransferMode.Streamed;
            this.PortSharingEnabled = false;
            this.TransactionFlow = false;
            
            // Disable reliable session
            this.ReliableSession = new OptionalReliableSession { Enabled = false };
        }
    }
}
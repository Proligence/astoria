﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WatchDogResult.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the result of using a service watch dog.
    /// </summary>
    [DataContract]
    public class WatchDogResult
    {
        [DataMember]
        private WatchDogStatus status;

        [DataMember]
        private IList<string> warnings;

        [DataMember]
        private IList<string> errors;

        public WatchDogResult()
        {
            this.status = WatchDogStatus.Ok;
            this.warnings = new List<string>();
            this.errors = new List<string>();
        }

        public WatchDogStatus Status
        {
            get { return this.status; }
        }

        public IEnumerable<string> Warnings
        {
            get { return this.warnings; }
        }

        public IEnumerable<string> Errors
        {
            get { return this.errors; }
        }

        public void AddWarning(string warning)
        {
            if (string.IsNullOrEmpty(warning))
            {
                throw new ArgumentNullException("warning");
            }

            this.warnings.Add(warning);
            
            if ((int)this.status < (int)WatchDogStatus.Warning)
            {
                this.status = WatchDogStatus.Warning;
            }
        }

        public void AddError(string error)
        {
            if (string.IsNullOrEmpty(error))
            {
                throw new ArgumentNullException("error");
            }

            this.errors.Add(error);

            if ((int)this.status < (int)WatchDogStatus.Error)
            {
                this.status = WatchDogStatus.Error;
            }
        }

        public override string ToString()
        {
            string message = this.status.ToString();
            
            if (this.Status == WatchDogStatus.Warning)
            {
                message += ": " + this.warnings.First();
            }
            else if (this.status == WatchDogStatus.Error)
            {
                message += ": " + this.errors.First();
            }

            return message;
        }
    }
}
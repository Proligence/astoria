﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceStartOptions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Defines startup settings for Astoria services.
    /// </summary>
    public class ServiceStartOptions
    {
        public ServiceStartOptions()
        {
            this.RunInWindowsService = true;
            this.AttachDebugger = false;
            this.InstallMode = false;
            this.EnableAutoDiscovery = true;
        }

        public ServiceStartOptions(IEnumerable<string> args)
        {
            if (args == null)
            {
                throw new ArgumentNullException("args");
            }

            // Avoid enumerating sequence multiple times.
            args = args.ToArray();

            if (args.Any(arg => arg == "-console")) 
            {
                this.RunInWindowsService = false;
            }
            else 
            {
                this.RunInWindowsService = true;
            }

            if (args.Any(arg => arg == "-debug"))
            {
                this.AttachDebugger = true;
            }

            if (args.Any(arg => arg == "-install"))
            {
                this.InstallMode = true;
            }

            if (args.Any(arg => arg == "-invisible"))
            {
                this.EnableAutoDiscovery = false;
            }
            else
            {
                this.EnableAutoDiscovery = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to run the service in a Windows Service. If 
        /// <see cref="RunInWindowsService"/> is set to <c>false</c> then the service will be started in a regular
        /// process.
        /// </summary>
        public bool RunInWindowsService { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a debugger should be attached when the service starts.
        /// </summary>
        public bool AttachDebugger { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the service installer should be only run.
        /// </summary>
        public bool InstallMode { get; set; }        
        
        /// <summary>
        /// Gets or sets a value indicating whether the auto-discovery feature is enabled for the service.
        /// </summary>
        public bool EnableAutoDiscovery { get; set; }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceChannelCache.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    using System;
    using System.ServiceModel.Channels;

    /// <summary>
    /// Caches and manages a single Astoria service channel.
    /// </summary>
    /// <typeparam name="TChannel">The type of the service's channel.</typeparam>
    public class ServiceChannelCache<TChannel> : IServiceChannelCache<TChannel>
        where TChannel : class, IAstoriaService
    {
        /// <summary>
        /// The Astoria service channel factory.
        /// </summary>
        private readonly IAstoriaChannelFactory channelFactory;

        /// <summary>
        /// Object used to synchronize access to the <see cref="channel"/> variable.
        /// </summary>
        private readonly object channelLock = new object();

        /// <summary>
        /// The service channel instance.
        /// </summary>
        private TChannel channel;

        /// <summary>
        /// Optional channel configuration action.
        /// </summary>
        private Action<IChannelFactory<TChannel>> configurationAction;

        public ServiceChannelCache(IAstoriaChannelFactory channelFactory)
        {
            this.channelFactory = channelFactory;
        }

        /// <summary>
        /// Gets or sets the address of the service.
        /// </summary>
        public string ServiceAddress { get; set; }

        /// <summary>
        /// Optional declaration of underlying channel configuration.
        /// </summary>
        /// <param name="configuration">Action that configures the underlying channel.</param>
        /// <returns>Self - object on which the method is called.</returns>
        public IServiceChannelCache<TChannel> WithConfig(Action<IChannelFactory<TChannel>> configuration) 
        {
            // Building multicast delegate to allow chaining WithConfig(...).WithConfig(...)
            this.configurationAction += configuration;
            return this;
        }

        /// <summary>
        /// Clears the current channel configuration options.
        /// </summary>
        /// <returns>Self - object on which the method is called.</returns>
        public IServiceChannelCache<TChannel> ClearConfig() 
        {
            this.configurationAction = null;
            return this;
        }

        /// <summary>
        /// Performs the specified operation using the service.
        /// </summary>
        /// <param name="action">The operation to perform.</param>
        public void Use(Action<TChannel> action)
        {
            this.Use(
                x =>
                {
                    action(channel);
                    return 0;
                });
        }

        /// <summary>
        /// Performs the specified operation using the service.
        /// </summary>
        /// <typeparam name="T">The type of the operation's result.</typeparam>
        /// <param name="action">The operation to perform.</param>
        /// <returns>The result of the operation.</returns>
        /// <exception cref="InvalidOperationException">Service address is not set.</exception>
        public T Use<T>(Func<TChannel, T> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            lock (this.channelLock)
            {
                string serviceAddress = this.ServiceAddress;
                if (serviceAddress == null)
                {
                    throw new InvalidOperationException("Service address is not set.");
                }

                if (this.channel == null)
                {
                    if (this.configurationAction == null)
                    {
                        this.channel = this.channelFactory.CreateChannel<TChannel>(
                        serviceAddress);
                    }
                    else
                    {
                        this.channel = this.channelFactory.CreateChannel(serviceAddress, this.configurationAction);
                    }
                }
            }

            try
            {
                return action(this.channel);
            }
            catch
            {
                // Recreate the channel if an error occured.
                this.channel = null;

                throw;
            }
        }
    }
}
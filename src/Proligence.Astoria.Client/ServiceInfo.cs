﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceInfo.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents information about a running Astoria service.
    /// </summary>
    [DataContract]
    public class ServiceInfo
    {
        public ServiceInfo()
        {
        }

        private ServiceInfo(IEnvironment environment)
        {
            this.MachineName = environment.MachineName;
            this.AccountName = string.IsNullOrEmpty(environment.UserDomainName)
                                   ? environment.UserName
                                   : environment.UserDomainName + "\\" + environment.UserName;
            this.UsedMemory = environment.WorkingSet;
        }

        /// <summary>
        /// Gets or sets the name of the machine on which the service is running.
        /// </summary>
        [DataMember]
        public string MachineName { get; set; }

        /// <summary>
        /// Gets or sets the name of the user account on which the service is running.
        /// </summary>
        [DataMember]
        public string AccountName { get; set; }

        /// <summary>
        /// Gets or sets the amount of memory allocated by the service.
        /// </summary>
        [DataMember]
        public long UsedMemory { get; set; }

        public static ServiceInfo CreateForEnvironment(IEnvironment environment)
        {
            return new ServiceInfo(environment);
        }
    }
}
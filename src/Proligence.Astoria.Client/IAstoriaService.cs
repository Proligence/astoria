﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAstoriaService.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceModel;

    /// <summary>
    /// Defines the API for all Astoria services.
    /// </summary>
    [ServiceContract]
    [ServiceKnownType("GetKnownTypes", typeof(AstoriaServiceKnownTypesProvider))]
    public interface IAstoriaService
    {
        /// <summary>
        /// Gets information about the service.
        /// </summary>
        /// <returns>The <see cref="ServiceInfo"/> object which contains information about the service.</returns>
        [OperationContract]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        ServiceInfo GetServiceInfo();

        /// <summary>
        /// Gets all watch dogs implemented by the service.
        /// </summary>
        /// <returns>A sequence of watch dog descriptors.</returns>
        [OperationContract]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IEnumerable<WatchDogInfo> GetWatchDogs();

        /// <summary>
        /// Invokes the specified watch dog.
        /// </summary>
        /// <param name="watchDogInfo">The watch dog to invoke.</param>
        /// <returns>The result of the invoked watch dog.</returns>
        [OperationContract]
        WatchDogResult InvokeWatchDog(WatchDogInfo watchDogInfo);

        /// <summary>
        /// Invokes a service extension.
        /// </summary>
        /// <param name="name">The name of the extension to invoke.</param>
        /// <param name="parameters">The parameters for the extension call.</param>
        /// <returns>The result returned by the extension.</returns>
        [OperationContract]
        object InvokeExtension(string name, params object[] parameters);
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceKnownTypesProvider.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Enables serialization of abstract types in astoria services.
    /// </summary>
    internal static class AstoriaServiceKnownTypesProvider
    {
        /// <summary>
        /// The sync root.
        /// </summary>
        private static readonly object SyncRoot = new object();

        /// <summary>
        /// The cached types.
        /// </summary>
        private static volatile IEnumerable<Type> cachedTypes;

        /// <summary>
        /// Lists known types the GraphManager service can operate on.
        /// </summary>
        /// <param name="provider">
        /// The provider.
        /// </param>
        /// <returns>
        /// List of types known to the GraphManager service.
        /// </returns>
        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "provider", 
            Justification = "Required by .NET framework API.")]
        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            if (cachedTypes == null)
            {
                lock (SyncRoot)
                {
                    if (cachedTypes == null)
                    {
                        var allTypes = AppDomain.CurrentDomain
                            .GetAssemblies()
                            .SelectMany(
                                a => a.GetLoadableTypes().Where(
                                    t => !t.IsGenericType && t.IsDefined(typeof(DataContractAttribute), true)))
                            .ToList();
                        
                        cachedTypes = allTypes
                            .Concat(new[] { typeof(DateTimeOffset) });
                    }
                }
            }

            return cachedTypes;
        }
    }
}
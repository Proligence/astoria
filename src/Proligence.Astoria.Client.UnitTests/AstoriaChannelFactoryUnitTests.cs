﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaChannelFactoryUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.Mocks;

    public class AstoriaChannelFactoryUnitTests
    {
        [Test]
        public void CreateChannelWhenAddressNull()
        {
            var factory = new TestAstoriaChannelFactory();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => factory.CreateChannel<IDummy>(null));
            
            Assert.That(exception.ParamName, Is.EqualTo("address"));
        }

        [Test]
        public void CreateChannelWhenAddressNotNull()
        {
            var factory = new TestAstoriaChannelFactory();

            var channel = factory.CreateChannel<IDummy>("net.tcp://localhost/dummy/ast");

            Assert.That(channel, Is.Not.Null);
        }

        [Test]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void CreateChannelWhenAddressDoesNotEndWithAst()
        {
            var factory = new TestAstoriaChannelFactory();
            var channelMock = new Mock<IDummy>();

            factory.FactoryMockConfigurtion = f =>
                {
                    var factoryMock = (Mock<IChannelFactory<IDummy>>)f;
                    factoryMock.Setup(x => x.CreateChannel(
                        It.Is<EndpointAddress>(a => a.Uri.ToString() == "net.tcp://localhost/dummy/ast")))
                    .Returns(channelMock.Object);
                };

            var channel = factory.CreateChannel<IDummy>("net.tcp://localhost/dummy");

            Assert.That(channel, Is.SameAs(channelMock.Object));
        }

        [Test]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void CreateMultipleChannelsForSameApi()
        {
            var factory = new TestAstoriaChannelFactory();

            var channel1 = factory.CreateChannel<IDummy>("net.tcp://localhost/dummy/ast");
            var channel2 = factory.CreateChannel<IDummy>("net.tcp://localhost/dummy/ast");

            Assert.That(channel1, Is.Not.Null);
            Assert.That(channel2, Is.Not.Null);
            Assert.That(factory.Factories.Count, Is.EqualTo(1));
        }

        private class TestAstoriaChannelFactory : AstoriaChannelFactory
        {
            public new IDictionary<Type, IChannelFactory> Factories
            {
                get { return base.Factories; }
            }

            public Action<object> FactoryMockConfigurtion { get; set; }

            protected override IChannelFactory<TApi> CreateChannelFactory<TApi>()
            {
                var factoryMock = new Mock<IChannelFactory<TApi>>();

                factoryMock.Setup(x => x.CreateChannel(It.IsAny<EndpointAddress>()))
                    .Returns(new Mock<TApi>().Object);

                if (this.FactoryMockConfigurtion != null)
                {
                    this.FactoryMockConfigurtion(factoryMock);
                }

                return factoryMock.Object;
            }
        }
    }
}
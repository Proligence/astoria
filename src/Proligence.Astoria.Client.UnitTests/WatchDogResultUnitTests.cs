﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WatchDogResultUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client.UnitTests
{
    using System;
    using System.Linq;
    using NUnit.Framework;

    [TestFixture]
    public class WatchDogResultUnitTests
    {
        [Test]
        public void WatchDogStatusShouldBeInitializedToOk()
        {
            var result = new WatchDogResult();

            Assert.That(result.Status, Is.EqualTo(WatchDogStatus.Ok));
        }

        [Test]
        public void WatchDogStatusShouldBeInitializedToWithZeroWarnings()
        {
            var result = new WatchDogResult();

            Assert.That(result.Warnings, Is.Empty);
        }

        [Test]
        public void WatchDogStatusShouldBeInitializedToWithZeroErrors()
        {
            var result = new WatchDogResult();

            Assert.That(result.Errors, Is.Empty);
        }

        [TestCase("")]
        [TestCase(null)]
        public void AddWarningWhenWarningNullOrEmpty(string warning)
        {
            var result = new WatchDogResult();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => result.AddWarning(warning));

            Assert.That(exception.ParamName, Is.EqualTo("warning"));
        }

        [Test]
        public void AddWarningWhenWarningsListEmpty()
        {
            var result = new WatchDogResult();

            result.AddWarning("My warning");

            Assert.That(result.Warnings.ToArray(), Is.EquivalentTo(new[] { "My warning" }));
            Assert.That(result.Status, Is.EqualTo(WatchDogStatus.Warning));
        }

        [Test]
        public void AddWarningWhenWarningsListNotEmpty()
        {
            var result = new WatchDogResult();
            result.AddWarning("My warning 1");

            result.AddWarning("My warning 2");

            Assert.That(result.Warnings.ToArray(), Is.EquivalentTo(new[] { "My warning 1", "My warning 2" }));
            Assert.That(result.Status, Is.EqualTo(WatchDogStatus.Warning));
        }

        [Test]
        public void AddWarningWhenErrorsListNotEmpty()
        {
            var result = new WatchDogResult();
            result.AddError("My error");

            result.AddWarning("My warning");

            Assert.That(result.Errors.ToArray(), Is.EquivalentTo(new[] { "My error" }));
            Assert.That(result.Warnings.ToArray(), Is.EquivalentTo(new[] { "My warning" }));
            Assert.That(result.Status, Is.EqualTo(WatchDogStatus.Error));
        }

        [TestCase("")]
        [TestCase(null)]
        public void AddErrorWhenErrorNullOrEmpty(string error)
        {
            var result = new WatchDogResult();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => result.AddError(error));

            Assert.That(exception.ParamName, Is.EqualTo("error"));
        }

        [Test]
        public void AddErrorsWhenErrorsListEmpty()
        {
            var result = new WatchDogResult();

            result.AddError("My error");

            Assert.That(result.Errors.ToArray(), Is.EquivalentTo(new[] { "My error" }));
            Assert.That(result.Status, Is.EqualTo(WatchDogStatus.Error));
        }

        [Test]
        public void AddErrorsWhenErrorsListNotEmpty()
        {
            var result = new WatchDogResult();
            result.AddError("My error 1");

            result.AddError("My error 2");

            Assert.That(result.Errors.ToArray(), Is.EquivalentTo(new[] { "My error 1", "My error 2" }));
            Assert.That(result.Status, Is.EqualTo(WatchDogStatus.Error));
        }

        [Test]
        public void AddErrorsWhenWarningsListNotEmpty()
        {
            var result = new WatchDogResult();
            result.AddWarning("My warning");

            result.AddError("My error");

            Assert.That(result.Errors.ToArray(), Is.EquivalentTo(new[] { "My error" }));
            Assert.That(result.Warnings.ToArray(), Is.EquivalentTo(new[] { "My warning" }));
            Assert.That(result.Status, Is.EqualTo(WatchDogStatus.Error));
        }

        [Test]
        public void ToStringWhenStatusOk()
        {
            var watchDogResult = new WatchDogResult();

            string result = watchDogResult.ToString();

            Assert.That(result, Is.EqualTo("Ok"));
        }

        [Test]
        public void ToStringWhenStatusWarning()
        {
            var watchDogResult = new WatchDogResult();
            watchDogResult.AddWarning("Warning 1");
            watchDogResult.AddWarning("Warning 2");

            string result = watchDogResult.ToString();

            Assert.That(result, Is.EqualTo("Warning: Warning 1"));
        }

        [Test]
        public void ToStringWhenStatusError()
        {
            var watchDogResult = new WatchDogResult();
            watchDogResult.AddError("Error 1");
            watchDogResult.AddError("Error 2");

            string result = watchDogResult.ToString();

            Assert.That(result, Is.EqualTo("Error: Error 1"));
        }
    }
}
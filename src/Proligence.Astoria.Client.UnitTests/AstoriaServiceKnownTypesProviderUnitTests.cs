﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceKnownTypesProviderUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="AstoriaServiceKnownTypesProvider"/> class.
    /// </summary>
    [TestFixture]
    public class AstoriaServiceKnownTypesProviderUnitTests
    {
        /// <summary>
        /// The types retuned by the <see cref="AstoriaServiceKnownTypesProvider.GetKnownTypes"/> method.
        /// </summary>
        private IEnumerable<Type> knownTypes;

        /// <summary>
        /// Sets up the test fixture.
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            this.knownTypes = AstoriaServiceKnownTypesProvider.GetKnownTypes(null);
        }

        /// <summary>
        /// Tests if the known types contain classes with the <see cref="DataContractAttribute"/>.
        /// </summary>
        [Test]
        public void KnownTypesContainClassesWithDataContractAttribute()
        {
            Assert.That(this.knownTypes.Contains(typeof(DummyContractClass)));
        }

        /// <summary>
        /// Tests if the known types contains the <see cref="DateTimeOffset"/> class.
        /// </summary>
        [Test]
        public void KnownTypesContainDateTimeOffset()
        {
            Assert.That(this.knownTypes.Contains(typeof(DateTimeOffset)));
        }

        /// <summary>
        /// Dummy class with <see cref="DataContractAttribute"/>.
        /// </summary>
        [DataContract]
        internal class DummyContractClass
        {
        }
    }
}
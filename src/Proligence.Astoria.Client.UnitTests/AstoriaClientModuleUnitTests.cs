﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaClientModuleUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client.UnitTests
{
    using Autofac;
    using NUnit.Framework;

    [TestFixture]
    public class AstoriaClientModuleUnitTests
    {
        private IContainer container;

        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new AstoriaClientModule());
            
            this.container = builder.Build();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            if (this.container != null)
            {
                this.container.Dispose();   
            }
        }

        [Test]
        public void ResolveIAstoriaChannelFactory()
        {
            var astoriaChannelFactory = this.container.Resolve<IAstoriaChannelFactory>();
            Assert.That(astoriaChannelFactory, Is.InstanceOf(typeof(AstoriaChannelFactory)));
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceInfoUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client.UnitTests
{
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.Client;

    [TestFixture]
    public class ServiceInfoUnitTests
    {
        [Test]
        public void CreateDefaultServiceInfo()
        {
            var serviceInfo = new ServiceInfo();
            
            Assert.That(serviceInfo.MachineName, Is.Null);
            Assert.That(serviceInfo.AccountName, Is.Null);
            Assert.That(serviceInfo.UsedMemory, Is.EqualTo(0));
        }

        [Test]
        public void CreateServiceInfo()
        {
            Mock<IEnvironment> environmentMock = CreateEnvironment("My machine", "MYDOMAIN", "my.user", 1000);
            ServiceInfo serviceInfo = ServiceInfo.CreateForEnvironment(environmentMock.Object);

            Assert.That(serviceInfo.MachineName, Is.EqualTo("My machine"));
            Assert.That(serviceInfo.AccountName, Is.EqualTo("MYDOMAIN\\my.user"));
            Assert.That(serviceInfo.UsedMemory, Is.EqualTo(1000));
            environmentMock.VerifyAll();
        }

        [Test]
        public void CreateServiceInfoWhenDomainNull()
        {
            Mock<IEnvironment> environmentMock = CreateEnvironment("My machine", null, "my.user", 1000);
            ServiceInfo serviceInfo = ServiceInfo.CreateForEnvironment(environmentMock.Object);

            Assert.That(serviceInfo.MachineName, Is.EqualTo("My machine"));
            Assert.That(serviceInfo.AccountName, Is.EqualTo("my.user"));
            Assert.That(serviceInfo.UsedMemory, Is.EqualTo(1000));
            environmentMock.VerifyAll();
        }

        [Test]
        public void CreateServiceInfoWhenDomainEmpty()
        {
            Mock<IEnvironment> environmentMock = CreateEnvironment("My machine", string.Empty, "my.user", 1000);
            ServiceInfo serviceInfo = ServiceInfo.CreateForEnvironment(environmentMock.Object);

            Assert.That(serviceInfo.MachineName, Is.EqualTo("My machine"));
            Assert.That(serviceInfo.AccountName, Is.EqualTo("my.user"));
            Assert.That(serviceInfo.UsedMemory, Is.EqualTo(1000));
            environmentMock.VerifyAll();
        }

        private static Mock<IEnvironment> CreateEnvironment(
            string machineName, string domainName, string userName, int workingSet)
        {
            var environmentMock = new Mock<IEnvironment>();
            environmentMock.Setup(x => x.MachineName).Returns(machineName);
            environmentMock.Setup(x => x.UserDomainName).Returns(domainName);
            environmentMock.Setup(x => x.UserName).Returns(userName);
            environmentMock.Setup(x => x.WorkingSet).Returns(workingSet);
            return environmentMock;
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceChannelCacheUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client.UnitTests
{
    using System;
    using Moq;
    using NUnit.Framework;
    using Proligence.Astoria.Mocks;

    [TestFixture]
    public class ServiceChannelCacheUnitTests
    {
        private Mock<IAstoriaService> channel;
        private AstoriaChannelFactoryMock channelFactory;
        private ServiceChannelCache<IAstoriaService> channelCache;

        [SetUp]
        public void Setup()
        {
            this.channel = new Mock<IAstoriaService>();
            
            this.channelFactory = new AstoriaChannelFactoryMock
            {
                Channel = this.channel.Object
            };

            this.channelCache = new ServiceChannelCache<IAstoriaService>(this.channelFactory)
            { 
                ServiceAddress = "dummy" 
            };
        }

        [Test]
        public void UseChannelWithoutResult()
        {
            IAstoriaService usedChannel = null;
            this.channelCache.Use(
                c =>
                {
                    usedChannel = c; 
                });

            Assert.That(usedChannel, Is.SameAs(this.channel.Object));
        }

        [Test]
        public void UseChannelWithResult()
        {
            IAstoriaService usedChannel = null;
            int result = this.channelCache.Use(
                c => 
                {
                    usedChannel = c;
                    return 7;
                });

            Assert.That(usedChannel, Is.SameAs(this.channel.Object));   
            Assert.That(result, Is.EqualTo(7));
        }

        [Test]
        public void TryInvokeUseWithoutInitializingServiceAddress()
        {
            var cache = new ServiceChannelCache<IAstoriaService>(this.channelFactory);
            var exception = Assert.Throws<InvalidOperationException>(() => cache.Use(c => { }));

            Assert.That(exception.Message, Is.EqualTo("Service address is not set."));
        }

        [Test]
        public void RecreateChannelOnError()
        {
            IAstoriaService channel1 = null;
            IAstoriaService expected1 = this.channelFactory.Channel;
            Assert.Throws<InvalidOperationException>(
                () => this.channelCache.Use(
                    c =>
                    {
                        channel1 = c;
                        throw new InvalidOperationException();
                    }));

            this.channel = new Mock<IAstoriaService>();
            this.channelFactory.Channel = this.channel.Object;

            IAstoriaService expected2 = this.channelFactory.Channel;
            IAstoriaService channel2 = null;
            this.channelCache.Use(
                c =>
                {
                    channel2 = c;
                });

            Assert.That(channel1, Is.SameAs(expected1));
            Assert.That(channel2, Is.SameAs(expected2));
        }
    }
}
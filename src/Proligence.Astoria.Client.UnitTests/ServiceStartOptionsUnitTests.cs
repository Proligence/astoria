﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceStartOptionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Client.UnitTests
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class ServiceStartOptionsUnitTests
    {
        [Test]
        public void RunInWindowsServiceTrueByDefault()
        {
            var options = new ServiceStartOptions();
            Assert.That(options.RunInWindowsService, Is.True);
        }

        [Test]
        public void AttachDebuggerFalseByDefault()
        {
            var options = new ServiceStartOptions();
            Assert.That(options.AttachDebugger, Is.False);
        }

        [Test]
        public void InstallModeFalseByDefault()
        {
            var options = new ServiceStartOptions();
            Assert.That(options.InstallMode, Is.False);
        }

        [Test]
        public void CreateStartupOptionsFromNullArgs()
        {
            Assert.That(() => new ServiceStartOptions(null), Throws.InstanceOf(typeof(ArgumentNullException)));
        }

        [Test]
        public void RunInWindowsServiceIfArgumentNotSpecified()
        {
            var options = new ServiceStartOptions(new string[0]);
            Assert.That(options.RunInWindowsService, Is.True);
        }

        [Test]
        public void AttachDebuggerFalseIfArgumentNotSpecified()
        {
            var options = new ServiceStartOptions(new string[0]);
            Assert.That(options.AttachDebugger, Is.False);
        }

        [Test]
        public void InstallModeFalseIfArgumentNotSpecified()
        {
            var options = new ServiceStartOptions(new string[0]);
            Assert.That(options.InstallMode, Is.False);
        }

        [Test]
        public void ParseConsoleCommandLineArgument()
        {
            var options = new ServiceStartOptions(new[] { "-console" });
            Assert.That(options.RunInWindowsService, Is.False);
        }

        [Test]
        public void ParseDebugCommandLineArgument()
        {
            var options = new ServiceStartOptions(new[] { "-debug" });
            Assert.That(options.AttachDebugger, Is.True);
        }

        [Test]
        public void ParseInstallCommandLineArgument()
        {
            var options = new ServiceStartOptions(new[] { "-install" });
            Assert.That(options.InstallMode, Is.True);
        }
    }
}
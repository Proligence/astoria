﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Environment.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel
{
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.Client;

    /// <summary>
    /// Provides data from the <see cref="System.Environment"/> class over the <see cref="IEnvironment"/> interface.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class Environment : IEnvironment
    {
        /// <summary>
        /// Gets the NetBIOS name of this local computer.
        /// </summary>
        public string MachineName
        {
            get
            {
                return System.Environment.MachineName;
            }
        }

        /// <summary>
        /// Gets the network domain name associated with the current user.
        /// </summary>
        public string UserDomainName
        {
            get
            {
                return System.Environment.UserDomainName;
            }
        }

        /// <summary>
        /// Gets the user name of the person who is currently logged on to the Windows operating system.
        /// </summary>
        public string UserName
        {
            get
            {
                return System.Environment.UserName;
            }
        }

        /// <summary>
        /// Gets the amount of physical memory mapped to the process context.
        /// </summary>
        public long WorkingSet
        {
            get
            {
                return System.Environment.WorkingSet;
            }
        }
    }
}
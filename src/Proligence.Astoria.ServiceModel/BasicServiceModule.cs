﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BasicServiceModule.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel
{
    using Autofac;
    using Proligence.Astoria.ServiceModel.Monitoring;

    /// <summary>
    /// Configures the dependency injection container components required for all Astoria services.
    /// </summary>
    public class BasicServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BehaviorManager>().As<IBehaviorManager>().SingleInstance();
            builder.RegisterType<WatchDogManager>().As<IWatchDogManager>().SingleInstance();
        }
    }
}
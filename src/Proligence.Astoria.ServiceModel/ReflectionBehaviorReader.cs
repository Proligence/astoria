﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionBehaviorReader.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Implements reading behaviors via reflection (attributes).
    /// </summary>
    internal class ReflectionBehaviorReader
    {
        /// <summary>
        /// Gets a sequence of service behaviors found by the <see cref="ReadServiceBehaviorsFromType"/> method.
        /// </summary>
        public IEnumerable<IAstoriaServiceBehavior> ServiceBehaviors { get; private set; }

        /// <summary>
        /// Reads all service behaviors from the specified <see cref="Type"/>.
        /// </summary>
        /// <param name="type">The type to read service behaviors from.</param>
        public void ReadServiceBehaviorsFromType(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            var behaviors = new List<IAstoriaServiceBehavior>();
            ReadServiceBehaviorsFromType(type, behaviors);
            
            this.ServiceBehaviors = behaviors;
        }

        /// <summary>
        /// Reads all service behaviors from the specified <see cref="Type"/> and its base types..
        /// </summary>
        /// <param name="type">The type to read service behaviors from.</param>
        /// <param name="behaviors">A collection to which found behaviors will be added.</param>
        private static void ReadServiceBehaviorsFromType(Type type, ICollection<IAstoriaServiceBehavior> behaviors)
        {
            if ((type.BaseType != null) && (type.BaseType != typeof(object)))
            {
                ReadServiceBehaviorsFromType(type.BaseType, behaviors);
            }

            object[] attributes = type.GetCustomAttributes(false);
            foreach (var behaviorAttribute in attributes.OfType<IAstoriaServiceBehavior>())
            {
                behaviors.Add(behaviorAttribute);
            }
        }
    }
}
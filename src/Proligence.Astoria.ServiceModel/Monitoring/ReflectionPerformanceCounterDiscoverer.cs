// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionPerformanceCounterDiscoverer.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Discovers performance counters using reflection on the
    /// <see cref="AstoriaService{TApi,TConfig}.PerformanceCounters"/> object.
    /// </summary>
    /// <remarks>
    /// The implementation gets all public properties which are decorated with the
    /// <see cref="PerformanceCounterAttribute"/> attribute and creates <see cref="IPerformanceCounter"/> using the
    /// <see cref="IPerformanceCounterFactory"/> resolved from the service's dependency injection container.
    /// </remarks>
    public class ReflectionPerformanceCounterDiscoverer : IPerformanceCounterDiscoverer
    {
        private readonly IPerformanceCounterFactory factory;

        public ReflectionPerformanceCounterDiscoverer(IPerformanceCounterFactory factory)
        {
            if (factory == null)
            {
                throw new ArgumentNullException("factory");
            }

            this.factory = factory;
        }

        /// <summary>
        /// Discovers all performance counters required by the service.
        /// </summary>
        /// <typeparam name="TConfig">The type which represents service's configuration.</typeparam>
        /// <typeparam name="TApi">The interface which defines the service's API.</typeparam>
        /// <param name="service">The service for which performance counters will be discovered.</param>
        /// <returns>
        /// A sequence of <see cref="IPerformanceCounter"/> objects which represent the performance counters required
        /// by the service.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public IEnumerable<IPerformanceCounter> DiscoverPerformanceCounters<TConfig, TApi>(
            AstoriaService<TApi, TConfig> service)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (service == null)
            {
                throw new ArgumentNullException("service");
            }

            if (service.PerformanceCounters == null)
            {
                return new IPerformanceCounter[0];
            }

            var counterProperties = new List<PropertyInfo>();
            Type performanceCountersType = service.PerformanceCounters.GetType();
            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            
            while (performanceCountersType != null)
            {
                PropertyInfo[] properties = performanceCountersType.GetProperties(
                    bindingFlags);
                counterProperties.AddRange(properties);
                performanceCountersType = performanceCountersType.BaseType;
            }

            var counters = new List<IPerformanceCounter>();

            foreach (PropertyInfo propertyInfo in counterProperties)
            {
                PerformanceCounterAttribute attr = (PerformanceCounterAttribute)Attribute.GetCustomAttribute(
                    propertyInfo,
                    typeof(PerformanceCounterAttribute));

                if (attr != null)
                {
                    if (propertyInfo.PropertyType != typeof(IPerformanceCounter))
                    {
                        throw new InvalidOperationException(string.Format(
                            CultureInfo.CurrentCulture,
                            "The type of the {0}.{1} property must be {2}.",
                            service.PerformanceCounters.GetType().Name,
                            propertyInfo.Name,
                            typeof(IPerformanceCounter).FullName));
                    }

                    if (counters.All(c => c.CounterName != propertyInfo.Name))
                    {
                        if (attr.CategoryName == null)
                        {
                            attr.CategoryName = service.ServiceName;
                        }

                        IPerformanceCounter counter = this.factory.CreatePerformanceCounter(attr);
                        
                        MethodInfo setter = propertyInfo.GetSetMethod(true);
                        if (setter == null)
                        {
                            Type declaringType = propertyInfo.DeclaringType;
                            if (declaringType != null)
                            {
                                PropertyInfo pi = declaringType.GetProperty(propertyInfo.Name, bindingFlags);
                                setter = pi.GetSetMethod(true);
                            }
                        }

                        if (setter != null)
                        {
                            setter.Invoke(service.PerformanceCounters, new object[] { counter });
                            counters.Add(counter);
                        }

                        if (attr.InitialValueSpecified)
                        {
                            try
                            {
                                counter.RawValue = attr.InitialValue;
                            }
                            // ReSharper disable EmptyGeneralCatchClause
                            catch
                            {
                                // Setting the performance counter will fail if the performance counter does not exist.
                                // This will happen if this method is invoked in intall mode to create the serivce's
                                // performance counters. Unfortunately, this method is also called in non-install mode
                                // and there is no way to check if the performance counter exist without elevated
                                // permissions (which are available only in service install mode).
                            }
                            // ReSharper restore EmptyGeneralCatchClause
                        }
                    }
                }
            }

            return counters;
        }
    }
}
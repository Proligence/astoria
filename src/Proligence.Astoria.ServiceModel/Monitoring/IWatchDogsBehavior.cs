﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWatchDogsBehavior.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    /// <summary>
    /// Astoria service behavior for watch dogs functionality.
    /// </summary>
    [AstoriaServiceBehavior(AllowMultiple = false)]
    public interface IWatchDogsBehavior : IAstoriaServiceBehavior
    {
        /// <summary>
        /// Gets or sets a value indicating whether the watch dogs functionality is enabled.
        /// </summary>
        bool WatchDogsEnabled { get; set; }

        /// <summary>
        /// Gets or sets a comma-delimited list of names of watch dogs which should be disabled.
        /// </summary>
        string DisabledWatchDogs { get; set; }
    }
}
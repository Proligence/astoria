﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InternalWatchDog.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// The base class for classes which implement watch dogs which inspect the service's internals.
    /// </summary>
    /// <typeparam name="TService">The type which implements the monitored service.</typeparam>
    /// <typeparam name="TApi">The interface of the monitored service's API.</typeparam>
    /// <typeparam name="TConfig">The type which represents the configuration of the monitored service.</typeparam>
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    [SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")]
    public abstract class InternalWatchDog<TService, TApi, TConfig> : ServiceWatchDog, IInternalWatchDog<TApi, TConfig>
        where TService : AstoriaService<TApi, TConfig>
        where TApi : class, IAstoriaService
        where TConfig : AstoriaServiceConfiguration
    {
        /// <summary>
        /// Gets or sets the monitored service instance.
        /// </summary>
        [ExcludeFromCodeCoverage]
        AstoriaService<TApi, TConfig> IInternalWatchDog<TApi, TConfig>.Service
        {
            get { return this.Service; }
            set { this.Service = (TService)value; }
        }

        /// <summary>
        /// Gets or sets the monitored service instance.
        /// </summary>
        public TService Service { get; set; }
    }
}
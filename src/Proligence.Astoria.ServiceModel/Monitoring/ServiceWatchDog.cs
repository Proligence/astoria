﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceWatchDog.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.Client;

    /// <summary>
    /// The base class for classes which implement service watch dogs.
    /// </summary>
    public abstract class ServiceWatchDog : IServiceWatchDog
    {
        /// <summary>
        /// Initializes the watch dog.
        /// </summary>
        public virtual void Initialize()
        {
        }

        /// <summary>
        /// Invokes the watch dog and returns the result of the inspection.
        /// </summary>
        /// <returns>The result of the watch dog inspection.</returns>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public WatchDogResult Inspect()
        {
            var result = new WatchDogResult();

            try
            {
                this.Inspect(result);
            }
            catch (Exception ex)
            {
                result.AddError(ex.ToString());
            }
            
            return result;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        /// <summary>
        /// Invokes the watch dog and returns the result of the inspection.
        /// </summary>
        /// <param name="result">The object which stores the result of the watch dog inspection.</param>
        [ExcludeFromCodeCoverage]
        protected virtual void Inspect(WatchDogResult result)
        {
        }
    }
}
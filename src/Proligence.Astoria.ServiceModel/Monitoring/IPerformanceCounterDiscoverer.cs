﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPerformanceCounterDiscoverer.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Defines the API for classes which implement discovering performance counters which will be used by the service.
    /// </summary>
    public interface IPerformanceCounterDiscoverer
    {
        /// <summary>
        /// Discovers all performance counters required by the service.
        /// </summary>
        /// <typeparam name="TConfig">The type which represents service's configuration.</typeparam>
        /// <typeparam name="TApi">The interface which defines the service's API.</typeparam>
        /// <param name="service">The service for which performance counters will be discovered.</param>
        /// <returns>
        /// A sequence of <see cref="IPerformanceCounter"/> objects which represent the performance counters required
        /// by the service.
        /// </returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        IEnumerable<IPerformanceCounter> DiscoverPerformanceCounters<TConfig, TApi>(
            AstoriaService<TApi, TConfig> service)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration;
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPerformanceCounterParameters.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    /// <summary>
    /// Represents the parameters required to create and initialize a single <see cref="IPerformanceCounter"/>.
    /// </summary>
    public interface IPerformanceCounterParameters
    {
        /// <summary>
        /// Gets the name that will be associated with the performance counter.
        /// </summary>
        string CounterName { get; }

        /// <summary>
        /// Gets the name of the category associated with the performance counter.
        /// </summary>
        string CategoryName { get; }

        /// <summary>
        /// Gets an instance name for the performance counter. An empty string means that the counter is a
        /// single-instance counter.
        /// </summary>
        string InstanceName { get; }

        /// <summary>
        /// Gets the initial value of the performance counter.
        /// </summary>
        long InitialValue { get; }

        /// <summary>
        /// Gets a value indicating whether the initial value of the performance counter should be set when the
        /// <see cref="IPerformanceCounter"/> object is initialized.
        /// </summary>
        bool InitialValueSpecified { get; }
    }
}
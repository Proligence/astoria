﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaMonitoringOperationBehavior.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;
    using Autofac;
    using Proligence.Astoria.ServiceModel.Logging;

    /// <summary>
    /// Configures a service operation to use the <see cref="MonitoringOperationInvoker"/> implementation as the
    /// service's <see cref="IOperationInvoker"/>.
    /// </summary>
    public class AstoriaMonitoringOperationBehavior : IOperationBehavior
    {
        private readonly IContainer container;
        private readonly ServiceLogger logger;

        public AstoriaMonitoringOperationBehavior(string serviceName, IContainer container, ServiceLogger logger)
        {
            if (string.IsNullOrEmpty(serviceName))
            {
                throw new ArgumentNullException("serviceName");
            }

            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            this.ServiceName = serviceName;
            this.container = container;
            this.logger = logger;
        }

        public string ServiceName { get; private set; }

        /// <summary>
        /// Implement to confirm that the operation meets some intended criteria.
        /// </summary>
        /// <param name="operationDescription">
        /// The operation being examined. Use for examination only. If the operation description is modified, the
        /// results are undefined.
        /// </param>
        [ExcludeFromCodeCoverage]
        public void Validate(OperationDescription operationDescription)
        {
        }

        /// <summary>
        /// Implements a modification or extension of the service across an operation.
        /// </summary>
        /// <param name="operationDescription">
        /// The operation being examined. Use for examination only. If the operation description is modified, the
        /// results are undefined.
        /// </param>
        /// <param name="dispatchOperation">
        /// The run-time object that exposes customization properties for the operation described by
        /// <paramref name="operationDescription"/>.
        /// </param>
        public void ApplyDispatchBehavior(
            OperationDescription operationDescription,
            DispatchOperation dispatchOperation)
        {
            if (operationDescription == null)
            {
                throw new ArgumentNullException("operationDescription");
            }

            if (dispatchOperation == null)
            {
                throw new ArgumentNullException("dispatchOperation");
            }

            dispatchOperation.Invoker = new MonitoringOperationInvoker(
                this.ServiceName,
                operationDescription.Name,
                dispatchOperation.Invoker,
                this.container.Resolve<IPerformanceCounterFactory>(),
                this.logger);
        }

        /// <summary>
        /// Implements a modification or extension of the client across an operation.
        /// </summary>
        /// <param name="operationDescription">The operation being examined. Use for examination only. If the operation
        /// description is modified, the results are undefined.
        /// </param>
        /// <param name="clientOperation">
        /// The run-time object that exposes customization properties for the operation described by
        /// <paramref name="operationDescription"/>.
        /// </param>
        [ExcludeFromCodeCoverage]
        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }

        /// <summary>
        /// Implement to pass data at runtime to bindings to support custom behavior.
        /// </summary>
        /// <param name="operationDescription">
        /// The operation being examined. Use for examination only. If the operation description is modified, the
        /// results are undefined.
        /// </param>
        /// <param name="bindingParameters">
        /// The collection of objects that binding elements require to support the behavior.
        /// </param>
        [ExcludeFromCodeCoverage]
        public void AddBindingParameters(
            OperationDescription operationDescription,
            BindingParameterCollection bindingParameters)
        {
        }
    }
}
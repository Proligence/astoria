﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInternalWatchDog.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Defines the API for internal watch dogs.
    /// </summary>
    /// <typeparam name="TApi">The interface of the monitored service's API.</typeparam>
    /// <typeparam name="TConfig">The type which represents the configuration of the monitored service.</typeparam>
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    public interface IInternalWatchDog<TApi, TConfig>
        where TApi : class, IAstoriaService
        where TConfig : AstoriaServiceConfiguration
    {
        /// <summary>
        /// Gets or sets the monitored service instance.
        /// </summary>
        AstoriaService<TApi, TConfig> Service { get; set; }
    }
}
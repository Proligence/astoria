﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaMonitoringServiceBehavior.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Collections.ObjectModel;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;
    using Autofac;
    using Proligence.Astoria.ServiceModel.Logging;

    /// <summary>
    /// Configures a service to use the <see cref="MonitoringOperationInvoker"/> implementation as the service's
    /// <see cref="IOperationInvoker"/> for all operations.
    /// </summary>
    public class AstoriaMonitoringServiceBehavior : IServiceBehavior
    {
        private readonly IContainer container;
        private readonly ServiceLogger logger;

        public AstoriaMonitoringServiceBehavior(string serviceName, IContainer container, ServiceLogger logger)
        {
            if (string.IsNullOrEmpty(serviceName))
            {
                throw new ArgumentNullException("serviceName");
            }

            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            this.ServiceName = serviceName;
            this.container = container;
            this.logger = logger;
        }

        public string ServiceName { get; private set; }

        /// <summary>
        /// Provides the ability to inspect the service host and the service description to confirm that the service
        /// can run successfully.
        /// </summary>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="serviceHostBase">The service host that is currently being constructed.</param>
        [ExcludeFromCodeCoverage]
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        /// <summary>
        /// Provides the ability to pass custom data to binding elements to support the contract implementation.
        /// </summary>
        /// <param name="serviceDescription">The service description of the service.</param>
        /// <param name="serviceHostBase">The host of the service.</param>
        /// <param name="endpoints">The service endpoints.</param>
        /// <param name="bindingParameters">Custom objects to which binding elements have access.</param>
        [ExcludeFromCodeCoverage]
        public void AddBindingParameters(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase,
            Collection<ServiceEndpoint> endpoints,
            BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// Provides the ability to change run-time property values or insert custom extension objects such as error
        /// handlers, message or parameter interceptors, security extensions, and other custom extension objects.
        /// </summary>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="serviceHostBase">The host that is currently being built.</param>
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            if (serviceDescription == null)
            {
                throw new ArgumentNullException("serviceDescription");
            }

            foreach (ServiceEndpoint endpoint in serviceDescription.Endpoints)
            {
                foreach (OperationDescription operation in endpoint.Contract.Operations)
                {
                    if (operation.Behaviors.Find<AstoriaMonitoringOperationBehavior>() == null)
                    {
                        var behavior = new AstoriaMonitoringOperationBehavior(
                            this.ServiceName,
                            this.container,
                            this.logger);

                        operation.Behaviors.Add(behavior);
                    }
                }
            }
        }
    }
}
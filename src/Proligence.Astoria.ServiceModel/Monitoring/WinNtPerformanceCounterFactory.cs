// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WinNtPerformanceCounterFactory.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Implements creation of <see cref="WinNtPerformanceCounter"/> objects which encapsulate Windows NT performance
    /// counters.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly")]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    public class WinNtPerformanceCounterFactory : IPerformanceCounterFactory
    {
        private readonly IBehaviorManager behaviorManager;

        public WinNtPerformanceCounterFactory(IBehaviorManager behaviorManager)
        {
            this.behaviorManager = behaviorManager;
        }

        /// <summary>
        /// Creates a performance counter with the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters of the performance counter to create.</param>
        /// <returns>The created performance counter.</returns>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public IPerformanceCounter CreatePerformanceCounter(IPerformanceCounterParameters parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }

            if (string.IsNullOrEmpty(parameters.CounterName))
            {
                throw new ArgumentException("The specified counter name is invalid.", "parameters");
            }

            if (!this.behaviorManager.GetRequiredServiceBehavior<IMonitoringBehavior>().PerformanceCountersEnabled)
            {
                return new NullPerformanceCounter(
                    parameters.CounterName,
                    parameters.CategoryName,
                    parameters.InstanceName);
            }

            var counter = new PerformanceCounter
            {
                CounterName = parameters.CounterName,
                ReadOnly = false
            };

            if (parameters.CategoryName != null)
            {
                counter.CategoryName = parameters.CategoryName;
            }

            if (parameters.InstanceName != null)
            {
                counter.InstanceName = parameters.InstanceName;
            }

            return new WinNtPerformanceCounter(counter);
        }
    }
}
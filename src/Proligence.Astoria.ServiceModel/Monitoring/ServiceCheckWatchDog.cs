﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceCheckWatchDog.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Implements a watch dog which checks an Astoria service at the specified address.
    /// </summary>
    /// <typeparam name="TService">The type which implements the monitored service.</typeparam>
    /// <typeparam name="TApi">The interface of the monitored service's API.</typeparam>
    /// <typeparam name="TConfig">The type which represents the configuration of the monitored service.</typeparam>
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    [SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")]
    public abstract class ServiceCheckWatchDog<TService, TApi, TConfig> : InternalWatchDog<TService, TApi, TConfig>
        where TService : AstoriaService<TApi, TConfig>
        where TApi : class, IAstoriaService
        where TConfig : AstoriaServiceConfiguration
    {
        private readonly IAstoriaChannelFactory channelFactory;

        protected ServiceCheckWatchDog(IAstoriaChannelFactory channelFactory)
        {
            this.channelFactory = channelFactory;
        }

        /// <summary>
        /// Gets or sets the address of the Astoria service to call.
        /// </summary>
        protected string ServiceAddress { get; set; }

        protected override void Inspect(WatchDogResult result)
        {
            IAstoriaService channel = this.channelFactory.CreateChannel<IAstoriaService>(this.ServiceAddress);
            channel.GetServiceInfo();
        }
    }
}
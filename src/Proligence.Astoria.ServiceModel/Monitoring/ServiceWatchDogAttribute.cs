﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceWatchDogAttribute.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;

    /// <summary>
    /// An attribute which describes classes which implement service watch dogs.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ServiceWatchDogAttribute : Attribute
    {
        public ServiceWatchDogAttribute()
        {
            this.Enabled = true;
        }

        /// <summary>
        /// Gets or sets the description of the watch dog.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the watch dog is enabled.
        /// </summary>
        public bool Enabled { get; set; }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MemoryWatchDog.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------
namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Globalization;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.ServiceModel.Windows;

    /// <summary>
    /// Implements a watch dog which checks the available disk space on the service's host.
    /// </summary>
    [ServiceWatchDog(Description = "Checks if the service's host has at least 5% of free memory.")]
    public class MemoryWatchDog : ServiceWatchDog
    {
        /// <summary>
        /// Free memory threshold (in percent).
        /// </summary>
        private const int FreeMemoryThreshold = 5;

        private readonly IMemoryInfo memoryInfo;

        public MemoryWatchDog(IMemoryInfo memoryInfo)
        {
            this.memoryInfo = memoryInfo;
        }

        protected override void Inspect(WatchDogResult result)
        {
            if (result == null)
            {
                throw new ArgumentNullException("result");
            }

            MemoryStatus memoryStatus = this.memoryInfo.GetGlobalMemoryStatus();
            if (memoryStatus.MemoryLoad > 100 - FreeMemoryThreshold)
            {
                string message = string.Format(
                        CultureInfo.CurrentCulture,
                        "Low memory ({0}%) detected. Machine name: {1}.",
                        100 - memoryStatus.MemoryLoad,
                        Environment.MachineName);

                result.AddWarning(message);
            }
        }
    }
}
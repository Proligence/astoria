﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DiskSpaceWatchDog.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------
namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Globalization;
    using System.IO;
    using Proligence.Astoria.Client;

    /// <summary>
    /// Implements a watch dog which checks the available disk space on the service's host.
    /// </summary>
    [ServiceWatchDog(Description = "Checks if the service's host has at least 5% of free disk space on each drive.")]
    public class DiskSpaceWatchDog : ServiceWatchDog
    {
        /// <summary>
        /// Free disk space threshold (in percent).
        /// </summary>
        private const int FreeDiskSpaceThreshold = 5;

        protected override void Inspect(WatchDogResult result)
        {
            if (result == null)
            {
                throw new ArgumentNullException("result");
            }

            foreach (DriveInfo driveInfo in DriveInfo.GetDrives())
            {
                if (driveInfo.DriveType != DriveType.Fixed)
                {
                    continue;
                }

                long size = driveInfo.TotalSize;
                long free = driveInfo.AvailableFreeSpace;
                
                double ratio = (double)free / size * 100;
                
                if (ratio < FreeDiskSpaceThreshold)
                {
                    string message = string.Format(
                        CultureInfo.CurrentCulture,
                        "Low disk space ({0}%) on drive {1}. Machine name: {2}.",
                        ratio,
                        driveInfo.Name,
                        System.Environment.MachineName);
                    
                    result.AddWarning(message);
                }
            }
        }
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPerformanceCounter.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;

    /// <summary>
    /// Represents the interface for objects which implement performance counters.
    /// </summary>
    public interface IPerformanceCounter : IDisposable
    {
        /// <summary>
        /// Gets the name of the performance counter that is associated with this <see cref="IPerformanceCounter"/>
        /// instance.
        /// </summary>
        string CounterName { get; }

        /// <summary>
        /// Gets the name of the category associated with the performance counter.
        /// </summary>
        string CategoryName { get; }

        /// <summary>
        /// Gets an instance name for this performance counter. An empty string means that the counter is a
        /// single-instance counter.
        /// </summary>
        string InstanceName { get; }

        /// <summary>
        /// Gets or sets the raw, or uncalculated, value of this counter.
        /// </summary>
        long RawValue { get; set; }

        /// <summary>
        /// Increments the associated performance counter by the specified value in a thread-safe manner.
        /// </summary>
        /// <param name="value">The value to increment by.</param>
        /// <returns>The incremented counter value.</returns>
        long Increment(long value = 1L);
        
        /// <summary>
        /// Decrements the associated performance counter by the specified value in a thread-safe manner.
        /// </summary>
        /// <param name="value">The value to decrement by</param>
        /// <returns>The decremented counter value.</returns>
        long Decrement(long value = 1L);
    }
}
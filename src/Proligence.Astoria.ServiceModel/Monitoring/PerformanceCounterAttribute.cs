﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PerformanceCounterAttribute.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Implements an attribute which stores the parameters required to create and initialize a single
    /// <see cref="IPerformanceCounter"/>.
    /// </summary>
    [SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes")]
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class PerformanceCounterAttribute : Attribute, IPerformanceCounterParameters
    {
        /// <summary>
        /// The initial value of the performance counter.
        /// </summary>
        private long initialValue;

        public PerformanceCounterAttribute(string counterName)
        {
            if (string.IsNullOrEmpty(counterName))
            {
                throw new ArgumentNullException("counterName");
            }

            this.CounterName = counterName;
        }

        /// <summary>
        /// Gets the name that will be associated with the performance counter.
        /// </summary>
        public string CounterName { get; private set; }

        /// <summary>
        /// Gets or sets the name of the category associated with the performance counter. If this property is
        /// <c>null</c>, then the name of the service will be used. If the property is set to a custom category name,
        /// then the category and counter must be manually created.
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// Gets or sets an instance name for the performance counter. An empty string means that the counter is a
        /// single-instance counter.
        /// </summary>
        public string InstanceName { get; set; }

        /// <summary>
        /// Gets or sets the initial value of the performance counter.
        /// </summary>
        public long InitialValue
        {
            get
            {
                return this.initialValue;
            }

            set
            {
                this.initialValue = value;
                this.InitialValueSpecified = true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the initial value of the performance counter should be set when the
        /// <see cref="IPerformanceCounter" /> object is initialized.
        /// </summary>
        public bool InitialValueSpecified { get; private set; }
    }
}
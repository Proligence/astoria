// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WinNtPerformanceCounterInstaller.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Implements installing and initializing Windows NT performance counters.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly")]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    public class WinNtPerformanceCounterInstaller : IPerformanceCounterInstaller
    {
        /// <summary>
        /// Installs and initializes the specified performance counters on the local machine.
        /// </summary>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <typeparam name="TApi">The interface type which defines the service's API.</typeparam>
        /// <param name="service">The service for which performance counters will be installed.</param>
        /// <param name="counters">The performance counters to initialize.</param>
        public void InstallPerformanceCounters<TConfig, TApi>(
            AstoriaService<TApi, TConfig> service,
            IEnumerable<IPerformanceCounter> counters)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (service == null)
            {
                throw new ArgumentNullException("service");
            }

            if (counters == null)
            {
                throw new ArgumentNullException("counters");
            }

            var counterNames = new List<string>();
            var countersToInstall = new List<IPerformanceCounter>();

            foreach (IPerformanceCounter counter in counters)
            {
                if (!counterNames.Contains(counter.CounterName))
                {
                    counterNames.Add(counter.CounterName);
                    countersToInstall.Add(counter);
                }
            }
            
            this.InstallPerformanceCounters(service.ServiceName, countersToInstall);
        }

        /// <summary>
        /// Installs the specified performance counter into the specified category.
        /// </summary>
        /// <param name="categoryName">The name of the category of the performance counter.</param>
        /// <param name="counters">The performance counter objects to create.</param>
        [ExcludeFromCodeCoverage]
        protected virtual void InstallPerformanceCounters(
            string categoryName,
            IEnumerable<IPerformanceCounter> counters)
        {
            if (counters == null)
            {
                throw new ArgumentNullException("counters");
            }

            if (PerformanceCounterCategory.Exists(categoryName))
            {
                PerformanceCounterCategory.Delete(categoryName);
            }

            var counterDataCollection = new CounterCreationDataCollection();
            foreach (IPerformanceCounter counter in counters)
            {
                counterDataCollection.Add(
                    new CounterCreationData
                    {
                        CounterType = PerformanceCounterType.NumberOfItems64, 
                        CounterName = counter.CounterName
                    });
            }

            PerformanceCounterCategory.Create(
                categoryName,
                categoryName,
                PerformanceCounterCategoryType.MultiInstance, 
                counterDataCollection);
        }
    }
}
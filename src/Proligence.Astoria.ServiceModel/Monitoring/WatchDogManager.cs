﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WatchDogManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Autofac;
    using Proligence.Astoria.Client;

    public class WatchDogManager : IWatchDogManager
    {
        private readonly IBehaviorManager behaviorManager;
        private readonly object syncLock = new object();

        public WatchDogManager(IComponentContext container, IBehaviorManager behaviorManager)
        {
            this.behaviorManager = behaviorManager;
            this.Container = container;
        }

        protected IComponentContext Container { get; private set; }

        /// <summary>
        /// Gets a dictionary which maps watch dog identifiers to their descriptors.
        /// </summary>
        protected IDictionary<Guid, WatchDogInfo> Descriptors { get; private set; }

        /// <summary>
        /// Gets a dictionary which maps watch dog identifiers to factory methods which instantiate them.
        /// </summary>
        protected Dictionary<Guid, Func<IServiceWatchDog>> Factories { get; private set; }

        public void Initialize()
        {
            var watchDogsBehavior = this.behaviorManager.GetRequiredServiceBehavior<IWatchDogsBehavior>();

            string[] disabledWatchDogs = new string[0];
            if (!string.IsNullOrEmpty(watchDogsBehavior.DisabledWatchDogs))
            {
                disabledWatchDogs = watchDogsBehavior.DisabledWatchDogs.Split(',');
                for (int i = 0; i < disabledWatchDogs.Length; i++)
                {
                    disabledWatchDogs[i] = disabledWatchDogs[i].Trim();
                }
            }

            lock (this.syncLock)
            {
                this.Descriptors = new Dictionary<Guid, WatchDogInfo>();
                this.Factories = new Dictionary<Guid, Func<IServiceWatchDog>>();

                foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies().Where(a => !a.IsDynamic))
                {
                    foreach (Type type in assembly.GetExportedTypes())
                    {
                        if (type.GetInterfaces().Any(i => i == typeof(IServiceWatchDog))
                            && !type.IsAbstract
                            && !type.IsGenericType)
                        {
                            var watchDogAttribute = (ServiceWatchDogAttribute)Attribute.GetCustomAttribute(
                                type,
                                typeof(ServiceWatchDogAttribute));

                            if ((watchDogAttribute != null) && watchDogAttribute.Enabled)
                            {
                                if (disabledWatchDogs.All(name => type.FullName != name))
                                {
                                    var watchDogInfo = new WatchDogInfo(type.FullName, watchDogAttribute.Description);
                                    this.Descriptors.Add(watchDogInfo.Id, watchDogInfo);

                                    Type watchDogType = type;
                                    this.Factories.Add(
                                        watchDogInfo.Id, () => (IServiceWatchDog)this.Container.Resolve(watchDogType));
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the <see cref="WatchDogInfo"/> objects for all registered watch dogs.
        /// </summary>
        /// <returns>The <see cref="WatchDogInfo"/> objects.</returns>
        public IEnumerable<WatchDogInfo> GetWatchDogs()
        {
            this.EnsureInitialized();

            var watchDogsBehavior = this.behaviorManager.GetRequiredServiceBehavior<IWatchDogsBehavior>();
            if (!watchDogsBehavior.WatchDogsEnabled)
            {
                return new WatchDogInfo[0];
            }

            lock (this.syncLock)
            {
                return this.Descriptors.Values.ToArray();
            }
        }

        /// <summary>
        /// Creates a new watch dog represented by the specified <see cref="WatchDogInfo"/>.
        /// </summary>
        /// <param name="watchDogInfo">The watch dog descriptor.</param>
        /// <returns>The created <see cref="IServiceWatchDog"/> object.</returns>
        public IServiceWatchDog CreateWatchDog(WatchDogInfo watchDogInfo)
        {
            if (watchDogInfo == null)
            {
                throw new ArgumentNullException("watchDogInfo");
            }

            this.EnsureInitialized();

            lock (this.syncLock)
            {
                Func<IServiceWatchDog> watchDogFactory;
                if (this.Factories.TryGetValue(watchDogInfo.Id, out watchDogFactory))
                {
                    return watchDogFactory();
                }

                throw new ArgumentException(
                    "The specified watch dog descriptor does not represent a registered watch dog.",
                    "watchDogInfo");
            }
        }

        /// <summary>
        /// Throws an <see cref="InvalidOperationException"/> if the <see cref="Initialize"/> method was not called
        /// for this <see cref="WatchDogManager"/>.
        /// </summary>
        private void EnsureInitialized()
        {
            if ((this.Descriptors == null) || (this.Factories == null))
            {
                throw new InvalidOperationException("The watch dog manager is not initialized.");
            }
        }
    }
}
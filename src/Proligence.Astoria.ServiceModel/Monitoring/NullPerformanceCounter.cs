﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NullPerformanceCounter.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Implements a stub for the <see cref="IPerformanceCounter"/> interface to be used when performance counters
    /// are disabled.
    /// </summary>
    [SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
    public class NullPerformanceCounter : IPerformanceCounter
    {
        /// <summary>
        /// Used to synchronize access to the <see cref="RawValue"/> property.
        /// </summary>
        private readonly object valueLock = new object();

        /// <summary>
        /// The value of the performance counter.
        /// </summary>
        private long rawValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="NullPerformanceCounter"/> class.
        /// </summary>
        /// <param name="counterName">The name of the performance counter.</param>
        /// <param name="categoryName">The name of the performance counter's category.</param>
        /// <param name="instanceName">The name of the performance counter's instance.</param>
        /// <param name="value">The initial value of the performance counter.</param>
        public NullPerformanceCounter(string counterName, string categoryName, string instanceName, long value = 0)
        {
            this.CounterName = counterName;
            this.CategoryName = categoryName;
            this.InstanceName = instanceName;
            this.rawValue = value;
        }

        /// <summary>
        /// Gets the name of the performance counter that is associated with this <see cref="IPerformanceCounter"/>
        /// instance.
        /// </summary>
        public string CounterName { get; private set; }

        /// <summary>
        /// Gets the name of the performance counter category for this performance counter.
        /// </summary>
        public string CategoryName { get; private set; }

        /// <summary>
        /// Gets an instance name for this performance counter. An empty string means that the counter is a
        /// single-instance counter.
        /// </summary>
        public string InstanceName { get; private set; }

        /// <summary>
        /// Gets or sets the raw, or uncalculated, value of this counter.
        /// </summary>
        public long RawValue
        {
            get
            {
                lock (this.valueLock)
                {
                    return this.rawValue;
                }
            }

            set
            {
                lock (this.valueLock)
                {
                    this.rawValue = value;
                }   
            }
        }

        /// <summary>
        /// Increments the associated performance counter by the specified value in a thread-safe manner.
        /// </summary>
        /// <param name="value">The value to increment by.</param>
        /// <returns>The incremented counter value.</returns>
        public long Increment(long value = 1)
        {
            lock (this.valueLock)
            {
                return this.RawValue = this.RawValue + value;
            }
        }

        /// <summary>
        /// Decrements the associated performance counter by the specified value in a thread-safe manner.
        /// </summary>
        /// <param name="value">The value to decrement by</param>
        /// <returns>The decremented counter value.</returns>
        public long Decrement(long value = 1)
        {
            lock (this.valueLock)
            {
                return this.RawValue = this.RawValue - value;
            }
        }

        [ExcludeFromCodeCoverage]
        [SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
        [SuppressMessage("Microsoft.Usage", "CA1816:CallGCSuppressFinalizeCorrectly")]
        public void Dispose()
        {
        }
    }
}
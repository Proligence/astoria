﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWatchDogManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.Client;

    /// <summary>
    /// Defines the API of the watch dog manager.
    /// </summary>
    public interface IWatchDogManager
    {
        /// <summary>
        /// Initializes the watch dog manager.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Gets the <see cref="WatchDogInfo"/> objects for all registered watch dogs.
        /// </summary>
        /// <returns>The <see cref="WatchDogInfo"/> objects.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IEnumerable<WatchDogInfo> GetWatchDogs();

        /// <summary>
        /// Creates a new watch dog represented by the specified <see cref="WatchDogInfo"/>.
        /// </summary>
        /// <param name="watchDogInfo">The watch dog descriptor.</param>
        /// <returns>The created <see cref="IServiceWatchDog"/> object.</returns>
        IServiceWatchDog CreateWatchDog(WatchDogInfo watchDogInfo);
    }
}
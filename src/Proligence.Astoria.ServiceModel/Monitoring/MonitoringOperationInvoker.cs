﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MonitoringOperationInvoker.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceModel.Dispatcher;
    using Proligence.Astoria.ServiceModel.Logging;

    /// <summary>
    /// Wraps a <see cref="IOperationInvoker"/> implementation and updates standard performance counters for Astoria
    /// services.
    /// </summary>
    public class MonitoringOperationInvoker : IOperationInvoker
    {
        private readonly ServiceLogger logger;

        public MonitoringOperationInvoker(
            string serviceName,
            string methodName,
            IOperationInvoker baseInvoker,
            IPerformanceCounterFactory counterFactory,
            ServiceLogger logger)
        {
            this.logger = logger;
            if (string.IsNullOrEmpty(serviceName))
            {
                throw new ArgumentNullException("serviceName");
            }

            if (string.IsNullOrEmpty(methodName))
            {
                throw new ArgumentNullException("methodName");
            }

            if (baseInvoker == null)
            {
                throw new ArgumentNullException("baseInvoker");
            }

            if (counterFactory == null)
            {
                throw new ArgumentNullException("counterFactory");
            }

            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            this.ServiceName = serviceName;
            this.MethodName = methodName;
            this.BaseInvoker = baseInvoker;

            this.TotalCallCounter = counterFactory.CreatePerformanceCounter(
                CreateCounterParameters(ServicePerformanceCounters.TotalCallCounterName, serviceName, methodName));

            this.FailedCallCounter = counterFactory.CreatePerformanceCounter(
                CreateCounterParameters(ServicePerformanceCounters.FailedCallCounterName, serviceName, methodName));
                
            this.CallDurationCounter = counterFactory.CreatePerformanceCounter(
                CreateCounterParameters(ServicePerformanceCounters.CallDurationCounterName, serviceName, methodName));
        }

        /// <summary>
        /// Gets the name of the service.
        /// </summary>
        public string ServiceName { get; private set; }
        
        /// <summary>
        /// Gets the name of the service operation method associated with the invoker.
        /// </summary>
        public string MethodName { get; private set; }

        /// <summary>
        /// Gets the base <see cref="IOperationInvoker"/>.
        /// </summary>
        public IOperationInvoker BaseInvoker { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the <see cref="Invoke"/> or <see cref="InvokeBegin"/> method is called
        /// by the dispatcher.
        /// </summary>
        /// <returns><c>true</c> if the dispatcher invokes the synchronous operation; otherwise, false.</returns>
        public bool IsSynchronous
        {
            get { return this.BaseInvoker.IsSynchronous; }
        }

        /// <summary>
        /// Gets the counter which presents total number of method calls.
        /// </summary>
        internal IPerformanceCounter TotalCallCounter { get; private set; }

        /// <summary>
        /// Gets the counter which presents the number of failed method calls.
        /// </summary>
        internal IPerformanceCounter FailedCallCounter { get; private set; }

        /// <summary>
        /// Gets the counter which presents the duration of the last call.
        /// </summary>
        internal IPerformanceCounter CallDurationCounter { get; private set; }

        /// <summary>
        /// Returns an <see cref="Array"/> of parameter objects.
        /// </summary>
        /// <returns>The parameters that are to be used as arguments to the operation.</returns>
        public object[] AllocateInputs()
        {
            return this.BaseInvoker.AllocateInputs();
        }

        /// <summary>
        /// Returns an object and a set of output objects from an instance and set of input objects.
        /// </summary>
        /// <param name="instance">The object to be invoked.</param>
        /// <param name="inputs">The inputs to the method.</param>
        /// <param name="outputs">The outputs from the method.</param>
        /// <returns>The return value.</returns>
        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            this.Try(this.TotalCallCounter, pc => pc.Increment());
            
            var stopwatch = new Stopwatch();
            object result;

            stopwatch.Start();
            try
            {
                result = this.BaseInvoker.Invoke(instance, inputs, out outputs);
            }
            catch (Exception)
            {
                this.Try(this.FailedCallCounter, pc => pc.Increment());
                throw;
            }
            finally
            {
                stopwatch.Stop();
                long elapsed = stopwatch.ElapsedMilliseconds;
                this.Try(this.CallDurationCounter, pc => pc.RawValue = elapsed);
            }

            return result;
        }

        /// <summary>
        /// An asynchronous implementation of the <see cref="Invoke"/> method.
        /// </summary>
        /// <param name="instance">The object to be invoked.</param>
        /// <param name="inputs">The inputs to the method.</param>
        /// <param name="callback">The asynchronous callback object.</param>
        /// <param name="state">Associated state data.</param>
        /// <returns>A <see cref="IAsyncResult"/> used to complete the asynchronous call.</returns>
        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            return this.BaseInvoker.InvokeBegin(instance, inputs, callback, state);
        }

        /// <summary>
        /// The asynchronous end method.
        /// </summary>
        /// <param name="instance">The object invoked.</param>
        /// <param name="outputs">The outputs from the method.</param>
        /// <param name="result">The <see cref="IAsyncResult"/> object.</param>
        /// <returns>The return value.</returns>
        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            return this.BaseInvoker.InvokeEnd(instance, out outputs, result);
        }

        /// <summary>
        /// Creates the <see cref="IPerformanceCounterParameters"/> object which describes the specified performance
        /// counter for the specified method and service.
        /// </summary>
        /// <param name="counterName">The name of the performance counter.</param>
        /// <param name="serviceName">The name of the service associated with the performance counter.</param>
        /// <param name="methodName">The name of the method associated with the performance counter.</param>
        /// <returns>The created performance counter parameters.</returns>
        private static IPerformanceCounterParameters CreateCounterParameters(
            string counterName,
            string serviceName,
            string methodName)
        {
            return new PerformanceCounterAttribute(counterName)
            {
                CategoryName = serviceName,
                InstanceName = methodName
            };
        }

        /// <summary>
        /// Performs the specified action on a <see cref="IPerformanceCounter"/> with recovery from potential errors.
        /// </summary>
        /// <param name="performanceCounter">The performance counter instance.</param>
        /// <param name="action">The action which will be performed on the performance counter.</param>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void Try(IPerformanceCounter performanceCounter, Action<IPerformanceCounter> action)
        {
            try
            {
                action(performanceCounter);
            }
            catch (Exception ex)
            {
                this.logger.LogWarning("Failed to update performance counter. " + ex.Message);
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WinNtPerformanceCounter.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Implements the <see cref="IPerformanceCounter"/> interface using Windows NT performance counters mechanism.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly")]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    public class WinNtPerformanceCounter : IPerformanceCounter
    {
        public WinNtPerformanceCounter(PerformanceCounter counter)
        {
            if (counter == null)
            {
                throw new ArgumentNullException("counter");
            }

            this.Counter = counter;
        }

        /// <summary>
        /// Gets the underlying <see cref="PerformanceCounter"/>.
        /// </summary>
        public PerformanceCounter Counter { get; private set; }

        /// <summary>
        /// Gets the name of the performance counter that is associated with this <see cref="IPerformanceCounter"/>
        /// instance.
        /// </summary>
        public string CounterName
        {
            get { return this.Counter.CounterName; }
        }

        /// <summary>
        /// Gets the name of the performance counter category for this performance counter.
        /// </summary>
        public string CategoryName
        {
            get { return this.Counter.CategoryName; }
        }

        /// <summary>
        /// Gets an instance name for this performance counter. An empty string means that the counter is a
        /// single-instance counter.
        /// </summary>
        public string InstanceName
        {
            get { return this.Counter.InstanceName; }
        }

        /// <summary>
        /// Gets or sets the raw, or uncalculated, value of this counter.
        /// </summary>
        public long RawValue
        {
            [ExcludeFromCodeCoverage]
            get
            {
                return this.Counter.RawValue;
            }

            [ExcludeFromCodeCoverage]
            set
            {
                this.Counter.RawValue = value;
            }
        }

        /// <summary>
        /// Increments the associated performance counter by the specified value in a thread-safe manner.
        /// </summary>
        /// <param name="value">The value to increment by.</param>
        /// <returns>The incremented counter value.</returns>
        [ExcludeFromCodeCoverage]
        public long Increment(long value = 1)
        {
            return this.Counter.IncrementBy(value);
        }

        /// <summary>
        /// Decrements the associated performance counter by the specified value in a thread-safe manner.
        /// </summary>
        /// <param name="value">The value to decrement by</param>
        /// <returns>The decremented counter value.</returns>
        [ExcludeFromCodeCoverage]
        public long Decrement(long value = 1)
        {
            return this.Counter.IncrementBy(value * -1);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Counter.Dispose();
            }
        }
    }
}
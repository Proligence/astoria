﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServicePerformanceCounters.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Monitoring
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Encapsulates all performance counters used by the service.
    /// </summary>
    public class ServicePerformanceCounters
    {
        /// <summary>
        /// The name of the <see cref="TotalCallCount"/> performance counter.
        /// </summary>
        public const string TotalCallCounterName = "TotalCallCount";

        /// <summary>
        /// The name of the <see cref="FailedCallCount"/> performance counter.
        /// </summary>
        public const string FailedCallCounterName = "FailedCallCount";

        /// <summary>
        /// The name of the <see cref="CallDuration"/> performance counter.
        /// </summary>
        public const string CallDurationCounterName = "CallDuration";
        
        // The counters defined below should not be used directly. The properites are there to allow the
        // reflection-based performance counter to discoverer and install the counters.
        // ReSharper disable UnusedMember.Local

        /// <summary>
        /// Gets or sets the counter which presents the total call count for each service method.
        /// </summary>
        [PerformanceCounter(TotalCallCounterName)]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private IPerformanceCounter TotalCallCount { get; set; }

        /// <summary>
        /// Gets or sets the counter which presents the failed call count for each service method.
        /// </summary>
        [PerformanceCounter(FailedCallCounterName)]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private IPerformanceCounter FailedCallCount { get; set; }

        /// <summary>
        /// Gets or sets the counter which presents the last call duration for each service method.
        /// </summary>
        [PerformanceCounter(CallDurationCounterName)]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private IPerformanceCounter CallDuration { get; set; }

        // ReSharper restore UnusedMember.Local
    }
}
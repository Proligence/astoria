﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExtensionsBehavior.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Extensions
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Astoria behavior for configuring service extensions.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    [SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes")]
    [SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix")]
    public class ExtensionsBehavior : Attribute, IExtensionsBehavior
    {
        /// <summary>
        /// Gets or sets a value indicating whether service extensions may be used.
        /// </summary>
        public bool ExtensionsEnabled { get; set; }

        /// <summary>
        /// Called to configure the service's extensions.
        /// </summary>
        /// <param name="extensionManager">The service extension manager.</param>
        public virtual void ConfigureExtensions(IServiceExtensionManager extensionManager)
        {
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceExtensionManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Extensions
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Implements the service extension manager.
    /// </summary>
    public class ServiceExtensionManager : IServiceExtensionManager
    {
        /// <summary>
        /// The registered service extensions.
        /// </summary>
        private readonly IEnumerable<IServiceExtension> extensions;

        /// <summary>
        /// Maps extension names to their <see cref="IServiceExtension"/> objects.
        /// </summary>
        private readonly IDictionary<string, IServiceExtension> extensionsMap;
        
        public ServiceExtensionManager(IEnumerable<IServiceExtension> extensions)
        {
            if (extensions == null)
            {
                throw new ArgumentNullException("extensions");
            }

            this.extensions = extensions;
            this.extensionsMap = new Dictionary<string, IServiceExtension>();
        }

        /// <summary>
        /// Gets a value indicating whether the service extension manager is initialized.
        /// </summary>
        public bool IsInitialized { get; private set; }

        /// <summary>
        /// Initializes the service extension manager.
        /// </summary>
        public void Initialize()
        {
            if (!this.IsInitialized)
            {
                this.IsInitialized = true;

                foreach (IServiceExtension extension in this.extensions)
                {
                    this.RegisterExtension(extension);
                }
            }
        }

        /// <summary>
        /// Gets the service extension with the specified name.
        /// </summary>
        /// <param name="name">The name of the extension to get.</param>
        /// <returns>The service extension or <c>null</c> if there is no extension with the specified name.</returns>
        public IServiceExtension GetExtension(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            this.EnsureInitialized();

            IServiceExtension extension;
            if (this.extensionsMap.TryGetValue(name, out extension))
            {
                return extension;
            }

            return null;
        }

        /// <summary>
        /// Registers a service extension.
        /// </summary>
        /// <param name="extension">The service extension to register.</param>
        public void RegisterExtension(IServiceExtension extension)
        {
            if (extension == null)
            {
                throw new ArgumentNullException("extension");
            }

            if (string.IsNullOrEmpty(extension.Name))
            {
                throw new InvalidOperationException(
                    "The service extension '" + extension.GetType().FullName + "' must specify a name.");
            }

            this.EnsureInitialized();

            if (this.extensionsMap.ContainsKey(extension.Name))
            {
                throw new InvalidOperationException("Duplicate service extension name: " + extension.Name);
            }

            this.extensionsMap.Add(extension.Name, extension);
        }

        /// <summary>
        /// Unregisters a service extension.
        /// </summary>
        /// <param name="name">The name of the service extension to unregister.</param>
        public void UnregisterExtension(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            this.EnsureInitialized();

            bool result = this.extensionsMap.Remove(name);

            if (!result)
            {
                throw new ArgumentException("Failed to find a service extension with the specified name.", "name");
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidOperationException"/> if the service extension manager is not initialized.
        /// </summary>
        private void EnsureInitialized()
        {
            if (!this.IsInitialized)
            {
                throw new InvalidOperationException("The service extension manager is not initialized.");
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IServiceExtensionManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Extensions
{
    /// <summary>
    /// Defines the API of the service extension manager.
    /// </summary>
    public interface IServiceExtensionManager
    {
        /// <summary>
        /// Gets a value indicating whether the service extension manager is initialized.
        /// </summary>
        bool IsInitialized { get; }

        /// <summary>
        /// Gets the service extension with the specified name.
        /// </summary>
        /// <param name="name">The name of the extension to get.</param>
        /// <returns>The service extension or <c>null</c> if there is no extension with the specified name.</returns>
        IServiceExtension GetExtension(string name);

        /// <summary>
        /// Initializes the service extension manager.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Registers a service extension.
        /// </summary>
        /// <param name="extension">The service extension to register.</param>
        void RegisterExtension(IServiceExtension extension);

        /// <summary>
        /// Unregisters a service extension.
        /// </summary>
        /// <param name="name">The name of the service extension to unregister.</param>
        void UnregisterExtension(string name);
    }
}
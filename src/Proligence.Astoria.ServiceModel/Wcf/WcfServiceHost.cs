﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WcfServiceHost.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Wcf
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;

    /// <summary>
    /// Internal wrapper for WCF service hosts.
    /// </summary>
    public class WcfServiceHost
    {
        public WcfServiceHost(ServiceHost serviceHost)
        {
            this.ServiceHost = serviceHost;
        }

        /// <summary>
        /// Gets the wrapped WCF service host.
        /// </summary>
        public ServiceHost ServiceHost { get; private set; }

        /// <summary>
        /// Gets the WCF service description.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public virtual ServiceDescription Description
        {
            get
            {
                return this.ServiceHost.Description;
            }
        }

        /// <summary>
        /// Gets the WCF service behaviors.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public virtual KeyedByTypeCollection<IServiceBehavior> Behaviors
        {
            get
            {
                return this.ServiceHost.Description.Behaviors;
            }
        }

        /// <summary>
        /// Adds a service endpoint to the hosted service with a specified contract, binding, and endpoint address.
        /// </summary>
        /// <returns>
        /// The <see cref="ServiceEndpoint"/> added to the hosted service.
        /// </returns>
        /// <param name="implementedContract">The <see cref="Type"/> of contract for the endpoint added.</param>
        /// <param name="binding">The <see cref="Binding"/> for the endpoint added.</param>
        /// <param name="address">The address for the endpoint added.</param>
        [ExcludeFromCodeCoverage]
        public virtual ServiceEndpoint AddServiceEndpoint(Type implementedContract, Binding binding, string address)
        {
            return this.ServiceHost.AddServiceEndpoint(implementedContract, binding, address);
        }

        /// <summary>
        /// Opens the service host.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public virtual void Open()
        {
            this.ServiceHost.Open();
        }

        /// <summary>
        /// Closes the service host.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public virtual void Close()
        {
            this.ServiceHost.Close();
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWcfHostConfigurator.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Wcf
{
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel.Hosting;

    /// <summary>
    /// Specifies the API for objects which configure WCF hosts for Astoria services.
    /// </summary>
    public interface IWcfHostConfigurator
    {
        /// <summary>
        /// Gets the Astoria binding which will be used by the service.
        /// </summary>
        AstoriaBinding Binding { get; }

        /// <summary>
        /// Sets up WCF service behaviors for the Astoria service.
        /// </summary>
        /// <typeparam name="TApi">
        /// The interface which represents the API of the service which will be configured.
        /// </typeparam>
        /// <typeparam name="TConfig">
        /// The type which represents the configuration of the service which will be configured.
        /// </typeparam>
        /// <param name="host">The Astoria service host which will be configured.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        void SetupWcfBehaviors<TApi, TConfig>(AstoriaServiceHost<TApi, TConfig> host)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration;

        /// <summary>
        /// Configures an Astoria binding for the specified service host.
        /// </summary>
        /// <typeparam name="TApi">
        /// The interface which represents the API of the service which will be configured.
        /// </typeparam>
        /// <typeparam name="TConfig">
        /// The type which represents the configuration of the service which will be configured.
        /// </typeparam>
        /// <param name="host">The Astoria service host which will be configured.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        void ConfigureAstoriaBinding<TApi, TConfig>(AstoriaServiceHost<TApi, TConfig> host)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration;
    }
}
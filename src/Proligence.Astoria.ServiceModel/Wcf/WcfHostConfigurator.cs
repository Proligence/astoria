﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WcfHostConfigurator.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Wcf
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel.Hosting;
    using Proligence.Astoria.ServiceModel.Logging;
    using Proligence.Astoria.ServiceModel.Monitoring;

    /// <summary>
    /// Configures a WCF host for Astoria services.
    /// </summary>
    public class WcfHostConfigurator : IWcfHostConfigurator
    {
        public WcfHostConfigurator()
        {
            this.Binding = new AstoriaBinding();
        }

        /// <summary>
        /// Gets the Astoria binding which will be used by the service.
        /// </summary>
        public AstoriaBinding Binding { get; private set; }

        /// <summary>
        /// Sets up WCF service behaviors for the Astoria service.
        /// </summary>
        /// <typeparam name="TApi">
        /// The interface which represents the API of the service which will be configured.
        /// </typeparam>
        /// <typeparam name="TConfig">
        /// The type which represents the configuration of the service which will be configured.
        /// </typeparam>
        /// <param name="host">The Astoria service host which will be configured.</param>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public virtual void SetupWcfBehaviors<TApi, TConfig>(AstoriaServiceHost<TApi, TConfig> host)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (host == null)
            {
                throw new ArgumentNullException("host");
            }

            KeyedByTypeCollection<IServiceBehavior> behaviors = host.WcfServiceHost.Behaviors;

            ServiceDebugBehavior debugBehavior = behaviors.Find<ServiceDebugBehavior>();
            debugBehavior.IncludeExceptionDetailInFaults = true;

            ServiceBehaviorAttribute serviceBehaviorAttribute = behaviors.Find<ServiceBehaviorAttribute>();
            serviceBehaviorAttribute.InstanceContextMode = InstanceContextMode.Single;

            if (host.UseMultithreading)
            {
                serviceBehaviorAttribute.ConcurrencyMode = ConcurrencyMode.Multiple;
            }

            if (host.MonitoringEnabled)
            {
                if (behaviors.Find<AstoriaMonitoringServiceBehavior>() == null)
                {
                    var behavior = new AstoriaMonitoringServiceBehavior(
                        host.Service.ServiceName,
                        host.Service.Container,
                        host.Service.Logger ?? new NullLogger());

                    behaviors.Add(behavior);
                }
            }
        }

        /// <summary>
        /// Configures an Astoria binding for the specified service host.
        /// </summary>
        /// <typeparam name="TApi">
        /// The interface which represents the API of the service which will be configured.
        /// </typeparam>
        /// <typeparam name="TConfig">
        /// The type which represents the configuration of the service which will be configured.
        /// </typeparam>
        /// <param name="host">The Astoria service host which will be configured.</param>
        public virtual void ConfigureAstoriaBinding<TApi, TConfig>(AstoriaServiceHost<TApi, TConfig> host)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (host == null)
            {
                throw new ArgumentNullException("host");
            }

            host.WcfServiceHost.AddServiceEndpoint(typeof(TApi), this.Binding, "ast");
        }
    }
}
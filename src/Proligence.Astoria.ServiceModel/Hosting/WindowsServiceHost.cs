﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowsServiceHost.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Hosting
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceProcess;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Hosts an Astoria service in a Windows service.
    /// </summary>
    /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
    /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
    internal class WindowsServiceHost<TApi, TConfig> : AstoriaServiceHost<TApi, TConfig>
        where TApi : class, IAstoriaService
        where TConfig : AstoriaServiceConfiguration
    {
        /// <summary>
        /// The object which implements the Windows Service which hosts the Astoria service.
        /// </summary>
        private readonly AstoriaWindowsService windowsService;

        public WindowsServiceHost(AstoriaService<TApi, TConfig> service)
            : base(service)
        {
            this.windowsService = new AstoriaWindowsService();
            this.windowsService.ServiceStarted += this.OnServiceStarted;
            this.windowsService.ServiceStopped += this.OnServiceStopped;
        }

        /// <summary>
        /// Initializes and starts the service host.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public override void StartHost()
        {
            ServiceBase.Run(this.windowsService);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.windowsService.Dispose();
            }

            base.Dispose(disposing);
        }

        private void OnServiceStarted(object sender, EventArgs e)
        {
            this.Service.LogMessage(this.Service.ServiceName + ": starting service.");

            try
            {
                this.StartWcfHost();
            }
            catch (Exception ex)
            {
                this.Service.LogError(this.Service.ServiceName + ": failed to start WCF service.", ex);
                throw;
            }
        }

        private void OnServiceStopped(object sender, EventArgs e)
        {
            this.Service.LogMessage(this.Service.ServiceName + ": stopping service.");
            this.Service.Shutdown();

            try 
            {
                this.StopWcfHost();
            }
            catch (Exception ex) 
            {
                this.Service.LogError(this.Service.ServiceName + ": failed to close WCF service.", ex);
                throw;
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceHost.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Hosting
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.ServiceModel;
    using Autofac;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel.Wcf;

    /// <summary>
    /// Base class for classes which implement hosting Astoria services.
    /// </summary>
    /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
    /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    public abstract class AstoriaServiceHost<TApi, TConfig> : IDisposable
        where TApi : class, IAstoriaService
        where TConfig : AstoriaServiceConfiguration
    {
        protected AstoriaServiceHost(AstoriaService<TApi, TConfig> service)
        {
            this.Service = service;
        }

        /// <summary>
        /// Occurs when the service's WCF host is started.
        /// </summary>
        public event EventHandler WcfHostStarting;

        /// <summary>
        /// Gets or sets a value indicating whether a new thread will be used for each service call.
        /// </summary>
        public bool UseMultithreading { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the service should use Astoria's monitoring mechanisms.
        /// </summary>
        public bool MonitoringEnabled { get; set; }

        /// <summary>
        /// Gets a value indicating whether the service host has been disposed.
        /// </summary>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Gets the object which implements the Windows Service WCF host.
        /// </summary>
        public WcfServiceHost WcfServiceHost { get; internal set; }

        /// <summary>
        /// Gets the object which implements the Astoria service.
        /// </summary>
        protected internal AstoriaService<TApi, TConfig> Service { get; private set; }

        /// <summary>
        /// Initializes and starts the service host.
        /// </summary>
        public abstract void StartHost();

        /// <summary>
        /// Initializes and starts the WCF service host.
        /// </summary>
        public virtual void StartWcfHost()
        {
            this.StopWcfHost();

            if ((this.Service.Configuration == null) || (this.Service.Configuration.Service == null))
            {
                throw new InvalidOperationException("The service's configuration is missing the Service section.");
            }

            if (string.IsNullOrWhiteSpace(this.Service.Configuration.Service.Address))
            {
                throw new InvalidOperationException("The service's configuration is missing the service's address.");
            }

            var address = new Uri(this.Service.Configuration.Service.Address);
            ServiceHost wcfServiceHost = this.CreateWcfServiceHost(address);
            this.WcfServiceHost = this.CreateWcfServiceHostWrapper(wcfServiceHost);

            var hostConfigurator = this.Service.Container.Resolve<IWcfHostConfigurator>();
            hostConfigurator.SetupWcfBehaviors(this);
            hostConfigurator.ConfigureAstoriaBinding(this);

            this.OnServiceHostStarting(EventArgs.Empty);

            var behaviors = this.Service.BehaviorManager.GetServiceBehaviors<IWcfHostBehavior>().ToArray();
            
            foreach (IWcfHostBehavior wcfHostBehavior in behaviors)
            {
                wcfHostBehavior.InitializeWcfHost(this.WcfServiceHost);
            }

            this.WcfServiceHost.Open();

            foreach (IWcfHostBehavior wcfHostBehavior in behaviors)
            {
                wcfHostBehavior.StartWcfHost(this.WcfServiceHost);
            }

            this.Service.LogMessage(
                this.Service.ServiceName + ": WCF service started." + Environment.NewLine +
                "Service address: " + address);
        }

        /// <summary>
        /// Stops the WCF service host.
        /// </summary>
        public virtual void StopWcfHost()
        {
            if (this.WcfServiceHost != null)
            {
                var behaviors = this.Service.BehaviorManager.GetServiceBehaviors<IWcfHostBehavior>();
                foreach (IWcfHostBehavior wcfHostBehavior in behaviors)
                {
                    wcfHostBehavior.CloseWcfHost(this.WcfServiceHost);
                }

                this.WcfServiceHost.Close();
                this.WcfServiceHost = null;

                this.Service.LogMessage(this.Service.ServiceName + ": WCF service closed.");
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);

            this.IsDisposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        /// <summary>
        /// Creates the WCF host for the service.
        /// </summary>
        /// <param name="address">The address to which will be bound to the WCF host.</param>
        /// <returns>The created <see cref="ServiceHost"/> instance.</returns>
        [ExcludeFromCodeCoverage]
        protected virtual ServiceHost CreateWcfServiceHost(Uri address)
        {
            return new ServiceHost(this.Service, address);
        }

        /// <summary>
        /// Creates the WCF host wrapper for the service.
        /// </summary>
        /// <param name="serviceHost">The WCF service host.</param>
        /// <returns>The created <see cref="WcfServiceHost"/> instance.</returns>
        [ExcludeFromCodeCoverage]
        protected virtual WcfServiceHost CreateWcfServiceHostWrapper(ServiceHost serviceHost)
        {
            return new WcfServiceHost(serviceHost);
        }

        [ExcludeFromCodeCoverage]
        protected virtual void OnServiceHostStarting(EventArgs e)
        {
            EventHandler handler = this.WcfHostStarting;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
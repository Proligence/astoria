﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProcessHost.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Hosting
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Threading;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Hosts an Astoria service in the current process.
    /// </summary>
    /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
    /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
    [ExcludeFromCodeCoverage]
    internal class ProcessHost<TApi, TConfig> : AstoriaServiceHost<TApi, TConfig>
        where TApi : class, IAstoriaService
        where TConfig : AstoriaServiceConfiguration
    {
        public ProcessHost(AstoriaService<TApi, TConfig> service)
            : base(service)
        {
            Console.CancelKeyPress += this.OnCancelKeyPress;
        }

        /// <summary>
        /// Initializes and starts the WCF service host.
        /// </summary>
        public override void StartHost()
        {
            this.StartWcfHost();
            Thread.Sleep(Timeout.Infinite);
        }

        /// <summary>
        /// Called when the <see cref="ConsoleModifiers.Control"/> modifier key (CTRL) and <see cref="ConsoleKey.C"/> 
        /// console key (C) are pressed simultaneously (CTRL+C).
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ConsoleCancelEventArgs"/> instance containing the event data.</param>
        private void OnCancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            this.Service.Shutdown();
            this.StopWcfHost();
        }
    }
}
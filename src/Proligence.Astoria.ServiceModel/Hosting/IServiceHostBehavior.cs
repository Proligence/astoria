﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IServiceHostBehavior.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Hosting
{
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Astoria service behavior which controls the host which the service runs in.
    /// </summary>
    [AstoriaServiceBehavior(AllowMultiple = false)]
    public interface IServiceHostBehavior : IAstoriaServiceBehavior
    {
        /// <summary>
        /// Gets or sets a value indicating whether a new thread will be used for each service call.
        /// </summary>
        bool UseMultithreading { get; set; }

        /// <summary>
        /// Creates a service host for the specified Astoria service.
        /// </summary>
        /// <param name="service">The service for which service host will be created.</param>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <returns>The created service host.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        AstoriaServiceHost<TApi, TConfig> CreateServiceHost<TApi, TConfig>(AstoriaService<TApi, TConfig> service)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration;

        /// <summary>
        /// Extension point called when the service is starting up.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <param name="service">The service for which service host will be created.</param>
        /// <param name="host">The service's host.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        void OnStartup<TApi, TConfig>(
            AstoriaService<TApi, TConfig> service,
            AstoriaServiceHost<TApi, TConfig> host)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration;

        /// <summary>
        /// Extension point called when the service is shutting up.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <param name="service">The service for which service host will be created.</param>
        /// <param name="host">The service's host.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        void OnShutdown<TApi, TConfig>(
            AstoriaService<TApi, TConfig> service,
            AstoriaServiceHost<TApi, TConfig> host)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration;
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationBehaviorReader.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.XPath;
    using Proligence.Astoria.Configuration;
    using Proligence.Helpers;

    /// <summary>
    /// Implements reading behaviors from the service's XML configuration.
    /// </summary>
    internal class ConfigurationBehaviorReader
    {
        /// <summary>
        /// Stores discovered service behavior types.
        /// </summary>
        private Type[] serviceBehaviorTypes;

        /// <summary>
        /// Gets a sequence of added service behaviors found by the
        /// <see cref="ReadServiceBehaviorsFromConfiguration"/> method.
        /// </summary>
        public IEnumerable<IAstoriaServiceBehavior> AddedServiceBehaviors { get; private set; }

        /// <summary>
        /// Gets a sequence of removed service behavior types found by the
        /// <see cref="ReadServiceBehaviorsFromConfiguration"/> method.
        /// </summary>
        public IEnumerable<Type> RemovedServiceBehaviors { get; private set; }

        /// <summary>
        /// Reads all service behaviors from the specified service configuration.
        /// </summary>
        /// <param name="configuration">The service configuration to read service behaviors from.</param>
        [SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly")]
        public void ReadServiceBehaviorsFromConfiguration(AstoriaServiceConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (configuration.Xml == null)
            {
                throw new ArgumentException("Configuration XML document not found.", "configuration");
            }

            var addedBehaviors = new List<IAstoriaServiceBehavior>();
            this.AddedServiceBehaviors = addedBehaviors;

            var removedBehaviors = new List<Type>();
            this.RemovedServiceBehaviors = removedBehaviors;

            XPathNavigator naviagator = configuration.Xml.CreateNavigator();

            // ReSharper disable once PossibleNullReferenceException
            XPathNodeIterator rootIterator = naviagator.Select("/");
            XmlNode rootNode = ((IHasXmlNode)rootIterator.Current).GetNode()
                .ChildNodes
                .Cast<XmlNode>()
                .FirstOrDefault(n => n.NodeType == XmlNodeType.Element);

            // ReSharper disable once PossibleNullReferenceException
            XmlNode serviceNode = rootNode.ChildNodes.Cast<XmlNode>().FirstOrDefault(n => n.Name == "Service");
            if (serviceNode == null)
            {
                return;
            }

            XmlNode behaviorsNode = serviceNode.ChildNodes.Cast<XmlNode>().FirstOrDefault(n => n.Name == "Behaviors");
            if (behaviorsNode == null)
            {
                return;
            }

            this.serviceBehaviorTypes = DiscoverServiceBehaviors();

            foreach (XmlNode behaviorNode in behaviorsNode.ChildNodes)
            {
                Type behaviorType = this.serviceBehaviorTypes.FirstOrDefault(t => t.FullName == behaviorNode.Name);
                if (behaviorType == null)
                {
                    string message =
                        @"Failed to find behavior type '" + behaviorNode.Name + @"'. Make sure that the class is " +
                        @"implemented and loaded into the current AppDomain.";

                    throw new ServiceConfigurationException(message, null, "Behaviors", behaviorNode.Name, null);
                }

                if (IsAddedBehavior(behaviorNode))
                {
                    IAstoriaServiceBehavior behavior = CreateServiceBehaviorFromXmlNode(behaviorNode, behaviorType);
                    addedBehaviors.Add(behavior);
                }
                else
                {
                    removedBehaviors.Add(behaviorType);
                }
            }
        }

        /// <summary>
        /// Discovers all non-abstract behavior implementations.
        /// </summary>
        /// <returns>An array of behavior types.</returns>
        private static Type[] DiscoverServiceBehaviors()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(a => a.GetTypes().Where(
                    t => typeof(IAstoriaServiceBehavior).IsAssignableFrom(t) && t.IsClass && !t.IsAbstract))
                .ToArray();
        }

        /// <summary>
        /// Determines whether the specified XML node represents an added behavior.
        /// </summary>
        /// <param name="behaviorNode">
        /// The XML node which represents the behavior in the service's configuration.
        /// </param>
        /// <returns>
        /// <c>true</c> if the XML node represents an added behavior or <c>false</c> if the XML node represents a
        /// removed behavior.
        /// </returns>
        private static bool IsAddedBehavior(XmlNode behaviorNode)
        {
            if (behaviorNode.Attributes != null)
            {
                IEnumerable<XmlAttribute> attributes = behaviorNode.Attributes.OfType<XmlAttribute>();
                if (attributes.Any(a => (a.Name == "remove") && (a.Value.ToUpperInvariant() == "TRUE")))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Parses a behavior from an <see cref="XmlNode"/>.
        /// </summary>
        /// <param name="behaviorNode">
        /// The XML node which represents the behavior in the service's configuration.
        /// </param>
        /// <param name="behaviorType">The type of the behavior represented by the XML node.</param>
        /// <returns>The created service behavior object.</returns>
        private static IAstoriaServiceBehavior CreateServiceBehaviorFromXmlNode(
            XmlNode behaviorNode,
            Type behaviorType)
        {
            var behavior = (IAstoriaServiceBehavior)Activator.CreateInstance(behaviorType);

            foreach (XmlNode propertyNode in behaviorNode.ChildNodes)
            {
                var bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
                PropertyInfo propertyInfo = behaviorType.GetProperty(propertyNode.Name, bindingFlags);
                if (propertyInfo == null)
                {
                    string message = string.Format(
                        CultureInfo.CurrentCulture,
                        "The service behavior '{0}' does not have a property '{1}'.",
                        behaviorNode.Name,
                        propertyNode.Name);

                    throw new ServiceConfigurationException(
                        message, null, behaviorNode.Name, propertyNode.Name, null);
                }

                string nodeValue = string.IsNullOrEmpty(propertyNode.InnerXml) 
                    ? propertyNode.InnerText
                    : propertyNode.InnerXml;

                object value = nodeValue;

                if (string.IsNullOrEmpty(nodeValue))
                {
                    value = null;
                }
                else if (propertyInfo.PropertyType != typeof(string))
                {
                    try
                    {
                        value = TypeConverter.ChangeType(
                            value,
                            propertyInfo.PropertyType,
                            CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex)
                    {
                        string message = string.Format(
                            CultureInfo.CurrentCulture,
                            "Failed to convert '{0}' to type '{1}'.",
                            propertyNode.InnerText,
                            propertyInfo.PropertyType.FullName);

                        throw new ServiceConfigurationException(
                            message, ex, behaviorNode.Name, propertyNode.Name, propertyNode.InnerText);
                    }
                }

                propertyInfo.SetValue(behavior, value, new object[0]);
            }

            return behavior;
        }
    }
}
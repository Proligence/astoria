﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaWindowsService.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceProcess;

    /// <summary>
    /// Implements a Windows Service which hosts an Astoria service.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class AstoriaWindowsService : ServiceBase
    {
        /// <summary>
        /// Occurs when the service is started.
        /// </summary>
        public event EventHandler ServiceStarted;

        /// <summary>
        /// Occurs when the service is stopped.
        /// </summary>
        public event EventHandler ServiceStopped;

        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service 
        /// Control Manager (SCM) or when the operating system starts (for a service that starts automatically). 
        /// Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        protected override void OnStart(string[] args)
        {
            this.OnServiceStarted(EventArgs.Empty);
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the service by the Service 
        /// Control Manager (SCM). Specifies actions to take when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            this.OnServiceStopped(EventArgs.Empty);
        }

        protected virtual void OnServiceStarted(EventArgs e)
        {
            EventHandler handler = this.ServiceStarted;
            if (handler != null) 
            {
                handler(this, e);
            }
        }

        protected virtual void OnServiceStopped(EventArgs e)
        {
            EventHandler handler = this.ServiceStopped;
            if (handler != null) 
            {
                handler(this, e);
            }
        }
    }
}
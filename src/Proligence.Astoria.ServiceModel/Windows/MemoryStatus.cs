﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MemoryStatus.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Windows
{
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Contains information about the current state of both physical and virtual memory, including extended memory.
    /// The <c>GlobalMemoryStatusEx</c> function stores information in this structure.
    /// </summary>
    [ExcludeFromCodeCoverage]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public sealed class MemoryStatus
    {
        /* ReSharper disable UnusedMember.Local */
        /* ReSharper disable FieldCanBeMadeReadOnly.Local */
        /* ReSharper disable UnassignedField.Local */
        /* ReSharper disable NotAccessedField.Local */

        /// <summary>
        /// The size of the structure, in bytes. You must set this member before calling <c>GlobalMemoryStatusEx</c>.
        /// </summary>
        private uint length;

        /// <summary>
        /// A number between 0 and 100 that specifies the approximate percentage of physical memory that is in use
        /// (0 indicates no memory use and 100 indicates full memory use).
        /// </summary>
        private uint memoryLoad;

        /// <summary>
        /// The amount of actual physical memory, in bytes.
        /// </summary>
        private long ullTotalPhys;

        /// <summary>
        /// The amount of physical memory currently available, in bytes. This is the amount of physical memory that
        /// can be immediately reused without having to write its contents to disk first. It is the sum of the size
        /// of the standby, free, and zero lists.
        /// </summary>
        private long ullAvailPhys;

        /// <summary>
        /// The current committed memory limit for the system or the current process, whichever is smaller, in bytes.
        /// To get the system-wide committed memory limit, call GetPerformanceInfo. 
        /// </summary>
        private long ullTotalPageFile;

        /// <summary>
        /// The maximum amount of memory the current process can commit, in bytes. This value is equal to or smaller
        /// than the system-wide available commit value. To calculate the system-wide available commit value, call
        /// <c>GetPerformanceInfo</c> and subtract the value of <c>CommitTotal</c> from the value of
        /// <c>CommitLimit</c>.
        /// </summary>
        private long ullAvailPageFile;

        /// <summary>
        /// The size of the user-mode portion of the virtual address space of the calling process, in bytes. This
        /// value depends on the type of process, the type of processor, and the configuration of the operating system.
        /// For example, this value is approximately 2 GB for most 32-bit processes on an x86 processor and
        /// approximately 3 GB for 32-bit processes that are large address aware running on a system with 4-gigabyte
        /// tuning enabled.
        /// </summary>
        private long ullTotalVirtual;

        /// <summary>
        /// The amount of unreserved and uncommitted memory currently in the user-mode portion of the virtual address
        /// space of the calling process, in bytes.
        /// </summary>
        private long ullAvailVirtual;

        /// <summary>
        /// Reserved. This value is always 0.
        /// </summary>
        // ReSharper disable once UnusedField.Compiler
        private long ullAvailExtendedVirtual;

        /* ReSharper restore NotAccessedField.Local */
        /* ReSharper restore UnassignedField.Local */
        /* ReSharper restore FieldCanBeMadeReadOnly.Local */
        /* ReSharper restore UnusedMember.Local */

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryStatus"/> class.
        /// </summary>
        public MemoryStatus()
        {
            this.length = (uint)Marshal.SizeOf(typeof(MemoryStatus));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryStatus" /> class.
        /// </summary>
        /// <param name="memoryLoad">
        /// A number between 0 and 100 that specifies the approximate percentage of physical memory that is in use.
        /// </param>
        /// <param name="totalPhysicalMemory">
        /// The amount of actual physical memory, in bytes.
        /// </param>
        /// <param name="availablePhysicalMemory">
        /// The amount of physical memory currently available, in bytes.
        /// </param>
        /// <param name="totalPageFileMemory">
        /// The current committed memory limit for the system or the current process, whichever is smaller, in bytes.
        /// </param>
        /// <param name="availablePageFileMemory">
        /// The maximum amount of memory the current process can commit, in bytes.
        /// </param>
        /// <param name="totalVirtualMemory">
        /// The size of the user-mode portion of the virtual address space of the calling process, in bytes.
        /// </param>
        /// <param name="availableVirtualMemory">
        /// The amount of unreserved and uncommitted memory currently in the user-mode portion of the virtual address
        /// space of the calling process, in bytes.
        /// </param>
        public MemoryStatus(
            uint memoryLoad,
            long totalPhysicalMemory,
            long availablePhysicalMemory,
            long totalPageFileMemory,
            long availablePageFileMemory,
            long totalVirtualMemory,
            long availableVirtualMemory)
        {
            this.memoryLoad = memoryLoad;
            this.ullAvailPhys = availablePhysicalMemory;
            this.ullTotalPhys = totalPhysicalMemory;
            this.ullTotalPageFile = totalPageFileMemory;
            this.ullAvailPageFile = availablePageFileMemory;
            this.ullTotalVirtual = totalVirtualMemory;
            this.ullAvailVirtual = availableVirtualMemory;
        }

        /// <summary>
        /// Gets a number between 0 and 100 that specifies the approximate percentage of physical memory that is in use
        /// (0 indicates no memory use and 100 indicates full memory use).
        /// </summary>
        public uint MemoryLoad
        {
            get { return this.length; }
        }

        /// <summary>
        /// Gets the amount of actual physical memory, in bytes.
        /// </summary>
        public long TotalPhysicalMemory
        {
            get { return this.ullTotalPhys; }
        }

        /// <summary>
        /// Gets the amount of physical memory currently available, in bytes.
        /// </summary>
        public long AvailablePhysicalMemory
        {
            get { return this.ullAvailPhys; }
        }

        /// <summary>
        /// Gets the current committed memory limit for the system or the current process, whichever is smaller,
        /// in bytes.
        /// </summary>
        public long TotalPageFileMemory
        {
            get { return this.ullTotalPageFile; }
        }

        /// <summary>
        /// Gets the maximum amount of memory the current process can commit, in bytes.
        /// </summary>
        public long AvailablePageFileMemory
        {
            get { return this.ullAvailPageFile; }
        }

        /// <summary>
        /// Gets the size of the user-mode portion of the virtual address space of the calling process, in bytes.
        /// </summary>
        public long TotalVirtualMemory
        {
            get { return this.ullTotalVirtual; }
        }

        /// <summary>
        /// Gets the amount of unreserved and uncommitted memory currently in the user-mode portion of the virtual
        /// address space of the calling process, in bytes.
        /// </summary>
        public long AvailableVirtualMemory
        {
            get { return this.ullAvailVirtual; }
        }
    }
}
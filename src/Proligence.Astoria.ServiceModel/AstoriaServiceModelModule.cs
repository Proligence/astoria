﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceModelModule.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Autofac;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.ServiceModel.Deployment;
    using Proligence.Astoria.ServiceModel.Discovery;
    using Proligence.Astoria.ServiceModel.Extensions;
    using Proligence.Astoria.ServiceModel.Logging;
    using Proligence.Astoria.ServiceModel.Monitoring;
    using Proligence.Astoria.ServiceModel.Wcf;
    using Proligence.Astoria.ServiceModel.Windows;

    /// <summary>
    /// Configures the dependency injection container for Astoria services.
    /// </summary>
    public class AstoriaServiceModelModule : BasicServiceModule
    {
        [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType(typeof(Environment)).As<IEnvironment>();
            builder.RegisterType(typeof(StringBuilderLog)).As<ILog>().InstancePerDependency();
            builder.RegisterType(typeof(WcfHostConfigurator)).As<IWcfHostConfigurator>();
            builder.RegisterType(typeof(ServiceDiscoveryServer)).As<IServiceDiscoveryServer>().SingleInstance();

            /* Deployment */

            builder.RegisterType(typeof(AstoriaDeploymentProvider))
                .As<AstoriaDeploymentProvider>()
                .As<IDeploymentProvider>();

            builder.RegisterType(typeof(EventLogManager)).As<IEventLogManager>();
            builder.RegisterType(typeof(PerformanceCounterManager)).As<IPerformanceCounterManager>();
            builder.RegisterType(typeof(WindowsServiceManager)).As<IWindowsServiceManager>();

            /* Extensions */

            builder.RegisterType<ServiceExtensionManager>()
                .As<IServiceExtensionManager>()
                .SingleInstance();

            builder.RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies())
                .AssignableTo<IServiceExtension>()
                .AsImplementedInterfaces();
            
            /* Monitoring */

            builder.RegisterType<WinNtPerformanceCounterFactory>()
                .As<IPerformanceCounterFactory>()
                .SingleInstance();
            
            builder.RegisterType<WinNtPerformanceCounterInstaller>()
                .As<IPerformanceCounterInstaller>()
                .SingleInstance();

            builder.RegisterType<ReflectionPerformanceCounterDiscoverer>()
                .As<IPerformanceCounterDiscoverer>()
                .SingleInstance();

            builder.RegisterType<ServicePerformanceCounters>();

            /* Windows */

            builder.RegisterType<MemoryInfo>().As<IMemoryInfo>();
        }
    }
}
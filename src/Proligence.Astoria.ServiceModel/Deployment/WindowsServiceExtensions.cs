﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowsServiceExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Deployment
{
    using System;
    using System.IO;
    using Proligence.Astoria.ServiceModel.Logging;

    public static class WindowsServiceExtensions
    {
        /// <summary>
        /// The maximum length of a service name.
        /// </summary>
        internal const int MaximumServiceNameLength = 255;

        /// <summary>
        /// The maximum length of a service display name.
        /// </summary>
        internal const int MaximumServiceDisplayNameLength = 255;

        /// <summary>
        /// Registers a new Windows Service.
        /// </summary>
        /// <param name="provider">The deployment provider.</param>
        /// <param name="logger">The object which should be used to log messages.</param>
        /// <param name="name">The name of the service.</param>
        /// <param name="displayName">The display name of the service.</param>
        /// <param name="path">The full path to the service application.</param>
        /// <returns>
        /// <c>true</c> if the service was registered or <c>false</c> if the service is already registered.
        /// </returns>
        public static bool CreateWindowsService(
            this IDeploymentProvider provider, ServiceLogger logger, string name, string displayName, string path)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException("name");
            }

            if (name.Length > MaximumServiceNameLength)
            {
                throw new ArgumentException(
                    "The maximum name for a service is " + MaximumServiceNameLength + " characters.", 
                    "name");
            }

            if (name.Contains("\\") || name.Contains("/"))
            {
                throw new ArgumentException(
                    @"Forward-slash (/) and backslash (\) are not valid service name characters.", "name");
            }

            if (string.IsNullOrEmpty(displayName))
            {
                displayName = name;
            }

            if (displayName.Length > MaximumServiceDisplayNameLength)
            {
                throw new ArgumentException(
                    "The maximum display name for a service is " + MaximumServiceDisplayNameLength + " characters.", 
                    "displayName");
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullException("path");
            }

            if (!File.Exists(path))
            {
                throw new ArgumentException("The file '" + path + "' does not exist.", "path");
            }

            IntPtr scmHandle = provider.WindowsServiceManager.OpenServiceControlManager();

            try
            {
                IntPtr? serviceHandle = provider.WindowsServiceManager.OpenService(scmHandle, name);
                if (serviceHandle != null)
                {
                    provider.WindowsServiceManager.CloseService(serviceHandle.Value);
                    return false;
                }
                
                provider.WindowsServiceManager.CreateService(scmHandle, name, displayName, path);
                logger.LogMessage("Created service: " + name);
            }
            finally
            {
                if (!provider.WindowsServiceManager.CloseServiceControlManager(scmHandle))
                {
                    logger.LogWarning("Failed to close handle to Service Control Manager.");
                }
            }

            return true;
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDeploymentProvider.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Deployment
{
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel.Logging;

    /// <summary>
    /// Implements installation of an Astoria service.
    /// </summary>
    public interface IDeploymentProvider
    {
        /// <summary>
        /// Gets the Event Log manager.
        /// </summary>
        IEventLogManager EventLogManager { get; }

        /// <summary>
        /// Gets the performance counter manager.
        /// </summary>
        IPerformanceCounterManager PerformanceCounterManager { get; }

        /// <summary>
        /// Gets the Windows Service manager.
        /// </summary>
        IWindowsServiceManager WindowsServiceManager { get; }

        /// <summary>
        /// Installs the Astoria service.
        /// </summary>
        /// <param name="serviceName">The service's name.</param>
        /// <param name="configuration">The service's configuration.</param>
        /// <param name="logger">The object which should be used to log messages.</param>
        void PerformInstall(string serviceName, AstoriaServiceConfiguration configuration, ServiceLogger logger);
    }
}
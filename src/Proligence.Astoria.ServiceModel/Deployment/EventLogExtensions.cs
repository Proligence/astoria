﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventLogExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Deployment
{
    using System;
    using System.Diagnostics;
    using Proligence.Astoria.ServiceModel.Logging;

    public static class EventLogExtensions
    {
        public static bool CreateEventLog(
            this IDeploymentProvider provider, ServiceLogger logger, string logName, string source)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (string.IsNullOrWhiteSpace(logName))
            {
                throw new ArgumentNullException("logName");
            }

            if (string.IsNullOrWhiteSpace(source))
            {
                throw new ArgumentNullException("source");
            }

            try 
            {
                if (provider.EventLogManager.LogExists(logName)) 
                {
                    return false;
                }

                var creationData = new EventSourceCreationData(source, logName);
                provider.EventLogManager.CreateLog(creationData);
            }
            catch (Exception ex)
            {
                logger.LogError("Failed to create event log: " + logName, ex);
                throw;
            }

            logger.LogMessage("Created event log: " + logName);

            return true;
        }
    }
}
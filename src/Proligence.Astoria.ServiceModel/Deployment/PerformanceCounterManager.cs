﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PerformanceCounterManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Deployment
{
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class PerformanceCounterManager : IPerformanceCounterManager
    {
        /// <summary>
        /// Checks if the specified performance counter category exists.
        /// </summary>
        /// <param name="categoryName">The name of the performance counter category.</param>
        /// <returns><c>true</c> if the specified category exists; otherwise, <c>false</c>.</returns>
        public bool CategoryExists(string categoryName)
        {
            return PerformanceCounterCategory.Exists(categoryName);
        }

        /// <summary>
        /// Checks if the specified performance counter exists.
        /// </summary>
        /// <param name="counterName">The name of the performance counter.</param>
        /// <param name="categoryName">The name of the performance counter category.</param>
        /// <returns><c>true</c> if the specified category exists; otherwise, <c>false</c>.</returns>
        public bool CounterExists(string counterName, string categoryName)
        {
            return PerformanceCounterCategory.CounterExists(counterName, categoryName);
        }

        /// <summary>
        /// Creates a performance counter category with the specified performance counters.
        /// </summary>
        /// <param name="categoryName">The name of the performance counter category.</param>
        /// <param name="categoryDescription">The description of the performance counter category.</param>
        /// <param name="categoryType">The type of the performance counter category.</param>
        /// <param name="counters">The counters to create in the category.</param>
        public void CreateCategory(
            string categoryName, 
            string categoryDescription, 
            PerformanceCounterCategoryType categoryType, 
            CounterCreationDataCollection counters)
        {
            PerformanceCounterCategory.Create(categoryName, categoryDescription, categoryType, counters);
        }

        /// <summary>
        /// Deletes the specified performance counter category.
        /// </summary>
        /// <param name="categoryName">The name of the category to delete.</param>
        public void DeleteCategory(string categoryName)
        {
            PerformanceCounterCategory.Delete(categoryName);
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaDeploymentProvider.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Deployment
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Reflection;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel.Logging;

    /// <summary>
    /// Installs a standard Astoria service.
    /// </summary>
    public class AstoriaDeploymentProvider : IDeploymentProvider
    {
        public AstoriaDeploymentProvider(
            IEventLogManager eventLogManager, 
            IPerformanceCounterManager performanceCounterManager,
            IWindowsServiceManager windowsServiceManager)
        {
            this.EventLogManager = eventLogManager;
            this.PerformanceCounterManager = performanceCounterManager;
            this.WindowsServiceManager = windowsServiceManager;
        }

        public IEventLogManager EventLogManager { get; private set; }
        public IPerformanceCounterManager PerformanceCounterManager { get; private set; }
        public IWindowsServiceManager WindowsServiceManager { get; private set; }
        protected string ServiceName { get; private set; }
        protected AstoriaServiceConfiguration Configuration { get; private set; }
        protected ServiceLogger Logger { get; private set; }

        /// <summary>
        /// Installs the Astoria service.
        /// </summary>
        /// <param name="serviceName">The service's name.</param>
        /// <param name="configuration">The service's configuration.</param>
        /// <param name="logger">The object which should be used to log messages.</param>
        public virtual void PerformInstall(
            string serviceName, AstoriaServiceConfiguration configuration, ServiceLogger logger)
        {
            if (string.IsNullOrEmpty(serviceName))
            {
                throw new ArgumentNullException("serviceName");
            }

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            this.ServiceName = serviceName;
            this.Configuration = configuration;
            this.Logger = logger;

            this.CreateEventLog();
            this.CreateWindowsService();
        }

        protected virtual void CreateEventLog()
        {
            string logName = this.ServiceName.Replace(" ", string.Empty);
            if (this.Configuration.Service != null) 
            {
                if (this.Configuration.Service.Logging != null) 
                {
                    if (!string.IsNullOrWhiteSpace(this.Configuration.Service.Logging.EventLogName)) 
                    {
                        logName = this.Configuration.Service.Logging.EventLogName;
                    }
                }
            }

            this.CreateEventLog(this.Logger, logName, logName);
        }

        protected virtual void CreateWindowsService()
        {
            this.CreateWindowsService(
                this.Logger, this.ServiceName, this.ServiceName, this.GetEntryAssemblyLocation());
        }

        [ExcludeFromCodeCoverage]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual string GetEntryAssemblyLocation()
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();
            if (entryAssembly == null)
            {
                throw new InvalidOperationException("Failed to get entry assembly.");
            }

            return entryAssembly.Location;
        }
    }
}
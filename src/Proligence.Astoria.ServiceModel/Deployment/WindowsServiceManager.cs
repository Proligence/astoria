﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowsServiceManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Deployment
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Manages creation of Windows Services.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class WindowsServiceManager : IWindowsServiceManager
    {
        /// <summary>
        /// Required to call the <c>CreateService</c> function to create a service object and add it to the database.
        /// </summary>
        private const int ScManagerCreateService = 0x0002;

        /// <summary>
        /// Specifies to create a service that runs in its own process.
        /// </summary>
        private const int ServiceWin32OwnProcess = 0x00000010;

        /// <summary>
        /// Specifies a service started automatically by the service control manager during system startup.
        /// </summary>
        private const int ServiceAutoStart = 0x00000002;

        /// <summary>
        /// The startup program logs the error in the event log but continues the startup operation.
        /// </summary>
        private const int ServiceErrorNormal = 0x00000001;

        /// <summary>
        /// Standard rights.
        /// </summary>
        private const int StandardRightsRequired = 0xF0000;
        
        /// <summary>
        /// Required to call the <c>QueryServiceConfig</c> and <c>QueryServiceConfig2</c> functions to query the
        /// service configuration.
        /// </summary>
        private const int ServiceQueryConfig = 0x0001;

        /// <summary>
        /// Required to call the <c>ChangeServiceConfig</c> or <c>ChangeServiceConfig2</c> function to change the
        /// service configuration. Because this grants the caller the right to change the executable file that the
        /// system runs, it should be granted only to administrators.
        /// </summary>
        private const int ServiceChangeConfig = 0x0002;
        
        /// <summary>
        /// Required to call the <c>QueryServiceStatus</c> or <c>QueryServiceStatusEx</c> function to ask the service
        /// control manager about the status of the service.
        /// Required to call the <c>NotifyServiceStatusChange</c> function to receive notification when a service
        /// changes status.
        /// </summary>
        private const int ServiceQueryStatus = 0x0004;

        /// <summary>
        /// Required to call the <c>EnumDependentServices</c> function to enumerate all the services dependent on the
        /// service.
        /// </summary>
        private const int ServiceEnumerateDependents = 0x0008;
        
        /// <summary>
        /// Required to call the <c>StartService</c> function to start the service.
        /// </summary>
        private const int ServiceStart = 0x0010;

        /// <summary>
        /// Required to call the <c>ControlService</c> function to stop the service.
        /// </summary>
        private const int ServiceStop = 0x0020;

        /// <summary>
        /// Required to call the <c>ControlService</c> function to pause or continue the service.
        /// </summary>
        private const int ServicePauseContinue = 0x0040;

        /// <summary>
        /// Required to call the <c>ControlService</c> function to ask the service to report its status immediately.
        /// </summary>
        private const int ServiceInterrogate = 0x0080;

        /// <summary>
        /// Required to call the <c>ControlService</c> function to specify a user-defined control code.
        /// </summary>
        private const int ServiceUserDefinedControl = 0x0100;

        /// <summary>
        /// Represents all access rights.
        /// </summary>
        private const int ServiceAllAccess =
            StandardRightsRequired |
            ServiceQueryConfig |
            ServiceChangeConfig |
            ServiceQueryStatus |
            ServiceEnumerateDependents |
            ServiceStart |
            ServiceStop |
            ServicePauseContinue |
            ServiceInterrogate |
            ServiceUserDefinedControl;

        /// <summary>
        /// Opens the Service Control Manager on the local machine.
        /// </summary>
        /// <returns>The handle to the opened Service Control Manager.</returns>
        public IntPtr OpenServiceControlManager()
        {
            IntPtr scmHandle = OpenSCManager(null, null, ScManagerCreateService);
            if (scmHandle.ToInt64() == 0) 
            {
                ThrowExceptionWithCode("Failed to open Service Control Manager.");
            }

            return scmHandle;
        }

        /// <summary>
        /// Closes a handle to the Service Control Manager.
        /// </summary>
        /// <param name="scmHandle">The handle to the Service Control Manager.</param>
        /// <returns><c>true</c> if the handle was successfully closed; otherwise, <c>false</c>.</returns>
        public bool CloseServiceControlManager(IntPtr scmHandle)
        {
            uint result = CloseServiceHandle(scmHandle);
            return result != 0;
        }

        /// <summary>
        /// Opens a service with the specified name.
        /// </summary>
        /// <param name="scmHandle">The handle to the Service Control Manager.</param>
        /// <param name="serviceName">The name of the service to open.</param>
        /// <returns>The handle to the opened service.</returns>
        public IntPtr? OpenService(IntPtr scmHandle, string serviceName)
        {
            IntPtr serviceHandle = OpenService(scmHandle, serviceName, ServiceAllAccess);
            if (serviceHandle.ToInt64() != 0) 
            {
                return serviceHandle;
            }

            return null;
        }

        /// <summary>
        /// Closes a service handle.
        /// </summary>
        /// <param name="handle">The service handle to close.</param>
        /// <returns><c>true</c> if the handle was successfully closed; otherwise, <c>false</c>.</returns>
        public bool CloseService(IntPtr handle)
        {
            uint result = CloseServiceHandle(handle);
            return result != 0;
        }

        /// <summary>
        /// Creates a new service.
        /// </summary>
        /// <param name="scmHandle">The handle to the Service Control Manager.</param>
        /// <param name="serviceName">The name of the new service.</param>
        /// <param name="displayName">The display name of the new service.</param>
        /// <param name="path">The path to the service's executable file.</param>
        public void CreateService(IntPtr scmHandle, string serviceName, string displayName, string path)
        {
            IntPtr serviceHandle = CreateService(
                scmHandle: scmHandle,
                serviceName: serviceName,
                displayName: displayName,
                desiredAccess: ServiceAllAccess,
                serviceType: ServiceWin32OwnProcess,
                startType: ServiceAutoStart,
                errorControl: ServiceErrorNormal,
                pathName: path,
                loadOrderGroup: null,
                tagId: 0,
                dependencies: null,
                serviceStartName: null,
                password: null);

            if (serviceHandle.ToInt64() == 0)
            {
                ThrowExceptionWithCode("Failed to create service in Service Control Manager.");
            }
        }

        /// <summary>
        /// Throws an exception with the last error code from WinAPI.
        /// </summary>
        /// <param name="message">The message to include in the exception</param>
        private static void ThrowExceptionWithCode(string message)
        {
            int code = Marshal.GetLastWin32Error();
            throw new InvalidOperationException(message + Environment.NewLine + "Error code: " + code + ".");
        }

        /// <summary>
        /// Establishes a connection to the service control manager on the specified computer and opens the specified 
        /// service control manager database.
        /// </summary>
        /// <param name="machineName">
        /// The name of the target computer. If the pointer is NULL or points to an empty string, the function connects
        /// to the service control manager on the local computer.
        /// </param>
        /// <param name="scdb">
        /// The name of the service control manager database. This parameter should be set to SERVICES_ACTIVE_DATABASE.
        /// If it is NULL, the SERVICES_ACTIVE_DATABASE database is opened by default.
        /// </param>
        /// <param name="desiredAccess">The access to the service control manager.</param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the specified service control manager database.
        /// If the function fails, the return value is NULL.
        /// </returns>
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        [SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass")]
        private static extern IntPtr OpenSCManager(string machineName, string scdb, int desiredAccess);

        /// <summary>
        /// Closes a handle to a service control manager or service object.
        /// </summary>
        /// <param name="scmHandle">
        /// A handle to the service control manager object or the service object to close.
        /// </param>
        /// <returns>
        /// If the function succeeds, the return value is nonzero. If the function fails, the return value is zero.
        /// </returns>
        [DllImport("advapi32.dll", SetLastError = true)]
        [SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass")]
        private static extern uint CloseServiceHandle(IntPtr scmHandle);

        /// <summary>
        /// Opens an existing service.
        /// </summary>
        /// <param name="scmHandle">A handle to the service control manager database.</param>
        /// <param name="serviceName">The name of the service to be opened.</param>
        /// <param name="numServiceArgs">The access to the service.</param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the service. If the function fails, the return 
        /// value is NULL.
        /// </returns>
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        [SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass")]
        private static extern IntPtr OpenService(IntPtr scmHandle, string serviceName, int numServiceArgs);

        /// <summary>
        /// Creates a service object and adds it to the specified Service Control Manager database.
        /// </summary>
        /// <param name="scmHandle">A handle to the service control manager database.</param>
        /// <param name="serviceName">The name of the service to install.</param>
        /// <param name="displayName">
        /// The display name to be used by user interface programs to identify the service. 
        /// </param>
        /// <param name="desiredAccess">The access to the service.</param>
        /// <param name="serviceType">The service type.</param>
        /// <param name="startType">The service start options.</param>
        /// <param name="errorControl">
        /// The severity of the error, and action taken, if this service fails to start.
        /// </param>
        /// <param name="pathName">The fully-qualified path to the service binary file.</param>
        /// <param name="loadOrderGroup">
        /// The names of the load ordering group of which this service is a member.
        /// </param>
        /// <param name="tagId">
        /// A pointer to a variable that receives a tag value that is unique in the group specified in the 
        /// <paramref name="loadOrderGroup"/> parameter. Specify NULL if you are not changing the existing tag.
        /// </param>
        /// <param name="dependencies">
        /// A pointer to a double null-terminated array of null-separated names of services or load ordering groups 
        /// that the system must start before this service. Specify NULL or an empty string if the service has no 
        /// dependencies.
        /// </param>
        /// <param name="serviceStartName">The name of the account under which the service should run.</param>
        /// <param name="password">
        /// The password to the account name specified by the <c>lpServiceStartName</c> parameter. Specify an empty
        /// string if the account has no password or if the service runs in the LocalService, NetworkService, or
        /// LocalSystem account.</param>
        /// <returns>
        /// If the function succeeds, the return value is a handle to the service. If the function fails, the return 
        /// value is NULL.
        /// </returns>
        [DllImport("Advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        [SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass")]
        private static extern IntPtr CreateService(
            IntPtr scmHandle,
            string serviceName,
            string displayName,
            int desiredAccess,
            int serviceType,
            int startType,
            int errorControl,
            string pathName,
            string loadOrderGroup,
            int tagId,
            string dependencies,
            string serviceStartName,
            string password);
    }
}
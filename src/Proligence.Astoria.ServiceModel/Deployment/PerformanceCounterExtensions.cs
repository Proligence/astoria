﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PerformanceCounterExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Deployment
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Proligence.Astoria.ServiceModel.Logging;

    public static class PerformanceCounterExtensions
    {
        /// <summary>
        /// Creates the specified performance counter category with the specified counters.
        /// </summary>
        /// <param name="provider">The deployment provider.</param>
        /// <param name="logger">The object which should be used to log messages.</param>
        /// <param name="categoryName">The name of the performance counter category.</param>
        /// <param name="categoryDescription">The description of the performance counter category.</param>
        /// <param name="categoryType">The type of the performance counter category.</param>
        /// <param name="counters">The performance counters to create.</param>
        /// <returns>
        /// <c>false</c> if the category and counters already exists or <c>true</c> if the category and counters have 
        /// been registered.
        /// </returns>
        public static bool CreatePerformanceCounterCategory(
            this IDeploymentProvider provider, 
            ServiceLogger logger,
            string categoryName,
            string categoryDescription,
            PerformanceCounterCategoryType categoryType = PerformanceCounterCategoryType.SingleInstance,
            params CounterCreationData[] counters)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }
            
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (string.IsNullOrWhiteSpace(categoryName))
            {
                throw new ArgumentNullException("categoryName");
            }

            if (string.IsNullOrWhiteSpace(categoryDescription))
            {
                throw new ArgumentNullException("categoryDescription");
            }

            if ((counters == null) || (counters.Length == 0))
            {
                throw new ArgumentException("No counters specified for performance counter category.", "counters");
            }

            if (AllCountersRegistered(provider.PerformanceCounterManager, categoryName, counters))
            {
                return false;
            }

            if (provider.PerformanceCounterManager.CategoryExists(categoryName)) 
            {
                provider.PerformanceCounterManager.DeleteCategory(categoryName);
                logger.LogMessage("Deleted existing performance counter category: " + categoryName);
            }

            var counterData = new CounterCreationDataCollection(counters);
            provider.PerformanceCounterManager.CreateCategory(
                categoryName, categoryDescription, categoryType, counterData);
            logger.LogMessage("Created performance counter category: " + categoryName);
            
            return true;
        }

        /// <summary>
        /// Checks if the specified performance counter category and counters exists on the current machine.
        /// </summary>
        /// <param name="manager">The performance counter manager.</param>
        /// <param name="categoryName">The name of the performance counter category.</param>
        /// <param name="counters">The performance counters which belong to the category.</param>
        /// <returns>
        /// <c>true</c> if the category and all its performance counters are registered; otherwise, <c>false</c>.
        /// </returns>
        private static bool AllCountersRegistered(
            IPerformanceCounterManager manager, 
            string categoryName, 
            IEnumerable<CounterCreationData> counters)
        {
            if (!manager.CategoryExists(categoryName))
            {
                return false;
            }

            foreach (CounterCreationData counterCreationData in counters) 
            {
                if (!manager.CounterExists(counterCreationData.CounterName, categoryName))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
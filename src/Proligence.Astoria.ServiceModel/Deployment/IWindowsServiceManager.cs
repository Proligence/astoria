﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWindowsServiceManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Deployment
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Manages creation of Windows Services.
    /// </summary>
    public interface IWindowsServiceManager
    {
        /// <summary>
        /// Opens the Service Control Manager on the local machine.
        /// </summary>
        /// <returns>The handle to the opened Service Control Manager.</returns>
        IntPtr OpenServiceControlManager();

        /// <summary>
        /// Closes a handle to the Service Control Manager.
        /// </summary>
        /// <param name="scmHandle">The handle to the Service Control Manager.</param>
        /// <returns><c>true</c> if the handle was successfully closed; otherwise, <c>false</c>.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        bool CloseServiceControlManager(IntPtr scmHandle);

        /// <summary>
        /// Opens a service with the specified name.
        /// </summary>
        /// <param name="scmHandle">The handle to the Service Control Manager.</param>
        /// <param name="serviceName">The name of the service to open.</param>
        /// <returns>The handle to the opened service.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        IntPtr? OpenService(IntPtr scmHandle, string serviceName);

        /// <summary>
        /// Closes a service handle.
        /// </summary>
        /// <param name="handle">The service handle to close.</param>
        /// <returns><c>true</c> if the handle was successfully closed; otherwise, <c>false</c>.</returns>
        bool CloseService(IntPtr handle);

        /// <summary>
        /// Creates a new service.
        /// </summary>
        /// <param name="scmHandle">The handle to the Service Control Manager.</param>
        /// <param name="serviceName">The name of the new service.</param>
        /// <param name="displayName">The display name of the new service.</param>
        /// <param name="path">The path to the service's executable file.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        void CreateService(IntPtr scmHandle, string serviceName, string displayName, string path);
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEventLogManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Deployment
{
    using System.Diagnostics;

    /// <summary>
    /// Manages Windows Event Logs.
    /// </summary>
    public interface IEventLogManager
    {
        /// <summary>
        /// Checks if an Event Log with the specified name is registered on the current machine.
        /// </summary>
        /// <param name="logName">The name of the log to check.</param>
        /// <returns><c>true</c> if there is an event log with the specified name; otherwise, <c>false</c>.</returns>
        bool LogExists(string logName);

        /// <summary>
        /// Creates a Event Log source and log.
        /// </summary>
        /// <param name="data">The log creation data.</param>
        void CreateLog(EventSourceCreationData data);
    }
}
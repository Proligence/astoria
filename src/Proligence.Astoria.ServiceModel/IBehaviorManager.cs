﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBehaviorManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the API of classes which implement managing service behaviors.
    /// </summary>
    public interface IBehaviorManager
    {
        /// <summary>
        /// Registers the specified service behavior.
        /// </summary>
        /// <typeparam name="TBehavior">The type of the behavior to register.</typeparam>
        /// <param name="behavior">The service behavior to register.</param>
        void RegisterServiceBehavior<TBehavior>(TBehavior behavior) 
            where TBehavior : IAstoriaServiceBehavior;

        /// <summary>
        /// Gets the registered service behavior of the specified type.
        /// </summary>
        /// <typeparam name="TBehavior">The type of service behavior to get.</typeparam>
        /// <returns>The found service behavior or <c>null</c>.</returns>
        TBehavior GetServiceBehavior<TBehavior>()
            where TBehavior : IAstoriaServiceBehavior;

        /// <summary>
        /// Gets the registered service behavior of the specified type or throws an exception if the service does
        /// not have any behavior of the specified type.
        /// </summary>
        /// <typeparam name="TBehavior">The type of service behavior to get.</typeparam>
        /// <returns>The found service behavior.</returns>
        TBehavior GetRequiredServiceBehavior<TBehavior>()
            where TBehavior : IAstoriaServiceBehavior;

        /// <summary>
        /// Gets all registered service behaviors of the specified type.
        /// </summary>
        /// <typeparam name="TBehavior">The type of service behaviors to get.</typeparam>
        /// <returns>A sequence of found service behaviors.</returns>
        IEnumerable<TBehavior> GetServiceBehaviors<TBehavior>()
            where TBehavior : IAstoriaServiceBehavior;

        /// <summary>
        /// Gets the registered service behavior of the specified type or registers a new one if the behavior is not
        /// registered.
        /// </summary>
        /// <typeparam name="TBehavior">The type of service behavior to get.</typeparam>
        /// <returns>The found or registered service behavior.</returns>
        TBehavior GetOrRegisterServiceBehavior<TBehavior>()
            where TBehavior : IAstoriaServiceBehavior, new();

        /// <summary>
        /// Removes all service behaviors of the specified type.
        /// </summary>
        /// <typeparam name="TBehavior">The type of service behaviors to remove.</typeparam>
        /// <returns>The removed service behaviors.</returns>
        IEnumerable<TBehavior> RemoveServiceBehavior<TBehavior>()
            where TBehavior : IAstoriaServiceBehavior;

        /// <summary>
        /// Removes all service behaviors of the specified type.
        /// </summary>
        /// <param name="behaviorType">The type of service behaviors to remove.</param>
        /// <returns>The removed service behaviors.</returns>
        IEnumerable<IAstoriaServiceBehavior> RemoveServiceBehavior(Type behaviorType);

        /// <summary>
        /// Removes the specified service behavior.
        /// </summary>
        /// <typeparam name="TBehavior">The type of the service behavior to remove.</typeparam>
        /// <param name="behavior">The service behavior to remove.</param>
        /// <returns><c>true</c> if the behavior was removed from the service; otherwise, <c>false</c>.</returns>
        bool RemoveServiceBehavior<TBehavior>(TBehavior behavior)
            where TBehavior : IAstoriaServiceBehavior;
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using Autofac;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel.Logging;

    /// <summary>
    /// Implements extension methods for the <see cref="AstoriaService{TApi,TConfig}"/> class.
    /// </summary>
    public static class AstoriaServiceExtensions
    {
        /// <summary>
        /// Writes the specified message to the service's log.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="message">The message text.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public static void LogMessage<TApi, TConfig>(this AstoriaService<TApi, TConfig> service, string message)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (service == null)
            {
                throw new ArgumentNullException("service");
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                return;
            }

            service.Logger.LogMessage(message);
        }

        /// <summary>
        /// Writes the specified warning to the service's log.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="message">The warning text.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public static void LogWarning<TApi, TConfig>(this AstoriaService<TApi, TConfig> service, string message)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (service == null) 
            {
                throw new ArgumentNullException("service");
            }

            if (string.IsNullOrWhiteSpace(message)) 
            {
                return;
            }

            service.Logger.LogWarning(message);
        }

        /// <summary>
        /// Writes the specified message to the service's log.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="message">The message text.</param>
        /// <param name="exception">The <see cref="Exception"/> which caused the error or <c>null</c>.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public static void LogError<TApi, TConfig>(
            this AstoriaService<TApi, TConfig> service, string message, Exception exception)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (service == null) 
            {
                throw new ArgumentNullException("service");
            }

            service.Logger.LogError(message, exception);
        }

        /// <summary>
        /// Executes the specified action and writes an entry to the service's log.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="action">Action to invoke.</param>
        /// <param name="logSuccess">
        /// If set to <c>true</c> succeeded calls will be logged; otherwise only failed calls will be logged.
        /// </param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public static void WithLog<TApi, TConfig>(
            this AstoriaService<TApi, TConfig> service, Action action, bool logSuccess = true)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (service == null) 
            {
                throw new ArgumentNullException("service");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            MethodBase callingMethod = new StackFrame(1).GetMethod();

            try 
            {
                action();

                if (logSuccess)
                {
                    string message = callingMethod.Name + ": call succeeded";
                    service.LogMessage(message);
                }
            }
            catch (Exception ex) 
            {
                string message = callingMethod.Name + ": call failed";
                service.LogError(message, ex);

                throw;
            }
        }

        /// <summary>
        /// Executes the specified function and writes an entry to the service's log.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <typeparam name="TResult">The type of the function's result.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="func">Function to invoke.</param>
        /// <param name="logSuccess">
        /// If set to <c>true</c> succeeded calls will be logged; otherwise only failed calls will be logged.
        /// </param>
        /// <returns>The result of the invoked function.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public static TResult WithLog<TApi, TConfig, TResult>(
            this AstoriaService<TApi, TConfig> service, Func<TResult> func, bool logSuccess = true)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (service == null) 
            {
                throw new ArgumentNullException("service");
            }

            if (func == null)
            {
                throw new ArgumentNullException("func");
            }

            MethodBase callingMethod = new StackFrame(1).GetMethod();

            try 
            {
                TResult result = func();

                // We need to make sure that the result has been evaluated completely at this point. If the result
                // is a lazy IEnumerable, then we need to force its evaluation and return a concrete result.
                var enumerable = result as IEnumerable;
                if (enumerable != null)
                {
                    result = CalculateEnumerable(result);
                }

                if (logSuccess) 
                {
                    string message = callingMethod.Name + ": call succeeded";
                    service.LogMessage(message);
                }

                return result;
            }
            catch (Exception ex) 
            {
                string message = callingMethod.Name + ": call failed";
                service.LogError(message, ex);

                throw;
            }
        }

        /// <summary>
        /// Executes the specified action and collects a log which is written to the service's log.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="action">Action to invoke.</param>
        /// <param name="logSuccess">
        /// If set to <c>true</c> succeeded calls will be logged; otherwise only failed calls will be logged.
        /// </param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public static void WithLog<TApi, TConfig>(
            this AstoriaService<TApi, TConfig> service, Action<ILog> action, bool logSuccess = true)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (service == null)
            {
                throw new ArgumentNullException("service");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            ILog log = service.Container.Resolve<ILog>();

            try
            {
                action(log);

                if (logSuccess)
                {
                    string message = log.ToString();
                    service.LogMessage(message);
                }
            }
            catch (Exception ex)
            {
                MethodBase callingMethod = new StackFrame(1).GetMethod();
                log.WriteLine(callingMethod.Name + ": call failed");

                string message = log.ToString();
                service.LogError(message, ex);

                throw;
            }
        }

        /// <summary>
        /// Executes the specified function and collects a log which is written to the service's log.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <typeparam name="TResult">The type of the function's result.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="func">Function to invoke.</param>
        /// <param name="logSuccess">
        /// If set to <c>true</c> succeeded calls will be logged; otherwise only failed calls will be logged.
        /// </param>
        /// <returns>The result of the invoked function.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public static TResult WithLog<TApi, TConfig, TResult>(
            this AstoriaService<TApi, TConfig> service, Func<ILog, TResult> func, bool logSuccess = true)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (service == null)
            {
                throw new ArgumentNullException("service");
            }

            if (func == null)
            {
                throw new ArgumentNullException("func");
            }

            ILog log = service.Container.Resolve<ILog>();
            
            try
            {
                TResult result = func(log);

                // We need to make sure that the result has been evaluated completely at this point. If the result
                // is a lazy IEnumerable, then we need to force its evaluation and return a concrete result.
                var enumerable = result as IEnumerable;
                if (enumerable != null)
                {
                    result = CalculateEnumerable(result);
                }

                if (logSuccess)
                {
                    string message = log.ToString();
                    service.LogMessage(message);
                }

                return result;
            }
            catch (Exception ex)
            {
                MethodBase callingMethod = new StackFrame(1).GetMethod();
                log.WriteLine(callingMethod.Name + ": call failed");

                string message = log.ToString();
                service.LogError(message, ex);

                throw;
            }
        }

        /// <summary>
        /// Invokes the iterator of the specified <see cref="IEnumerable"/> and returns the evaluated items.
        /// </summary>
        /// <typeparam name="TEnumerable">The type of the enumerable.</typeparam>
        /// <param name="result">The numerable value.</param>
        /// <returns>Sequence of enumerated items.</returns>
        private static TEnumerable CalculateEnumerable<TEnumerable>(TEnumerable result)
        {
            if (Attribute.IsDefined(result.GetType(), typeof(CompilerGeneratedAttribute)))
            {
                if (typeof(TEnumerable).IsGenericType)
                {
                    if (typeof(TEnumerable).GetGenericTypeDefinition() == typeof(IEnumerable<>))
                    {
                        Type itemType = typeof(TEnumerable).GetGenericArguments()[0];
                        IList items = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(itemType));
                        foreach (object item in (IEnumerable)result)
                        {
                            items.Add(item);
                        }

                        return (TEnumerable)items;
                    }
                }
            }

            return result;
        }
    }
}
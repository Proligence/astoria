﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoggingBehavior.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Logging
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Autofac;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Specifies logging behavior for Astoria services.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    [SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix")]
    public sealed class LoggingBehavior : Attribute, ILoggingBehavior
    {
        /// <summary>
        /// Gets or sets the target for service log messages.
        /// </summary>
        public ServiceLogTarget LogTarget { get; set; }

        /// <summary>
        /// Creates a new service logger.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <param name="service">The service for which logger will be created.</param>
        /// <param name="container">The service's dependency injection container.</param>
        /// <returns>The created logger.</returns>
        public ServiceLogger CreateLogger<TApi, TConfig>(
            AstoriaService<TApi, TConfig> service,
            IComponentContext container)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (service == null)
            {
                throw new ArgumentNullException("service");
            }

            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            if (this.LogTarget == ServiceLogTarget.Console)
            {
                return new ConsoleLogger();
            }

            if (this.LogTarget == ServiceLogTarget.EventLog)
            {
                return CreateEventLogLogger(
                    service.ServiceName,
                    service.Configuration.Service.Logging);
            }

            return new NullLogger();
        }

        /// <summary>
        /// Creates a <see cref="ServiceLogger"/> which writes log entries to the Windows Event Log.
        /// </summary>
        /// <param name="serviceName">The name of the service.</param>
        /// <param name="loggingConfiguration">
        /// The <see cref="LoggingSection"/> section of the service's configuration.
        /// </param>
        /// <returns>The created <see cref="EventLogLogger"/>.</returns>
        private static EventLogLogger CreateEventLogLogger(string serviceName, LoggingSection loggingConfiguration)
        {
            string logName = serviceName.Replace(" ", string.Empty);
            if (loggingConfiguration != null)
            {
                if (!string.IsNullOrWhiteSpace(loggingConfiguration.EventLogName))
                {
                    logName = loggingConfiguration.EventLogName;
                }
            }

            return new EventLogLogger(logName, logName, Environment.MachineName);
        }
    }
}
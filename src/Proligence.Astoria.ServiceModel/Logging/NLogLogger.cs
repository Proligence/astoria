﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NLogLogger.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Logging
{
    using System;
    using System.Diagnostics;
    using NLog;
    using NLog.Config;
    using NLog.Targets;

    /// <summary>
    /// Logs service messages using NLog library.
    /// </summary>
    /// <remarks>
    /// This type is thread safe.
    /// </remarks>
    public class NLogLogger : ServiceLogger
    {
        /// <summary>
        /// The default layout of log messages.
        /// </summary>
        public const string DefaultLayout = "${longdate} ${level:uppercase=true} ${logger} ${message}";

        public NLogLogger(Target target)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            this.LogTarget = target;
        }

        public NLogLogger(LoggingConfiguration loggingConfiguration)
        {
            if (loggingConfiguration == null)
            {
                throw new ArgumentNullException("loggingConfiguration");
            }

            this.LoggingConfiguration = loggingConfiguration;
        }

        /// <summary>
        /// Gets the NLog logger to which service messages will be written.
        /// </summary>
        public Logger EventLogger { get; private set; }

        /// <summary>
        /// Gets or sets the NLog target which will be used to log service messages.
        /// </summary>
        public Target LogTarget { get; protected set; }

        /// <summary>
        /// Gets or sets the NLog configuration to use.
        /// </summary>
        public LoggingConfiguration LoggingConfiguration { get; protected set; }

        /// <summary>
        /// Initializes the logger.
        /// </summary>
        protected override void InitializeInternal()
        {
            if (this.LoggingConfiguration == null)
            {
                this.LoggingConfiguration = this.CreateLoggingConfiguration();
                LogManager.Configuration = this.LoggingConfiguration;
            }

            Type callerType = new StackFrame(2).GetMethod().DeclaringType;
            
            // ReSharper disable once PossibleNullReferenceException
            this.EventLogger = LogManager.GetLogger(callerType.FullName);
        }

        /// <summary>
        /// Gets the NLog configuration.
        /// </summary>
        /// <returns>The NLog configuration.</returns>
        protected virtual LoggingConfiguration CreateLoggingConfiguration()
        {
            LoggingConfiguration loggingConfiguration = new LoggingConfiguration();
            loggingConfiguration.AddTarget("ServiceLogger", this.LogTarget);

            LoggingRule defaultRule = new LoggingRule("*", LogLevel.Trace, this.LogTarget);
            loggingConfiguration.LoggingRules.Add(defaultRule);
            
            return loggingConfiguration;
        }

        /// <summary>
        /// Write the specified debug message to the service's log.
        /// </summary>
        /// <param name="message">The message text.</param>
        protected override void LogDebugMessageInternal(string message)
        {
            if (!this.IsInitialized)
            {
                throw new InvalidOperationException("The logger is not initialized.");
            }

            this.EventLogger.Debug(message);
        }

        /// <summary>
        /// Writes the specified message to the service's log.
        /// </summary>
        /// <param name="message">The message text.</param>
        protected override void LogMessageInternal(string message)
        {
            if (!this.IsInitialized)
            {
                throw new InvalidOperationException("The logger is not initialized.");
            }

            this.EventLogger.Info(message);
        }

        /// <summary>
        /// Writes the specified warning to the service's log.
        /// </summary>
        /// <param name="message">The warning text.</param>
        protected override void LogWarningInternal(string message)
        {
            if (!this.IsInitialized)
            {
                throw new InvalidOperationException("The logger is not initialized.");
            }

            this.EventLogger.Warn(message);
        }

        /// <summary>
        /// Writes the specified error message to the service's log.
        /// </summary>
        /// <param name="message">The message text.</param>
        /// <param name="exception">The <see cref="Exception"/> which caused the error or <c>null</c>.</param>
        protected override void LogErrorInternal(string message, Exception exception)
        {
            if (!this.IsInitialized)
            {
                throw new InvalidOperationException("The logger is not initialized.");
            }

            if (exception != null)
            {
                string msg = this.FormatErrorMessage(message, exception);
                if (!string.IsNullOrWhiteSpace(exception.StackTrace))
                {
                    msg += Environment.NewLine + exception.StackTrace;
                }

                this.EventLogger.ErrorException(msg, exception);
            }
            else
            {
                this.EventLogger.Error(message);
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventLogLogger.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Logging
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using NLog.Targets;

    /// <summary>
    /// Saves service log messages to the Windows Event Log.
    /// </summary>
    /// <remarks>
    /// This type is thread safe.
    /// </remarks>
    public class EventLogLogger : NLogLogger
    {
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public EventLogLogger(string log, string source, string machine)
            : base(CreateTarget(log, source, machine))
        {
            if (string.IsNullOrEmpty(log))
            {
                throw new ArgumentNullException("log");
            }

            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException("source");
            }

            if (string.IsNullOrEmpty(machine))
            {
                throw new ArgumentNullException("machine");
            }

            this.LogName = log;
            this.Source = source;
            this.MachineName = machine;
        }

        /// <summary>
        /// Gets the name of the event log.
        /// </summary>
        public string LogName { get; private set; }

        /// <summary>
        /// Gets the name of the event source.
        /// </summary>
        public string Source { get; private set; }

        /// <summary>
        /// Gets the name of the machine on which messages are logged.
        /// </summary>
        public string MachineName { get; private set; }

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static Target CreateTarget(string log, string source, string machine, string layout = DefaultLayout)
        {
            return new EventLogTarget 
            {
                Log = log,
                Source = source,
                MachineName = machine,
                Layout = layout
            };    
        }
    }
}
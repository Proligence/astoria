﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILoggingBehavior.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Logging
{
    using System.Diagnostics.CodeAnalysis;
    using Autofac;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Astoria service behavior for logging-related functionality.
    /// </summary>
    [AstoriaServiceBehavior(AllowMultiple = false)]
    public interface ILoggingBehavior : IAstoriaServiceBehavior
    {
        /// <summary>
        /// Creates a new service logger.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <param name="service">The service for which logger will be created.</param>
        /// <param name="container">The service's dependency injection container.</param>
        /// <returns>The created logger.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        ServiceLogger CreateLogger<TApi, TConfig>(
            AstoriaService<TApi, TConfig> service,
            IComponentContext container)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration;
    }
}
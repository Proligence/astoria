// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILog.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Logging
{
    /// <summary>
    /// Represents the interface of classes which represent message logs.
    /// </summary>
    public interface ILog
    {
        /// <summary>
        /// Appends the specified <see cref="string"/> to the log.
        /// </summary>
        /// <param name="message">The <see cref="string"/> which will be appended to the log.</param>
        void Write(string message);

        /// <summary>
        /// Appends the specified formatted message to the log.
        /// </summary>
        /// <param name="format">The format string of the message to append.</param>
        /// <param name="args">The arguments for the specified format string.</param>
        /* ReSharper disable MethodOverloadWithOptionalParameter */
        void Write(string format, params object[] args);
        /* ReSharper restore MethodOverloadWithOptionalParameter */

        /// <summary>
        /// Appends the specified <see cref="string"/> with a newline sequence to the log.
        /// </summary>
        /// <param name="message">The <see cref="string"/> which will be appended to the log.</param>
        void WriteLine(string message);

        /// <summary>
        /// Appends the specified formatted message with a newline sequence to the log.
        /// </summary>
        /// <param name="format">The format string of the message to append.</param>
        /// <param name="args">The arguments for the specified format string.</param>
        /* ReSharper disable MethodOverloadWithOptionalParameter */
        void WriteLine(string format, params object[] args);
        /* ReSharper restore MethodOverloadWithOptionalParameter */

        /// <summary>
        /// Appends a newline sequence to the log.
        /// </summary>
        void WriteLine();

        /// <summary>
        /// Returns the contents of the log as a single <see cref="string"/>.
        /// </summary>
        /// <returns>A <see cref="string"/> which contains the entire log.</returns>
        string ToString();
    }
}
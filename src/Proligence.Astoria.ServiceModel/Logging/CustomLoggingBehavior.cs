﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomLoggingBehavior.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Logging
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Xml;
    using Autofac;
    using NLog;
    using NLog.Config;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Implements custom logging behavior using NLog.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    [SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix")]
    public sealed class CustomLoggingBehavior : Attribute, ILoggingBehavior
    {
        /// <summary>
        /// Gets or sets the NLog configuration.
        /// </summary>
        public string Configuration { get; set; }

        /// <summary>
        /// Creates a new service logger.
        /// </summary>
        /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
        /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
        /// <param name="service">The service for which logger will be created.</param>
        /// <param name="container">The service's dependency injection container.</param>
        /// <returns>The created logger.</returns>
        [SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly")]
        public ServiceLogger CreateLogger<TApi, TConfig>(
            AstoriaService<TApi, TConfig> service, 
            IComponentContext container)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (string.IsNullOrEmpty(this.Configuration))
            {
                throw new ServiceConfigurationException(
                    @"No NLog configuration specified for CustomLoggingBehavior.", 
                    null,
                    typeof(CustomLoggingBehavior).FullName,
                    "Configuration",
                    this.Configuration);
            }

            try
            {
                using (var textReader = new StringReader(this.Configuration))
                {
                    XmlReader xmlReader = XmlReader.Create(textReader);
                    LogManager.Configuration = new XmlLoggingConfiguration(xmlReader, null);
                }
            }
            catch (Exception ex)
            {
                throw new ServiceConfigurationException(
                    @"The NLog configuration specified for CustomLoggingBehavior is invalid.",
                    ex,
                    typeof(CustomLoggingBehavior).FullName,
                    "Configuration",
                    this.Configuration);
            }

            return new NLogLogger(LogManager.Configuration);
        }
    }
}
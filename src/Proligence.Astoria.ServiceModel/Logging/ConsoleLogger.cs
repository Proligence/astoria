﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConsoleLogger.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Logging
{
    using System.Diagnostics.CodeAnalysis;
    using NLog.Targets;

    /// <summary>
    /// Writes service messages to the console.
    /// </summary>
    /// <remarks>
    /// This type is thread safe.
    /// </remarks>
    public class ConsoleLogger : NLogLogger
    {
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public ConsoleLogger() 
            : base(CreateTarget())
        {
        }

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static Target CreateTarget(string layout = DefaultLayout)
        {
            return new ColoredConsoleTarget 
            {
                Layout = layout
            };
        }
    }
}
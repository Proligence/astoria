﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceLogger.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Logging
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The base class for classes which implement logging messages from Astoria services.
    /// </summary>
    /// <remarks>
    /// This type is thread safe.
    /// </remarks>
    public abstract class ServiceLogger : IDisposable
    {
        private readonly object syncRoot = new object();

        /// <summary>
        /// Gets a value indicating whether the logger is initialized.
        /// </summary>
        public bool IsInitialized { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the logger has been disposed.
        /// </summary>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether debug messages should be written to the service's log.
        /// </summary>
        public bool DebugOutputEnabled { get; set; }

        /// <summary>
        /// Initializes the logger.
        /// </summary>
        public void Initialize()
        {
            this.InitializeInternal();
            this.IsInitialized = true;
        }

        /// <summary>
        /// Write the specified debug message to the service's log.
        /// </summary>
        /// <param name="message">The message text.</param>
        public virtual void LogDebugMessage(string message)
        {
            if (this.DebugOutputEnabled)
            {
                lock (this.syncRoot)
                {
                    this.LogDebugMessageInternal(message);
                }
            }
        }

        /// <summary>
        /// Writes the specified message to the service's log.
        /// </summary>
        /// <param name="message">The message text.</param>
        public virtual void LogMessage(string message)
        {
            lock (this.syncRoot)
            {
                this.LogMessageInternal(message);
            }
        }

        /// <summary>
        /// Writes the specified warning to the service's log.
        /// </summary>
        /// <param name="message">The warning text.</param>
        public virtual void LogWarning(string message)
        {
            lock (this.syncRoot)
            {
                this.LogWarningInternal(message);
            }
        }

        /// <summary>
        /// Writes the specified error message to the service's log.
        /// </summary>
        /// <param name="message">The message text.</param>
        /// <param name="exception">The <see cref="Exception"/> which caused the error or <c>null</c>.</param>
        public virtual void LogError(string message, Exception exception)
        {
            lock (this.syncRoot)
            {
                this.LogErrorInternal(message, exception);
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);

            this.IsDisposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        /// <summary>
        /// Initializes the logger.
        /// </summary>
        protected virtual void InitializeInternal()
        {
        }

        /// <summary>
        /// Write the specified debug message to the service's log.
        /// </summary>
        /// <param name="message">The message text.</param>
        [ExcludeFromCodeCoverage]
        protected virtual void LogDebugMessageInternal(string message)
        {
        }

        /// <summary>
        /// Writes the specified message to the service's log.
        /// </summary>
        /// <param name="message">The message text.</param>
        [ExcludeFromCodeCoverage]
        protected virtual void LogMessageInternal(string message)
        {
        }

        /// <summary>
        /// Writes the specified warning to the service's log.
        /// </summary>
        /// <param name="message">The warning text.</param>
        [ExcludeFromCodeCoverage]
        protected virtual void LogWarningInternal(string message)
        {
        }

        /// <summary>
        /// Writes the specified error message to the service's log.
        /// </summary>
        /// <param name="message">The message text.</param>
        /// <param name="exception">The <see cref="Exception"/> which caused the error or <c>null</c>.</param>
        [ExcludeFromCodeCoverage]
        protected virtual void LogErrorInternal(string message, Exception exception)
        {
        }

        /// <summary>
        /// Formats an error message with the details of the specified exception.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="exception">The exception object.</param>
        /// <returns>The formatted error message.</returns>
        protected virtual string FormatErrorMessage(string message, Exception exception)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullException("message");
            }

            message = message.Replace(Environment.NewLine, string.Empty);

            string str = string.Empty;
            if (!message.EndsWith(" ", StringComparison.Ordinal)) 
            {
                str += message + " ";
            }
            else 
            {
                str += message;
            }

            while (exception != null) 
            {
                if (!str.Contains(exception.Message.Trim())) 
                {
                    if (!exception.Message.EndsWith(" ", StringComparison.Ordinal)) 
                    {
                        str += exception.Message + " ";
                    }
                    else 
                    {
                        str += exception.Message;
                    }
                }

                exception = exception.InnerException;
            }

            return str.Trim();
        }
    }
}
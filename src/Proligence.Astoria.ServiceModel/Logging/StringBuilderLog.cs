﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringBuilderLog.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Logging
{
    using System;
    using System.Globalization;
    using System.Text;

    /// <summary>
    /// Implements a message log using the <see cref="StringBuilder"/> class.
    /// </summary>
    /// <remarks>
    /// This type is thread safe.
    /// </remarks>
    public class StringBuilderLog : ILog
    {
        /// <summary>
        /// The <see cref="StringBuilder"/> object which contains the log's contents.
        /// </summary>
        private readonly StringBuilder log;

        public StringBuilderLog()
        {
            this.log = new StringBuilder();
        }

        /// <summary>
        /// Appends the specified <see cref="string"/> to the log.
        /// </summary>
        /// <param name="message">The <see cref="string"/> which will be appended to the log.</param>
        public void Write(string message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            lock (this.log)
            {
                this.log.Append(message);
            }
        }

        /* ReSharper disable MethodOverloadWithOptionalParameter */

        /// <summary>
        /// Appends the specified formatted message to the log.
        /// </summary>
        /// <param name="format">The format string of the message to append.</param>
        /// <param name="args">The arguments for the specified format string.</param>
        public void Write(string format, params object[] args)
        {
            lock (this.log)
            {
                this.log.AppendFormat(CultureInfo.CurrentCulture, format, args);
            }
        }

        /* ReSharper restore MethodOverloadWithOptionalParameter */

        /// <summary>
        /// Appends the specified <see cref="string"/> with a newline sequence to the log.
        /// </summary>
        /// <param name="message">The <see cref="string"/> which will be appended to the log.</param>
        public void WriteLine(string message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            lock (this.log)
            {
                this.log.AppendLine(message);
            }
        }

        /* ReSharper disable MethodOverloadWithOptionalParameter */

        /// <summary>
        /// Appends the specified formatted message with a newline sequence to the log.
        /// </summary>
        /// <param name="format">The format string of the message to append.</param>
        /// <param name="args">The arguments for the specified format string.</param>
        public void WriteLine(string format, params object[] args)
        {
            lock (this.log)
            {
                this.log.AppendFormat(CultureInfo.CurrentCulture, format, args);
                this.log.AppendLine();
            }
        }

        /* ReSharper restore MethodOverloadWithOptionalParameter */

        /// <summary>
        /// Appends a newline sequence to the log.
        /// </summary>
        public void WriteLine()
        {
            lock (this.log)
            {
                this.log.AppendLine();
            }
        }

        public override string ToString()
        {
            lock (this.log)
            {
                return this.log.ToString();
            }
        }

        public override int GetHashCode()
        {
            return this.log.ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            StringBuilderLog other = obj as StringBuilderLog;
            if (other != null)
            {
                return other.log.Equals(this.log);
            }

            return false;
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceEntryPoint.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Reflection;
    using Autofac;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel.Hosting;
    using Proligence.Helpers.Autofac;
    using ContainerBuildOptions = Proligence.Helpers.Autofac.ContainerBuildOptions;

    /// <summary>
    /// Implements the entry point for Astoria service processes.
    /// </summary>
    /// <typeparam name="TService">The concrete type which implements the service.</typeparam>
    /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
    /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    [SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")]
    public class ServiceEntryPoint<TService, TApi, TConfig>
        where TApi : class, IAstoriaService
        where TService : AstoriaService<TApi, TConfig>
        where TConfig : AstoriaServiceConfiguration
    {
        /// <summary>
        /// Gets the command-line arguments passed to the service process.
        /// </summary>
        public IEnumerable<string> Arguments { get; private set; }

        /// <summary>
        /// Gets the service's dependency injection container.
        /// </summary>
        public IContainer Container { get; private set; }

        /// <summary>
        /// Gets or sets the options for building the dependency injection container.
        /// </summary>
        public ContainerBuildOptions ContainerBuildOptions { get; set; }

        /// <summary>
        /// Gets or sets the types which assemblies will be loaded to the service's AppDomain.
        /// </summary>
        public IEnumerable<Type> TypesToResolve { get; set; }

        /// <summary>
        /// Gets or sets the handler which initializes the service process.
        /// </summary>
        public Action<ServiceEntryPoint<TService, TApi, TConfig>> Initialization { get; set; }

        /// <summary>
        /// Gets or sets the handler which sets service startup options.
        /// </summary>
        public Action<ServiceEntryPoint<TService, TApi, TConfig>, ServiceStartOptions> StartOptionsConfiguration
        {
            get; 
            set;
        }

        /// <summary>
        /// Gets or sets the handler which configures the service instance.
        /// </summary>
        public Action<ServiceEntryPoint<TService, TApi, TConfig>, TService> ServiceConfiguration { get; set; }

        /// <summary>
        /// Gets or sets the handler which is called when an exception occurs during service startup.
        /// </summary>
        public Action<ServiceEntryPoint<TService, TApi, TConfig>, Exception> ErrorHandler { get; set; }

        /// <summary>
        /// Runs the service.
        /// </summary>
        /// <param name="args">The command-line arguments passed to the service process.</param>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public void Run(string[] args)
        {
            this.Arguments = args;

            TService service = null;

            try 
            {
                if (this.Initialization != null)
                {
                    this.Initialization(this);
                }

                this.LoadAssembliesFromServiceDirectory();

                if (this.TypesToResolve != null)
                {
                    foreach (Type type in this.TypesToResolve)
                    {
                        this.ResolveType(type);
                    }
                }

                using (var container = this.BuildIocContainer())
                {
                    this.Container = container;

                    var startOptions = new ServiceStartOptions(args);
                    if (this.StartOptionsConfiguration != null)
                    {
                        this.StartOptionsConfiguration(this, startOptions);
                    }

                    service = this.CreateService();
                    service.SetDependencyContainer(container);

                    if (this.ServiceConfiguration != null)
                    {
                        this.ServiceConfiguration(this, service);
                    }

                    this.RunService(service, startOptions);
                }
            }
            catch (Exception ex)
            {
                // Write error details to console before attaching debugger if the service is running in a
                // process-based host.
                if ((service != null) && (service.BehaviorManager != null))
                {
                    if (service.BehaviorManager.GetServiceBehavior<IServiceHostBehavior>() is ProcessHostBehavior)
                    {
                        this.WriteExceptionDetails(ex, Console.Out);
                    }
                }

                this.LaunchDebugger();

                if (this.ErrorHandler != null) 
                {
                    this.ErrorHandler(this, ex);
                }
                else
                {
                    this.WriteExceptionDetails(ex, Console.Out);
                }
            }
        }

        /// <summary>
        /// Loads all assemblies from the directory which contains the service's entry assembly.
        /// </summary>
        [ExcludeFromCodeCoverage]
        [SuppressMessage("Microsoft.Reliability", "CA2001:AvoidCallingProblematicMethods")]
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        protected virtual void LoadAssembliesFromServiceDirectory()
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();
            if (entryAssembly != null)
            {
                string serviceDirectory = Path.GetDirectoryName(entryAssembly.Location);
                if ((serviceDirectory != null) && Directory.Exists(serviceDirectory))
                {
                    string[] fileNames = Directory.GetFiles(serviceDirectory, "*.dll");
                    foreach (string fileName in fileNames)
                    {
                        try 
                        {
                            Assembly.LoadFrom(fileName);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Failed to load assembly '" + fileName + "'. " + ex.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Loads the assembly in which the specified type is defined into the service's AppDomain.
        /// </summary>
        /// <param name="type">The type which assembly to load.</param>
        [ExcludeFromCodeCoverage]
        protected virtual void ResolveType(Type type)
        {
            // Referencing the type will load the assembly in which the type is declared into the serivce's AppDomain.
            if (type != null)
            {
                Trace.WriteLine("Resolving type: " + type.FullName);
            }
        }

        /// <summary>
        /// Builds the service's IoC container.
        /// </summary>
        /// <returns>The created IoC container.</returns>
        [ExcludeFromCodeCoverage]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        protected virtual IContainer BuildIocContainer()
        {
            if (this.ContainerBuildOptions == null)
            {
                this.ContainerBuildOptions = new ContainerBuildOptions
                {
                    RegisterAssemblyModules = false,
                    RegisterAssemblyTypes = false
                };
            }

            return AppDomain.CurrentDomain.BuildIocContainer(
                this.ContainerBuildOptions,
                builder => builder.RegisterConfiguration<TConfig>());
        }

        /// <summary>
        /// Creates the <typeparamref name="TService"/> instance.
        /// </summary>
        /// <returns>The created service instance.</returns>
        [ExcludeFromCodeCoverage]
        protected virtual TService CreateService()
        {
            return this.Container.Resolve<TService>();
        }

        /// <summary>
        /// Runs the specified Astoria service.
        /// </summary>
        /// <param name="service">The service to run.</param>
        /// <param name="startOptions">The service's startup options.</param>
        [ExcludeFromCodeCoverage]
        protected virtual void RunService(TService service, ServiceStartOptions startOptions)
        {
            service.Run(startOptions);
        }

        /// <summary>
        /// Launches the debugger.
        /// </summary>
        [ExcludeFromCodeCoverage]
        protected virtual void LaunchDebugger()
        {
            Debugger.Launch();
        }

        /// <summary>
        /// Writes the details of the specified exception to a <see cref="TextWriter"/>.
        /// </summary>
        /// <param name="exception">The exception object.</param>
        /// <param name="writer">The writer to which exception details will be written.</param>
        protected void WriteExceptionDetails(Exception exception, TextWriter writer)
        {
            if (exception == null)
            {
                throw new ArgumentNullException("exception");
            }

            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            writer.WriteLine(exception.ToString());

            var reflectionTypeLoadException = exception as ReflectionTypeLoadException;
            if (reflectionTypeLoadException != null)
            {
                foreach (Exception loaderException in reflectionTypeLoadException.LoaderExceptions)
                {
                    writer.WriteLine();
                    writer.WriteLine(loaderException.ToString());
                }
            }
        }
    }
}
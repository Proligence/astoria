// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IServiceDiscoveryServer.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Discovery
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Defines an interface for the service discovery server.
    /// </summary>
    public interface IServiceDiscoveryServer : IDisposable
    {
        /// <summary>
        /// Gets or sets the service's discovery configuration section.
        /// </summary>
        DiscoverySection Configuration { get; set; }

        /// <summary>
        /// Starts a listener (on a separate thread) that responds to broadcasted UDP discovery requests.
        /// Listener responds with service contract (interface) name and service address.
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        void StartFor<TApi, TConfig>(AstoriaService<TApi, TConfig> service)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration;

        /// <summary>
        /// Stops the listener and it's underlying thread.
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        void StopFor<TApi, TConfig>(AstoriaService<TApi, TConfig> service)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration;
    }
}
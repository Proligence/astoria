﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceDiscoveryServer.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel.Discovery
{
    using System;
    using System.Collections.Concurrent;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Client.Discovery;
    using Proligence.Astoria.Configuration;

    /// <summary>
    /// Default implementation of the service discovery server.
    /// </summary>
    public class ServiceDiscoveryServer : IServiceDiscoveryServer
    {
        /// <summary>
        /// Thread on which the discovery listener runs.
        /// </summary>
        private readonly ConcurrentDictionary<Type, Thread> threads;

        /// <summary>
        /// Variable used to terminate listener threads.
        /// </summary>
        private volatile bool serverRunning = true;

        public ServiceDiscoveryServer()
        {
            this.threads = new ConcurrentDictionary<Type, Thread>();
        }

        public DiscoverySection Configuration { get; set; }
        public bool IsDisposed { get; private set; }

        private bool IsLoggingEnabled
        {
            get
            {
                DiscoverySection config = this.Configuration;
                return (config != null) && config.LoggingEnabled;
            }
        }

        /// <summary>
        /// Starts a listener (on a separate thread) that responds to broadcasted UDP discovery requests.
        /// Listener responds with service contract (interface) name and service address.
        /// </summary>
        public void StartFor<TApi, TConfig>(AstoriaService<TApi, TConfig> service)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            this.StopFor(service);

            if (!this.threads.TryAdd(
                service.GetType(), 
                new Thread(() => this.ListenerThread(service)) { IsBackground = true }))
            {
                service.LogWarning(
                    service.ServiceName
                    + ": Cannot start auto-discovery listener thread. It already exists.");
                return;
            }

            Thread thread;
            if (this.threads.TryGetValue(service.GetType(), out thread))
            {
                try
                {
                    thread.Start();
                }
                catch (Exception e)
                {
                    service.LogError(
                        service.ServiceName + ": Cannot start auto-discovery listener thread.", e);

                    this.threads.TryRemove(service.GetType(), out thread);
                    throw;
                }
            }
            else
            {
                service.LogWarning(
                    service.ServiceName + ": Cannot start auto-discovery listener thread. It does not exist.");
            }
        }

        /// <summary>
        /// Stops the listener and it's underlying thread.
        /// </summary>
        public void StopFor<TApi, TConfig>(AstoriaService<TApi, TConfig> service)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            if (service == null)
            {
                return;
            }

            Thread thread;
            if (!this.threads.TryRemove(service.GetType(), out thread))
            {
                return;
            }

            try
            {
                thread.Abort();

                if (this.IsLoggingEnabled)
                {
                    service.LogMessage(service.ServiceName + ": Auto-discovery listener has been stopped.");
                }
            }
            catch (Exception ex)
            {
                service.LogError(service.ServiceName + ": Auto-discovery listener could not be stopped.", ex);
                throw;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);

            this.IsDisposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            this.serverRunning = false;
            this.threads.Clear();
        }

        /// <summary>
        /// The thread which listens for incoming requests to the discovery server.
        /// </summary>
        /// <typeparam name="TApi">Service API interface.</typeparam>
        /// <typeparam name="TConfig">Service configuration type.</typeparam>
        /// <param name="service">The service instance.</param>
        private void ListenerThread<TApi, TConfig>(AstoriaService<TApi, TConfig> service)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            //// NOTE: We need to restart the discovery server in case of an error.

            while (this.serverRunning)
            {
                this.RunListenerFor(service);
            }
        }

        /// <summary>
        /// Creates the discovery request listener.
        /// This method is synchronous - it blocks current thread.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void RunListenerFor<TApi, TConfig>(AstoriaService<TApi, TConfig> service)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            var localEndPoint = new IPEndPoint(IPAddress.Any, ServiceDiscoverer.Port);
            using (var udpServer = new UdpClient())
            {
                try
                {
                    udpServer.EnableBroadcast = true;
                    udpServer.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                    udpServer.Client.Bind(localEndPoint);

                    if (this.IsLoggingEnabled)
                    {
                            service.LogMessage(service.ServiceName
                                + ": Auto-discovery listener started on port "
                                + ServiceDiscoverer.Port);
                    }

                    while (!service.IsDisposed && !this.IsDisposed)
                    {
                        var remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                        var request = udpServer.Receive(ref remoteEndPoint);

                        if (this.IsLoggingEnabled)
                        {
                            service.LogMessage(
                                service.ServiceName
                                + ": Received discovery request from address " + remoteEndPoint.Address
                                + ":" + remoteEndPoint.Port);
                        }

                        if (Encoding.UTF8.GetString(request).Trim() != ServiceDiscoverer.DiscoveryRequestToken)
                        {
                            continue;
                        }

                        var descriptionAttribute =
                            typeof(TApi).GetCustomAttributes(
                            typeof(DescriptionAttribute), 
                            false).FirstOrDefault() as DescriptionAttribute;

                        var textToSend = string.Join(
                            "::",
                            ServiceDiscoverer.DiscoveryResponseToken,
                            typeof(TApi).Name,
                            service.Configuration.Service.Address,
                            service.ServiceName,
                            descriptionAttribute != null ? descriptionAttribute.Description : null);

                        var dataToSend = Encoding.ASCII.GetBytes(textToSend);
                        udpServer.Send(dataToSend, dataToSend.Length, remoteEndPoint);

                        if (this.IsLoggingEnabled)
                        {
                            service.LogMessage(
                                service.ServiceName + ": Sent discovery response '" + textToSend + "' to "
                                + remoteEndPoint.Address + ":" + remoteEndPoint.Port);
                        }
                    }

                    if (this.IsLoggingEnabled)
                    {
                        service.LogMessage(service.ServiceName + ": Auto-discovery listener stopped.");
                    }
                }
                catch (Exception e)
                {
                    service.LogError(
                        service.ServiceName + ": Auto-discovery listener encountered an error.", e);
                }
            }
        }
    }
}
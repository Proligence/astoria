﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BehaviorManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Manages service behaviors.
    /// </summary>
    public class BehaviorManager : IBehaviorManager
    {
        public BehaviorManager()
        {
            this.Behaviors = new List<IAstoriaServiceBehavior>();
            this.BehaviorsLock = new object();
        }

        /// <summary>
        /// Gets a list of registered service behaviors.
        /// </summary>
        protected IList<IAstoriaServiceBehavior> Behaviors { get; private set; }

        /// <summary>
        /// Gets the object used to synchronize access to the service behaviors list.
        /// </summary>
        protected object BehaviorsLock { get; private set; }

        /// <summary>
        /// Registers the specified service behavior.
        /// </summary>
        /// <typeparam name="TBehavior">The type of the behavior to register.</typeparam>
        /// <param name="behavior">The service behavior to register.</param>
        public void RegisterServiceBehavior<TBehavior>(TBehavior behavior)
            where TBehavior : IAstoriaServiceBehavior
        {
            if (ReferenceEquals(behavior, null))
            {
                throw new ArgumentNullException("behavior");
            }

            lock (this.BehaviorsLock)
            {
                this.AddBehavior(behavior);
            }
        }

        /// <summary>
        /// Gets the registered service behavior of the specified type.
        /// </summary>
        /// <typeparam name="TBehavior">The type of service behavior to get.</typeparam>
        /// <returns>The found service behavior or <c>null</c>.</returns>
        public TBehavior GetServiceBehavior<TBehavior>()
            where TBehavior : IAstoriaServiceBehavior
        {
            lock (this.BehaviorsLock)
            {
                return this.Behaviors.OfType<TBehavior>().FirstOrDefault();
            }
        }

        /// <summary>
        /// Gets the registered service behavior of the specified type or throws an exception if the service does
        /// not have any behavior of the specified type.
        /// </summary>
        /// <typeparam name="TBehavior">The type of service behavior to get.</typeparam>
        /// <returns>The found service behavior.</returns>
        public TBehavior GetRequiredServiceBehavior<TBehavior>()
            where TBehavior : IAstoriaServiceBehavior
        {
            lock (this.BehaviorsLock)
            {
                TBehavior result = this.Behaviors.OfType<TBehavior>().FirstOrDefault();
                if (ReferenceEquals(result, null))
                {
                    throw new InvalidOperationException(
                        "Failed to find service behavior of type '" + typeof(TBehavior).FullName + "'.");
                }

                return result;
            }
        }

        /// <summary>
        /// Gets all registered service behaviors of the specified type.
        /// </summary>
        /// <typeparam name="TBehavior">The type of service behaviors to get.</typeparam>
        /// <returns>A sequence of found service behaviors.</returns>
        public IEnumerable<TBehavior> GetServiceBehaviors<TBehavior>()
            where TBehavior : IAstoriaServiceBehavior
        {
            lock (this.BehaviorsLock)
            {
                return this.Behaviors.OfType<TBehavior>().ToArray();
            }
        }

        /// <summary>
        /// Gets the registered service behavior of the specified type or registers a new one if the behavior is not
        /// registered.
        /// </summary>
        /// <typeparam name="TBehavior">The type of service behavior to get.</typeparam>
        /// <returns>The found or registered service behavior.</returns>
        public TBehavior GetOrRegisterServiceBehavior<TBehavior>()
            where TBehavior : IAstoriaServiceBehavior, new()
        {
            lock (this.BehaviorsLock)
            {
                TBehavior result = this.Behaviors.OfType<TBehavior>().FirstOrDefault();
                if (ReferenceEquals(result, null))
                {
                    result = new TBehavior();
                    this.AddBehavior(result);
                }

                return result;
            }
        }

        /// <summary>
        /// Removes all service behaviors of the specified type.
        /// </summary>
        /// <typeparam name="TBehavior">The type of service behaviors to remove.</typeparam>
        /// <returns>The removed service behaviors.</returns>
        public IEnumerable<TBehavior> RemoveServiceBehavior<TBehavior>()
            where TBehavior : IAstoriaServiceBehavior
        {
            lock (this.BehaviorsLock)
            {
                TBehavior[] behaviorsToRemove = this.Behaviors.OfType<TBehavior>().ToArray();
                foreach (TBehavior behavior in behaviorsToRemove)
                {
                    this.Behaviors.Remove(behavior);
                }

                return behaviorsToRemove;
            }
        }

        /// <summary>
        /// Removes all service behaviors of the specified type.
        /// </summary>
        /// <param name="behaviorType">The type of service behaviors to remove.</param>
        /// <returns>The removed service behaviors.</returns>
        public IEnumerable<IAstoriaServiceBehavior> RemoveServiceBehavior(Type behaviorType)
        {
            lock (this.BehaviorsLock)
            {
                IAstoriaServiceBehavior[] behaviorsToRemove = this.Behaviors
                    .Where(behaviorType.IsInstanceOfType)
                    .ToArray();

                foreach (IAstoriaServiceBehavior behavior in behaviorsToRemove)
                {
                    this.Behaviors.Remove(behavior);
                }

                return behaviorsToRemove;
            }
        }

        /// <summary>
        /// Removes the specified service behavior.
        /// </summary>
        /// <typeparam name="TBehavior">The type of the service behavior to remove.</typeparam>
        /// <param name="behavior">The service behavior to remove.</param>
        /// <returns><c>true</c> if the behavior was removed from the service; otherwise, <c>false</c>.</returns>
        public bool RemoveServiceBehavior<TBehavior>(TBehavior behavior)
            where TBehavior : IAstoriaServiceBehavior
        {
            if (ReferenceEquals(behavior, null))
            {
                throw new ArgumentNullException("behavior");
            }

            lock (this.BehaviorsLock)
            {
                return this.Behaviors.Remove(behavior);
            }
        }

        /// <summary>
        /// Adds the specified behavior to the list of the service's behaviors.
        /// </summary>
        /// <typeparam name="TBehavior">The type of the behavior to add.</typeparam>
        /// <param name="behavior">The behavior to add.</param>
        private void AddBehavior<TBehavior>(TBehavior behavior)
            where TBehavior : IAstoriaServiceBehavior
        {
            var behaviorsToRemove = new List<object>();

            foreach (Type behaviorInterface in this.GetSingleInstanceServiceBehaviorInterfaces(behavior.GetType()))
            {
                foreach (IAstoriaServiceBehavior serviceBehavior in this.Behaviors)
                {
                    if (serviceBehavior.GetType().GetInterfaces().Any(i => i == behaviorInterface))
                    {
                        if (!behaviorsToRemove.Contains(serviceBehavior))
                        {
                            behaviorsToRemove.Add(serviceBehavior);
                        }
                    }
                }
            }

            foreach (IAstoriaServiceBehavior serviceBehavior in behaviorsToRemove)
            {
                this.Behaviors.Remove(serviceBehavior);
            }

            this.Behaviors.Add(behavior);
        }

        /// <summary>
        /// Gets interfaces implemented by the specified type, which represent single-instance service behaviors.
        /// </summary>
        /// <param name="type">The type to get single-instance service behaviors from.</param>
        /// <returns>The types of the single-instance service behaviors.</returns>
        private IEnumerable<Type> GetSingleInstanceServiceBehaviorInterfaces(Type type)
        {
            var result = new List<Type>();

            foreach (Type interfaceType in type.GetInterfaces())
            {
                var serviceBehaviorAttribute = (AstoriaServiceBehaviorAttribute)Attribute.GetCustomAttribute(
                    interfaceType,
                    typeof(AstoriaServiceBehaviorAttribute));

                if ((serviceBehaviorAttribute != null) && !serviceBehaviorAttribute.AllowMultiple)
                {
                    result.Add(interfaceType);
                }
            }

            return result;
        }
    }
}
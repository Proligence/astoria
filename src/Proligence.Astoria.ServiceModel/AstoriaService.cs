﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaService.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.ServiceModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Reflection;
    using System.Threading;
    using Autofac;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel.Deployment;
    using Proligence.Astoria.ServiceModel.Discovery;
    using Proligence.Astoria.ServiceModel.Extensions;
    using Proligence.Astoria.ServiceModel.Hosting;
    using Proligence.Astoria.ServiceModel.Logging;
    using Proligence.Astoria.ServiceModel.Monitoring;
    using Proligence.Astoria.ServiceModel.Wcf;

    /// <summary>
    /// Base class for Astoria service implementations.
    /// </summary>
    /// <typeparam name="TApi">The interface which defines the services API.</typeparam>
    /// <typeparam name="TConfig">The type which represents the service's configuration.</typeparam>
    [WindowsServiceHostBehavior(UseMultithreading = true)]
    [LoggingBehavior(LogTarget = ServiceLogTarget.EventLog)]
    [MonitoringBehavior(PerformanceCountersEnabled = true)]
    [DiscoveryBehavior(DiscoveryServerEnabled = true)]
    [WatchDogsBehavior(WatchDogsEnabled = true)]
    [ExtensionsBehavior(ExtensionsEnabled = true)]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
    public abstract class AstoriaService<TApi, TConfig> : IAstoriaService, IDisposable, IPerformanceCounters
        where TApi : class, IAstoriaService
        where TConfig : AstoriaServiceConfiguration
    {
        /// <summary>
        /// The object which providers information about the service's environment.
        /// </summary>
        private readonly IEnvironment environment;

        /// <summary>
        /// The object which hosts the Astoria service.
        /// </summary>
        private AstoriaServiceHost<TApi, TConfig> serviceHost;

        protected AstoriaService(string serviceName, TConfig configuration = null)
        {
            this.ServiceName = serviceName;
            this.Configuration = configuration;
            this.environment = new Environment();
        }

        protected AstoriaService(string serviceName, IEnvironment environment, TConfig configuration)
            : this(serviceName, configuration)
        {
            this.environment = environment ?? new Environment();
        }

        /// <summary>
        /// Gets the name of the service.
        /// </summary>
        public string ServiceName { get; private set; }

        /// <summary>
        /// Gets or sets the service's configuration.
        /// </summary>
        public TConfig Configuration { get; protected set; }

        /// <summary>
        /// Gets the object which implements logging messages from the service.
        /// </summary>
        public ServiceLogger Logger { get; private set; }

        /// <summary>
        /// Gets the object which manages the service's behaviors.
        /// </summary>
        public IBehaviorManager BehaviorManager { get; private set; }

        /// <summary>
        /// Gets or sets the service's performance counters.
        /// </summary>
        public ServicePerformanceCounters PerformanceCounters { get; protected set; }

        /// <summary>
        /// Gets a value indicating whether the service has been disposed.
        /// </summary>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Gets the watch dog manager.
        /// </summary>
        internal IWatchDogManager WatchDogManager { get; private set; }

        /// <summary>
        /// Gets the service extension manager.
        /// </summary>
        internal IServiceExtensionManager ExtensionManager { get; private set; }

        /// <summary>
        /// Gets the dependency injection container.
        /// </summary>
        protected internal IContainer Container { get; private set; }

        /// <summary>
        /// Gets the service's WCF host.
        /// </summary>
        /// <returns>The WCF service host instance.</returns>
        protected WcfServiceHost WcfServiceHost
        {
            get
            {
                return this.serviceHost.WcfServiceHost;
            }
        }

        /// <summary>
        /// Starts the Astoria service.
        /// </summary>
        public void Run()
        {
            this.Run(new ServiceStartOptions());
        }

        /// <summary>
        /// Starts the Astoria service.
        /// </summary>
        /// <param name="options">
        /// The <see cref="ServiceStartOptions"/> object which contains startup settings for the service.
        /// </param>
        public void Run(ServiceStartOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException("options");
            }

            if (options.AttachDebugger)
            {
                this.AttachDebugger();
            }

            /* Service initialization */

            try
            {
                // Load any additional assemblies sepcified in the service's configuration.
                if (this.Configuration.Service.Assemblies != null)
                {
                    this.LoadAssemblies(this.Configuration.Service.Assemblies);
                }

                this.Initialize(options);
            }
            catch (Exception ex)
            {
                // Log any exceptions to console
                using (var logger = new ConsoleLogger())
                {
                    logger.Initialize();
                    logger.LogError("Failed to initialize service.", ex);
                }

                throw;
            }

            /* Service statup */

            try
            {
                if (!this.Start(options))
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                this.LogError("Unexpected error occurred while the service was started.", ex);
                throw;
            }

            /* Service runtime */

            ThreadPool.QueueUserWorkItem(this.InitializeAsync);

            this.serviceHost.StartHost();
        }

        /// <summary>
        /// Initializes the service's objects and data structures before starting the service.
        /// </summary>
        /// <param name="options">
        /// The <see cref="ServiceStartOptions"/> object which contains startup settings for the service.
        /// </param>
        /// <remarks>
        /// This method may be called by unit tests to initialize all objects required by the service (particularly,
        /// the <see cref="Logger"/> property). Otherwise, there is no need to call this method, since it is called
        /// by the <see cref="Run()"/> method. Additionally, this method may be safely called multiple times.
        /// </remarks>
        public virtual void Initialize(ServiceStartOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException("options");
            }

            if (this.BehaviorManager == null)
            {
                this.BehaviorManager = this.Container.Resolve<IBehaviorManager>();
                this.InitializeServiceBehaviors();

                if (!options.RunInWindowsService)
                {
                    // If the service is running in a regular process, use a ProcessHostBehavior.
                    var serviceHostBehavior = this.BehaviorManager.GetServiceBehavior<IServiceHostBehavior>();
                    if (serviceHostBehavior is WindowsServiceHostBehavior)
                    {
                        var newServiceHostBehavior = new ProcessHostBehavior
                        {
                            UseMultithreading = serviceHostBehavior.UseMultithreading
                        };

                        this.BehaviorManager.RegisterServiceBehavior(newServiceHostBehavior);
                    }

                    // Apply process-based host policy.
                    var processHostBehavior = this.BehaviorManager.GetServiceBehavior<ProcessHostBehavior>();
                    if ((processHostBehavior != null) && processHostBehavior.ApplyProcessHostPolicy)
                    {
                        this.ApplyProcessHostPolicy();
                    }
                }
            }

            if (this.Logger == null)
            {
                this.Logger = this.CreateServiceLogger(options);

                // Register the logger in the service's container.
                var containerBuilder = new ContainerBuilder();
                containerBuilder.RegisterInstance(this.Logger).As<ServiceLogger>();
                containerBuilder.Update(this.Container);

                this.Logger.Initialize();
            }

            if (this.WatchDogManager == null)
            {
                this.WatchDogManager = this.Container.Resolve<IWatchDogManager>();
            }

            if (this.ExtensionManager == null)
            {
                this.ExtensionManager = this.Container.Resolve<IServiceExtensionManager>();

                var extensionsBehavior = this.BehaviorManager.GetServiceBehavior<IExtensionsBehavior>();
                if ((extensionsBehavior != null) && extensionsBehavior.ExtensionsEnabled)
                {
                    this.ExtensionManager.Initialize();
                    extensionsBehavior.ConfigureExtensions(this.ExtensionManager);
                }
            }
        }

        /// <summary>
        /// Gets information about the service.
        /// </summary>
        /// <returns>The <see cref="ServiceInfo"/> object which contains information about the service.</returns>
        public ServiceInfo GetServiceInfo()
        {
            return ServiceInfo.CreateForEnvironment(this.environment);
        }

        /// <summary>
        /// Gets all watch dogs implemented by the service.
        /// </summary>
        /// <returns>A sequence of watch dog descriptors.</returns>
        public IEnumerable<WatchDogInfo> GetWatchDogs()
        {
            return this.WatchDogManager.GetWatchDogs();
        }

        /// <summary>
        /// Invokes the specified watch dog.
        /// </summary>
        /// <param name="watchDogInfo">The watch dog to invoke.</param>
        /// <returns>The result of the invoked watch dog.</returns>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public WatchDogResult InvokeWatchDog(WatchDogInfo watchDogInfo)
        {
            try
            {
                using (IServiceWatchDog watchDog = this.WatchDogManager.CreateWatchDog(watchDogInfo))
                {
                    watchDog.Initialize();

                    var internalWatchDog = watchDog as IInternalWatchDog<TApi, TConfig>;
                    if (internalWatchDog != null)
                    {
                        internalWatchDog.Service = this;
                    }

                    var externalWatchDog = watchDog as IExternalWatchDog<TApi>;
                    if (externalWatchDog != null)
                    {
                        externalWatchDog.Service = this.CreateChannelToSelf();
                    }

                    var genericExternalWatchDog = watchDog as IExternalWatchDog<IAstoriaService>;
                    if (genericExternalWatchDog != null)
                    {
                        genericExternalWatchDog.Service = this.CreateChannelToSelf();
                    }

                    return watchDog.Inspect();
                }
            }
            catch (Exception ex)
            {
                var result = new WatchDogResult();
                result.AddError(ex.ToString());
                return result;
            }
        }

        /// <summary>
        /// Invokes a service extension.
        /// </summary>
        /// <param name="name">The name of the extension to invoke.</param>
        /// <param name="parameters">The parameters for the extension call.</param>
        /// <returns>The result returned by the extension.</returns>
        [SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        public object InvokeExtension(string name, params object[] parameters)
        {
            return this.WithLog(
                () =>
                {
                    IServiceExtension extension = this.ExtensionManager.GetExtension(name);
                    if (extension == null)
                    {
                        throw new ArgumentException("Extension with name '" + name + "' is not registered.", "name");
                    }

                    return extension.Invoke(parameters);
                },
                false);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);

            this.IsDisposed = true;
        }

        /// <summary>
        /// Shuts down the Astoria service.
        /// </summary>
        /// <remarks>
        /// This method should be called by the service host before the WCF host is closed.
        /// </remarks>
        internal void Shutdown()
        {
            this.OnShutdown();
            
            var serviceHostBehavior = this.BehaviorManager.GetRequiredServiceBehavior<IServiceHostBehavior>();
            serviceHostBehavior.OnShutdown(this, this.serviceHost);
        }

        /// <summary>
        /// Sets the service's dependency injection container.
        /// </summary>
        /// <param name="container">The container instance.</param>
        protected internal void SetDependencyContainer(IContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            this.Container = container;
        }

        /// <summary>
        /// Loads the specified assemblies into the service's AppDomain.
        /// </summary>
        /// <param name="assemblyFileNames">The file names of the assemblies to load.</param>
        [ExcludeFromCodeCoverage]
        [SuppressMessage("Microsoft.Reliability", "CA2001:AvoidCallingProblematicMethods")]
        protected virtual void LoadAssemblies(IEnumerable<string> assemblyFileNames)
        {
            if (assemblyFileNames != null)
            {
                foreach (var fileName in assemblyFileNames)
                {
                    Assembly.LoadFrom(fileName);
                }
            }
        }

        /// <summary>
        /// Initializes the service's behaviors.
        /// </summary>
        protected virtual void InitializeServiceBehaviors()
        {
            IEnumerable<IAstoriaServiceBehavior> reflectionBehaviors = this.DiscoverBehaviorsByReflection();
            foreach (IAstoriaServiceBehavior serviceBehavior in reflectionBehaviors)
            {
                this.BehaviorManager.RegisterServiceBehavior(serviceBehavior);
            }

            IEnumerable<Type> behaviorsToRemove;
            IEnumerable<IAstoriaServiceBehavior> configurationBehaviors =
                this.DiscoverBehaviorsByConfiguration(out behaviorsToRemove);

            foreach (Type behaviorType in behaviorsToRemove)
            {
                this.BehaviorManager.RemoveServiceBehavior(behaviorType);
            }

            foreach (IAstoriaServiceBehavior serviceBehavior in configurationBehaviors)
            {
                this.BehaviorManager.RegisterServiceBehavior(serviceBehavior);
            }
        }

        /// <summary>
        /// Discovers the service's behaviors using reflection.
        /// </summary>
        /// <returns>The discovered service behaviors.</returns>
        [ExcludeFromCodeCoverage]
        protected virtual IEnumerable<IAstoriaServiceBehavior> DiscoverBehaviorsByReflection()
        {
            var behaviorReader = new ReflectionBehaviorReader();
            behaviorReader.ReadServiceBehaviorsFromType(this.GetType());

            return behaviorReader.ServiceBehaviors;
        }

        /// <summary>
        /// Discovers the service's behaviors using reflection.
        /// </summary>
        /// <param name="behaviorsToRemove">
        /// A sequence of behavior types which should be removed from the <see cref="IBehaviorManager"/>.
        /// </param>
        /// <returns>The discovered service behaviors.</returns>
        [ExcludeFromCodeCoverage]
        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters")]
        protected virtual IEnumerable<IAstoriaServiceBehavior> DiscoverBehaviorsByConfiguration(
            out IEnumerable<Type> behaviorsToRemove)
        {
            var behaviorReader = new ConfigurationBehaviorReader();
            behaviorReader.ReadServiceBehaviorsFromConfiguration(this.Configuration);

            behaviorsToRemove = behaviorReader.RemovedServiceBehaviors;
            return behaviorReader.AddedServiceBehaviors;
        }

        /// <summary>
        /// Creates a <see cref="ServiceLogger"/> object for the service.
        /// </summary>
        /// <param name="options">
        /// The <see cref="ServiceStartOptions"/> object which contains startup settings for the service.
        /// </param>
        /// <returns>The service's logger.</returns>
        protected virtual ServiceLogger CreateServiceLogger(ServiceStartOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException("options");
            }

            var loggingBehavior = this.BehaviorManager.GetRequiredServiceBehavior<ILoggingBehavior>();

            ServiceLogger logger;
            try
            {
                logger = loggingBehavior.CreateLogger(this, this.Container);
            }
            catch (Exception)
            {
                this.Dispose();
                throw;
            }

            if (this.Configuration.Service.Logging != null)
            {
                logger.DebugOutputEnabled = this.Configuration.Service.Logging.DebugOutputEnabled;
            }

            return logger;
        }

        /// <summary>
        /// Implements service startup.
        /// </summary>
        /// <param name="options">The service start options.</param>
        /// <returns><c>true</c> if service was started successfully; otherwise, <c>false</c>.</returns>
        protected virtual bool Start(ServiceStartOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException("options");
            }

            if (options.InstallMode)
            {
                IDeploymentProvider deploymentProvider = this.GetDeploymentProvider();
                deploymentProvider.PerformInstall(this.ServiceName, this.Configuration, this.Logger);
            }

            this.InitializePerformanceCounters(options.InstallMode);

            // Nothing more to do if the service is running in install mode.
            if (options.InstallMode)
            {
                return false;
            }

            var serviceHostBehavior = this.BehaviorManager.GetRequiredServiceBehavior<IServiceHostBehavior>();
            this.serviceHost = serviceHostBehavior.CreateServiceHost(this);

            this.OnStartup();
            serviceHostBehavior.OnStartup(this, this.serviceHost);

            this.StartDiscoveryServer(options);

            if (this.Configuration.Service.Monitoring != null)
            {
                this.serviceHost.MonitoringEnabled = !this.Configuration.Service.Monitoring.MonitoringDisabled;
            }
            else
            {
                this.serviceHost.MonitoringEnabled = true;
            }

            this.serviceHost.WcfHostStarting -= this.OnServiceHostWcfHostStarting;
            this.serviceHost.WcfHostStarting += this.OnServiceHostWcfHostStarting;

            return true;
        }

        /// <summary>
        /// Initializes the service asynchronously after it has been started.
        /// </summary>
        /// <param name="startOptions">The <see cref="ServiceStartOptions"/> object.</param>
        protected virtual void InitializeAsync(object startOptions)
        {
            this.WatchDogManager.Initialize();
        }

        /// <summary>
        /// Creates a service channel to the current service.
        /// </summary>
        /// <returns>The created channel.</returns>
        protected TApi CreateChannelToSelf()
        {
            if (!this.BehaviorManager.GetRequiredServiceBehavior<IServiceHostBehavior>().UseMultithreading)
            {
                // NOTE: We can't create a real channel if the service does not support multithreading.
                return this as TApi;
            }

            var channelFactory = this.Container.Resolve<IAstoriaChannelFactory>();
            return channelFactory.CreateChannel<TApi>(this.Configuration.Service.Address);
        }

        /// <summary>
        /// Gets the object which implements the service's installation.
        /// </summary>
        /// <returns>The service's deployment provider.</returns>
        [ExcludeFromCodeCoverage]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual IDeploymentProvider GetDeploymentProvider()
        {
            return this.Container.Resolve<AstoriaDeploymentProvider>();
        }

        /// <summary>
        /// Gets the object which runs the service discovery server.
        /// </summary>
        /// <returns>The service discovery server.</returns>
        [ExcludeFromCodeCoverage]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual IServiceDiscoveryServer GetDiscoveryServer()
        {
            return this.Container.Resolve<IServiceDiscoveryServer>();
        }

        /// <summary>
        /// Attaches the debugger to the Astoria service.
        /// </summary>
        [ExcludeFromCodeCoverage]
        protected virtual void AttachDebugger()
        {
            Debugger.Launch();
        }

        /// <summary>
        /// Called when the service's WCF host is started.
        /// </summary>
        [ExcludeFromCodeCoverage]
        protected virtual void OnWcfHostStartup()
        {
        }

        /// <summary>
        /// Called when the service is starting up.
        /// </summary>
        [ExcludeFromCodeCoverage]        
        protected virtual void OnStartup()
        {
        }

        /// <summary>
        /// Called when the service is shutting down.
        /// </summary>
        [ExcludeFromCodeCoverage]        
        protected virtual void OnShutdown()
        {
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if ((this.serviceHost != null) && !this.serviceHost.IsDisposed)
                {
                    this.serviceHost.Dispose();
                    this.serviceHost = null;
                }

                if ((this.Logger != null) && !this.Logger.IsDisposed)
                {
                    this.Logger.Dispose();
                    this.Logger = null;
                }
            }
        }

        /// <summary>
        /// Called when the service's WCF host is started.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        [ExcludeFromCodeCoverage]
        private void OnServiceHostWcfHostStarting(object sender, EventArgs e)
        {
            this.OnWcfHostStartup();
        }

        /// <summary>
        /// Applies Astoria's policy for process-based hosts.
        /// </summary>
        private void ApplyProcessHostPolicy()
        {
            // Use console log target
            LoggingBehavior loggingBehavior = this.BehaviorManager.GetServiceBehavior<LoggingBehavior>();
            if (loggingBehavior != null)
            {
                loggingBehavior.LogTarget = ServiceLogTarget.Console;
            }

            // Disabled performance counters
            var monitoringBehavior = this.BehaviorManager.GetRequiredServiceBehavior<IMonitoringBehavior>();
            monitoringBehavior.PerformanceCountersEnabled = false;
        }

        /// <summary>
        /// Initializes the service's performance counters.
        /// </summary>
        /// <param name="installMode">Specifies whether the service is running in install mode.</param>
        private void InitializePerformanceCounters(bool installMode)
        {
            this.PerformanceCounters = this.Container.ResolveOptional<ServicePerformanceCounters>();

            IEnumerable<IPerformanceCounter> performanceCounters = null;
            var performanceCounterDiscoverer = this.Container.ResolveOptional<IPerformanceCounterDiscoverer>();
            if (performanceCounterDiscoverer != null)
            {
                performanceCounters = performanceCounterDiscoverer.DiscoverPerformanceCounters(this);
            }

            if (installMode)
            {
                var performanceCounterInstaller = this.Container.ResolveOptional<IPerformanceCounterInstaller>();
                if ((performanceCounterInstaller != null) && (performanceCounters != null))
                {
                    performanceCounterInstaller.InstallPerformanceCounters(this, performanceCounters);
                }
            }
        }

        /// <summary>
        /// Starts the service's discovery server.
        /// </summary>
        /// <param name="options">The service's start options.</param>
        private void StartDiscoveryServer(ServiceStartOptions options)
        {
            var discoveryBehavior = this.BehaviorManager.GetRequiredServiceBehavior<IDiscoveryBehavior>();
            if (!discoveryBehavior.DiscoveryServerEnabled)
            {
                return;
            }
            
            bool discoverServerEnabled = true;
            DiscoverySection discoveryConfig = this.Configuration.Service.Discovery;
            if (discoveryConfig != null)
            {
                discoverServerEnabled = !discoveryConfig.DiscoveryServerDisabled;
            }
                
            if (options.EnableAutoDiscovery && discoverServerEnabled)
            {
                IServiceDiscoveryServer discoveryServer = this.GetDiscoveryServer();
                discoveryServer.Configuration = discoveryConfig;
                discoveryServer.StartFor(this);
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventLogManagerMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.ServiceModel.Deployment;

    public class EventLogManagerMock : IEventLogManager
    {
        public EventLogManagerMock()
        {
            this.Logs = new List<string>();
        }

        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> Logs { get; private set; }

        public Exception Failure { get; set; }

        public bool LogExists(string logName)
        {
            return this.Logs.Contains(logName);
        }

        public void CreateLog(EventSourceCreationData data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            if (this.Failure != null) 
            {
                throw this.Failure;
            }

            this.Logs.Add(data.LogName);
        }
    }
}
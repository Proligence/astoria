﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaDeploymentProviderMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System.Diagnostics.CodeAnalysis;
    using System.Reflection;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel.Deployment;
    using Proligence.Astoria.ServiceModel.Logging;

    public class AstoriaDeploymentProviderMock : AstoriaDeploymentProvider
    {
        public AstoriaDeploymentProviderMock()
            : base(new EventLogManagerMock(), new PerformanceCounterManagerMock(), new WindowsServiceManagerMock())
        {
            this.EntryAssemblyLocation = Assembly.GetExecutingAssembly().Location;
        }

        public new EventLogManagerMock EventLogManager
        {
            get
            {
                return (EventLogManagerMock)base.EventLogManager;
            }
        }

        public new PerformanceCounterManagerMock PerformanceCounterManager
        {
            get
            {
                return (PerformanceCounterManagerMock)base.PerformanceCounterManager;
            }
        }

        public new WindowsServiceManagerMock WindowsServiceManager
        {
            get
            {
                return (WindowsServiceManagerMock)base.WindowsServiceManager;
            }
        }

        public bool InstallPerformed { get; private set; }

        [SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        public string EntryAssemblyLocation { get; set; }

        public override void PerformInstall(
            string serviceName, AstoriaServiceConfiguration configuration, ServiceLogger logger)
        {
            base.PerformInstall(serviceName, configuration, logger);
            this.InstallPerformed = true;
        }

        protected override string GetEntryAssemblyLocation()
        {
            return this.EntryAssemblyLocation;
        }
    }
}
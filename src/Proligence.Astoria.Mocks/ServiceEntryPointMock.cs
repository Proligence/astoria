﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceEntryPointMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System;
    using System.Collections.ObjectModel;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using Autofac;
    using Moq;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel;

    [SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")]
    public class ServiceEntryPointMock<TService, TApi, TConfig> : ServiceEntryPoint<TService, TApi, TConfig>
        where TApi : class, IAstoriaService
        where TService : AstoriaService<TApi, TConfig>
        where TConfig : AstoriaServiceConfiguration
    {
        private Mock<IContainer> containerMock;

        public TService Service { get; set; }
        public Collection<Type> ResolvedTypes { get; private set; }
        public TService StartedService { get; private set; }
        public ServiceStartOptions StartOptions { get; private set; }
        public bool DebuggerLaunched { get; private set; }

        public void InvokeWriteExceptionDetails(Exception exception, TextWriter writer)
        {
            this.WriteExceptionDetails(exception, writer);
        }

        protected override void ResolveType(Type type)
        {
            if (this.ResolvedTypes == null) 
            {
                this.ResolvedTypes = new Collection<Type>();
            }

            this.ResolvedTypes.Add(type);
        }

        protected override IContainer BuildIocContainer()
        {
            this.containerMock = new Mock<IContainer>();
            return this.containerMock.Object;
        }

        protected override TService CreateService()
        {
            return this.Service;
        }

        protected override void RunService(TService service, ServiceStartOptions startOptions)
        {
            this.StartedService = service;
            this.StartOptions = startOptions;
        }

        protected override void LaunchDebugger()
        {
            this.DebuggerLaunched = true;
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WcfServiceHostMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using Moq;
    using Proligence.Astoria.ServiceModel.Wcf;

    public class WcfServiceHostMock : WcfServiceHost
    {
        public WcfServiceHostMock()
            : base(new Mock<ServiceHost>().Object)
        {
            this.ServiceHostMock = Mock.Get(this.ServiceHost);
            
            this.MockedDescription = new ServiceDescription();
            this.MockedBehaviors = new KeyedByTypeCollection<IServiceBehavior>();
            this.AddedEndpoints = new List<ServiceEndpoint>();
        }

        public WcfServiceHostMock(ServiceHost serviceHost)
            : base(serviceHost)
        {
            this.MockedDescription = new ServiceDescription();
            this.MockedBehaviors = new KeyedByTypeCollection<IServiceBehavior>();
            this.AddedEndpoints = new List<ServiceEndpoint>();
        }

        public bool IsOpen { get; private set; }
        public Mock<ServiceHost> ServiceHostMock { get; private set; }
        public ServiceDescription MockedDescription { get; set; }
        public KeyedByTypeCollection<IServiceBehavior> MockedBehaviors { get; private set; }
        public IList<ServiceEndpoint> AddedEndpoints { get; private set; }

        public override ServiceDescription Description
        {
            get
            {
                return this.MockedDescription;
            }
        }

        public override KeyedByTypeCollection<IServiceBehavior> Behaviors
        {
            get
            {
                return this.MockedBehaviors;
            }
        }

        public override ServiceEndpoint AddServiceEndpoint(Type implementedContract, Binding binding, string address)
        {
            if (implementedContract == null)
            {
                throw new ArgumentNullException("implementedContract");
            }

            var endpoint = new ServiceEndpoint(
                new ContractDescription(implementedContract.FullName),
                binding,
                new EndpointAddress("net.tcp://dummy/" + address));

            this.AddedEndpoints.Add(endpoint);

            return endpoint;
        }

        public override void Open()
        {
            this.IsOpen = true;
        }

        public override void Close()
        {
            this.IsOpen = false;
        }
    }
}
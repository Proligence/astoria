﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MockConfigurationProvider.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System.IO;
    using Proligence.Astoria.Configuration;

    public class MockConfigurationProvider<TConfig> : IConfigurationProvider<TConfig>
        where TConfig : AstoriaServiceConfiguration
    {
        private readonly DataContractSerializationConfigurationProviderMock<TConfig> configurationProvider;

        public MockConfigurationProvider(string configuration)
        {
            this.configurationProvider = new DataContractSerializationConfigurationProviderMock<TConfig>();
            this.configurationProvider.ConfigStream = CreateStreamForString(configuration);
        }

        public TConfig GetConfiguration(string fileName)
        {
            return this.configurationProvider.GetConfiguration(fileName);
        }

        private static Stream CreateStreamForString(string str)
        {
            var stream = new MemoryStream();

            var writer = new StreamWriter(stream);
            writer.Write(str);
            writer.Flush();

            stream.Seek(0, SeekOrigin.Begin);

            return stream;
        }
    }
}
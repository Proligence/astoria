﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceHostMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System;
    using System.ServiceModel;
    using Moq;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel;
    using Proligence.Astoria.ServiceModel.Hosting;
    using Proligence.Astoria.ServiceModel.Wcf;

    public class AstoriaServiceHostMock : AstoriaServiceHost<IDummy, AstoriaServiceConfiguration>
    {
        public AstoriaServiceHostMock(AstoriaService<IDummy, AstoriaServiceConfiguration> service)
            : base(service)
        {
            this.HostStarted = false;
        }

        public new AstoriaService<IDummy, AstoriaServiceConfiguration> Service 
        { 
            get 
            {
                return base.Service;
            }
        }

        public bool HostStarted { get; private set; }

        public override void StartHost()
        {
            this.StartWcfHost();
            this.HostStarted = true;
        }

        protected override ServiceHost CreateWcfServiceHost(Uri address)
        {
            return new Mock<ServiceHost>().Object;
        }

        protected override WcfServiceHost CreateWcfServiceHostWrapper(ServiceHost serviceHost)
        {
            return new WcfServiceHostMock(serviceHost);
        }
    }
}
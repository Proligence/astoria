﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PerformanceCounterManagerMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using Proligence.Astoria.ServiceModel.Deployment;

    public class PerformanceCounterManagerMock : IPerformanceCounterManager
    {
        public PerformanceCounterManagerMock()
        {
            this.Categories = new Dictionary<string, List<string>>();
        }

        public Dictionary<string, List<string>> Categories { get; private set; }

        public bool CategoryExists(string categoryName)
        {
            return this.Categories.ContainsKey(categoryName);
        }

        public bool CounterExists(string counterName, string categoryName)
        {
            List<string> counters;
            if (!this.Categories.TryGetValue(categoryName, out counters))
            {
                return false;
            }

            return counters.Contains(counterName);
        }

        public void CreateCategory(
            string categoryName, 
            string categoryDescription, 
            PerformanceCounterCategoryType categoryType, 
            CounterCreationDataCollection counters)
        {
            List<string> countersList = counters
                .Cast<CounterCreationData>()
                .Select(c => c.CounterName)
                .ToList();

            this.Categories.Add(categoryName, countersList);
        }

        public void DeleteCategory(string categoryName)
        {
            if (!this.Categories.ContainsKey(categoryName))
            {
                throw new InvalidOperationException("The specified category does not exist.");
            }

            this.Categories.Remove(categoryName);
        }
    }
}
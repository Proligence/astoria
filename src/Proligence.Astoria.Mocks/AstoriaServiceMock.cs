﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Reflection;
    using Autofac;
    using Moq;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel;
    using Proligence.Astoria.ServiceModel.Deployment;
    using Proligence.Astoria.ServiceModel.Discovery;
    using Proligence.Astoria.ServiceModel.Extensions;
    using Proligence.Astoria.ServiceModel.Hosting;
    using Proligence.Astoria.ServiceModel.Logging;
    using Proligence.Astoria.ServiceModel.Monitoring;
    using Proligence.Astoria.ServiceModel.Wcf;
    using Environment = Proligence.Astoria.ServiceModel.Environment;

    public class AstoriaServiceMock : AstoriaService<IDummy, AstoriaServiceConfiguration>, IDummy
    {
        private readonly ServiceLogger ctorLogger;

        public AstoriaServiceMock()
            : this("My Service", new Environment())
        {
        }

        public AstoriaServiceMock(string serviceName, IEnvironment environment, ServiceLogger ctorLogger = null)
            : base(serviceName, environment, CreateDummyServiceConfiguration())
        {
            this.ctorLogger = ctorLogger;
            this.ServiceHost = new AstoriaServiceHostMock(this);
            this.DeploymentProvider = new AstoriaDeploymentProviderMock();
            this.DiscoveryServer = new AstoriaDiscoveryServerMock();

            var builder = new ContainerBuilder();
            builder.RegisterType(typeof(StringBuilderLog)).As<ILog>().InstancePerDependency();
            builder.RegisterType<ServicePerformanceCounters>();
            builder.RegisterType<BehaviorManager>().As<IBehaviorManager>();
            builder.RegisterInstance(new Mock<IWatchDogManager>().Object).As<IWatchDogManager>();
            builder.RegisterInstance(new Mock<IServiceExtensionManager>().Object).As<IServiceExtensionManager>();
            builder.RegisterInstance(new Mock<WcfHostConfigurator>().Object).As<IWcfHostConfigurator>();
            this.SetDependencyContainer(builder.Build());
        }

        public IServiceHostBehavior OriginalServiceHostBehavior { get; private set; }
        public ILoggingBehavior OriginalLoggingBehavior { get; private set; }
        public bool DebuggerAttached { get; private set; }
        public bool DoInitializeServiceBehaviors { get; set; }
        public Action StartupAction { get; set; }
        public Action ShutdownAction { get; set; }
        public Func<IEnumerable<IAstoriaServiceBehavior>> ReflectionBehaviorsFunc { get; set; }
        public Func<Tuple<IEnumerable<IAstoriaServiceBehavior>, IEnumerable<Type>>> ConfigBehaviorsFunc { get; set; }

        public new IContainer Container 
        { 
            get
            {
                return base.Container;
            }
        }

        public new WcfServiceHost WcfServiceHost
        {
            get
            {
                return base.WcfServiceHost;
            }
        }

        [SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        public AstoriaDeploymentProviderMock DeploymentProvider { get; private set; }

        [SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        public IServiceDiscoveryServer DiscoveryServer { get; set; }

        public new AstoriaServiceConfiguration Configuration
        {
            get { return base.Configuration; }
            set { base.Configuration = value; }
        }

        public new ServicePerformanceCounters PerformanceCounters
        {
            get { return base.PerformanceCounters; }
            set { base.PerformanceCounters = value; }
        }

        public Collection<string> LoadedAssemblies { get; private set; }
        
        public new ServiceLogger Logger
        {
            get
            {
                return base.Logger;
            }
        }

        public AstoriaServiceHost<IDummy, AstoriaServiceConfiguration> ServiceHost
        {
            get
            {
                /* ReSharper disable PossibleNullReferenceException */
                FieldInfo fieldInfo = this.GetType().BaseType.GetField(
                    "serviceHost", BindingFlags.Instance | BindingFlags.NonPublic);
                return (AstoriaServiceHost<IDummy, AstoriaServiceConfiguration>)fieldInfo.GetValue(this);
                /* ReSharper restore PossibleNullReferenceException */
            }

            set
            {
                /* ReSharper disable PossibleNullReferenceException */
                FieldInfo fieldInfo = this.GetType().BaseType.GetField(
                    "serviceHost", BindingFlags.Instance | BindingFlags.NonPublic);
                fieldInfo.SetValue(this, value);
                /* ReSharper restore PossibleNullReferenceException */
            }
        }

        public override void Initialize(ServiceStartOptions options)
        {
            base.Initialize(options);

            this.OriginalServiceHostBehavior = this.BehaviorManager.GetServiceBehavior<IServiceHostBehavior>();
            this.BehaviorManager.RegisterServiceBehavior(new MockServiceHostBehavior());
        }

        public void InvokeInitializeAsync(object startOptions)
        {
            this.InitializeAsync(startOptions);
        }

        public IDeploymentProvider InvokeGetDeploymentProvider()
        {
            return this.GetDeploymentProvider();
        }

        protected override void OnStartup()
        {
            if (this.StartupAction != null)
            {
                this.StartupAction();
            }
        }

        protected override void OnShutdown()
        {
            if (this.ShutdownAction != null)
            {
                this.ShutdownAction();
            }
        }

        protected override void AttachDebugger()
        {
            this.DebuggerAttached = true;
        }

        protected override ServiceLogger CreateServiceLogger(ServiceStartOptions options)
        {
            if (this.ctorLogger != null)
            {
                return this.ctorLogger;
            }
            
            return base.CreateServiceLogger(options);
        }

        protected override void LoadAssemblies(IEnumerable<string> assemblyFileNames)
        {
            if (assemblyFileNames == null)
            {
                throw new ArgumentNullException("assemblyFileNames");
            }

            this.LoadedAssemblies = new Collection<string>();
            
            foreach (string fileName in assemblyFileNames)
            {
                this.LoadedAssemblies.Add(fileName);   
            }
        }

        protected override void InitializeServiceBehaviors()
        {
            if (this.DoInitializeServiceBehaviors)
            {
                base.InitializeServiceBehaviors();
            }
            
            /* Register required service behaviors */

            this.OriginalLoggingBehavior = this.BehaviorManager.GetServiceBehavior<ILoggingBehavior>();

            // Check if logging behavior is mocked.
            Mock<ILoggingBehavior> mock = null;
            try
            {
                mock = Mock.Get(this.OriginalLoggingBehavior);
            }
            catch (ArgumentException)
            {
            }

            if (mock == null)
            {
                this.BehaviorManager.RegisterServiceBehavior(
                    new LoggingBehavior { LogTarget = ServiceLogTarget.Console });
            }

            if (this.BehaviorManager.GetServiceBehavior<IMonitoringBehavior>() == null)
            {
                this.BehaviorManager.RegisterServiceBehavior(
                    new MonitoringBehavior { PerformanceCountersEnabled = false });
            }

            if (this.BehaviorManager.GetServiceBehavior<IWatchDogsBehavior>() == null)
            {
                this.BehaviorManager.RegisterServiceBehavior(
                    new WatchDogsBehavior { WatchDogsEnabled = true });
            }

            if (this.BehaviorManager.GetServiceBehavior<IDiscoveryBehavior>() == null)
            {
                this.BehaviorManager.RegisterServiceBehavior(
                    new DiscoveryBehavior { DiscoveryServerEnabled = false });
            }
        }

        protected override IEnumerable<IAstoriaServiceBehavior> DiscoverBehaviorsByReflection()
        {
            if (this.ReflectionBehaviorsFunc != null)
            {
                return this.ReflectionBehaviorsFunc();
            }

            return base.DiscoverBehaviorsByReflection();
        }

        protected override IEnumerable<IAstoriaServiceBehavior> DiscoverBehaviorsByConfiguration(
            out IEnumerable<Type> behaviorsToRemove)
        {
            if (this.ConfigBehaviorsFunc != null)
            {
                Tuple<IEnumerable<IAstoriaServiceBehavior>, IEnumerable<Type>> result = this.ConfigBehaviorsFunc();
                behaviorsToRemove = result.Item2;
                return result.Item1;
            }

            return base.DiscoverBehaviorsByConfiguration(out behaviorsToRemove);
        }

        protected override IDeploymentProvider GetDeploymentProvider()
        {
            return this.DeploymentProvider;
        }

        protected override IServiceDiscoveryServer GetDiscoveryServer()
        {
            return this.DiscoveryServer;
        }

        private static AstoriaServiceConfiguration CreateDummyServiceConfiguration()
        {
            string configuration =
@"<AstoriaServiceConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
  <Service>
    <Address>net.tcp://localhost/AstoriaServiceMock</Address>
  </Service>
</AstoriaServiceConfiguration>";

            var mockConfigurationProvider = new MockConfigurationProvider<AstoriaServiceConfiguration>(configuration);
            return mockConfigurationProvider.GetConfiguration("dummy");
        }
    }
}
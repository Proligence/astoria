﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MockServiceHostBehavior.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceModel;
    using Moq;
    using Proligence.Astoria.Client;
    using Proligence.Astoria.Configuration;
    using Proligence.Astoria.ServiceModel;
    using Proligence.Astoria.ServiceModel.Hosting;
    using Proligence.Astoria.ServiceModel.Wcf;

    [AttributeUsage(AttributeTargets.Class)]
    [SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes")]
    [SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix")]
    public class MockServiceHostBehavior : Attribute, IServiceHostBehavior
    {
        public bool UseMultithreading { get; set; }
        public object ServiceHostMock { get; private set; }

        public AstoriaServiceHost<TApi, TConfig> CreateServiceHost<TApi, TConfig>(
            AstoriaService<TApi, TConfig> service)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
            var wcfHostMock = new Mock<ServiceHost>();
            var hostMock = new Mock<AstoriaServiceHost<TApi, TConfig>>(service) { CallBase = true };
            hostMock.Object.WcfServiceHost = new WcfServiceHost(wcfHostMock.Object);
            
            this.ServiceHostMock = hostMock;

            return hostMock.Object;
        }

        public virtual void OnStartup<TApi, TConfig>(
            AstoriaService<TApi, TConfig> service,
            AstoriaServiceHost<TApi, TConfig> host)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
        }

        public virtual void OnShutdown<TApi, TConfig>(
            AstoriaService<TApi, TConfig> service,
            AstoriaServiceHost<TApi, TConfig> host)
            where TApi : class, IAstoriaService
            where TConfig : AstoriaServiceConfiguration
        {
        }
    }
}
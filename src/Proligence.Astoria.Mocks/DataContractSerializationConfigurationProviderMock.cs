﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataContractSerializationConfigurationProviderMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using Proligence.Astoria.Configuration;

    [ExcludeFromCodeCoverage]
    public class DataContractSerializationConfigurationProviderMock<TConfig> 
        : DataContractSerializationConfigurationProvider<TConfig>
        where TConfig : AstoriaServiceConfiguration
    {
        public Stream ConfigStream { get; set; }
        public bool PerformValidation { get; set; }

        protected override Stream GetFileStream(string fileName)
        {
            return this.ConfigStream;
        }

        protected override void ValidateDataAnnotations(object obj, string sectionName)
        {
            if (this.PerformValidation)
            {
                base.ValidateDataAnnotations(obj, sectionName);
            }
        }
    }
}
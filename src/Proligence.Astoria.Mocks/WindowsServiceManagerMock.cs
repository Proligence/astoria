﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowsServiceManagerMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using Proligence.Astoria.ServiceModel.Deployment;

    public class WindowsServiceManagerMock : IWindowsServiceManager
    {
        public WindowsServiceManagerMock()
        {
            this.ScmHandles = new Collection<IntPtr>();
            this.ServiceHandles = new Collection<IntPtr>();
            this.Services = new Dictionary<string, string>();
        }

        public Collection<IntPtr> ScmHandles { get; private set; }
        public Collection<IntPtr> ServiceHandles { get; private set; }
        public IDictionary<string, string> Services { get; private set; }

        public IntPtr OpenServiceControlManager()
        {
            IntPtr handle = new IntPtr(this.ScmHandles.Count + 1);
            this.ScmHandles.Add(handle);

            return handle;
        }

        public bool CloseServiceControlManager(IntPtr scmHandle)
        {
            if (!VerifyHandle(scmHandle, this.ScmHandles))
            {
                return false;
            }
            
            this.ScmHandles.Remove(scmHandle);
            return true;
        }

        public IntPtr? OpenService(IntPtr scmHandle, string serviceName)
        {
            if (!this.Services.ContainsKey(serviceName))
            {
                return null;
            }

            IntPtr handle = new IntPtr(this.ServiceHandles.Count + 1);
            this.ServiceHandles.Add(handle);

            return handle;
        }

        public bool CloseService(IntPtr handle)
        {
            if (!VerifyHandle(handle, this.ServiceHandles))
            {
                return false;
            }
            
            this.ServiceHandles.Remove(handle);
            return true;
        }

        public void CreateService(IntPtr scmHandle, string serviceName, string displayName, string path)
        {
            if (!VerifyHandle(scmHandle, this.ScmHandles))
            {
                throw new InvalidOperationException("Invalid Service Control Manager handle.");
            }

            if (this.Services.ContainsKey(serviceName))
            {
                throw new InvalidOperationException("A service with the specified name already exists.");
            }

            this.Services.Add(serviceName, displayName + "," + path);
        }

        private static bool VerifyHandle(IntPtr handle, Collection<IntPtr> handleList)
        {
            int index = handleList.IndexOf(handle);
            if (index == -1) 
            {
                Trace.WriteLine("Invalid handle detected!");
                return false;
            }

            return true;
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceLoggerMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Mocks
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using Proligence.Astoria.ServiceModel.Logging;

    public class ServiceLoggerMock : ServiceLogger
    {
        public ServiceLoggerMock()
        {
            this.DebugMessages = new List<string>();
            this.Messages = new List<string>();
            this.Warnings = new List<string>();
            this.Errors = new List<KeyValuePair<string, Exception>>();
        }

        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> DebugMessages { get; private set; }

        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> Messages { get; private set; }

        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> Warnings { get; private set; }

        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<KeyValuePair<string, Exception>> Errors { get; private set; }

        public new string FormatErrorMessage(string message, Exception exception)
        {
            return base.FormatErrorMessage(message, exception);
        }

        protected override void LogDebugMessageInternal(string message)
        {
            this.DebugMessages.Add(message);
        }

        protected override void LogMessageInternal(string message)
        {
            this.Messages.Add(message);
        }

        protected override void LogWarningInternal(string message)
        {
            this.Warnings.Add(message);
        }

        protected override void LogErrorInternal(string message, Exception exception)
        {
            this.Errors.Add(new KeyValuePair<string, Exception>(message, exception));
        }
    }
}
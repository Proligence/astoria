﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceConfigurationExceptionUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration.UnitTests
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using NUnit.Framework;

    [TestFixture]
    public class ServiceConfigurationExceptionUnitTests
    {
        [Test]
        public void CreateServiceConfigurationExceptionFromValidationException()
        {
            var validationResult = new ValidationResult("My validation message.", new[] { "MySetting" });
            var validationException = new ValidationException(validationResult, null, 7);
            var exception = new ServiceConfigurationException(validationException, "MySection");

            Assert.That(
                exception.Message,
                Is.EqualTo("The service's configuration is invalid. My validation message."));

            Assert.That(exception.SectionName, Is.EqualTo("MySection"));
            Assert.That(exception.SettingName, Is.EqualTo("MySetting"));
            Assert.That(exception.SettingValue, Is.EqualTo(7));
        }

        [Test]
        public void CreateServiceConfigurationExceptionFromValidationExceptionWhenValidationExceptionIsNull()
        {
            var exception = new ServiceConfigurationException(null, "MySection");

            Assert.That(exception.Message, Is.EqualTo("The service's configuration is invalid."));
        }

        [Test]
        public void CreateServiceConfigurationExceptionWithMessage()
        {
            var exception = new ServiceConfigurationException("My message.");

            Assert.That(exception.Message, Is.EqualTo("My message."));
        }

        [Test]
        public void CreateServiceConfigurationExceptionWithMessageAndInnerException()
        {
            var innerException = new InvalidOperationException();
            var exception = new ServiceConfigurationException("My message.", innerException);

            Assert.That(exception.Message, Is.EqualTo("My message."));
            Assert.That(exception.InnerException, Is.SameAs(innerException));
        }

        [Test]
        public void CreateServiceConfigurationExceptionWithFullData()
        {
            var innerException = new InvalidOperationException();
            var exception = new ServiceConfigurationException(
                "My message.", innerException, "section", "setting", "value");

            Assert.That(exception.Message, Is.EqualTo("My message."));
            Assert.That(exception.InnerException, Is.SameAs(innerException));
            Assert.That(exception.SectionName, Is.EqualTo("section"));
            Assert.That(exception.SettingName, Is.EqualTo("setting"));
            Assert.That(exception.SettingValue, Is.EqualTo("value"));
        }

        [Test]
        public void GetValidationExceptionWhenInnerExceptionIsValidationException()
        {
            var innerException = new ValidationException();
            var exception = new ServiceConfigurationException("My message.", innerException);

            Assert.That(exception.ValidationException, Is.SameAs(innerException));
        }

        [Test]
        public void GetValidationExceptionWhenInnerExceptionIsNotValidationException()
        {
            var innerException = new InvalidOperationException();
            var exception = new ServiceConfigurationException("My message.", innerException);

            Assert.That(exception.ValidationException, Is.Null);
        }

        [Test]
        public void TestSerialization()
        {
            var innerException = new InvalidOperationException();
            var exception = new ServiceConfigurationException(
                "My message.", innerException, "section", "setting", "value");

            ServiceConfigurationException deserializedException = null;
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, exception);
                stream.Position = 0;

                deserializedException = (ServiceConfigurationException)formatter.Deserialize(stream);
            }

            Assert.That(deserializedException.Message, Is.EqualTo("My message."));
            Assert.That(deserializedException.InnerException.Message, Is.EqualTo(innerException.Message));
            Assert.That(deserializedException.SectionName, Is.EqualTo("section"));
            Assert.That(deserializedException.SettingName, Is.EqualTo("setting"));
            Assert.That(deserializedException.SettingValue, Is.EqualTo("value"));
        }
    }
}
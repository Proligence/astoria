﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataContractSerializationConfigurationProviderUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration.UnitTests
{
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Xml;
    using NUnit.Framework;
    using Proligence.Astoria.Mocks;

    [TestFixture]
    public class DataContractSerializationConfigurationProviderUnitTests
    {
        private DataContractSerializationConfigurationProviderMock<TestConfiguration> configurationProvider;

        [SetUp]
        public void Setup()
        {
            this.configurationProvider = new DataContractSerializationConfigurationProviderMock<TestConfiguration>
            {
                PerformValidation = true
            };
        }

        [TearDown]
        public void Teardown()
        {
            if (this.configurationProvider.ConfigStream != null)
            {
                this.configurationProvider.ConfigStream.Dispose();
                this.configurationProvider.ConfigStream = null;
            }
        }

        [Test]
        public void GetTestSettingFromConfig()
        {
            using (this.configurationProvider.ConfigStream = CreateStreamForString(
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost:81/Test</Address>
  </Service>
  <TestSection>
    <TestSetting>Test setting value</TestSetting>
  </TestSection>
</TestConfiguration>"))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");
                Assert.That(configuration.TestSection.TestSetting, Is.EqualTo("Test setting value"));   
            }
        }

        [Test]
        public void GetServiceAddressFromConfig()
        {
            using (this.configurationProvider.ConfigStream = CreateStreamForString(
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost:81/Test</Address>
  </Service>
</TestConfiguration>"))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");
                Assert.That(configuration.Service.Address, Is.EqualTo("net.tcp://localhost:81/Test"));   
            }
        }

        [Test]
        public void GetAssemblyList()
        {
            using (this.configurationProvider.ConfigStream = CreateStreamForString(
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost:81/Test</Address>
    <Assemblies>
        <Assembly>assembly1.dll</Assembly>
        <Assembly>assembly2.dll</Assembly>
        <Assembly>assembly3.dll</Assembly>
    </Assemblies>
  </Service>
</TestConfiguration>"))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");
                Assert.That(configuration.Service.Assemblies, Is.Not.Null);
                Assert.That(configuration.Service.Assemblies[0], Is.EqualTo("assembly1.dll"));
                Assert.That(configuration.Service.Assemblies[1], Is.EqualTo("assembly2.dll"));
                Assert.That(configuration.Service.Assemblies[2], Is.EqualTo("assembly3.dll"));   
            }
        }

        [Test]
        public void GetEmptyAssemblyList()
        {
            using (this.configurationProvider.ConfigStream = CreateStreamForString(
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost:81/Test</Address>
    <Assemblies/>
  </Service>
</TestConfiguration>"))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");
                Assert.That(configuration.Service.Assemblies, Is.Empty);   
            }
        }

        [Test]
        public void GetEventLogName()
        {
            using (this.configurationProvider.ConfigStream = CreateStreamForString(
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost:81/Test</Address>
    <Logging>
       <EventLogName>MyEventLog</EventLogName>
    </Logging>
  </Service>
</TestConfiguration>"))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");
                Assert.That(configuration.Service.Logging.EventLogName, Is.EqualTo("MyEventLog"));   
            }
        }

        [Test]
        public void ParseSettingsWhenSettingsInAlphabeticalOrder()
        {
            using (this.configurationProvider.ConfigStream = CreateStreamForString(
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
  </Service>
  <TestSection>
    <SettingA>a value</SettingA>
    <SettingB>b value</SettingB>
    <SettingC>c value</SettingC>
  </TestSection>
</TestConfiguration>"))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");
                Assert.That(configuration.TestSection.SettingA, Is.EqualTo("a value"));
                Assert.That(configuration.TestSection.SettingB, Is.EqualTo("b value"));
                Assert.That(configuration.TestSection.SettingC, Is.EqualTo("c value"));   
            }
        }

        [Test]
        public void ParseSettingsWhenSettingsNotInAlphabeticalOrder()
        {
            using (this.configurationProvider.ConfigStream = CreateStreamForString(
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
  </Service>
  <TestSection>
    <SettingA>a value</SettingA>
    <SettingC>c value</SettingC>
    <SettingB>b value</SettingB>
  </TestSection>
</TestConfiguration>"))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");
                Assert.That(configuration.TestSection.SettingA, Is.EqualTo("a value"));
                Assert.That(configuration.TestSection.SettingB, Is.EqualTo("b value"));
                Assert.That(configuration.TestSection.SettingC, Is.EqualTo("c value"));   
            }
        }

        [Test]
        public void GetXmlFromParsedConfiguration()
        {
            string xml = 
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost:81/Test</Address>
  </Service>
</TestConfiguration>";

            using (this.configurationProvider.ConfigStream = CreateStreamForString(xml))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");

                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);

                Assert.That(configuration.XmlDocument.OuterXml, Is.EqualTo(xmlDoc.OuterXml));
            }
        }

        [Test]
        public void EnsureValidationAttributesCheckedWhenParsingConfiguration()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
  </Service>
  <TestSection>
    <SettingA>value</SettingA>
  </TestSection>
</TestConfiguration>";

            using (this.configurationProvider.ConfigStream = CreateStreamForString(xml))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");

                Assert.That(TestValidationAttribute.ValidatedObject, Is.SameAs(configuration.TestSection));
            }
        }

        [Test]
        public void EnsureValidationAttributesCheckedInSubsectionsWhenParsingConfiguration()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
  </Service>
  <DataAnnotationsTestSection>
    <Number>1</Number>
  </DataAnnotationsTestSection>
</TestConfiguration>";

            using (this.configurationProvider.ConfigStream = CreateStreamForString(xml))
            {
                ServiceConfigurationException exception = Assert.Throws<ServiceConfigurationException>(
                    () => this.configurationProvider.GetConfiguration("dummy.config"));

                Assert.That(exception.Message, Is.StringContaining("The field Number must be between 2 and 4."));
                Assert.That(exception.SectionName, Is.EqualTo("DataAnnotationsTestSection"));
                Assert.That(exception.SettingName, Is.EqualTo("Number"));
                Assert.That(exception.SettingValue, Is.EqualTo(1));
            }
        }

        [Test]
        public void ShouldDeserializeSettingsSection()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
    <Settings>
        <Setting Name='foo' Value='bar' />
        <Setting Name='baz' Value='quux' />
    </Settings>
  </Service>
</TestConfiguration>";

            using (this.configurationProvider.ConfigStream = CreateStreamForString(xml))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");

                Assert.That(configuration.Service.Settings["foo"], Is.EqualTo("bar"));
                Assert.That(configuration.Service.Settings["baz"], Is.EqualTo("quux"));
            }
        }

        private static Stream CreateStreamForString(string str)
        {
            var stream = new MemoryStream();
            
            var writer = new StreamWriter(stream);
            writer.Write(str);
            writer.Flush();

            stream.Seek(0, SeekOrigin.Begin);

            return stream;
        }
    }
}
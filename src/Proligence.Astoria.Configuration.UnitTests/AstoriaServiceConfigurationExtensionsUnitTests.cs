﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaServiceConfigurationExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration.UnitTests
{
    using System.IO;
    using NUnit.Framework;
    using Proligence.Astoria.Mocks;

    [TestFixture]
    public class AstoriaServiceConfigurationExtensionsUnitTests
    {
        private DataContractSerializationConfigurationProviderMock<TestConfiguration> configurationProvider;

        [SetUp]
        public void Setup()
        {
            this.configurationProvider = new DataContractSerializationConfigurationProviderMock<TestConfiguration>();
        }

        [Test]
        public void GetSettingShouldReturnSettingValueWhenSettingExists()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
    <Settings>
        <Setting Name='foo' Value='bar' />
    </Settings>
  </Service>
</TestConfiguration>";

            using (this.configurationProvider.ConfigStream = CreateStreamForString(xml))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");

                string result = configuration.GetSetting("foo");

                Assert.That(result, Is.EqualTo("bar"));
            }
        }

        [Test]
        public void GetSettingShouldReturnNullWhenSettingDoesNotExist()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
    <Settings>
        <Setting Name='foo' Value='bar' />
    </Settings>
  </Service>
</TestConfiguration>";

            using (this.configurationProvider.ConfigStream = CreateStreamForString(xml))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");

                string result = configuration.GetSetting("baz");

                Assert.That(result, Is.Null);
            }
        }

        [Test]
        public void GetSettingShouldReturnNullWhenNoSettingsInConfiguration()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
  </Service>
</TestConfiguration>";

            using (this.configurationProvider.ConfigStream = CreateStreamForString(xml))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");

                string result = configuration.GetSetting("foo");

                Assert.That(result, Is.Null);
            }
        }

        [Test]
        public void GetRequiredSettingShouldReturnSettingValueWhenSettingExists()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
    <Settings>
        <Setting Name='foo' Value='bar' />
    </Settings>
  </Service>
</TestConfiguration>";

            using (this.configurationProvider.ConfigStream = CreateStreamForString(xml))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");

                string result = configuration.GetRequiredSetting("foo");

                Assert.That(result, Is.EqualTo("bar"));
            }
        }

        [Test]
        public void GetRequiredSettingShouldThrowExceptionWhenSettingDoesNotExist()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
    <Settings>
        <Setting Name='foo' Value='bar' />
    </Settings>
  </Service>
</TestConfiguration>";

            using (this.configurationProvider.ConfigStream = CreateStreamForString(xml))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");

                var exception = Assert.Throws<ServiceConfigurationException>(
                    () => configuration.GetRequiredSetting("baz"));

                Assert.That(
                    exception.Message,
                    Is.EqualTo("Failed to find setting 'baz' in the service's configuration."));
            }
        }

        [Test]
        public void GetRequiredSettingShouldThrowExceptionWhenSettingValueIsNullOrEmpty()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
    <Settings>
        <Setting Name='foo' Value='' />
    </Settings>
  </Service>
</TestConfiguration>";

            using (this.configurationProvider.ConfigStream = CreateStreamForString(xml))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");

                var exception = Assert.Throws<ServiceConfigurationException>(
                    () => configuration.GetRequiredSetting("foo"));

                Assert.That(
                    exception.Message,
                    Is.EqualTo("Failed to find setting 'foo' in the service's configuration."));
            }
        }

        [Test]
        public void GetRequiredSettingShouldReturnNullWhenNoSettingsInConfiguration()
        {
            string xml =
@"<TestConfiguration xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration.UnitTests"">
  <Service xmlns=""http://schemas.datacontract.org/2004/07/Proligence.Astoria.Configuration"">
    <Address>net.tcp://localhost/dummy</Address>
  </Service>
</TestConfiguration>";

            using (this.configurationProvider.ConfigStream = CreateStreamForString(xml))
            {
                TestConfiguration configuration = this.configurationProvider.GetConfiguration("dummy.config");

                var exception = Assert.Throws<ServiceConfigurationException>(
                    () => configuration.GetRequiredSetting("foo"));

                Assert.That(
                    exception.Message,
                    Is.EqualTo("Failed to find setting 'foo' in the service's configuration."));
            }
        }

        private static Stream CreateStreamForString(string str)
        {
            var stream = new MemoryStream();

            var writer = new StreamWriter(stream);
            writer.Write(str);
            writer.Flush();

            stream.Seek(0, SeekOrigin.Begin);

            return stream;
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestConfigurationSection.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Configuration.UnitTests
{
    using System.Runtime.Serialization;

    [DataContract]
    public class TestConfigurationSection
    {
        [DataMember(Name = "TestSetting")]
        private string testSetting;

        [DataMember(Name = "SettingA")]
        private string settingA;

        [DataMember(Name = "SettingB")]
        private string settingB;

        [DataMember(Name = "SettingC")]
        private string settingC;

        public string TestSetting
        {
            get { return this.testSetting; }
        }

        public string SettingA
        {
            get { return this.settingA; }
        }

        public string SettingB
        {
            get { return this.settingB; }
        }

        public string SettingC
        {
            get { return this.settingC; }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InvokeWatchDogCommand.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Management
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Management.Automation;
    using Autofac;
    using Proligence.Astoria.Client;

    [Cmdlet("Invoke", "WatchDog")]
    public class InvokeWatchDogCommand : AstoriaCmdlet
    {
        private IAstoriaService service;

        [Parameter(Mandatory = true, Position = 1)]
        [ValidateNotNullOrEmpty]
        public string ServiceAddress { get; set; }

        [Parameter(Mandatory = false, Position = 2)]
        public string Name { get; set; }

        [Parameter(Mandatory = false, ValueFromPipeline = true)]
        public WatchDogInfo WatchDogInfo { get; set; }

        protected override void BeginProcessing()
        {
            IAstoriaChannelFactory factory = Container.Resolve<IAstoriaChannelFactory>();
            this.service = factory.CreateChannel<IAstoriaService>(this.ServiceAddress);
        }

        protected override void ProcessRecord()
        {
            if (this.WatchDogInfo != null)
            {
                this.WriteObject(this.service.InvokeWatchDog(this.WatchDogInfo));
                return;
            }

            IEnumerable<WatchDogInfo> watchDogs = this.service.GetWatchDogs();

            if (!string.IsNullOrEmpty(this.Name))
            {
                watchDogs = watchDogs.Where(wd => wd.Name.Contains(this.Name));
            }

            var result = new Dictionary<string, WatchDogResult>();
            foreach (WatchDogInfo watchDogInfo in watchDogs)
            {
                WatchDogResult watchDogResult = this.service.InvokeWatchDog(watchDogInfo);
                result.Add(watchDogInfo.Name, watchDogResult);
            }

            if (result.Count == 1)
            {
                this.WriteObject(result.Single().Value);
            }
            else
            {
                this.WriteObject(result);
            }
        }
    }
}
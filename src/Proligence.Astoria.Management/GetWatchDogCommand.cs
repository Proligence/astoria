﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetWatchDogCommand.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Management
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Management.Automation;
    using Autofac;
    using Proligence.Astoria.Client;

    [Cmdlet("Get", "WatchDog")]
    public class GetWatchDogCommand : AstoriaCmdlet
    {
        [Parameter(Mandatory = true, Position = 1)]
        [ValidateNotNullOrEmpty]
        public string ServiceAddress { get; set; }

        [Parameter(Mandatory = false, Position = 2)]
        public string Name { get; set; }

        protected override void ProcessRecord()
        {
            IAstoriaChannelFactory factory = Container.Resolve<IAstoriaChannelFactory>();
            IAstoriaService service = factory.CreateChannel<IAstoriaService>(this.ServiceAddress);
            
            IEnumerable<WatchDogInfo> watchDogs = service.GetWatchDogs();

            if (!string.IsNullOrEmpty(this.Name))
            {
                watchDogs = watchDogs.Where(wd => wd.Name.Contains(this.Name));
            }

            this.WriteObject(watchDogs, true);
        }
    }
}
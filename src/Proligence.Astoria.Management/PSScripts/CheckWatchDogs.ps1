# --------------------------------------------------------------------------------------------------------------------
# <copyright file="CheckWatchDogs.ps1" company="Proligence">
#   Copyright (C) Proligence
# </copyright>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
#
# For commercial license contact info@proligence.pl.
# --------------------------------------------------------------------------------------------------------------------

param ($ServiceAddress, $Email)

Add-PSSnapin Proligence.Astoria.Management

function SendNotification($subject, $body) 
{
    $msg = New-Object Net.Mail.MailMessage
    $msg.From = "noreply@proligence.pl"
    $msg.To.Add($Email)
    $msg.subject = $subject
    $msg.body = $body
    $msg.IsBodyHtml = $true
    $msg.Priority = [System.Net.Mail.MailPriority]::High
    
    $smtp = New-Object Net.Mail.SmtpClient('smtp.sendgrid.net')
    $smtp.Port = 587;
    $smtp.Credentials = New-Object System.Net.NetworkCredential("username", "password");

    $smtp.Send($msg)
}

try
{
    $results = Invoke-WatchDog $ServiceAddress

    $StatusOk = $true
    $message = '<html><body style="font-family:Calibri">'

    foreach($name in $results.Keys) 
    {
        if (-not ($results[$name].Status -eq 'Ok')) 
        {
            $StatusOk = $false
            $message += '<p><b>' + $name + '</b></p>'
        
            $message += '<div style="color:Red">'
            foreach ($error in $results[$name].Errors) {
                $message += '<p>' + $error + '</p>'
            }
            $message += '</div>'
        
            $message += '<div style="color:GoldenRod">'
            foreach ($warning in $results[$name].Warnings) {
                $message += '<p>' + $warning + '</p>'
            }
            $message += '</div>'
        }
    }

    $message += '</body></html>'

    if (-not $StatusOk)
    {
        SendNotification ('Watch Dog Notifications - ' + $ServiceAddress) $message
    }
}
catch
{
    $message = '<html><body style="font-family:Calibri">'
    $message += '<p style="color:Red">' + $error[0] + '</p>'
    $message += '</body></html>'

    SendNotification ('Watch Dog Failure - ' + $ServiceAddress) $message
}
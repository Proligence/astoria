﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AstoriaCmdlet.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Management
{
    using System;
    using System.Diagnostics;
    using System.Management.Automation;
    using Autofac;
    using Proligence.Helpers.Autofac;

    /// <summary>
    /// The base class for classes which implement Astoria cmdlets.
    /// </summary>
    public class AstoriaCmdlet : Cmdlet
    {
        private static IContainer container;

        /// <summary>
        /// Gets the dependency injection container.
        /// </summary>
        protected static IContainer Container
        {
            get
            {
                if (container == null) 
                {
                    // Force loading Proligence.Astoria.Client into the AppDomain
                    Trace.WriteLine(typeof(Client.IAstoriaService).ToString());
                    container = AppDomain.CurrentDomain.BuildIocContainer();
                }

                return container;
            }
        }

        protected void WriteArgumentExceptionError(
            string message,
            string errorId,
            ErrorCategory errorCategory = ErrorCategory.NotSpecified,
            object target = null,
            bool terminating = false)
        {
            var errorRecord = new ErrorRecord(new ArgumentException(message), errorId, errorCategory, target);
            
            if (terminating)
            {
                this.ThrowTerminatingError(errorRecord);
            }
            else
            {
                this.WriteError(errorRecord);
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetServiceInfoCommand.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Astoria.Management
{
    using System.Management.Automation;
    using Autofac;
    using Proligence.Astoria.Client;

    [Cmdlet("Get", "ServiceInfo")]
    public class GetServiceInfoCommand : AstoriaCmdlet
    {
        [Parameter(Mandatory = true, Position = 1)]
        [ValidateNotNullOrEmpty]
        public string ServiceAddress { get; set; }

        protected override void ProcessRecord()
        {
            IAstoriaChannelFactory factory = Container.Resolve<IAstoriaChannelFactory>();
            IAstoriaService service = factory.CreateChannel<IAstoriaService>(this.ServiceAddress);
            ServiceInfo serviceInfo = service.GetServiceInfo();
            this.WriteObject(serviceInfo);
        }
    }
}